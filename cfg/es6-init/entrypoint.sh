#!/bin/bash

for i in {45..0}; do
    if curl search:9200; then
    
        sleep 20
        
        curl -X PUT  http://search:9200/meta.dz_phys/ -H 'Content-Type: application/json' --data-binary @mapping_meta.dz_phys.json
        curl -X PUT  http://search:9200/meta.dz_log/ -H 'Content-Type: application/json' --data-binary @mapping_meta.dz_log.json

        break;
    fi
    sleep 3
done
