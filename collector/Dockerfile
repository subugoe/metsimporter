FROM golang:1.21-alpine3.17 AS builder

LABEL maintainer="panzer@sub.uni-goettingen.de"

ARG DEBUG=false
ARG GC_FLAGS="all=-N -l"

# install build essentials
RUN apk --update upgrade && \
    apk add --no-cache busybox gcc libc-dev

RUN mkdir -p /go/src/api

WORKDIR /go/src/api


COPY . .

# download go dependencies
RUN go mod download


RUN if test "$DEBUG" = 'true' ; then \
        go get github.com/go-delve/delve/cmd/dlv && \
        go build -gcflags="${GC_FLAGS}" -o goapp ; \
    else go build -o goapp ; fi

WORKDIR /usr/local/bin/

# Copy binary from build to main folder
RUN cp /go/src/api/goapp ./collector

# Build a optimized image
FROM alpine:3.17

COPY --from=builder /usr/local/bin/collector /usr/local/bin/collector
