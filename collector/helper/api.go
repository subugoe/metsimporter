package helper

import (
	"encoding/json"
	"io"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"

	sub "gitlab.gwdg.de/subugoe/shared-product-configs/sub"

	"gitlab.gwdg.de/subugoe/metsimporter/indexer/types"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/util"
)

var (
	config  util.Config
	log     logrus.Logger
	logFile string = ""

	inClient    *s3.S3
	redisClient *redis.Client

	idToDoctype map[string]string
)

// OldSolrDatesMsg ...
type OldSolrDatesMsg struct {
	ID           string `json:"id"`
	DateModified string `json:"datemodified,omitempty"`
	DateIndexed  string `json:"dateindexed,omitempty"`
}

// IndexerJob ...
type IndexerJob struct {
	BaseDir     string `json:"basedir"`
	FulltextDir string `json:"fulltextdir"`
	Document    string `json:"document"` // document == ID
	Context     string `json:"context"`
	Key         string `json:"inkey"`
	Product     string `json:"product"` // product == bucket
	Ftype       string `json:"ftype"`   // fulltext type HTML, TXT_1, TXT_2, TEI_2
	Reindex     bool   `json:"reindex"`
}

// SolrResponse ...
type SolrResponse struct {
	ResponseHeader SolrResponseHeader `json:"responseHeader"`
	Response       SolrResponseBody   `json:"response"`
}

// SolrResponseHeader ..
type SolrResponseHeader struct {
	Status int64   `json:"status"`
	QTime  float64 `json:"QTime"`
	Params Params  `json:"params"`
}

// SolrResponseBody ...
type SolrResponseBody struct {
	NumFound int64     `json:"numFound"`
	Start    int64     `json:"start"`
	Docs     []SolrDoc `json:"docs"`
}

// Params ..
type Params struct {
	Q    string `json:"q"`
	Fl   string `json:"fl"`
	Rows string `json:"rows"`
	WT   string `json:"wt"`
}

// SolrDoc ..
type SolrDoc struct {
	ID               string `json:"id,omitempty" bson:"id,omitempty"`
	RecordIdentifier string `json:"record_identifier,omitempty" bson:"record_identifier,omitempty"`
	DateIndexed      string `json:"date_indexed,omitempty" bson:"date_indexed,omitempty"`
	DateModified     string `json:"date_modified,omitempty" bson:"date_modified,omitempty"`
}

func init() {
	config, log = util.LoadConfig()

	if config.CContext == "digizeit" {

		for {
			resp, err := http.Get(config.ElasticsearchHost)
			if err != nil {
				log.Printf("Info Elasticsearch not running yet, due to %s", err)
				time.Sleep(180 * time.Second)
				continue
			}
			defer resp.Body.Close()

			if resp.StatusCode > 300 {
				log.Printf("INFO Elasticsearch response with status code %v", resp.StatusCode)
				time.Sleep(180 * time.Second)
				continue
			} else {
				break
			}

		}

		/*
			if config.ElasticsearchHost != "" && config.LogESIndex != "" {
				log.Infof("Setting ES Logging index to %s on %s", config.LogESIndex, config.ElasticsearchHost)
				client, err := elastic.NewClient(elastic.SetURL(config.ElasticsearchHost))
				if err != nil {
					log.Panic(err)
				}
				serviceName := strings.Split(filepath.Base(config.LogFile), ".")[0]
				var logLevel logrus.Level
				logLevel, _ = logrus.ParseLevel(config.LogLevel)
				hook, err := elogrus.NewAsyncElasticHook(client, serviceName, logLevel, config.LogESIndex)
				if err != nil {
					log.Panic(err)
				}
				log.Hooks.Add(hook)
			}
		*/
	}

	if config.CContext != "ocrd" {
		inClient = getS3Client()
	}

	redisClient = redis.NewClient(&redis.Options{
		Addr:       config.RedisAdr,
		DB:         config.RedisDB,
		MaxRetries: 3,
	})

	var err error
	idToDoctype, err = sub.GetAnchorIDs(config.Product)
	if err != nil {
		log.Errorf("could not read anchor ids for %s, due to %s", config.Product, err)
	}
}

func CreateAllManifests() int64 {
	var j int64

	log.Info("Determine METS for iiif manifest re-creation...")

	if config.CContext == "digizeit" && !config.StorageIsLocal {
		log.Info("Using V2 API to list bucket contents")
		j = listMetsInBucketV2(config.Product, "manifestbuilder")
	} else {
		log.Info("Using V1 API to list bucket contents")
		j = listMetsInBucket(config.Product, "manifestbuilder")
	}

	time.Sleep(300 * time.Second)
	return j
}

func CreateAllCitations() int64 {
	var j int64

	log.Info("Determine METS for citation re-indexing...")

	if config.CContext == "digizeit" && !config.StorageIsLocal {
		log.Info("Using V2 API to list bucket contents")
		j = listMetsInBucketV2(config.Product, "citationbuilder")
	} else {
		log.Info("Using V1 API to list bucket contents")
		j = listMetsInBucket(config.Product, "citationbuilder")
	}

	time.Sleep(300 * time.Second)
	return j
}

func IndexAll() int64 {

	var j int64

	if logFile != "" {
		file, err := os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0755)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		mw := io.MultiWriter(os.Stdout, file)
		log.SetOutput(mw)
	}

	log.Info("Determine METS for re-indexing...")

	if config.ValidationRun {

		for {

			val, err := redisClient.HGet(config.RedisStatusHsetQueue, config.RedisFinishImageKeyResolving).Result()
			if err != nil {
				log.Errorf("preprocessing not finished, queue %s field %s is stil empty", config.RedisStatusHsetQueue, config.RedisFinishImageKeyResolving)
				time.Sleep(60 * time.Second)
			} else {
				if val == "1" {
					break
				}
				time.Sleep(60 * time.Second)
			}
		}

		j = listMetsFromPath(config.MetsPath, "indexer")

	} else {
		if config.CContext == "digizeit" && !config.StorageIsLocal {
			log.Info("Using V2 API to list bucket contents")
			j = listMetsInBucketV2(config.Product, "indexer")
		} else {
			log.Info("Using V1 API to list bucket contents")
			j = listMetsInBucket(config.Product, "indexer")
		}
	}

	_, err := redisClient.HSet(config.RedisStatusHsetQueue, config.RedisFinishMetsKeyResolving, true).Result()
	if err != nil {
		log.Errorf("could not write to redis HSET, due to %s", err)
	}

	log.Printf("Collector Service finished processing, enqueued %v jobs", j)

	return j
}

func getS3Client() *s3.S3 {
	// Readded from pull request
	log.Printf("INFO Waiting for S3 Storage %s...", config.StorageEndpoint)

	for {
		resp, err := http.Get(config.StorageEndpoint)
		if err != nil {
			log.Printf("ERROR S3 %s GET Request failed, due to %s", config.StorageEndpoint, err)
			time.Sleep(180 * time.Second)
			continue
		}
		defer resp.Body.Close()
		log.Printf("INFO S3 %s response with status code %v", config.StorageEndpoint, resp.StatusCode)
		if resp.StatusCode >= 500 {
			time.Sleep(180 * time.Second)
			continue
		} else {
			log.Infof("Request successful, S3 service ready")
			break
		}

	}

	var err error
	var sess *session.Session
	inCreds := credentials.NewStaticCredentials(config.StorageKey, config.StorageSecret, config.StorageSessionToken)
	sess, err = session.NewSession(&aws.Config{
		Region:           aws.String(config.StorageRegion),
		Credentials:      inCreds,
		Endpoint:         aws.String(config.StorageEndpoint),
		DisableSSL:       aws.Bool(true),
		S3ForcePathStyle: aws.Bool(true),
		MaxRetries:       aws.Int(3),

		HTTPClient: &http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   time.Duration(config.HttpTransportTimeout) * time.Second,
					KeepAlive: time.Duration(config.HttpTransportKeepAlive) * time.Second,
				}).DialContext,
				TLSHandshakeTimeout:   time.Duration(config.HttpTransportTslHandshakteTimeout) * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
				MaxIdleConns:          config.HttpTransportMaxIdleConns,
				MaxIdleConnsPerHost:   config.HttpTransportMaxIdleConnsPerHost,
				IdleConnTimeout:       90 * time.Second,
			},
		},
	})

	if err != nil {
		log.Fatalf("failed to create a new session, due to %s ", err.Error())
	}

	return s3.New(sess)
}

func listMetsInBucket(bucket string, target string) int64 {

	var j int64 = 0

	if config.CContext != "ocrd" {

		productConfig, err := sub.GetProductCfg(bucket, "mets")
		if err != nil {
			log.Errorf("could not read product config for %s, due to %s, exiting", bucket, err)
			return j
		}

		var bb bool = true
		var truncatedListing = &bb

		query := &s3.ListObjectsInput{
			Bucket: aws.String(bucket),
			Prefix: aws.String(config.CollectFrom),
		}

		log.Printf("List contents of prefix '%s' for bucket '%s'", config.CollectFrom, bucket)

		for *truncatedListing {

			if config.TestRun && j > int64(config.TestRunLimit) {
				break
			}

			resp, err := inClient.ListObjects(query)

			if err != nil {
				log.Errorf("failed to list objects for bucket %s, due to %s", bucket, err.Error())
			}
			for _, cont := range resp.Contents {

				if config.TestRun && j > int64(config.TestRunLimit) {
					break
				}

				inKey := string(*cont.Key)
				i := strings.LastIndex(inKey, "/")
				if inKey[i+1:] == "" || strings.HasPrefix(inKey, "mets/._") {
					log.Errorf("key s3://%s/%s can't be processed, exiting", bucket, inKey)
					continue
				}

				var re *regexp.Regexp
				// TODO move regex to config file
				if strings.HasPrefix(config.CContext, "nlh") {
					re = regexp.MustCompile("^mets/((\\S*).mets.xml)$")
				} else {
					re = regexp.MustCompile("^mets/((\\S*).xml)$")
				}

				match := re.FindStringSubmatch(inKey)

				if (len(match) != 3) || (match[2] == "") {
					log.Errorf("key %s doesn't match pattern /^mets/((\\S*).(mets.xml|xml))$/", inKey)
					continue
				}

				msg := IndexerJob{
					Document: match[2],
					Context:  config.CContext,
					Product:  bucket,
					Reindex:  false,
					Key:      inKey,
					Ftype:    productConfig.FType,
				}
				log.Tracef("Adding '%s' from bucket '%s' to queue", match[2], bucket)

				enqueue(msg, target)

				j++
			}

			if len(resp.Contents) > 0 {
				truncatedListing = resp.IsTruncated
				if *truncatedListing {
					query.SetMarker(string(*resp.NextMarker))
				}
			} else {
				log.Infof("Response was empty!")
			}

		}
	}
	time.Sleep(30 * time.Second)

	return j
}

func enqueue(msg IndexerJob, target string) {
	var err error
	var jsonData []byte
	var queue string

	if target == "indexer" {
		queue = config.RedisIndexQueue
		jsonData, err = json.Marshal(msg)
		if err != nil {
			log.Errorf("could not marshall message %v, due to %s, exiting", msg, err)
			return
		}
	} else if target == "manifestbuilder" {
		queue = config.RedisIiifManifestQueue

		citationData := types.ExportWork{
			WorkID:  msg.Document,
			Context: msg.Context,
			Product: msg.Product,
		}

		jsonData, err = json.Marshal(citationData)
		if err != nil {
			log.Errorf("could not marshall message %v, due to %s, exiting", msg, err)
			return
		}
	} else if target == "citationbuilder" {
		queue = config.RedisCitationQueue

		manifestData := types.ExportWork{
			WorkID:  msg.Document,
			Context: msg.Context,
			Product: msg.Product,
		}

		jsonData, err = json.Marshal(manifestData)
		if err != nil {
			log.Errorf("could not marshall message %v, due to %s, exiting", msg, err)
			return
		}
	}

	if idToDoctype[msg.Document] == "anchor" {
		_, err = redisClient.RPush(queue, string(jsonData)).Result()
	} else {
		_, err = redisClient.LPush(queue, string(jsonData)).Result()
	}
	if err != nil {
		log.Errorf("could not push data to redis, due to %s, exiting", err)
	}
}

func listMetsInBucketV2(bucket string, target string) int64 {

	var j int64 = 0

	productConfig, err := sub.GetProductCfg(bucket, "mets")
	if err != nil {
		log.Errorf("could not read product config for %s, due to %s, exiting", bucket, err)
		return j
	}

	var bb = true
	var truncatedListing = new(bool)
	truncatedListing = &bb

	query := &s3.ListObjectsV2Input{
		Bucket: aws.String(bucket),
		Prefix: aws.String(config.CollectFrom),
	}

	log.Printf("List contents of prefix '%s' for bucket '%s'", config.CollectFrom, bucket)

	for *truncatedListing {

		if config.TestRun && j > int64(config.TestRunLimit) {
			break
		}

		resp, err := inClient.ListObjectsV2(query)

		if err != nil {
			log.Errorf("failed to list objects for bucket %s, due to %s, exiting", bucket, err.Error())
		}
		for _, cont := range resp.Contents {

			if config.TestRun && j > int64(config.TestRunLimit) {
				break
			}

			inKey := string(*cont.Key)
			i := strings.LastIndex(inKey, "/")
			if inKey[i+1:] == "" {
				continue
			}

			// TODO move regex to config file
			re := regexp.MustCompile("^mets/((\\S*).xml)$")

			match := re.FindStringSubmatch(inKey)

			if (len(match) != 3) || (match[2] == "") {
				log.Errorf("key %s doesn't match pattern '/^mets/((\\S*).(mets.xml|xml))$/', exiting", inKey)
				continue
			}

			msg := IndexerJob{
				Document: match[2],
				Context:  config.CContext,
				Product:  bucket,
				Reindex:  false,
				Key:      inKey,
				Ftype:    productConfig.FType,
			}
			log.Tracef("Adding '%s' from bucket '%s' to queue", match[2], bucket)

			enqueue(msg, target)

			j++

		}

		if len(resp.Contents) > 0 {
			truncatedListing = resp.IsTruncated
			if *truncatedListing {
				query.SetContinuationToken(string(*resp.NextContinuationToken))
			}
		} else {
			log.Infof("Response was empty!")
		}
	}

	time.Sleep(30 * time.Second)

	return j
}

func listMetsFromPath(path string, target string) int64 {

	var j int64 = 0

	if config.Product == "" {
		log.Fatalln("Env Variable PRODUCT is required but not set")
	}

	productConfig, err := sub.GetProductCfg(config.Product, "mets")
	if err != nil {
		log.Errorf("could not read product config for %s, due to %s, exiting", config.Product, err)
		return j
	}

	files, err := FilePathWalkDir(path)
	if err != nil {
		log.Errorf("could not recursive list %s, due to %s, exiting", path, err.Error())
		return j
	}

	log.Debugf("number of files %v in path %s", len(files), path)

	for _, fobj := range files {

		if config.TestRun && j > int64(config.TestRunLimit) {
			break
		}

		if !strings.HasSuffix(strings.ToLower(fobj), productConfig.InSuffix) {
			log.Debugf("file %s doesn't fit with sufix %s", fobj, productConfig.InSuffix)
			continue
		}

		inKey := string(fobj)
		i := strings.LastIndex(inKey, "/")
		if inKey[i+1:] == "" {
			continue
		}

		var re *regexp.Regexp
		// TODO move regex to config file
		if strings.HasPrefix(config.CContext, "nlh") {
			re = regexp.MustCompile("^\\S*/((\\S*).mets.xml)$")
		} else {
			re = regexp.MustCompile("^\\S*/((\\S*).xml)$")
		}

		match := re.FindStringSubmatch(inKey)

		if (len(match) != 3) || (match[2] == "") {
			log.Errorf("key %s doesn't match pattern '/^mets/((\\S*).(mets.xml|xml))$/', exiting", inKey)
			continue
		}

		msg := IndexerJob{
			BaseDir:     config.MetsPath,
			FulltextDir: config.FulltextPath,
			Ftype:       productConfig.FType,
			Document:    match[2],
			Context:     config.CContext,
			Product:     config.Product,
			Reindex:     false,
			Key:         inKey,
		}

		enqueue(msg, target)

		j++

	}

	time.Sleep(30 * time.Second)

	return j
}

// FilePathWalkDir will retrieve an root path an walks recursively over it an return an array of file path.
// If Get encounters any errors, it will return an error.
func FilePathWalkDir(root string) ([]string, error) {
	log.Debugf("FilePathWalkDir root: %s", root)
	var files []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})
	return files, err
}
