package main

import (
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.gwdg.de/subugoe/metsimporter/collector/helper"
)

var (
	log logrus.Logger
)

func main() {
	j := helper.IndexAll()
	log.Printf("Collector found %v METS files to process", j)

	for {
		// add infinite loop, to prevent docker based autorestart
		log.Println("Collector in idle mode")
		time.Sleep(300 * time.Second)
	}
}
