Test Data for `collector` and `indexer`
=======================================

This directory contains a `Dockerfile` to build a preloaded [Minio](https://min.io/) container with some METS test files. This allows to mock a S3 Enpoint with real data

# Building

From the root of the repository run:

```
DOCKER_BUILDKIT=1 docker build -f docker/s3/Dockerfile -t docker.gitlab.gwdg.de/subugoe/metsimporter:test-s3-master .
```

If you're atartiing the build from another directory (or with the wronc context), the test data directory can't be found.

## Arguments

* `DATA_FILE` -  The path and name of the compressed (gzipped tar) contents. Make sure it's accessable from the given Docker context.