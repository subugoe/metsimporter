package main

import (
	"archive/tar"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.gwdg.de/subugoe/metsimporter/image_resolver/util"

	"github.com/akamensky/argparse"
	log "github.com/sirupsen/logrus"

	"github.com/go-redis/redis"
)

var (
	//appCfg         *viper.Viper
	//appCfgPath     string
	config util.Config

	//verbose bool

	//inDocker       bool
	//standardFields log.Fields

	redisClient *redis.Client
	//redisAdr                                 string
	//redisDb                                  int
	//redisFileHsetQueue, redisStatusHsetQueue string

	//testRun      bool
	//testRunLimit int

	//validationRun bool

	//imgFType string
	//imgFPath string
	re *regexp.Regexp
)

func init() {

	var err error

	parser := argparse.NewParser("image_resolver", "Collect METS files from path")
	c := parser.String("c", "config", &argparse.Options{Required: false, Help: "Path to configuration file"})
	d := parser.String("d", "config-dir", &argparse.Options{Required: false, Help: "Path to configuration directory"})
	e := parser.Flag("e", "env", &argparse.Options{Required: false, Help: "Configure using environment"})
	v := parser.Flag("v", "verbose", &argparse.Options{Required: false, Help: "Enable debug mode"})

	err = parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
		os.Exit(1)
	}

	var verbose bool
	if *v {
		verbose = true
		log.Info("Enabled verbose mode")
	}

	config, err = util.LoadConfig(*c, *e, *d)
	if err != nil {
		log.Fatal("cannot load config:", err)
	}

	if config.TestRun && config.TestRunLimit == 0 {
		config.TestRunLimit = 5
	}

	//testRun = appCfg.GetBool("TEST_RUN")
	//testRunLimit = appCfg.GetInt("TEST_RUN_LIMIT")
	//validationRun = appCfg.GetBool("VALIDATION_RUN")

	//imgFType = appCfg.GetString("IMGFTYPE")
	//imgFPath = appCfg.GetString("IMGFPATH")
	//redisFileHsetQueue = appCfg.GetString("REDIS_FILE_HSET_QUEUE")
	//redisStatusHsetQueue = appCfg.GetString("REDIS_STATUS_HSET_QUEUE")

	// TODO put in config
	re = regexp.MustCompile("^(\\S*)/(\\S*.(tif|TIF|jpg|JPG|gif|GIF))$")
	//re = regexp.MustCompile("^(\\S*)/(\\S*.[tif|TIF|jpg|JPG|gif|GIF])$")
	//re = regexp.MustCompile("^(.*)/(.*.[tif|TIF|jpg|JPG|gif|GIF])$")

	/*
		if len(os.Args) > 1 && os.Args[1] == "startedViaDocker" {
			redisAdr = appCfg.GetString("REDIS_DOCKER_ADR")
		} else {
			redisAdr = appCfg.GetString("REDIS_LOCAL_ADR")
		}
	*/
	//redisDb = appCfg.GetInt("REDIS_DB")

	redisClient = redis.NewClient(&redis.Options{
		Addr:       config.RedisAdr,
		DB:         config.RedisDB,
		MaxRetries: 3,
	})

	var logLevel log.Level
	if verbose {
		logLevel = log.TraceLevel
	} else {
		logLevel, err = log.ParseLevel(config.LogLevel)
		if err != nil {
			log.Error("could not parse the log level: ", err)
			logLevel = log.InfoLevel
		}
	}

	log.SetLevel(logLevel)
	log.Infof("Set Log level to %s", logLevel)

	log.SetFormatter(
		&log.JSONFormatter{
			TimestampFormat:  "",
			DisableTimestamp: false,
			DataKey:          "",
			FieldMap: log.FieldMap{
				log.FieldKeyTime:  "@timestamp",
				log.FieldKeyLevel: "log.level",
				log.FieldKeyMsg:   "message",
				log.FieldKeyFunc:  "function.name",
			},
			PrettyPrint: false,
		},
	)

	log.SetOutput(os.Stdout)

	log.SetReportCaller(true)
}

func main() {

	if !config.ValidationRun {

		_, err := redisClient.HSet(config.RedisStatusHsetQueue, config.RedisFinishImgageKeyResolving, true).Result()
		if err != nil {
			log.Errorf("could not write to redis HSET, due to %s", err)
		} else {
			log.Info("finish preprocessing")
		}

		return
	}

	/*var baseDir string
	if inDocker {
		baseDir = "/data/fpath/"
	} else {
		baseDir = ImagePath
	}*/

	files, err := FilePathWalkDir(config.ImagePath)
	if err != nil {
		log.Errorf("could not recursive list path %s, due to %s", config.ImagePath, err.Error())
		return
	}
	log.Debugf("%v files are in path %s", len(files), config.ImagePath)

	if strings.ToLower(config.ImageType) == "tar" {

		i := 0
		for _, file := range files {

			if !strings.HasSuffix(file, ".tar") {
				continue
			}

			i++
			if i%50 == 0 {
				log.Info("number of processed TARs: ", i)
			}

			if config.TestRun && i > config.TestRunLimit {
				break
			}

			log.Debug("process img tar: ", file)

			tarFile, err := os.Open(file)

			if err != nil {
				log.Errorf("could not open %s, due to %s", file, err.Error())
				continue
			}

			tarReader := tar.NewReader(tarFile)

			for {
				header, err := tarReader.Next()

				if err == io.EOF {
					break
				}

				if err != nil {
					log.Errorf("failed to extract tar file %s, due to %s", file, err.Error())
					break
				}

				switch header.Typeflag {
				case tar.TypeReg:
					addFileToQueue(header.Name, file)
				default:
					//log.Errorf("uknown type: %s in %s", string(header.Typeflag), header.Name)
					continue
				}

			}

			tarFile.Close()
		}
	} else if strings.ToLower(config.ImageType) == "file" {

		i := 0
		for _, fobj := range files {

			i++
			if i%500 == 0 {
				log.Printf("INFO %v files processed", i)
			}

			if config.TestRun && i > config.TestRunLimit {
				break
			}

			addFileToQueue(fobj, "")
		}

	}

	_, err = redisClient.HSet(config.RedisStatusHsetQueue, config.RedisFinishImgageKeyResolving, true).Result()
	if err != nil {
		log.Errorf("could not write redis HSET, due to %s", err)
	} else {
		log.Info("finish preprocessing")
	}

}

func addFileToQueue(fobj string, tarpath string) {

	match := re.FindStringSubmatch(fobj)

	if (len(match) != 4) || (match[2] == "") {
		log.Debugf("key %s doesn't match pattern %s", fobj, re.String())
		return
	}

	log.Infof("queue: %s, match[2]: %s, tar: %s", config.RedisFileHsetQueue, match[2], tarpath)

	var err error
	// redis-cli -p 8442 -n 9 HSET redis_file_hset $f $path >/dev/null
	// redis-cli -p 8442 -n 9 HGET redis_file_hset $f $path >/dev/null
	if tarpath != "" {
		_, err = redisClient.HSet(config.RedisFileHsetQueue, match[2], tarpath).Result()
	} else {
		_, err = redisClient.HSet(config.RedisFileHsetQueue, match[2], match[1]).Result()
	}

	if err != nil {
		log.Errorf("could not write to redis HSET, due to %s ", err.Error())
	}
}

// FilePathWalkDir will retrieve an root path an walks recursively over it an return an array of file path.
// If Get encounters any errors, it will return an error.
func FilePathWalkDir(root string) ([]string, error) {
	log.Debugf("FilePathWalkDir root: %s", root)
	var files []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})
	return files, err
}
