package util

import (
	"log"

	"github.com/spf13/viper"
)

const (
	// Config files
	//buildInConfigDir = "/etc/env/"
	//buildInConfigDir = "../cfg/env/"

	buildInConfigAppEnv       = "app.env"
	buildInConfigAppStaticEnv = "app-static.env"

	buildInConfigCommonsEnv       = "commons.env"
	buildInConfigCommonsStaticEnv = "commons-static.env"

	buildInConfigIiifEnv = "iiif.env"

	buildInConfigDockerEnv = "docker.env"
)

// Config ...
type Config struct {

	// from docker-env

	Environment string `mapstructure:"ENVIRONMENT"`

	CContext string `mapstructure:"CONTEXT"`

	CopyFromPath string `mapstructure:"COPYFROMPATH"`
	CopyToPath   string `mapstructure:"COPYTOPATH"`

	MetsPath       string `mapstructure:"METS_PATH"`
	MetsAnchorPath string `mapstructure:"METS_ANCHOR_PATH"`
	FulltextPath   string `mapstructure:"FULLTEXT_PATH"`
	ImagePath      string `mapstructure:"IMAGE_PATH"`
	ImageType      string `mapstructure:"IMAGE_TYPE"`

	// Config files

	// General settings

	Product string `mapstructure:"PRODUCT"`

	CollectFrom            string `mapstructure:"COLLECT_FROM"`
	CollectFromMETSKey     string `mapstructure:"COLLECT_FROM_METS_KEY"`
	CollectFromImageKey    string `mapstructure:"COLLECT_FROM_IMAGE_KEY"`
	CollectFromFulltextKey string `mapstructure:"COLLECT_FROM_FULLTEXT_KEY"`
	CollectFromPDFKey      string `mapstructure:"COLLECT_FROM_PDF_KEY"`

	Concurrency int `mapstructure:"CONCURRENCY"`

	//	Port string `mapstructure:"server_port"`

	TestRun      bool `mapstructure:"TEST_RUN"`
	TestRunLimit int  `mapstructure:"TEST_RUN_LIMIT"`

	OAPrefixes     string `mapstructure:"OA_PREFIXES"`
	ProcessDoctype string `mapstructure:"PROCESS_DOCTYPE"`

	StorageKey          string `mapstructure:"STORAGE_KEY"`
	StorageSecret       string `mapstructure:"STORAGE_SECRET"`
	StorageSessionToken string `mapstructure:"STORAGE_SESSION_TOKEN"`
	StorageRegion       string `mapstructure:"STORAGE_REGION"`
	StorageEndpoint     string `mapstructure:"STORAGE_ENDPOINT"`
	StorageProvider     string `mapstructure:"STORAGE_PROVIDER"`

	// REDIS
	RedisAdr        string `mapstructure:"REDIS_ADR"`
	RedisDB         int    `mapstructure:"REDIS_DB"`
	RedisMaxRetries int    `mapstructure:"REDIS_MAX_RETRIES"`
	RedisMinIDConns int    `mapstructure:"REDIS_MIN_ID_CONNS"`
	RedisPoolSize   int    `mapstructure:"REDIS_POOL_SIZE"`

	RedisInfoJsonQueue            string `mapstructure:"REDIS_INFOJSON_QUEUE"`
	RedisIndexQueue               string `mapstructure:"REDIS_INDEX_QUEUE"`
	RedisIndexQueue2              string `mapstructure:"REDIS_INDEX_QUEUE2"`
	RedisCheckKeyQueue1           string `mapstructure:"REDIS_CHECK_KEY_QUEUE_1"`
	RedisCheckKeyQueue2           string `mapstructure:"REDIS_CHECK_KEY_QUEUE_2"`
	RedisKeyNotExistQueue         string `mapstructure:"REDIS_KEY_NOT_EXIST_QUEUE"`
	RedisCounterQueue             string `mapstructure:"REDIS_COUNTER_QUEUE"`
	RedisFileHsetQueue            string `mapstructure:"REDIS_FILE_HSET_QUEUE"`
	RedisStatusHsetQueue          string `mapstructure:"REDIS_STATUS_HSET_QUEUE"`
	RedisIndexingFinishedKey      string `mapstructure:"REDIS_INDEXING_FINISHED_KEY"`
	RedisStopIndexerKey           string `mapstructure:"REDIS_STOP_INDEXER_KEY"`
	RedisFinishImgageKeyResolving string `mapstructure:"REDIS_FINISH_IMGAGE_KEY_RESOLVING"`
	RedisFinishMetsKeyResolving   string `mapstructure:"REDIS_FINISH_METS_KEY_RESOLVING"`
	ContentIDDateKvStore          string `mapstructure:"REDIS_CONTENT_ID_DATE_KV_STORE"`
	RedisWorkConvertQueue         string `mapstructure:"REDIS_WORK_CONVERT_QUEUE"`
	RedisImgConvertFullQueue      string `mapstructure:"REDIS_IMG_CONVERT_FULL_QUEUE"`
	RedisImgConvertLogQueue       string `mapstructure:"REDIS_IMG_CONVERT_LOG_QUEUE"`
	RedisUniqueQueue              string `mapstructure:"REDIS_UNIQUE_QUEUE"`
	OldSolrDates                  string `mapstructure:"REDIS_OLD_SOLR_DATES_QUEUE"`
	OldESDates                    string `mapstructure:"REDIS_OLD_ES_DATES_QUEUE"`
	RedisStopConverterKey         string `mapstructure:"REDIS_STOP_CONVERTER_KEY"`

	// S3 key pattern for export
	ExportKeyPatternRis     string `mapstructure:"EXPORT_KEY_PATTERN_RIS"`
	ExportKeyPatternBibtex  string `mapstructure:"EXPORT_KEY_PATTERN_BIBTEX"`
	ExportKeyPatternEndnote string `mapstructure:"EXPORT_KEY_PATTERN_ENDNOTE"`
	ExportKeyPatternIiif    string `mapstructure:"EXPORT_KEY_PATTERN_IIIF"`

	// HTTP connection settings
	HttpClientTimeout                 int `mapstructure:"HTTP_CLIENT_TIMEOUT"`
	HttpTransportTimeout              int `mapstructure:"HTTP_TRANSPORT_TIMEOUT"`
	HttpTransportKeepAlive            int `mapstructure:"HTTP_TRANSPORT_KEEP_ALIVE"`
	HttpTransportTslHandshakteTimeout int `mapstructure:"HTTP_TRANSPORT_TSL_HANDSHAKTE_TIMEOUT"`
	HttpTransportMaxIdleConns         int `mapstructure:"HTTP_TRANSPORT_MAX_IDLE_CONNS"`
	HttpTransportMaxIdleConnsPerHost  int `mapstructure:"HTTP_TRANSPORT_MAX_IDLE_CONNS_PER_HOST"`

	// Solr settings
	SolrHost         string `mapstructure:"SOLR_HOST"`
	SolrUpdatePath   string `mapstructure:"SOLR_UPDATE_PATH"`
	SolrReadPath     string `mapstructure:"SOLR_Read_PATH"`
	LiveSolrHost     string `mapstructure:"LIVE_SOLR_HOST"`
	LiveSolrReadPath string `mapstructure:"LIVE_SOLR_Read_PATH"`

	PurlURI     string `mapstructure:"PURL_PATTERN"`
	PurlBaseURI string `mapstructure:"PURL_BASE_PATTERN"`

	// todo for NLH
	OpacURI string `mapstructure:"OPAC_URI"`

	KalliopeURI     string `mapstructure:"KALLIOPE_URI"`
	KalliopeSruPath string `mapstructure:"KALLIOPE_SRU_PATH"`
	KalliopePath    string `mapstructure:"KALLIOPE_PATH"`

	UnapiURI  string `mapstructure:"UNAPI_URI"`
	UnapiPath string `mapstructure:"UNAPI_PATH"`

	DigizeitLogIndex  string `mapstructure:"DIGIZEIT_LOG_INDEX"`
	DigizeitPhysIndex string `mapstructure:"DIGIZEIT_PHYS_INDEX"`

	LogIndexCollection  string `mapstructure:"LOG_INDEX_COLLECTION"`
	PhysIndexCollection string `mapstructure:"PHYS_INDEX_COLLECTION"`

	ContributionTypes       []string `mapstructure:"CONTRIBUTION_TYPES"`
	DatebasedProducts       []string `mapstructure:"DATEBASED_PRODUCTS"`
	MultiPeriodicalProducts []string `mapstructure:"MULTI_PERIODICAL_PRODUCTS"`

	ElasticsearchHost string `mapstructure:"ELASTICSEARCH_HOST"`

	// ValidationRun determines whether validation or indexing is running
	ValidationRun bool `mapstructure:"VALIDATION_RUN"`

	LogLevel string `mapstructure:"LOG_LEVEL"`

	// --- iiif part (for indexer)
	IiifManifestURLGDZ      string `mapstructure:"Iiif_Manifest_URL_GDZ"`
	IiifManifestURLDigizeit string `mapstructure:"Iiif_Manifest_URL_Digizeit"`
	IiifManifestURLNLH      string `mapstructure:"Iiif_Manifest_URL_NLH"`

	IiifImageConverterURLGDZ      string `mapstructure:"Iiif_Image_Converter_URL_GDZ"`
	IiifImageConverterURLDigizeit string `mapstructure:"Iiif_Image_Converter_URL_Digizeit"`
	IiifImageConverterURLNLH      string `mapstructure:"Iiif_Image_Converter_URL_NLH"`

	IiifThumbTemplateGDZ      string `mapstructure:"Iiif_Thumb_Template_GDZ"`
	IiifThumbTemplateDigizeit string `mapstructure:"Iiif_Thumb_Template_Digizeit"`
	IiifThumbTemplateNLH      string `mapstructure:"Iiif_Thumb_Template_NLH"`

	IiifThumbIDTemplateGDZ      string `mapstructure:"Iiif_Thumb_ID_Template_GDZ"`
	IiifThumbIDTemplateDigizeit string `mapstructure:"Iiif_Thumb_ID_Template_Digizeit"`
	IiifThumbIDTemplateNLH      string `mapstructure:"Iiif_Thumb_ID_Template_NLH"`

	IiifViewerLogResolvingGDZ      string `mapstructure:"Iiif_Viewer_Log_Resolving_GDZ"`
	IiifViewerLogResolvingDigizeit string `mapstructure:"Iiif_Viewer_Log_Resolving_Digizeit"`
	IiifViewerLogResolvingNLH      string `mapstructure:"Iiif_Viewer_Log_Resolving_NLH"`

	IiifViewerSequenceResolvingURLGDZ      string `mapstructure:"Iiif_Viewer_Sequence_Resolving_URL_GDZ"`
	IiifViewerSequenceResolvingURLDigizeit string `mapstructure:"Iiif_Viewer_Sequence_Resolving_URL_Digizeit"`
	IiifViewerSequenceResolvingURLNLH      string `mapstructure:"Iiif_Viewer_Sequence_Resolving_URL_NLH"`

	IiifViewerResolvingURLGDZ      string `mapstructure:"Iiif_Viewer_Resolving_URL_GDZ"`
	IiifViewerResolvingURLDigizeit string `mapstructure:"Iiif_Viewer_Resolving_URL_Digizeit"`
	IiifViewerResolvingURLNLH      string `mapstructure:"Iiif_Viewer_Resolving_URL_NLH"`

	IiifViewerCanvasResolvingURLGDZ      string `mapstructure:"Iiif_Viewer_Canvas_Resolving_URL_GDZ"`
	IiifViewerCanvasResolvingURLDigizeit string `mapstructure:"Iiif_Viewer_Canvas_Resolving_URL_Digizeit"`
	IiifViewerCanvasResolvingURLNLH      string `mapstructure:"Iiif_Viewer_Canvas_Resolving_URL_NLH"`

	IiifViewerAnnotationResolvingURLGDZ      string `mapstructure:"Iiif_Viewer_Annotation_Resolving_URL_GDZstring"`
	IiifViewerAnnotationResolvingURLDigizeit string `mapstructure:"Iiif_Viewer_Annotation_Resolving_URL_Digizeitstring"`
	IiifViewerAnnotationResolvingURLNLH      string `mapstructure:"Iiif_Viewer_Annotation_Resolving_URL_NLHstring"`

	IiifContext             string `mapstructure:"Iiif_Context"`
	IiifProfile             string `mapstructure:"Iiif_Profile"`
	IiifImageType           string `mapstructure:"Iiif_Image_Type"`
	IiifManifestType        string `mapstructure:"Iiif_Manifest_Type"`
	IiifSequenceType        string `mapstructure:"Iiif_Sequence_Type"`
	IiifCanvasType          string `mapstructure:"Iiif_Canvas_Type"`
	IiifAnnotationType      string `mapstructure:"Iiif_Annotation_Type"`
	IiifAnnotationBodyType  string `mapstructure:"Iiif_Annotation_Body_Type"`
	IiifImageMotivationType string `mapstructure:"Iiif_Image_Motivation_Type"`
	IiifStructureType       string `mapstructure:"Iiif_Structure_Type"`
	IiifImageFormat         string `mapstructure:"Iiif_Image_Format"`
	IiifImageMimetype       string `mapstructure:"Iiif_Image_Mimetype"`
	IiifThumbnailFormat     string `mapstructure:"Iiif_Thumbnail_Format"`

	LogoGDZ      string `mapstructure:"Logo_GDZ"`
	LogoDigizeit string `mapstructure:"Logo_Digizeit"`
	LogoNLH      string `mapstructure:"Logo_NLH"`

	MetsDownloadURLGDZ      string `mapstructure:"Mets_Download_URL_GDZ"`
	MetsDownloadURLDigizeit string `mapstructure:"Mets_Download_URL_Digizeit"`
	MetsDownloadURLNLH      string `mapstructure:"Mets_Download_URL_NLH"`

	MetsFormat string `mapstructure:"Mets_Format"`

	PdfDownloadURLGDZ      string `mapstructure:"Pdf_Download_URLG_DZ"`
	PdfDownloadURLDigizeit string `mapstructure:"Pdf_Download_URL_Digizeit"`
	PdfDownloadURLNLH      string `mapstructure:"Pdf_Download_URL_NLH"`

	PdfDownloadLabel string `mapstructure:"Pdf_Download_Label"`
	PdfFormat        string `mapstructure:"Pdf_Format"`

	DfgViewerBaseURL string `mapstructure:"Dfg_Viewer_Base_URL"`
	DfgViewerLabel   string `mapstructure:"Dfg_Viewer_Label"`
	DfgViewerFormat  string `mapstructure:"Dfg_Viewer_Format"`

	MetsProfile string `mapstructure:"Mets_Profile"`
}

func LoadConfig(cfgpath string, e bool, cfgdirpath string) (Config, error) {

	var config Config

	viper.SetConfigType("env")

	if e {
		viper.AutomaticEnv()
	}

	if cfgpath != "" {
		viper.SetConfigFile(cfgpath)
	} else if cfgdirpath != "" {
		viper.AddConfigPath(cfgdirpath)
		viper.SetConfigName(buildInConfigCommonsEnv)
	}

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			log.Fatalf("Config file not found, due to %v", err)
		} else {
			// Config file was found but another error was produced
			log.Fatalf("Config file was found but another error was produced, due to %s", err.Error())
		}
	}

	// merge commons config

	if cfgpath == "" {
		viper.SetConfigName(buildInConfigCommonsStaticEnv)
		viper.MergeInConfig()

		viper.SetConfigName(buildInConfigDockerEnv)
		viper.MergeInConfig()

		viper.SetConfigName(buildInConfigAppEnv)
		viper.MergeInConfig()

		viper.SetConfigName(buildInConfigAppStaticEnv)
		viper.MergeInConfig()

		viper.SetConfigName(buildInConfigIiifEnv)
		viper.MergeInConfig()
	}

	err := viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("Could not Unmarshal config file, due to %v", err)
	}

	return config, nil
}
