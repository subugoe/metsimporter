module gitlab.gwdg.de/subugoe/metsimporter/indexer

go 1.23

require (
	github.com/PuerkitoBio/goquery v1.9.0
	github.com/akamensky/argparse v1.4.0
	github.com/aws/aws-sdk-go v1.50.24
	github.com/elastic/go-elasticsearch/v6 v6.8.10
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/olivere/elastic v6.2.37+incompatible
	github.com/sirupsen/logrus v1.9.3
	github.com/spf13/viper v1.18.2
	gopkg.in/gographics/imagick.v3 v3.6.0
	gopkg.in/sohlich/elogrus.v3 v3.0.0-20180410122755-1fa29e2f2009
)

require (
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/fsnotify/fsnotify v1.7.0 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/magiconair/properties v1.8.7 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.34.1 // indirect
	github.com/pelletier/go-toml/v2 v2.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rogpeppe/go-internal v1.11.0 // indirect
	github.com/sagikazarmark/locafero v0.4.0 // indirect
	github.com/sagikazarmark/slog-shim v0.1.0 // indirect
	github.com/sourcegraph/conc v0.3.0 // indirect
	github.com/spf13/afero v1.11.0 // indirect
	github.com/spf13/cast v1.6.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/subosito/gotenv v1.6.0 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.9.0 // indirect
	golang.org/x/exp v0.0.0-20250106191152-7588d65b2ba8 // indirect
	golang.org/x/net v0.27.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
	golang.org/x/text v0.16.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
