package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"

	"gitlab.gwdg.de/subugoe/metsimporter/indexer/export"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/index"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/mets"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/util"
)

var (
	concurrency int // = 30
	config      util.Config
	log         logrus.Logger
	logFile     string = ""
)

// S3Key ...
type S3Key struct {
	Name           string `json:"name"`
	Type           string `json:"type"`
	InRegex        string `json:"in_regex"`
	BaseDir        string `json:"base_dir"`
	InSuffix       string `json:"in_suffix"`
	InBucket       string `json:"in_bucket"`
	InKey          string `json:"in_key"`
	InPrefix       string `json:"in_prefix"`
	OutBucket      string `json:"out_bucket"`
	OutKey         string `json:"out_key"`
	OutPrefix      string `json:"out_prefix"`
	PdfOutKey      string `json:"pdf_out_key"`
	Packaging      string `json:"packaging"`
	Storage        string `json:"storage"`
	ForceOverwrite bool   `json:"force_overwrite"`
	Ftype          string `json:"ftype"` // fulltext type HTML, TXT_1, TXT_2, TEI_2

}

func init() {

	config, log = util.LoadConfig()

	if config.TestRun && config.TestRunLimit == 0 {
		config.TestRun = false
	}

	if config.TestRun {
		concurrency = 1
	} else {
		concurrency = config.Concurrency
	}

	// TODO product specific
	if config.CContext == "digizeit" || config.CContext == "ocrd" {

		for {
			resp, err := http.Get(config.ElasticsearchHost)
			if err != nil {
				log.Printf("Info Elasticsearch not running yet, due to %s", err)
				time.Sleep(180 * time.Second)
				continue
			}
			defer resp.Body.Close()

			if resp.StatusCode > 300 {
				log.Printf("INFO Elasticsearch response with status code %v", resp.StatusCode)
				time.Sleep(180 * time.Second)
				continue
			} else {
				break
			}

		}

	}

}

func main() {

	var file *os.File
	var err error

	if logFile != "" {
		file, err = os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0755)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		mw := io.MultiWriter(os.Stdout, file)
		log.SetOutput(mw)
	}

	log.Info("Indexer service starts processing...")

	redisClient := getRedisClient()
	defer redisClient.Close()

	// TODO product specific
	// TODO GDZ related!
	if config.CContext != "ocrd" {
		_, err = redisClient.HSet(config.RedisStatusHsetQueue, config.RedisIndexingFinishedKey, false).Result()
		if err != nil {
			log.Errorf("could not set queue %s field %s to false", config.RedisStatusHsetQueue, config.RedisIndexingFinishedKey)
			time.Sleep(20 * time.Second)
		}

		readIndexDatesAndPushToRedis(redisClient)
	}

	for w := 1; w <= concurrency; w++ {
		go mets.ParseMetsWorker(w)
		go mets.ProcessMetsWorker(w)

		if config.CContext != "nlh" {

			go export.CreateIiifManifest(w)

			// TODO product specific
			if config.CContext != "ocrd" {
				go export.CreateCitationExport(w)
			}
			// else {
			// 	go export.CreateInfoJsonManifest(w)
			// }
		}
	}

	for {
		time.Sleep(60 * time.Second)
	}

}

func getRedisClient() *redis.Client {

	return redis.NewClient(&redis.Options{
		Addr:         config.RedisAdr,
		DB:           config.RedisDB, // use default DB
		MaxRetries:   config.RedisMaxRetries,
		MinIdleConns: config.RedisMinIDConns,
		DialTimeout:  time.Minute,
		ReadTimeout:  time.Minute,
	})
}

func readIndexDatesAndPushToRedis(redisClient *redis.Client) {

	csvFile, err := os.Open(config.OldIndexDatesFile)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened CSV file")
	defer csvFile.Close()

	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		fmt.Println(err)
	}
	for _, line := range csvLines {

		if strings.HasPrefix(line[0], "#") {
			continue
		}

		date := index.OldSolrDatesMsg{
			RecordIdentifier: line[0],
			Dateindexed:      line[1],
			Datemodified:     line[2],
		}
		b, err := json.Marshal(date)
		if err != nil {
			log.Errorf("could not marshal date %v, due to %s", date, err.Error())
		}
		redisClient.HSet(config.OldIndexDates, line[0], b)

	}
}
