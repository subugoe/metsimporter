package util

import (
	"fmt"
	"os"
	"time"

	"github.com/olivere/elastic"
	"github.com/sirupsen/logrus"
	"gopkg.in/sohlich/elogrus.v3"

	"github.com/spf13/viper"

	"github.com/akamensky/argparse"

	"encoding/json"
)

const (
	buildInConfigAppEnv           = "app.env"
	buildInConfigAppStaticEnv     = "app-static.env"
	buildInConfigCommonsEnv       = "commons.env"
	buildInConfigCommonsStaticEnv = "commons-static.env"
	buildInConfigIiifEnv          = "iiif.env"
	buildInConfigDockerEnv        = "docker.env"
)

type BagInfoTXTConfig struct {
	//BagSoftwareAgent           string `mapstructure:"Bag-Software-Agent:"`
	OcrdIdentifier             string `mapstructure:"Ocrd-Identifier"`
	OcrdWorkIdentifier         string `mapstructure:"Ocrd-Work-Identifier"`
	OcrdMets                   string `mapstructure:"Ocrd-Mets"`
	OlahdSearchPrevPID         string `mapstructure:"Olahd-Search-Prev-PID"`
	OlahdSearchImageFilegrp    string `mapstructure:"Olahd-Search-Image-Filegrp"`
	OlahdSearchFulltextFilegrp string `mapstructure:"Olahd-Search-Fulltext-Filegrp"`
	OlahdSearchFulltextFtype   string `mapstructure:"Olahd-Search-Fulltext-Ftype"`
	OlahdGT                    bool   `mapstructure:"Olahd-GT"`
	OlahdImporterInstitution   string `mapstructure:"Olahd-Importer"`
}

// Config ...
type Config struct {
	Prepared                     bool
	Environment                  string `mapstructure:"ENVIRONMENT"`
	CContext                     string `mapstructure:"CONTEXT"`
	CopyFromPath                 string `mapstructure:"COPYFROMPATH"`
	CopyToPath                   string `mapstructure:"COPYTOPATH"`
	MetsPath                     string `mapstructure:"METS_PATH"`
	MetsAnchorPath               string `mapstructure:"METS_ANCHOR_PATH"`
	FulltextPath                 string `mapstructure:"FULLTEXT_PATH"`
	ImagePath                    string `mapstructure:"IMAGE_PATH"`
	ImageType                    string `mapstructure:"IMAGE_TYPE"`
	ImageTemplateDirectory       string `mapstructure:"IMAGE_TEMPLATE_DIRECTORY"`
	Product                      string `mapstructure:"PRODUCT"`
	CollectFrom                  string `mapstructure:"COLLECT_FROM"`
	CollectFromMETSKey           string `mapstructure:"COLLECT_FROM_METS_KEY"`
	CollectFromImageKey          string `mapstructure:"COLLECT_FROM_IMAGE_KEY"`
	CollectFromFulltextKey       string `mapstructure:"COLLECT_FROM_FULLTEXT_KEY"`
	CollectFromPDFKey            string `mapstructure:"COLLECT_FROM_PDF_KEY"`
	Concurrency                  int    `mapstructure:"CONCURRENCY"`
	TestRun                      bool   `mapstructure:"TEST_RUN"`
	TestRunLimit                 int    `mapstructure:"TEST_RUN_LIMIT"`
	OAPrefixes                   string `mapstructure:"OA_PREFIXES"`
	ProcessDoctype               string `mapstructure:"PROCESS_DOCTYPE"`
	StorageKey                   string `mapstructure:"STORAGE_KEY"`
	StorageSecret                string `mapstructure:"STORAGE_SECRET"` //  Storage
	StorageSessionToken          string `mapstructure:"STORAGE_SESSION_TOKEN"`
	StorageRegion                string `mapstructure:"STORAGE_REGION"`
	StorageEndpoint              string `mapstructure:"STORAGE_ENDPOINT"`
	StorageProvider              string `mapstructure:"STORAGE_PROVIDER"`
	StorageIsLocal               bool   `mapstructure:"STORAGE_IS_LOCAL"`
	RedisAdr                     string `mapstructure:"REDIS_ADR"` //  Redis
	RedisDB                      int    `mapstructure:"REDIS_DB"`
	RedisMaxRetries              int    `mapstructure:"REDIS_MAX_RETRIES"`
	RedisMinIDConns              int    `mapstructure:"REDIS_MIN_ID_CONNS"`
	RedisPoolSize                int    `mapstructure:"REDIS_POOL_SIZE"`
	RedisImageInfoHSetQueue      string `mapstructure:"REDIS_IMAGE_INFO_HSET_QUEUE"`
	RedisInfoJsonQueue           string `mapstructure:"REDIS_INFOJSON_QUEUE"`
	RedisIndexQueue              string `mapstructure:"REDIS_INDEX_QUEUE"`
	RedisIndexQueue2             string `mapstructure:"REDIS_INDEX_QUEUE2"`
	RedisCheckKeyQueue1          string `mapstructure:"REDIS_CHECK_KEY_QUEUE_1"`
	RedisCheckKeyQueue2          string `mapstructure:"REDIS_CHECK_KEY_QUEUE_2"`
	RedisKeyNotExistQueue        string `mapstructure:"REDIS_KEY_NOT_EXIST_QUEUE"`
	RedisCounterQueue            string `mapstructure:"REDIS_COUNTER_QUEUE"`
	RedisFileHsetQueue           string `mapstructure:"REDIS_FILE_HSET_QUEUE"`
	RedisStatusHsetQueue         string `mapstructure:"REDIS_STATUS_HSET_QUEUE"`
	RedisIndexingFinishedKey     string `mapstructure:"REDIS_INDEXING_FINISHED_KEY"`
	RedisStopIndexerKey          string `mapstructure:"REDIS_STOP_INDEXER_KEY"`
	RedisFinishImageKeyResolving string `mapstructure:"REDIS_FINISH_IMAGE_KEY_RESOLVING"`
	RedisFinishMetsKeyResolving  string `mapstructure:"REDIS_FINISH_METS_KEY_RESOLVING"`
	ContentIDDateKvStore         string `mapstructure:"REDIS_CONTENT_ID_DATE_KV_STORE"`
	RedisWorkConvertQueue        string `mapstructure:"REDIS_WORK_CONVERT_QUEUE"`
	RedisImgConvertFullQueue     string `mapstructure:"REDIS_IMG_CONVERT_FULL_QUEUE"`
	RedisImgConvertLogQueue      string `mapstructure:"REDIS_IMG_CONVERT_LOG_QUEUE"`
	RedisUniqueQueue             string `mapstructure:"REDIS_UNIQUE_QUEUE"`
	OldIndexDates                string `mapstructure:"REDIS_OLD_INDEX_DATES_QUEUE"`
	RedisStopConverterKey        string `mapstructure:"REDIS_STOP_CONVERTER_KEY"`
	RedisIiifManifestQueue       string `mapstructure:"REDIS_IIIF_MANIFEST_QUEUE"`
	RedisIiifManifestRangeQueue  string `mapstructure:"REDIS_IIIF_MANIFEST_RANGE_QUEUE"`
	RedisCitationQueue           string `mapstructure:"REDIS_CITATION_QUEUE"`
	ServiceAdr                   string `mapstructure:"SERVICE_ADR"`
	ServicesExternalPort         string `mapstructure:"SERVICES_EXTERNAL_PORT"`
	ServicesInternalPort         string `mapstructure:"SERVICES_INTERNAL_PORT"`

	ConverterCtxPath string `mapstructure:"CONVERTER_CTX_PATH"`

	IndexerCtxPath   string `mapstructure:"INDEXER_CTX_PATH"`
	ReindexerCtxPath string `mapstructure:"REINDEXER_CTX_PATH"`

	ManifestJobCtxPath      string `mapstructure:"MANIFEST_CTX_PATH"`
	ManifestRecreateCtxPath string `mapstructure:"MANIFEST_RECREATE_CTX_PATH"`
	ManifestAccessCtxPath   string `mapstructure:"MANIFEST_ACCESS_CTX_PATH"`

	RangeManifestCtxPath         string `mapstructure:"RANGE_MANIFEST_CTX_PATH"`
	RangeManifestRecreateCtxPath string `mapstructure:"RANGE_MANIFEST_RECREATE_CTX_PATH"`
	RangeManifestAccessCtxPath   string `mapstructure:"RANGE_MANIFEST_ACCESS_CTX_PATH"`

	InfojsonCtxPath         string `mapstructure:"INFOJSON_CTX_PATH"`
	InfojsonRecreateCtxPath string `mapstructure:"INFOJSON_RECREATE_CTX_PATH"`
	InfojsonAccessCtxPath   string `mapstructure:"INFOJSON_ACCESS_CTX_PATH"`

	ImageAccessCtxPath string `mapstructure:"IMAGE_ACCESS_CTX_PATH"`

	CitationCtxPath           string `mapstructure:"CITATION_CTX_PATH"`
	RecreateCitationCtxPath   string `mapstructure:"RECREATE_CITATION_CTX_PATH"`
	PurgeCtxPath              string `mapstructure:"PURGE_CTX_PATH"`
	ExportKeyPatternRis       string `mapstructure:"EXPORT_KEY_PATTERN_RIS"`        // S3 key pattern for export
	ExportKeyPatternBibtex    string `mapstructure:"EXPORT_KEY_PATTERN_BIBTEX"`     // S3 key pattern for export
	ExportKeyPatternEndnote   string `mapstructure:"EXPORT_KEY_PATTERN_ENDNOTE"`    // S3 key pattern for export
	ExportKeyPatternIiif      string `mapstructure:"EXPORT_KEY_PATTERN_IIIF"`       // S3 key pattern for export
	ExportKeyPatternIiifRange string `mapstructure:"EXPORT_KEY_PATTERN_IIIF_RANGE"` // S3 key pattern for export
	ExportKeyPatternInfoJson  string `mapstructure:"EXPORT_KEY_PATTERN_INFO_JSON"`  // S3 key pattern for export
	ExportKeyPatternImage     string `mapstructure:"EXPORT_KEY_PATTERN_IMAGE"`      // S3 key pattern for export

	S3ImageKeyPattern        string `mapstructure:"S3_IMAGE_KEY_PATTERN"`
	S3MetsKeyPattern         string `mapstructure:"S3_METS_KEY_PATTERN"`
	S3IiifManifestKeyPattern string `mapstructure:"S3_IIIF_MANIFEST_KEY_PATTERN"`
	S3IiifRangeKeyPattern    string `mapstructure:"S3_IIIF_RANGE_KEY_PATTERN"`
	S3IiifInfoJsonKeyPattern string `mapstructure:"S3_IIIF_INFO_JSON_KEY_PATTERN"`
	S3PdfKeyPattern          string `mapstructure:"S3_PDF_KEY_PATTERN"`

	HttpClientTimeout                 int    `mapstructure:"HTTP_CLIENT_TIMEOUT"`                    // HTTP connection settings
	HttpTransportTimeout              int    `mapstructure:"HTTP_TRANSPORT_TIMEOUT"`                 // HTTP connection settings
	HttpTransportKeepAlive            int    `mapstructure:"HTTP_TRANSPORT_KEEP_ALIVE"`              // HTTP connection settings
	HttpTransportTslHandshakteTimeout int    `mapstructure:"HTTP_TRANSPORT_TSL_HANDSHAKTE_TIMEOUT"`  // HTTP connection settings
	HttpTransportMaxIdleConns         int    `mapstructure:"HTTP_TRANSPORT_MAX_IDLE_CONNS"`          // HTTP connection settings
	HttpTransportMaxIdleConnsPerHost  int    `mapstructure:"HTTP_TRANSPORT_MAX_IDLE_CONNS_PER_HOST"` // HTTP connection settings
	SolrHost                          string `mapstructure:"SOLR_HOST"`                              // Solr settings
	SolrUpdatePath                    string `mapstructure:"SOLR_UPDATE_PATH"`                       // Solr settings
	SolrReadPath                      string `mapstructure:"SOLR_Read_PATH"`                         // Solr settings
	LiveSolrHost                      string `mapstructure:"LIVE_SOLR_HOST"`                         // Solr settings
	LiveSolrReadPath                  string `mapstructure:"LIVE_SOLR_Read_PATH"`                    // Solr settings
	PurlURI                           string `mapstructure:"PURL_PATTERN"`
	PurlURIOcrd                       string `mapstructure:"PURL_PATTERN_OCRD"`

	// TODO for NLH
	OpacURI                     string   `mapstructure:"OPAC_URI"` // Catalogue
	KalliopeURI                 string   `mapstructure:"KALLIOPE_URI"`
	KalliopeSruPath             string   `mapstructure:"KALLIOPE_SRU_PATH"`
	KalliopePath                string   `mapstructure:"KALLIOPE_PATH"`
	UnapiURI                    string   `mapstructure:"UNAPI_URI"`
	UnapiPath                   string   `mapstructure:"UNAPI_PATH"`
	LogIndex                    string   `mapstructure:"LOG_INDEX"` // Indexes
	PhysIndex                   string   `mapstructure:"PHYS_INDEX"`
	ContributionTypes           []string `mapstructure:"CONTRIBUTION_TYPES"`
	MultivolumeworkTypes        []string `mapstructure:"MULTIVOLUMEWORK_TYPES"`
	DatebasedProducts           []string `mapstructure:"DATEBASED_PRODUCTS"`
	MultiPeriodicalProducts     []string `mapstructure:"MULTI_PERIODICAL_PRODUCTS"`
	ElasticsearchHost           string   `mapstructure:"ELASTICSEARCH_HOST"`
	OldIndexDatesFile           string   `mapstructure:"OLD_INDEX_DATES_FILE"`
	HostBaseURL                 string   `mapstructure:"HOST_BASE_URL"`
	OlahdsMetsKeyPattern        string   `mapstructure:"OLAHDS_METS_KEY_PATTERN"`
	OlahdsFileKeyPattern        string   `mapstructure:"OLAHDS_FILE_KEY_PATTERN"`
	OlahdsDefaulImgFilegrp      string   `mapstructure:"OLAHDS_DEFAUL_IMG_FILEGRP"`
	OlahdsDefaulFulltextFilegrp string   `mapstructure:"OLAHDS_DEFAUL_FULLTEXT_FILEGRP"`
	OlahdsDefaulFtype           string   `mapstructure:"OLAHDS_DEFAUL_FTYPE"`
	// ValidationRun determines whether validation or indexing is running
	ValidationRun bool   `mapstructure:"VALIDATION_RUN"`
	LogLevel      string `mapstructure:"LOG_LEVEL"`    // Logging
	LogFile       string `mapstructure:"LOG_FILE"`     // Logging
	LogPath       string `mapstructure:"LOG_PATH"`     // Logging
	LogESIndex    string `mapstructure:"LOG_ES_INDEX"` // Logging

	// --- iiif part (for indexer)
	IiifBaseURLGDZ      string `mapstructure:"Iiif_Base_Url_GDZ"`
	IiifBaseURLDigizeit string `mapstructure:"Iiif_Base_Url_Digizeit"`
	IiifBaseURL         string `mapstructure:"Iiif_Base_Url"`
	IiifBaseURLNLH      string `mapstructure:"Iiif_Base_Url_NLH"`

	IiifManifestURLGDZ      string `mapstructure:"Iiif_Manifest_URL_GDZ"`
	IiifManifestURLDigizeit string `mapstructure:"Iiif_Manifest_URL_Digizeit"`
	IiifManifestURL         string `mapstructure:"Iiif_Manifest_URL"`
	IiifManifestURLNLH      string `mapstructure:"Iiif_Manifest_URL_NLH"`

	IiifManifestURLRelatedGDZ      string `mapstructure:"Iiif_Manifest_URL_Related_GDZ"`
	IiifManifestURLRelatedDigizeit string `mapstructure:"Iiif_Manifest_URL_Related_Digizeit"`
	IiifManifestURLRelated         string `mapstructure:"Iiif_Manifest_URL_Related"`
	IiifManifestURLRelatedNLH      string `mapstructure:"Iiif_Manifest_URL_Related_NLH"`

	IiifSearchByIdURLGDZ      string `mapstructure:"Iiif_Search_By_Id_URL_GDZ"`
	IiifSearchByIdURLDigizeit string `mapstructure:"Iiif_Search_By_Id_URL_Digizeit"`
	IiifSearchByIdURL         string `mapstructure:"Iiif_Search_By_Id_URL"`
	IiifSearchByIdURLNLH      string `mapstructure:"Iiif_Search_By_Id_URL_NLH"`

	IiifImageConverterURLGDZ      string `mapstructure:"Iiif_Image_Converter_URL_GDZ"`
	IiifImageConverterURLDigizeit string `mapstructure:"Iiif_Image_Converter_URL_Digizeit"`
	IiifImageConverterURL         string `mapstructure:"Iiif_Image_Converter_URL"`
	IiifImageConverterURLNLH      string `mapstructure:"Iiif_Image_Converter_URL_NLH"`

	IiifImageConverterServiceURL string `mapstructure:"Iiif_Image_Converter_Service_URL"`

	IiifThumbTemplateGDZ      string `mapstructure:"Iiif_Thumb_Template_GDZ"`
	IiifThumbTemplateDigizeit string `mapstructure:"Iiif_Thumb_Template_Digizeit"`
	IiifThumbTemplate         string `mapstructure:"Iiif_Thumb_Template"`
	IiifThumbTemplateNLH      string `mapstructure:"Iiif_Thumb_Template_NLH"`

	IiifThumbIDTemplateGDZ      string `mapstructure:"Iiif_Thumb_ID_Template_GDZ"`
	IiifThumbIDTemplateDigizeit string `mapstructure:"Iiif_Thumb_ID_Template_Digizeit"`
	IiifThumbIDTemplate         string `mapstructure:"Iiif_Thumb_ID_Template"`
	IiifThumbIDTemplateNLH      string `mapstructure:"Iiif_Thumb_ID_Template_NLH"`

	IiifViewerResolvingGDZ      string `mapstructure:"Iiif_Viewer_Resolving_GDZ"`
	IiifViewerResolvingDigizeit string `mapstructure:"Iiif_Viewer_Resolving_Digizeit"`
	IiifViewerResolving         string `mapstructure:"Iiif_Viewer_Resolving"`
	IiifViewerResolvingNLH      string `mapstructure:"Iiif_Viewer_Resolving_NLH"`

	IiifViewerLogResolvingGDZ      string `mapstructure:"Iiif_Viewer_Log_Resolving_GDZ"`
	IiifViewerLogResolvingDigizeit string `mapstructure:"Iiif_Viewer_Log_Resolving_Digizeit"`
	IiifViewerLogResolving         string `mapstructure:"Iiif_Viewer_Log_Resolving"`
	IiifViewerLogResolvingNLH      string `mapstructure:"Iiif_Viewer_Log_Resolving_NLH"`

	IiifViewerSequenceResolvingURLGDZ      string `mapstructure:"Iiif_Viewer_Sequence_Resolving_URL_GDZ"`
	IiifViewerSequenceResolvingURLDigizeit string `mapstructure:"Iiif_Viewer_Sequence_Resolving_URL_Digizeit"`
	IiifViewerSequenceResolvingURL         string `mapstructure:"Iiif_Viewer_Sequence_Resolving_URL"`
	IiifViewerSequenceResolvingURLNLH      string `mapstructure:"Iiif_Viewer_Sequence_Resolving_URL_NLH"`

	IiifViewerResolvingURLGDZ      string `mapstructure:"Iiif_Viewer_Resolving_URL_GDZ"`
	IiifViewerResolvingURLDigizeit string `mapstructure:"Iiif_Viewer_Resolving_URL_Digizeit"`
	IiifViewerResolvingURL         string `mapstructure:"Iiif_Viewer_Resolving_URL"`
	IiifViewerResolvingURLNLH      string `mapstructure:"Iiif_Viewer_Resolving_URL_NLH"`

	IiifViewerCanvasResolvingURLGDZ      string `mapstructure:"Iiif_Viewer_Canvas_Resolving_URL_GDZ"`
	IiifViewerCanvasResolvingURLDigizeit string `mapstructure:"Iiif_Viewer_Canvas_Resolving_URL_Digizeit"`
	IiifViewerCanvasResolvingURL         string `mapstructure:"Iiif_Viewer_Canvas_Resolving_URL"`
	IiifViewerCanvasResolvingURLNLH      string `mapstructure:"Iiif_Viewer_Canvas_Resolving_URL_NLH"`

	IiifViewerAnnotationResolvingURLGDZ      string `mapstructure:"Iiif_Viewer_Annotation_Resolving_URL_GDZ"`
	IiifViewerAnnotationResolvingURLDigizeit string `mapstructure:"Iiif_Viewer_Annotation_Resolving_URL_Digizeit"`
	IiifViewerAnnotationResolvingURL         string `mapstructure:"Iiif_Viewer_Annotation_Resolving_URL"`
	IiifViewerAnnotationResolvingURLNLH      string `mapstructure:"Iiif_Viewer_Annotation_Resolving_URL_NLH"`

	IiifImageContext        string `mapstructure:"Iiif_Image_Context"`
	IiifImageProfile        string `mapstructure:"Iiif_Image_Profile"`
	IiifPresentationContext string `mapstructure:"Iiif_Presentation_Context"`
	IiifPresentationProfile string `mapstructure:"Iiif_Presentation_Profile"`

	IiifImageType           string `mapstructure:"Iiif_Image_Type"`
	IiifManifestType        string `mapstructure:"Iiif_Manifest_Type"`
	IiifSequenceType        string `mapstructure:"Iiif_Sequence_Type"`
	IiifCanvasType          string `mapstructure:"Iiif_Canvas_Type"`
	IiifAnnotationType      string `mapstructure:"Iiif_Annotation_Type"`
	IiifAnnotationBodyType  string `mapstructure:"Iiif_Annotation_Body_Type"`
	IiifImageMotivationType string `mapstructure:"Iiif_Image_Motivation_Type"`
	IiifStructureType       string `mapstructure:"Iiif_Structure_Type"`
	IiifImageFormat         string `mapstructure:"Iiif_Image_Format"`
	IiifImageMimetype       string `mapstructure:"Iiif_Image_Mimetype"`
	IiifThumbnailFormat     string `mapstructure:"Iiif_Thumbnail_Format"`

	CcBy40LicenseLink string `mapstructure:"CC_BY_40_License_Link"`

	LogoGDZ      string `mapstructure:"Logo_GDZ"`
	LogoDigizeit string `mapstructure:"Logo_Digizeit"`
	LogoOcrd     string `mapstructure:"Logo_Ocrd"`
	LogoNLH      string `mapstructure:"Logo_NLH"`

	MetsDownloadURLGDZ      string `mapstructure:"Mets_Download_URL_GDZ"`
	MetsDownloadURLDigizeit string `mapstructure:"Mets_Download_URL_Digizeit"`
	MetsDownloadURL         string `mapstructure:"Mets_Download_URL"`
	MetsDownloadURLNLH      string `mapstructure:"Mets_Download_URL_NLH"`

	MetsFormat string `mapstructure:"Mets_Format"`

	PdfDownloadURLGDZ      string `mapstructure:"Pdf_Download_URLG_DZ"`
	PdfDownloadURLDigizeit string `mapstructure:"Pdf_Download_URL_Digizeit"`
	PdfDownloadURL         string `mapstructure:"Pdf_Download_URL"`
	PdfDownloadURLNLH      string `mapstructure:"Pdf_Download_URL_NLH"`

	PdfDownloadLabel string `mapstructure:"Pdf_Download_Label"`
	PdfFormat        string `mapstructure:"Pdf_Format"`

	DfgViewerBaseURL string `mapstructure:"Dfg_Viewer_Base_URL"`
	DfgViewerLabel   string `mapstructure:"Dfg_Viewer_Label"`
	DfgViewerFormat  string `mapstructure:"Dfg_Viewer_Format"`

	MetsProfile string `mapstructure:"Mets_Profile"`

	// --- export part (for indexer)
	BibDownloadURLGDZ      string `mapstructure:"Bib_Download_URL_GDZ"`
	BibDownloadURLDigizeit string `mapstructure:"Bib_Download_URL_Digizeit"`
	BibDownloadURL         string `mapstructure:"Bib_Download_URL"`
	BibProfile             string `mapstructure:"Bib_Profile"`
	BibFormat              string `mapstructure:"Bib_Format"`

	RisDownloadURLGDZ      string `mapstructure:"Ris_Download_URL_GDZ"`
	RisDownloadURLDigizeit string `mapstructure:"Ris_Download_URL_Digizeit"`
	RisDownloadURL         string `mapstructure:"Ris_Download_URL"`
	RisProfile             string `mapstructure:"Ris_Profile"`
	RisFormat              string `mapstructure:"Ris_Format"`

	EndnoteDownloadURLGDZ      string `mapstructure:"Endnote_Download_URL_GDZ"`
	EndnoteDownloadURLDigizeit string `mapstructure:"Endnote_Download_URL_Digizeit"`
	EndnoteDownloadURL         string `mapstructure:"Endnote_Download_URL"`
	EndnoteProfile             string `mapstructure:"Endnote_Profile"`
	EndnoteFormat              string `mapstructure:"Endnote_Format"`
}

var (
	c, d    *string
	e, v    *bool
	verbose bool
	config  Config
	log     *logrus.Logger
)

func init() {

	log = logrus.New()

	var err error

	parser := argparse.NewParser("indexer", "Indexing records from job queue")
	c = parser.String("c", "config", &argparse.Options{Required: false, Help: "Path to configuration file"})
	d = parser.String("d", "config-dir", &argparse.Options{Required: false, Help: "Path to configuration directory"})
	e = parser.Flag("e", "env", &argparse.Options{Required: false, Help: "Configure using environment"})
	v = parser.Flag("v", "verbose", &argparse.Options{Required: false, Help: "Enable debug mode"})

	err = parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
		os.Exit(1)
	}

	if *v {
		verbose = true
		log.Info("Enabled verbose mode")
	}

	viper.SetConfigType("env")

	if *e {
		viper.AutomaticEnv()
	}

	if *c != "" {
		viper.SetConfigFile(*c)
	} else if *d != "" {
		viper.AddConfigPath(*d)
		viper.SetConfigName(buildInConfigCommonsEnv)
	}

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			log.Fatalf("Config file not found, due to %v", err)
		} else {
			// Config file was found but another error was produced
			log.Fatalf("Config file was found but another error was produced, due to %s", err.Error())
		}
	}

	// merge configs
	if *c == "" {
		viper.SetConfigName(buildInConfigCommonsStaticEnv)
		viper.MergeInConfig()

		viper.SetConfigName(buildInConfigDockerEnv)
		viper.MergeInConfig()

		viper.SetConfigName(buildInConfigAppEnv)
		viper.MergeInConfig()

		viper.SetConfigName(buildInConfigAppStaticEnv)
		viper.MergeInConfig()

		viper.SetConfigName(buildInConfigIiifEnv)
		viper.MergeInConfig()
	}

	err = viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("Could not Unmarshal config file, due to %v", err)
	}

	var logLevel logrus.Level

	if verbose {
		config.LogLevel = "TRACE"
	}
	logLevel, err = logrus.ParseLevel(config.LogLevel)
	if err != nil {
		log.Error("could not parse the log level: ", err)
		logLevel = logrus.InfoLevel
	}

	if config.TestRun && config.TestRunLimit == 0 {
		config.TestRun = false
		log.Infof("Disabled Testrun, since limit was set to 0")
	}

	log.SetLevel(logLevel)
	log.Infof("Set Log level to %s", logLevel)

	json.MarshalIndent(config, " ", "  ")

	log.SetFormatter(
		&logrus.JSONFormatter{
			TimestampFormat:   "",
			DisableTimestamp:  false,
			DisableHTMLEscape: false,
			DataKey:           "",
			FieldMap: logrus.FieldMap{
				logrus.FieldKeyTime:  "@timestamp",
				logrus.FieldKeyLevel: "level_name",
				logrus.FieldKeyMsg:   "Message",
				logrus.FieldKeyFunc:  "Func",
				logrus.FieldKeyFile:  "File"},
			//CallerPrettyfier: func(*runtime.Frame) (string, string) {},
			PrettyPrint: false,
		},
	)

	log.SetOutput(os.Stdout)
	log.SetReportCaller(true)

	//log.Infof("ES HOST: '%s', Index: '%s'", config.ElasticsearchHost, config.LogESIndex)
	if config.ElasticsearchHost != "" && config.LogESIndex != "" {
		client, err := elastic.NewClient(elastic.SetURL(config.ElasticsearchHost), elastic.SetSniff(false))
		if err != nil {
			log.Panic(err)
		}
		hook, err := elogrus.NewAsyncElasticHook(client, config.ElasticsearchHost, logLevel, config.LogESIndex)
		if err != nil {
			log.Panic(err)
		}
		log.Hooks.Add(hook)
	}

	var cstZone = time.FixedZone("CST", 2*3600) // East 8 District
	time.Local = cstZone
}

func LoadConfig() (Config, logrus.Logger) {
	return config, *log
}
