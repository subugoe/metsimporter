package fulltext

import (
	"encoding/xml"
	"fmt"
	"io"

	"regexp"
	"strings"

	//"log"
	log "github.com/sirupsen/logrus"

	"github.com/PuerkitoBio/goquery"

	"gitlab.gwdg.de/subugoe/metsimporter/indexer/helper"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/types"
)

// XML fulltext
type XML struct {
	XMLName xml.Name `xml:"TEI.2"`
	Word    []string `xml:"text>body>p"` // `xml:"mdWrap>xmlData>mods>name"`
}

// Word ...
type Word struct {
	Content string `xml:",chardata"`
}

// TXT1 ... nlh_tda1, nlh-tda2
type TXT1 struct {
	XMLName  xml.Name          `xml:"articles"`
	Articles []TXT1ArticleInfo `xml:"artInfo"`
}

// TXT1ArticleInfo ...
type TXT1ArticleInfo struct {
	XMLName     xml.Name    `xml:"artInfo"`
	ID          string      `xml:"id,attr"`
	ProductLink string      `xml:"ProductLink"`
	OCRText     TXT1OCRText `xml:"ocrText"`
}

// TXT1OCRText ...
type TXT1OCRText struct {
	XMLName xml.Name `xml:"ocrText"`
	Value   string   `xml:",chardata"`
}

// TXT2 ... nlh-ecc
type TXT2 struct {
	XMLName   xml.Name   `xml:"page"`
	TXTHeader TXT2Header `xml:"pageInfo"`
	Text      TXT2Text   `xml:"pageContent"`
}

// TXT2Header ...
type TXT2Header struct {
	XMLName    xml.Name `xml:"pageInfo"`
	PageID     string   `xml:"pageID"`
	RecordID   string   `xml:"recordID"`
	SourcePage string   `xml:"sourcePage"`
	OCR        string   `xml:"ocr"`
	ImageLink  string   `xml:"imageLink"`
}

// TXT2Text ...
type TXT2Text struct {
	XMLName    xml.Name        `xml:"pageContent"`
	Paragraphs []TXT2Paragraph `xml:"p"`
}

// TXT2Paragraph ...
type TXT2Paragraph struct {
	XMLName xml.Name   `xml:"p"`
	ID      string     `xml:"id,attr"`
	Words   []TXT2Word `xml:"w"`
}

// TXT2Word ...
type TXT2Word struct {
	XMLName xml.Name `xml:"wd"`
	Pos     string   `xml:"pos,attr"`
	Value   string   `xml:",chardata"`
}

// TXT3 ... nlh-tls
type TXT3 struct {
	XMLName  xml.Name      `xml:"page"`
	PageID   string        `xml:"pageid"`
	Articles []TXT3Article `xml:"article"`
}

// TXT3Article ...
type TXT3Article struct {
	XMLName xml.Name `xml:"article"`
	Type    string   `xml:"type,attr"`
	ID      string   `xml:"id"`
	Text    TXT3Text `xml:"text"`
}

// TXT3Text ...
type TXT3Text struct {
	XMLName    xml.Name    `xml:"text"`
	TitleBlock TXT3Block   `xml:"text.title"`
	TextBlocks []TXT3Block `xml:"text.cr"`
}

// TXT3Block ...
type TXT3Block struct {
	Pg TXT3PG  `xml:"pg"`
	P  []TXT3P `xml:"p"`
}

// TXT3PG ...
type TXT3PG struct {
	XMLName xml.Name `xml:"pg"`
	Pgref   string   `xml:"pgref,attr"`
	Clipref string   `xml:"clipref,attr"`
	Pos     string   `xml:"pos,attr"`
}

// TXT3P ...
type TXT3P struct {
	XMLName xml.Name `xml:"p"`
	Type    string   `xml:"type,attr"`
	WDs     []TXT3WD `xml:"wd"`
}

// TXT3WD ...
type TXT3WD struct {
	XMLName xml.Name `xml:"wd"`
	Pos     string   `xml:"pos,attr"`
	Value   string   `xml:",chardata"`
}

// TXT3a ... nlh-ncn, nlh-bcn, nlh-bln
type TXT3a struct {
	XMLName xml.Name    `xml:"page"`
	PageID  string      `xml:"page-id"`
	IssueID string      `xml:"issue-id"`
	Texts   []TXT3aText `xml:"text"`
}

// TXT3aText ...
type TXT3aText struct {
	XMLName    xml.Name     `xml:"text"`
	TitleBlock TXT3aBlock   `xml:"text.title"`
	TextBlocks []TXT3aBlock `xml:"text.cr"`
}

// TXT3aBlock ...
type TXT3aBlock struct {
	Pg TXT3aPG  `xml:"pg"`
	P  []TXT3aP `xml:"p"`
}

// TXT3aPG ...
type TXT3aPG struct {
	XMLName xml.Name `xml:"pg"`
	Pgref   string   `xml:"pgref,attr"`
	Clipref string   `xml:"clipref,attr"`
	Pos     string   `xml:"pos,attr"`
}

// TXT3aP ...
type TXT3aP struct {
	XMLName xml.Name `xml:"p"`
	Type    string   `xml:"type,attr"`
	WDs     []TXT3WD `xml:"wd"`
}

// TXT3aWD ...
type TXT3aWD struct {
	XMLName xml.Name `xml:"wd"`
	Pos     string   `xml:"pos,attr"`
	Value   string   `xml:",chardata"`
}

// TXT3b ... nlh-bln
type TXT3b struct {
	XMLName    xml.Name     `xml:"page"`
	PageID     string       `xml:"page-id"`
	IssueID    string       `xml:"issue-id"`
	TextBlocks []TXT3aBlock `xml:"text.cr"`
}

// TXT4 ... nlh-ahn
type TXT4 struct {
	XMLName xml.Name   `xml:"articles"`
	Texts   []TXT3Text `xml:"text"`
}

// TXT5 ... nlh-nid
type TXT5 struct {
	XMLName xml.Name `xml:"IMAGE"`
	Name    string   `xml:"NAME,attr"`
	WDs     []TXT5WD `xml:"WD"`
}

// TXT5WD ...
type TXT5WD struct {
	XMLName xml.Name `xml:"WD"`
	Pos     string   `xml:"POS,attr"`
	Value   string   `xml:",chardata"`
}

// TEI2 ... nlh-eai1, nlh-eai2
type TEI2 struct {
	XMLName   xml.Name   `xml:"TEI.2"`
	TEIHeader TEI2Header `xml:"teiHeader"`
	Text      TEI2Text   `xml:"text"`
}

// TEI2Header ..
type TEI2Header struct {
	XMLName xml.Name `xml:"teiHeader"`
}

// TEI2Text ..
type TEI2Text struct {
	XMLName xml.Name `xml:"text"`
	Body    TEI2Body `xml:"body"`
}

// TEI2Body ..
type TEI2Body struct {
	XMLName    xml.Name        `xml:"body"`
	Paragraphs []TEI2Paragraph `xml:"p"`
}

// TEI2Paragraph ..
type TEI2Paragraph struct {
	XMLName xml.Name   `xml:"p"`
	ID      string     `xml:"id,attr"`
	Style   string     `xml:"style,attr"`
	Words   []TEI2Word `xml:"w"`
}

// TEI2Word ..
type TEI2Word struct {
	XMLName xml.Name `xml:"w"`
	Style   string   `xml:"style,attr"`
	Value   string   `xml:",chardata"`
}

// TEI2a ... gdz
type TEI2a struct {
	XMLName   xml.Name    `xml:"TEI.2"`
	TEIHeader TEI2aHeader `xml:"teiHeader"`
	Text      TEI2aText   `xml:"text"`
}

// TEI2aHeader ..
type TEI2aHeader struct {
	XMLName xml.Name `xml:"teiHeader"`
}

// TEI2aText ..
type TEI2aText struct {
	XMLName xml.Name  `xml:"text"`
	Body    TEI2aBody `xml:"body"`
}

// TEI2aBody ..
type TEI2aBody struct {
	XMLName    xml.Name         `xml:"body"`
	Paragraphs []TEI2aParagraph `xml:"p"`
}

// TEI2aParagraph ..
type TEI2aParagraph struct {
	XMLName xml.Name    `xml:"p"`
	ID      string      `xml:"id,attr"`
	Words   []TEI2aWord `xml:"w"`
}

// TEI2aWord ..
type TEI2aWord struct {
	XMLName  xml.Name `xml:"w"`
	Function string   `xml:"function,attr"`
	Value    string   `xml:",chardata"`
}

// PAGEXML1 ... gdz
type PAGEXML1 struct {
	XMLName  xml.Name         `xml:"PcGts"`
	Metadata PAGEXML1Metadata `xml:"Metadata"`
	Page     PAGEXML1Page     `xml:"Page"` // array?
}

// PAGEXML1Metadata ..
type PAGEXML1Metadata struct {
	XMLName    xml.Name `xml:"Metadata"`
	Creator    string   `xml:"Creator"`
	Created    string   `xml:"Created"`
	LastChange string   `xml:"LastChange"`
}

// PAGEXML1Page ..
type PAGEXML1Page struct {
	XMLName          xml.Name `xml:"Page"`
	ImageFilename    string   `xml:"imageFilename,attr"`
	ImageWidth       string   `xml:"imageWidth,attr"`
	ImageHeight      string   `xml:"imageHeight,attr"`
	ImageXResolution string   `xml:"imageXResolution,attr"`
	ImageYResolution string   `xml:"imageYResolution,attr"`
	Type             string   `xml:"type,attr"`

	Border       PAGEXML1Border       `xml:"Border"`
	ReadingOrder PAGEXML1ReadingOrder `xml:"ReadingOrder"`
	Relations    PAGEXML1Relations    `xml:"Relations"`
	TextRegion   PAGEXML1TextRegion   `xml:"TextRegion"`
}

// --- Border

// PAGEXML1Border ..
type PAGEXML1Border struct {
	XMLName xml.Name       `xml:"Border"`
	Coords  PAGEXML1Coords `xml:"Coords"` // array?
}

// PAGEXML1Coords ..
type PAGEXML1Coords struct {
	XMLName xml.Name `xml:"Coords"`
	Points  string   `xml:"points,attr"`
}

// --- ReadingOrder

// PAGEXML1ReadingOrder ..
type PAGEXML1ReadingOrder struct {
	XMLName xml.Name `xml:"ReadingOrder"`

	OrderedGroup PAGEXML1OrderedGroup `xml:"OrderedGroup"` // array?
}

// PAGEXML1OrderedGroup ..
type PAGEXML1OrderedGroup struct {
	XMLName xml.Name `xml:"OrderedGroup"`
	ID      string   `xml:"id,attr"`
	Caption string   `xml:"caption,attr"`

	RegionRefIndexed []PAGEXML1RegionRefIndexed `xml:"RegionRefIndexed"` // array?
}

// PAGEXML1RegionRefIndexed ..
type PAGEXML1RegionRefIndexed struct {
	XMLName   xml.Name `xml:"RegionRefIndexed"`
	Index     string   `xml:"index,attr"`
	RegionRef string   `xml:"regionRef,attr"`
}

// --- Relations

// PAGEXML1Relations ..
type PAGEXML1Relations struct {
	XMLName xml.Name `xml:"Relations"`

	Relation PAGEXML1Relation `xml:"Relation"` // array?
}

// PAGEXML1Relation ..
type PAGEXML1Relation struct {
	XMLName xml.Name `xml:"Relation"`
	ID      string   `xml:"id,attr"`
	Type    string   `xml:"type,attr"`

	SourceRegionRef []PAGEXML1SourceRegionRef `xml:"SourceRegionRef"`
	TargetRegionRef []PAGEXML1TargetRegionRef `xml:"TargetRegionRef"`
}

// PAGEXML1SourceRegionRef ..
type PAGEXML1SourceRegionRef struct {
	XMLName   xml.Name `xml:"SourceRegionRef"`
	RegionRef string   `xml:"regionRef,attr"`
}

// PAGEXML1TargetRegionRef ..
type PAGEXML1TargetRegionRef struct {
	XMLName   xml.Name `xml:"TargetRegionRef"`
	RegionRef string   `xml:"regionRef,attr"`
}

// --- TextRegion

// PAGEXML1TextRegion ..
type PAGEXML1TextRegion struct {
	XMLName           xml.Name `xml:"TextRegion"`
	ID                string   `xml:"id,attr"`
	Type              string   `xml:"type,attr"`
	PrimaryLanguage   string   `xml:"primaryLanguage,attr"`
	SecondaryLanguage string   `xml:"secondaryLanguage,attr"`

	Coords    PAGEXML1Coords      `xml:"Coords"`    // array?
	TextLine  []PAGEXML1TextLine  `xml:"TextLine"`  // array?
	TextEquiv []PAGEXML1TextEquiv `xml:"TextEquiv"` // array?
}

// PAGEXML1TextLine ..
//
// index this Coords + TextEquiv to index lines
type PAGEXML1TextLine struct {
	XMLName         xml.Name `xml:"TextLine"`
	ID              string   `xml:"id,attr"`
	PrimaryLanguage string   `xml:"primaryLanguage,attr"`

	Coords    PAGEXML1Coords    `xml:"Coords"`    // array?
	TextEquiv PAGEXML1TextEquiv `xml:"TextEquiv"` // array?
	Baseline  PAGEXML1Baseline  `xml:"Baseline"`  // array?

	Word PAGEXML1Word `xml:"Word"` // array?
}

// PAGEXML1Baseline ..
type PAGEXML1Baseline struct {
	XMLName xml.Name `xml:"Baseline"`
	Points  string   `xml:"points,attr"`
}

// PAGEXML1Word ..
//
// index this Coords + TextQuiv to index words
type PAGEXML1Word struct {
	XMLName  xml.Name `xml:"Word"`
	ID       string   `xml:"id,attr"`
	Language string   `xml:"language,attr"`

	Coords    PAGEXML1Coords    `xml:"Coords"`    // array?
	TextEquiv PAGEXML1TextEquiv `xml:"TextEquiv"` // array?
}

// PAGEXML1TextEquiv ..
type PAGEXML1TextEquiv struct {
	XMLName xml.Name `xml:"TextEquiv"`

	Unicode PAGEXML1Unicode `xml:"Unicode"` // array?
}

// PAGEXML1Unicode ..
type PAGEXML1Unicode struct {
	XMLName xml.Name `xml:"Unicode"`

	Value string `xml:",chardata"` // array?
}

// PAGEXML1Paragraph ..
type PAGEXML1Paragraph struct {
	XMLName xml.Name       `xml:"p"`
	ID      string         `xml:"id,attr"`
	Words   []PAGEXML1Word `xml:"w"`
}

func getContentFromPAGEXML1(decoder *xml.Decoder, key string) string {

	//var coords []string
	var texts []string

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Printf("ERROR Failed to read token in %s, due to %s", key, tokenErr.Error())
				return ""
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "PcGts" {
				page := &PAGEXML1{}
				decErr := decoder.DecodeElement(page, &startElem)
				if decErr != nil {
					log.Printf("ERROR Could not decode PcGts page in %s, due to %s", key, decErr.Error())
					continue
				}

				text := ""
				//coord := ""
				for _, line := range page.Page.TextRegion.TextLine {

					//coords = append(coords, line.Coords.Points)
					text = types.Join(line.TextEquiv.Unicode.Value, "\n")
					texts = append(texts, text)

				}
			}
		}

	}

	str := ""
	for _, t := range texts {
		str = types.Join(str, t)
	}

	return str
}

func getContentFromTXT4(decoder *xml.Decoder, key string) string {

	var articles []string

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Printf("ERROR Failed to read token in %s, due to %s", key, tokenErr.Error())
				return ""
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "articles" {
				artcls := &TXT4{}
				decErr := decoder.DecodeElement(artcls, &startElem)
				if decErr != nil {
					log.Printf("ERROR Could not decode TXT4 articles in %s, due to %s", key, decErr.Error())
					continue
				}

				for _, textBlock := range artcls.Texts {

					title := ""

					for _, p := range textBlock.TitleBlock.P {
						for _, word := range p.WDs {
							title = types.Join(title, word.Value, " ")
						}
						title = types.Join(title, "\n")
					}
					articles = append(articles, title)

					text := ""
					for _, textBlock := range textBlock.TextBlocks {
						for _, p := range textBlock.P {
							for _, word := range p.WDs {
								text = types.Join(text, word.Value, " ")
							}
							text = types.Join(text, "\n")
						}
					}
					articles = append(articles, text)
				}

			}
		}

	}

	str := ""
	for _, article := range articles {
		str = types.Join(str, article, "\n")
	}

	return str
}

func getContentFromTXT5(decoder *xml.Decoder, key string) string {

	var texts []string

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Printf("ERROR Failed to read token in %s, due to %s", key, tokenErr.Error())
				return ""
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "IMAGE" {
				image := &TXT5{}
				decErr := decoder.DecodeElement(image, &startElem)
				if decErr != nil {
					log.Printf("ERROR Could not decode TXT5 articles in %s, due to %s", key, decErr.Error())
					continue
				}

				text := ""
				for _, wd := range image.WDs {
					text = types.Join(text, wd.Value, " ")
				}
				text = types.Join(text, "\n")
				texts = append(texts, text)

			}
		}

	}

	str := ""
	for _, t := range texts {
		str = types.Join(str, t, "\n")
	}

	return str
}

func getContentFromTEI2(decoder *xml.Decoder, key string) string {

	var paragraph []string
	var line string

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Printf("ERROR Failed to read token in %s, due to %s", key, tokenErr.Error())
				return ""
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "p" {
				//
			} else if startElem.Name.Local == "w" {
				word := &TEI2Word{}
				decErr := decoder.DecodeElement(word, &startElem)
				if decErr != nil {
					log.Printf("ERROR Could not decode TEI2 word in %s, due to %s", key, decErr.Error())
					continue
				}
				line = types.Join(line, word.Value, " ")
			}
		case xml.EndElement:
			if startElem.Name.Local == "pb" {
				//
			} else if startElem.Name.Local == "lb" {
				line = strings.TrimSpace(line)
				paragraph = append(paragraph, line)
				line = ""
			}
		}
	}

	str := ""
	for _, line := range paragraph {
		space := regexp.MustCompile(`\s+`)
		l := space.ReplaceAllString(line, " ")
		l = strings.ReplaceAll(l, " ,", ", ")
		l = strings.ReplaceAll(l, " .", ". ")
		l = strings.ReplaceAll(l, " ?", "? ")
		l = strings.ReplaceAll(l, " !", "! ")
		str = types.Join(str, l, "\n")
	}
	return str
}

func getContentFromTEI2a(decoder *xml.Decoder, key string) string {

	var paragraph []string
	var line string

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Printf("ERROR Failed to read token in %s, due to %s", key, tokenErr.Error())
				return ""
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "p" {
				//
			} else if startElem.Name.Local == "w" {
				word := &TEI2aWord{}
				decErr := decoder.DecodeElement(word, &startElem)
				if decErr != nil {
					log.Printf("ERROR Could not decode TEI2a word in %s, due to %s", key, decErr.Error())
					continue
				}

				line = types.Join(line, word.Value, " ")

			}
		case xml.EndElement:
			if startElem.Name.Local == "pb" {
				//
			} else if startElem.Name.Local == "lb" {
				//
			} else if startElem.Name.Local == "w" {
				//
			} else if startElem.Name.Local == "p" {
				line = strings.TrimSpace(line)
				paragraph = append(paragraph, line)
				line = ""
			}
		}
	}

	str := ""
	for _, line := range paragraph {
		space := regexp.MustCompile(`\s+`)
		l := space.ReplaceAllString(line, " ")
		l = strings.ReplaceAll(l, " ,", ", ")
		l = strings.ReplaceAll(l, " .", ". ")
		l = strings.ReplaceAll(l, " ?", "? ")
		l = strings.ReplaceAll(l, " !", "! ")
		str = types.Join(str, l, "\n")
	}
	return str
}

func getContentFromTXT1(decoder *xml.Decoder, key string) string {

	var articles []string

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Printf("ERROR Failed to read token in %s, due to %s", key, tokenErr.Error())
				return ""
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "articles" {
				artcls := &TXT1{}
				decErr := decoder.DecodeElement(artcls, &startElem)
				if decErr != nil {
					log.Printf("ERROR Could not decode TXT1 articles in %s, due to %s", key, decErr.Error())
					continue
				}

				for _, article := range artcls.Articles {
					articles = append(articles, article.OCRText.Value)
				}
			}
		}

	}

	str := ""
	for _, article := range articles {
		str = types.Join(str, article, "\n")
	}

	return str
}

func getContentFromTXT2(decoder *xml.Decoder, key string) string {

	var paragraph []string
	var p string

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Printf("ERROR Failed to read token in %s, due to %s", key, tokenErr.Error())
				return ""
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "p" {
				//
			} else if startElem.Name.Local == "wd" {
				word := &TXT2Word{}
				decErr := decoder.DecodeElement(word, &startElem)
				if decErr != nil {
					log.Printf("ERROR Could not decode TXT2 word in %s, due to %s", key, decErr.Error())
					continue
				}

				p = types.Join(p, word.Value, " ")

			}
		case xml.EndElement:
			if startElem.Name.Local == "p" {
				p = strings.TrimSpace(p)
				paragraph = append(paragraph, p)
				p = ""
			}
		}
	}

	str := ""
	for _, p := range paragraph {
		space := regexp.MustCompile(`\s+`)
		pp := space.ReplaceAllString(p, " ")
		pp = strings.ReplaceAll(pp, " ,", ", ")
		pp = strings.ReplaceAll(pp, " .", ". ")
		pp = strings.ReplaceAll(pp, " ?", "? ")
		pp = strings.ReplaceAll(pp, " !", "! ")
		str = types.Join(str, pp, "\n")
	}

	return str
}

func getContentFromTXT3(decoder *xml.Decoder, key string) string {

	var articles []string

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Printf("ERROR Failed to read token in %s, due to %s", key, tokenErr.Error())
				return ""
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "page" {
				page := &TXT3{}
				decErr := decoder.DecodeElement(page, &startElem)
				if decErr != nil {
					log.Printf("ERROR Could not decode TXT3 page in %s, due to %s", key, decErr.Error())
					continue
				}

				for _, article := range page.Articles {

					title := ""
					for _, p := range article.Text.TitleBlock.P {
						for _, word := range p.WDs {
							title = types.Join(title, word.Value, " ")
						}
						title = types.Join(title, "\n")
					}
					articles = append(articles, title)
					text := ""
					for _, textBlock := range article.Text.TextBlocks {
						for _, p := range textBlock.P {
							for _, word := range p.WDs {
								text = types.Join(text, word.Value, " ")
							}
							text = types.Join(text, "\n")
						}
						text = types.Join(text, "\n")
					}
					articles = append(articles, text)
				}

			}
		}

	}

	str := ""
	for _, article := range articles {
		str = types.Join(str, article, "\n")
	}

	return str
}

func getContentFromTXT3a(decoder *xml.Decoder, key string) string {

	var texts []string

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Printf("ERROR Failed to read token in %s, due to %s", key, tokenErr.Error())
				return ""
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "page" {
				page := &TXT3a{}
				decErr := decoder.DecodeElement(page, &startElem)
				if decErr != nil {
					log.Printf("ERROR Could not decode TXT3a page in %s, due to %s", key, decErr.Error())
					continue
				}

				for _, text := range page.Texts {

					title := ""
					for _, p := range text.TitleBlock.P {
						for _, word := range p.WDs {
							title = types.Join(title, word.Value, " ")
						}
						title = types.Join(title, "\n")
					}

					texts = append(texts, title)

					content := ""
					for _, textBlock := range text.TextBlocks {
						for _, p := range textBlock.P {
							for _, word := range p.WDs {
								content = types.Join(content, word.Value, " ")
							}
							content = types.Join(content, "\n")
						}
						content = types.Join(content, "\n")
					}

					texts = append(texts, content)
				}

			}
		}

	}

	str := ""
	for _, text := range texts {
		str = types.Join(str, text, "\n")
	}

	return str
}

func getContentFromTXT3b(decoder *xml.Decoder, key string) string {

	var texts []string

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Printf("ERROR Failed to read token in %s, due to %s", key, tokenErr.Error())
				return ""
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "page" {
				page := &TXT3b{}
				decErr := decoder.DecodeElement(page, &startElem)
				if decErr != nil {
					log.Printf("ERROR Could not decode TXT3a page in %s, due to %s", key, decErr.Error())
					continue
				}

				for _, text := range page.TextBlocks {

					content := ""

					for _, p := range text.P {
						for _, word := range p.WDs {
							content = types.Join(content, word.Value, " ")
						}
						content = types.Join(content, "\n")
					}

					texts = append(texts, content)
				}

			}
		}

	}

	str := ""
	for _, text := range texts {
		str = types.Join(str, text, "\n")
	}

	return str
}

// GetFulltext ...
func GetFulltext(bucket string, key string, format string, ftype string, context string) (string, error) {

	// load Fulltext from S3 or FS

	var str string
	var err error

	// TODO product specific
	if context == "ocrd" {
		str, err = helper.GetFileFromOLAHDS(key)
	} else {
		str, err = helper.GetXMLFrom(bucket, key, context, "fulltext")
	}

	if err != nil {
		return "", fmt.Errorf("could not access fulltext, due to %s", err)
	}

	if ftype == "TXT" {

		return str, nil

	} else if ftype == "TEI_2" {
		reader := strings.NewReader(str)
		decoder := xml.NewDecoder(reader)

		str := getContentFromTEI2(decoder, key)
		return str, nil

	} else if ftype == "TEI_2a" {
		reader := strings.NewReader(str)
		decoder := xml.NewDecoder(reader)

		str := getContentFromTEI2a(decoder, key)
		return str, nil

	} else if ftype == "TXT_1" {
		reader := strings.NewReader(str)
		decoder := xml.NewDecoder(reader)

		str := getContentFromTXT1(decoder, key)
		return str, nil
	} else if ftype == "TXT_2" {
		reader := strings.NewReader(str)
		decoder := xml.NewDecoder(reader)

		str := getContentFromTXT2(decoder, key)
		return str, nil
	} else if ftype == "TXT_3" {
		reader := strings.NewReader(str)
		decoder := xml.NewDecoder(reader)

		str := getContentFromTXT3(decoder, key)
		return str, nil
	} else if ftype == "TXT_3a" {
		reader := strings.NewReader(str)
		decoder := xml.NewDecoder(reader)

		str := getContentFromTXT3a(decoder, key)
		return str, nil
	} else if ftype == "TXT_3b" {
		reader := strings.NewReader(str)
		decoder := xml.NewDecoder(reader)

		str := getContentFromTXT3b(decoder, key)
		return str, nil
	} else if ftype == "TXT_4" {
		reader := strings.NewReader(str)
		decoder := xml.NewDecoder(reader)

		str := getContentFromTXT4(decoder, key)
		return str, nil
	} else if ftype == "TXT_5" {
		reader := strings.NewReader(str)
		decoder := xml.NewDecoder(reader)

		str := getContentFromTXT5(decoder, key)
		return str, nil
	} else if ftype == "HTML" {
		reader := strings.NewReader(str)
		doc, _ := goquery.NewDocumentFromReader(reader)
		return doc.Find("body").Text(), nil
	} else if ftype == "PAGEXML_1" {
		reader := strings.NewReader(str)
		decoder := xml.NewDecoder(reader)

		str := getContentFromPAGEXML1(decoder, key)
		return str, nil
	} else {
		return "", fmt.Errorf("wrong fulltext format (or not configured) for record %s/%s", bucket, key)
	}

}
