package templating

import (
	"bytes"
	"embed"
	"fmt"
	"text/template"
)

var (
	//go:embed templates/*.tmpl
	f embed.FS
)

type Data struct {
	WorkID  string
	PageId  string
	Key     string
	Width1  float32
	Width2  float32
	Width3  float32
	Width4  float32
	Width5  float32
	Height1 float32
	Height2 float32
	Height3 float32
	Height4 float32
	Height5 float32
}

type ImageInfo struct {
	Depth       uint    `json:"depth"`
	Height      int32   `json:"height"`
	Width       int32   `json:"width"`
	Size        int32   `json:"size"`
	X           float64 `json:"x"`
	Y           float64 `json:"y"`
	Document    string  `json:"document"`
	Context     string  `json:"context"`
	Product     string  `json:"product"`
	Page        string  `json:"page"`
	InfoJsonKey string  `json:"infojsonkey"`
}

func Create(imageInfo ImageInfo) string {

	for {

		key := fmt.Sprintf("%s:%s:%s", imageInfo.Product, imageInfo.Document, imageInfo.Page)
		w := float32(imageInfo.Width)
		h := float32(imageInfo.Height)

		data := Data{
			WorkID:  imageInfo.Document,
			PageId:  imageInfo.Page,
			Key:     key,
			Width1:  w / 1.0,
			Width2:  w / 2.0,
			Width3:  w / 4.0,
			Width4:  w / 8.0,
			Width5:  w / 16.0,
			Height1: h / 1.0,
			Height2: h / 2.0,
			Height3: h / 4.0,
			Height4: h / 8.0,
			Height5: h / 16.0,
		}
		infojson := ProcessFile("templates/info.tmpl", data)
		return infojson
	}

}

// process applies the data structure 'vars' onto an already
// parsed template 't', and returns the resulting string.
func process(t *template.Template, vars interface{}) string {
	var tmplBytes bytes.Buffer

	err := t.Execute(&tmplBytes, vars)
	if err != nil {
		panic(err)
	}
	return tmplBytes.String()
}

// ProcessString parses the supplied string and compiles it using
// the given variables.
func ProcessString(str string, vars interface{}) string {
	tmpl, err := template.New("tmpl").Parse(str)

	if err != nil {
		panic(err)
	}
	return process(tmpl, vars)
}

// ProcessFile parses the supplied filename and compiles its template
// using the given variables.
func ProcessFile(fileName string, vars interface{}) string {

	tmpl, err := template.ParseFS(f, fileName)
	//tmpl, err := f.ParseFiles(fileName)

	if err != nil {
		panic(err)
	}
	return process(tmpl, vars)
}
