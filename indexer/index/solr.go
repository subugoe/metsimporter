package index

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"

	"math"
	"net/http"
	"sort"
	"strings"
	"time"

	"gitlab.gwdg.de/subugoe/metsimporter/indexer/helper"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/types"
)

var (
	writeToSolrJobs chan []byte
)

func init() {

	writeToSolrJobs = make(chan []byte, 60)
	for w := 1; w <= config.Concurrency; w++ {
		go writeToSolr(writeToSolrJobs)

	}
}

func writeToSolr(writeToSolrJobs <-chan []byte) {

	netClient := connectionPool.Get().(*http.Client)
	defer connectionPool.Put(netClient)

	url := config.SolrHost + config.SolrUpdatePath

	for writeToSolrJob := range writeToSolrJobs {

		attempts := 0
		var resp *http.Response
		var err error
		for {
			resp, err = netClient.Post(url, "application/json", bytes.NewBuffer(writeToSolrJob))
			attempts++
			if err != nil {
				time.Sleep(time.Duration(6*math.Pow(float64(attempts), 2)) * time.Second)
				if attempts > 5 {
					log.Errorf("Solr post request failed, due to %s", err.Error())
					return
				}
				continue
			}
			break
		}

		if resp.StatusCode > 300 {
			log.Errorf("could not push data to solr, due to %s", resp.Status)
		}

		resp.Body.Close()
	}
}

// ReadFromSolrNLH ...
func ReadFromSolrNLH(id string) (types.SolrDocNLH, error) {

	netClient := connectionPool.Get().(*http.Client)
	defer connectionPool.Put(netClient)

	url := config.SolrHost + fmt.Sprintf(config.SolrReadPath, id)

	resp, err := netClient.Get(url)
	if err != nil {
		return types.SolrDocNLH{}, fmt.Errorf("HTTP request to %s failed, due to %s", url, err.Error())
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return types.SolrDocNLH{}, fmt.Errorf("could not read solr response body, due to %s", err.Error())
	}
	_, _ = io.Copy(ioutil.Discard, resp.Body)

	var response types.SolrResponseNLH
	err = json.Unmarshal(body, &response)
	if err != nil {
		return types.SolrDocNLH{}, fmt.Errorf("could not unmarshal solr response, due to %s", err.Error())
	}

	if response.Response.NumFound == 1 {
		return response.Response.Docs[0], nil
	} else if response.Response.NumFound == 0 {
		return types.SolrDocNLH{}, fmt.Errorf("no index document found for %s", id)
	} else {
		return types.SolrDocNLH{}, fmt.Errorf("more than one index document exist for %s", id)
	}
}

// ReadFromSolrGDZ ...
func ReadFromSolrGDZ(id string) (types.SolrDocGDZ, error) {

	netClient := connectionPool.Get().(*http.Client)
	defer connectionPool.Put(netClient)

	url := config.SolrHost + fmt.Sprintf(config.SolrReadPath, id)

	resp, err := netClient.Get(url)
	if err != nil {
		return types.SolrDocGDZ{}, fmt.Errorf("HTTP request to %s failed, due to %s", url, err.Error())
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return types.SolrDocGDZ{}, fmt.Errorf("could not read solr response body, due to %s", err.Error())
	}
	_, _ = io.Copy(ioutil.Discard, resp.Body)

	var response types.SolrResponseGDZ
	err = json.Unmarshal(body, &response)
	if err != nil {
		return types.SolrDocGDZ{}, fmt.Errorf("could not unmarshal solr response, due to %s", err.Error())
	}

	if response.Response.NumFound == 1 {
		return response.Response.Docs[0], nil
	} else if response.Response.NumFound == 0 {
		return types.SolrDocGDZ{}, fmt.Errorf("no index document found for %s", id)
	} else {
		return types.SolrDocGDZ{}, fmt.Errorf("more than one index document exist for %s", id)
	}
}

// WriteToGDZIndex ...
func WriteToGDZIndex(esDoc types.ESDoc) {

	var solrDoc types.SolrDocGDZ
	var solrLogChildDocs []types.SolrLogChildDocGDZ
	var tPage int64

	firstLog := true

	var logLv []types.KV
	for k, v := range esDoc.ESLogMap {
		logLv = append(logLv, types.KV{
			Key:   k,
			Value: int(v.Index)})
	}

	sort.Slice(logLv, func(i, j int) bool {
		return logLv[i].Value < logLv[j].Value
	})

	for _, kv := range logLv {

		if firstLog {

			solrDoc.Bycreator = esDoc.ESLogMap[kv.Key].Bycreator
			solrDoc.Byperson = esDoc.ESLogMap[kv.Key].Byperson
			solrDoc.Bytitle = esDoc.ESLogMap[kv.Key].Bytitle

			for _, creator := range esDoc.ESLogMap[kv.Key].CreatorInfos {
				solrDoc.Creator = append(solrDoc.Creator, creator.Name)
				solrDoc.CreatorType = append(solrDoc.CreatorType, creator.Type)
				solrDoc.CreatorGNDURI = append(solrDoc.CreatorGNDURI, creator.GndURI)
				solrDoc.CreatorGNDNumber = append(solrDoc.CreatorGNDNumber, creator.GndNumber)
				solrDoc.CreatorRoleterm = append(solrDoc.CreatorRoleterm, creator.Roleterm)
				if creator.Type == "personal" {
					solrDoc.FacetCreatorPersonal = append(solrDoc.FacetCreatorPersonal, creator.Name)
				} else if creator.Type == "corporate" {
					solrDoc.FacetCreatorCorporate = append(solrDoc.FacetCreatorCorporate, creator.Name)
				}
			}

			for _, person := range esDoc.ESLogMap[kv.Key].PersonInfos {
				solrDoc.Person = append(solrDoc.Person, person.Name)
				solrDoc.PersonType = append(solrDoc.PersonType, person.Type)
				solrDoc.PersonGNDURI = append(solrDoc.PersonGNDURI, person.GndURI)
				solrDoc.PersonGNDNumber = append(solrDoc.PersonGNDNumber, person.GndNumber)
				solrDoc.PersonRoleterm = append(solrDoc.PersonRoleterm, person.Roleterm)
				if person.Type == "personal" {
					solrDoc.FacetPersonPersonal = append(solrDoc.FacetPersonPersonal, person.Name)
				} else if person.Type == "corporate" {
					solrDoc.FacetPersonCorporate = append(solrDoc.FacetPersonCorporate, person.Name)
				}
			}

			for _, curr := range esDoc.ESLogMap[kv.Key].Currentno {
				solrDoc.Currentno = curr
				break
			}
			solrDoc.Currentnosort = esDoc.ESLogMap[kv.Key].Currentnosort

			solrDoc.DateIndexed = esDoc.ESLogMap[kv.Key].DateIndexed
			solrDoc.DateModified = esDoc.ESLogMap[kv.Key].DateModified

			solrDoc.Dc = esDoc.ESLogMap[kv.Key].Dc

			solrDoc.Docstrct = esDoc.ESLogMap[kv.Key].Type   // monograph, volume
			solrDoc.Doctype = esDoc.ESLogMap[kv.Key].Doctype // monograph, volume

			solrDoc.ID = esDoc.ESLogMap[kv.Key].ID
			solrDoc.RecordIdentifier = esDoc.ESLogMap[kv.Key].RecordIdentifier
			solrDoc.Identifier = esDoc.ESLogMap[kv.Key].Identifier
			solrDoc.Issn = esDoc.ESLogMap[kv.Key].Issn

			solrDoc.Isanchor = esDoc.ESLogMap[kv.Key].Isanchor
			solrDoc.Iswork = esDoc.ESLogMap[kv.Key].Iswork

			solrDoc.Lang = esDoc.ESLogMap[kv.Key].Lang
			solrDoc.Scriptterm = esDoc.ESLogMap[kv.Key].Scriptterm

			solrDoc.Sponsor = esDoc.ESLogMap[kv.Key].Sponsor

			solrDoc.Genre = esDoc.ESLogMap[kv.Key].Genre
			solrDoc.NormalizedGenre = esDoc.ESLogMap[kv.Key].NormalizedGenre
			solrDoc.NormalizedPlaceTerm = esDoc.ESLogMap[kv.Key].NormalizedPlaceTerm

			for _, note := range esDoc.ESLogMap[kv.Key].NoteInfos {
				solrDoc.Note = append(solrDoc.Note, note.Value)
				solrDoc.NoteType = append(solrDoc.NoteType, note.Type)
			}

			if esDoc.ESLogMap[kv.Key].PublishInfos != nil {
				solrDoc.Place = esDoc.ESLogMap[kv.Key].PublishInfos.Place
				solrDoc.Publisher = esDoc.ESLogMap[kv.Key].PublishInfos.Publisher
				solrDoc.YearPublishString = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishString
				solrDoc.YearPublish = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublish
				solrDoc.YearPublishStart = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishStart
				solrDoc.YearPublishEnd = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishEnd
				solrDoc.PublishEdition = esDoc.ESLogMap[kv.Key].PublishInfos.Edition
			}

			if esDoc.ESLogMap[kv.Key].DigitizationInfos != nil {
				solrDoc.PlaceDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.PlaceDigitization
				solrDoc.PublisherDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.PublisherDigitization
				solrDoc.YearDigitizationString = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationString
				solrDoc.YearDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitization
				solrDoc.YearDigitizationStart = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationStart
				solrDoc.YearDigitizationEnd = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationEnd
				solrDoc.DigitizationEdition = esDoc.ESLogMap[kv.Key].DigitizationInfos.Edition
			}

			if esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos != nil {
				solrDoc.DigitalOrigin = append(solrDoc.DigitalOrigin, esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos.DigitalOrigin)
				solrDoc.Extent = esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos.Extent
			}

			if esDoc.ESLogMap[kv.Key].Doctype == "work" {
				solrDoc.Purl = fmt.Sprintf(config.PurlURI, esDoc.ESLogMap[kv.Key].Work)
			} else {
				solrDoc.Purl = fmt.Sprintf(config.PurlURI, esDoc.ESLogMap[kv.Key].Collection)
			}
			solrDoc.Catalogue = esDoc.ESLogMap[kv.Key].Catalogue

			// TODO product specific
			if esDoc.ESLogMap[kv.Key].Context == "nlh" {
				for _, ri := range esDoc.ESLogMap[kv.Key].RelatedItemInfos {
					solrDoc.ParentdocID = append(solrDoc.ParentdocID, ri.RelateditemID)
					solrDoc.ParentdocTitle = append(solrDoc.ParentdocTitle, ri.RelateditemTitle)
					solrDoc.ParentdocType = append(solrDoc.ParentdocType, ri.RelateditemType)
					solrDoc.ParentdocNote = append(solrDoc.ParentdocNote, ri.RelateditemNote)
					solrDoc.ParentdocTitlePartname = append(solrDoc.ParentdocTitlePartname, ri.RelateditemTitlePartname)
					solrDoc.ParentdocTitleAbbreviated = append(solrDoc.ParentdocTitleAbbreviated, ri.RelateditemTitleAbbreviated)
				}
			} else {
				for _, ri := range esDoc.ESLogMap[kv.Key].RelatedItemInfos {
					solrDoc.RelateditemID = append(solrDoc.RelateditemID, ri.RelateditemID)
					solrDoc.RelateditemTitle = append(solrDoc.RelateditemTitle, ri.RelateditemTitle)
					solrDoc.RelateditemType = append(solrDoc.RelateditemType, ri.RelateditemType)
					solrDoc.RelateditemNote = append(solrDoc.RelateditemNote, ri.RelateditemNote)
					solrDoc.RelateditemTitleAbbreviated = append(solrDoc.RelateditemTitleAbbreviated, ri.RelateditemTitleAbbreviated)
				}
				if esDoc.ESLogMap[kv.Key].ParentDivInfo.ParentID != "" {
					solrDoc.ParentdocWork = append(solrDoc.ParentdocWork, esDoc.ESLogMap[kv.Key].ParentDivInfo.ParentID)
					solrDoc.ParentdocLabel = append(solrDoc.ParentdocLabel, esDoc.ESLogMap[kv.Key].ParentDivInfo.ParentLabel)
					solrDoc.ParentdocType = append(solrDoc.ParentdocType, esDoc.ESLogMap[kv.Key].ParentDivInfo.ParentType)
				}

			}

			for _, ac := range esDoc.ESLogMap[kv.Key].RightsAccessConditionInfos {
				solrDoc.AccessCondition = ac.Value
				break
			}

			if len(esDoc.ESLogMap[kv.Key].LicenseForUseAndReproduction) > 0 {
				solrDoc.LicenseForUseAndReproduction = esDoc.ESLogMap[kv.Key].LicenseForUseAndReproduction
			}

			if esDoc.ESLogMap[kv.Key].RightsInfo.Owner == "Digitalisierungszentrum der Niedersächsischen Staats- und Universitätsbibliothek Göttingen" {
				// TODO move to config
				solrDoc.Owner = "Niedersächsische Staats- und Universitätsbibliothek Göttingen"
				solrDoc.OwnerSiteURL = "http://gdz.sub.uni-goettingen.de"
				solrDoc.OwnerContact = "Niedersächsische Staats- und Universitätsbibliothek Göttingen, Platz der Göttinger Sieben 1, 37073 Göttingen"
				solrDoc.OwnerLogo = "http://gdz.sub.uni-goettingen.de/logo_gdz_dfgv.png"
			} else {
				solrDoc.Owner = esDoc.ESLogMap[kv.Key].RightsInfo.Owner
				solrDoc.OwnerSiteURL = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerSiteURL
				solrDoc.OwnerContact = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerContact
				solrDoc.OwnerLogo = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerLogo
			}

			solrDoc.RightsLicense = esDoc.ESLogMap[kv.Key].RightsInfo.License
			solrDoc.RightsSponsor = esDoc.ESLogMap[kv.Key].RightsInfo.Sponsor

			if esDoc.ESLogMap[kv.Key].License != "" {
				solrDoc.RightsLicense = esDoc.ESLogMap[kv.Key].License
			}

			var geographicSubjectFacet []string
			var topicSubjectFacet []string
			var temporalSubjectFacet []string
			var hierarchicalGeographicSubjectFacet []string

			for _, subject := range esDoc.ESLogMap[kv.Key].SubjectInfos {

				var subjectStr string
				var subjectTypeStr string

				if len(subject.Geographics) > 0 {

					for _, geo := range subject.Geographics {
						v := geo.Value
						t := "geographic"
						subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
						geographicSubjectFacet = append(geographicSubjectFacet, v)
					}
				}

				if len(subject.Topics) > 0 {
					for _, top := range subject.Topics {
						v := top.Value
						t := "topic"
						subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
						topicSubjectFacet = append(topicSubjectFacet, v)
					}
				}
				if len(subject.Temporal) > 0 {
					for _, tempo := range subject.Temporal {
						v := tempo.Value
						t := "temporal"
						subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
						temporalSubjectFacet = append(temporalSubjectFacet, v)
					}
				}

				if len(subject.HierarchicalGeographic) > 0 {
					for _, hgra := range subject.HierarchicalGeographic {
						v := hgra.Country + "/" + hgra.State + "/" + hgra.City
						t := "hierarchicalGeographic"
						subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
						hierarchicalGeographicSubjectFacet = append(hierarchicalGeographicSubjectFacet, v)
					}
				}

				solrDoc.Subject = append(solrDoc.Subject, subjectStr)
				solrDoc.SubjectType = append(solrDoc.SubjectType, subjectTypeStr)
			}

			solrDoc.FacetSubjectGeographic = append(solrDoc.FacetSubjectGeographic, geographicSubjectFacet...)
			solrDoc.FacetSubjectTopic = append(solrDoc.FacetSubjectTopic, topicSubjectFacet...)
			solrDoc.FacetSubjectTemporal = append(solrDoc.FacetSubjectTemporal, temporalSubjectFacet...)
			solrDoc.FacetSubjectHierarchicalGeographic = append(solrDoc.FacetSubjectHierarchicalGeographic, hierarchicalGeographicSubjectFacet...)

			for _, sheflmark := range esDoc.ESLogMap[kv.Key].Shelfmark {
				solrDoc.Shelfmark = append(solrDoc.Shelfmark, sheflmark.Shelfmark)
			}

			if esDoc.ESLogMap[kv.Key].Title != nil {
				solrDoc.Title = append(solrDoc.Title, esDoc.ESLogMap[kv.Key].Title.Title)
				solrDoc.Subtitle = append(solrDoc.Subtitle, esDoc.ESLogMap[kv.Key].Title.Subtitle)
				solrDoc.Sorttitle = append(solrDoc.Sorttitle, esDoc.ESLogMap[kv.Key].Title.Sorttitle)
			}

			solrDoc.IssueNumber = esDoc.ESLogMap[kv.Key].IssueNumber
			solrDoc.VolumeNumber = esDoc.ESLogMap[kv.Key].VolumeNumber

			solrDoc.Work = esDoc.ESLogMap[kv.Key].Work
			solrDoc.Collection = esDoc.ESLogMap[kv.Key].Collection
			solrDoc.Context = esDoc.ESLogMap[kv.Key].Context
			solrDoc.Product = esDoc.ESLogMap[kv.Key].Product
			solrDoc.Productseries = esDoc.ESLogMap[kv.Key].Productseries

			solrDoc.PhysFirstPageIndex = esDoc.ESLogMap[kv.Key].StartPageIndex
			solrDoc.PhysLastPageIndex = esDoc.ESLogMap[kv.Key].EndPageIndex

			tPage = esDoc.ESLogMap[kv.Key].TitlePageIndex

			firstLog = false
		} else if !esDoc.ESLogMap[kv.Key].Isanchor {
			// TODO Isanchor? check/add for other contexts

			solrDoc.LogID = append(solrDoc.LogID, esDoc.ESLogMap[kv.Key].LogID)
			solrDoc.LogType = append(solrDoc.LogType, esDoc.ESLogMap[kv.Key].Type)
			solrDoc.LogLabel = append(solrDoc.LogLabel, esDoc.ESLogMap[kv.Key].Label)
			solrDoc.LogDmdID = append(solrDoc.LogDmdID, esDoc.ESLogMap[kv.Key].DmdID)
			solrDoc.LogLevel = append(solrDoc.LogLevel, esDoc.ESLogMap[kv.Key].Level)
			solrDoc.LogIndex = append(solrDoc.LogIndex, esDoc.ESLogMap[kv.Key].Index)
			solrDoc.LogOrder = append(solrDoc.LogOrder, esDoc.ESLogMap[kv.Key].Order)
			solrDoc.LogStartPageIndex = append(solrDoc.LogStartPageIndex, esDoc.ESLogMap[kv.Key].StartPageIndex)
			solrDoc.LogEndPageIndex = append(solrDoc.LogEndPageIndex, esDoc.ESLogMap[kv.Key].EndPageIndex)

			// TODO remove GDZ switch, because the func only processes GDZ METS
			if esDoc.ESLogMap[kv.Key].Context == "gdz" {
				var solrLogChildDoc types.SolrLogChildDocGDZ

				solrLogChildDoc.Bycreator = esDoc.ESLogMap[kv.Key].Bycreator
				solrLogChildDoc.Byperson = esDoc.ESLogMap[kv.Key].Byperson
				solrLogChildDoc.Bytitle = esDoc.ESLogMap[kv.Key].Bytitle

				for _, creator := range esDoc.ESLogMap[kv.Key].CreatorInfos {
					solrLogChildDoc.Creator = append(solrLogChildDoc.Creator, creator.Name)
					solrLogChildDoc.CreatorType = append(solrLogChildDoc.CreatorType, creator.Type)
					solrLogChildDoc.CreatorGNDURI = append(solrLogChildDoc.CreatorGNDURI, creator.GndURI)
					solrLogChildDoc.CreatorRoleterm = append(solrLogChildDoc.CreatorRoleterm, creator.Roleterm)
					if creator.Type == "personal" {
						solrLogChildDoc.FacetCreatorPersonal = append(solrLogChildDoc.FacetCreatorPersonal, creator.Name)
					} else if creator.Type == "corporate" {
						solrLogChildDoc.FacetCreatorCorporate = append(solrLogChildDoc.FacetCreatorCorporate, creator.Name)
					}
				}

				for _, person := range esDoc.ESLogMap[kv.Key].PersonInfos {
					solrLogChildDoc.Person = append(solrLogChildDoc.Person, person.Name)
					solrLogChildDoc.PersonType = append(solrLogChildDoc.PersonType, person.Type)
					solrLogChildDoc.PersonGNDURI = append(solrLogChildDoc.PersonGNDURI, person.GndURI)
					solrLogChildDoc.PersonGNDNumber = append(solrLogChildDoc.PersonGNDNumber, person.GndNumber)
					solrLogChildDoc.PersonRoleterm = append(solrLogChildDoc.PersonRoleterm, person.Roleterm)
					if person.Type == "personal" {
						solrLogChildDoc.FacetPersonPersonal = append(solrLogChildDoc.FacetPersonPersonal, person.Name)
					} else if person.Type == "corporate" {
						solrLogChildDoc.FacetPersonCorporate = append(solrLogChildDoc.FacetPersonCorporate, person.Name)
					}
				}

				for _, curr := range esDoc.ESLogMap[kv.Key].Currentno {
					solrLogChildDoc.Currentno = curr
					break
				}
				solrLogChildDoc.Currentnosort = esDoc.ESLogMap[kv.Key].Currentnosort
				solrLogChildDoc.Dc = esDoc.ESLogMap[kv.Key].Dc
				solrLogChildDoc.Doctype = "log"
				solrLogChildDoc.ID = fmt.Sprintf("%s___%v", esDoc.ESLogMap[kv.Key].Work, esDoc.ESLogMap[kv.Key].Log)
				solrLogChildDoc.RecordIdentifier = esDoc.ESLogMap[kv.Key].RecordIdentifier
				solrLogChildDoc.Identifier = esDoc.ESLogMap[kv.Key].Identifier
				solrLogChildDoc.Lang = esDoc.ESLogMap[kv.Key].Lang
				solrLogChildDoc.Scriptterm = esDoc.ESLogMap[kv.Key].Scriptterm
				solrLogChildDoc.Sponsor = esDoc.ESLogMap[kv.Key].Sponsor
				solrLogChildDoc.Genre = esDoc.ESLogMap[kv.Key].Genre
				solrLogChildDoc.NormalizedGenre = esDoc.ESLogMap[kv.Key].NormalizedGenre
				solrLogChildDoc.NormalizedPlaceTerm = esDoc.ESLogMap[kv.Key].NormalizedPlaceTerm
				solrLogChildDoc.IsContribution = esDoc.ESLogMap[kv.Key].Iscontribution

				for _, note := range esDoc.ESLogMap[kv.Key].NoteInfos {
					solrLogChildDoc.Note = append(solrLogChildDoc.Note, note.Value)
					solrLogChildDoc.NoteType = append(solrLogChildDoc.NoteType, note.Type)
				}

				if esDoc.ESLogMap[kv.Key].PublishInfos != nil {
					solrLogChildDoc.Place = esDoc.ESLogMap[kv.Key].PublishInfos.Place
					solrLogChildDoc.Publisher = esDoc.ESLogMap[kv.Key].PublishInfos.Publisher
					solrLogChildDoc.YearPublishString = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishString
					solrLogChildDoc.YearPublish = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublish
					solrLogChildDoc.YearPublishStart = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishStart
					solrLogChildDoc.YearPublishEnd = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishEnd
					solrLogChildDoc.PublishEdition = esDoc.ESLogMap[kv.Key].PublishInfos.Edition
				}

				if esDoc.ESLogMap[kv.Key].DigitizationInfos != nil {
					solrLogChildDoc.PlaceDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.PlaceDigitization
					solrLogChildDoc.PublisherDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.PublisherDigitization
					solrLogChildDoc.YearDigitizationString = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationString
					solrLogChildDoc.YearDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitization
					solrLogChildDoc.YearDigitizationStart = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationStart
					solrLogChildDoc.YearDigitizationEnd = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationEnd
					solrLogChildDoc.DigitizationEdition = esDoc.ESLogMap[kv.Key].DigitizationInfos.Edition
				}

				if esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos != nil {
					solrLogChildDoc.DigitalOrigin = append(solrLogChildDoc.DigitalOrigin, esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos.DigitalOrigin)
					solrLogChildDoc.Extent = esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos.Extent
				}

				solrLogChildDoc.Purl = fmt.Sprintf(config.PurlURI, esDoc.ESLogMap[kv.Key].Work, esDoc.ESLogMap[kv.Key].Log)
				solrLogChildDoc.Catalogue = esDoc.ESLogMap[kv.Key].Catalogue

				for _, ri := range esDoc.ESLogMap[kv.Key].RelatedItemInfos {
					solrLogChildDoc.RelateditemID = append(solrLogChildDoc.RelateditemID, ri.RelateditemID)
					solrLogChildDoc.RelateditemTitle = append(solrLogChildDoc.RelateditemTitle, ri.RelateditemTitle)
					solrLogChildDoc.RelateditemType = append(solrLogChildDoc.RelateditemType, ri.RelateditemType)
					solrLogChildDoc.RelateditemNote = append(solrLogChildDoc.RelateditemNote, ri.RelateditemNote)
					solrLogChildDoc.RelateditemTitleAbbreviated = append(solrLogChildDoc.RelateditemTitleAbbreviated, ri.RelateditemTitleAbbreviated)
				}

				for _, ac := range esDoc.ESLogMap[kv.Key].RightsAccessConditionInfos {
					solrLogChildDoc.AccessCondition = ac.Value
					break
				}

				if esDoc.ESLogMap[kv.Key].RightsInfo.Owner == "Digitalisierungszentrum der Niedersächsischen Staats- und Universitätsbibliothek Göttingen" {
					// TODO move to config
					solrLogChildDoc.Owner = "Niedersächsische Staats- und Universitätsbibliothek Göttingen"
					solrLogChildDoc.OwnerSiteURL = "http://gdz.sub.uni-goettingen.de"
					solrLogChildDoc.OwnerContact = "Niedersächsische Staats- und Universitätsbibliothek Göttingen, Platz der Göttinger Sieben 1, 37073 Göttingen"
					solrLogChildDoc.OwnerLogo = "http://gdz.sub.uni-goettingen.de/logo_gdz_dfgv.png"
				} else {
					solrLogChildDoc.Owner = esDoc.ESLogMap[kv.Key].RightsInfo.Owner
					solrLogChildDoc.OwnerSiteURL = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerSiteURL
					solrLogChildDoc.OwnerContact = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerContact
					solrLogChildDoc.OwnerLogo = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerLogo
				}

				solrLogChildDoc.RightsLicense = esDoc.ESLogMap[kv.Key].RightsInfo.License
				solrLogChildDoc.RightsSponsor = esDoc.ESLogMap[kv.Key].RightsInfo.Sponsor

				if esDoc.ESLogMap[kv.Key].License != "" {
					solrLogChildDoc.RightsLicense = esDoc.ESLogMap[kv.Key].License
				}

				var geographicSubjectFacet []string
				var topicSubjectFacet []string
				var temporalSubjectFacet []string
				var hierarchicalGeographicSubjectFacet []string

				for _, subject := range esDoc.ESLogMap[kv.Key].SubjectInfos {

					var subjectStr string
					var subjectTypeStr string

					if len(subject.Geographics) > 0 {

						for _, geo := range subject.Geographics {
							v := geo.Value
							t := "geographic"
							subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
							geographicSubjectFacet = append(geographicSubjectFacet, v)
						}
					}

					if len(subject.Topics) > 0 {
						for _, top := range subject.Topics {
							v := top.Value
							t := "topic"
							subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
							topicSubjectFacet = append(topicSubjectFacet, v)
						}
					}
					if len(subject.Temporal) > 0 {
						for _, tempo := range subject.Temporal {
							v := tempo.Value
							t := "temporal"
							subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
							temporalSubjectFacet = append(temporalSubjectFacet, v)
						}
					}

					if len(subject.HierarchicalGeographic) > 0 {
						for _, hgra := range subject.HierarchicalGeographic {
							v := hgra.Country + "/" + hgra.State + "/" + hgra.City
							t := "hierarchicalGeographic"
							subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
							hierarchicalGeographicSubjectFacet = append(hierarchicalGeographicSubjectFacet, v)
						}
					}

					solrLogChildDoc.Subject = append(solrLogChildDoc.Subject, subjectStr)
					solrLogChildDoc.SubjectType = append(solrLogChildDoc.SubjectType, subjectTypeStr)
				}

				solrLogChildDoc.FacetSubjectGeographic = append(solrLogChildDoc.FacetSubjectGeographic, geographicSubjectFacet...)
				solrLogChildDoc.FacetSubjectTopic = append(solrLogChildDoc.FacetSubjectTopic, topicSubjectFacet...)
				solrLogChildDoc.FacetSubjectTemporal = append(solrLogChildDoc.FacetSubjectTemporal, temporalSubjectFacet...)
				solrLogChildDoc.FacetSubjectHierarchicalGeographic = append(solrLogChildDoc.FacetSubjectHierarchicalGeographic, hierarchicalGeographicSubjectFacet...)

				for _, sheflmark := range esDoc.ESLogMap[kv.Key].Shelfmark {
					solrLogChildDoc.Shelfmark = append(solrLogChildDoc.Shelfmark, sheflmark.Shelfmark)
				}

				if esDoc.ESLogMap[kv.Key].Title != nil {
					solrLogChildDoc.Title = append(solrLogChildDoc.Title, esDoc.ESLogMap[kv.Key].Title.Title)
					solrLogChildDoc.Subtitle = append(solrLogChildDoc.Subtitle, esDoc.ESLogMap[kv.Key].Title.Subtitle)
					solrLogChildDoc.Sorttitle = append(solrLogChildDoc.Sorttitle, esDoc.ESLogMap[kv.Key].Title.Sorttitle)
				}

				solrLogChildDoc.IssueNumber = esDoc.ESLogMap[kv.Key].IssueNumber
				solrLogChildDoc.VolumeNumber = esDoc.ESLogMap[kv.Key].VolumeNumber
				solrLogChildDoc.WorkID = esDoc.ESLogMap[kv.Key].Work
				solrLogChildDoc.Context = esDoc.ESLogMap[kv.Key].Context
				solrLogChildDoc.Product = esDoc.ESLogMap[kv.Key].Product
				// TODO comment in after schema switch
				solrLogChildDoc.Productseries = esDoc.ESLogMap[kv.Key].Productseries
				solrLogChildDoc.LogID = append(solrLogChildDoc.LogID, esDoc.ESLogMap[kv.Key].LogID)
				solrLogChildDoc.LogType = append(solrLogChildDoc.LogType, esDoc.ESLogMap[kv.Key].Type)
				solrLogChildDoc.LogLabel = append(solrLogChildDoc.LogLabel, esDoc.ESLogMap[kv.Key].Label)
				solrLogChildDoc.LogDmdID = esDoc.ESLogMap[kv.Key].DmdID
				solrLogChildDoc.LogLevel = append(solrLogChildDoc.LogLevel, esDoc.ESLogMap[kv.Key].Level)
				solrLogChildDoc.LogOrder = append(solrLogChildDoc.LogOrder, esDoc.ESLogMap[kv.Key].Order)
				solrLogChildDoc.LogStartPageIndex = append(solrLogChildDoc.LogStartPageIndex, esDoc.ESLogMap[kv.Key].StartPageIndex)
				solrLogChildDocs = append(solrLogChildDocs, solrLogChildDoc)
			}
		}

	}

	var fulltextExist bool
	first := true
	firstPhys := true
	var physKv []types.KV
	for k, v := range esDoc.ESPhysMap {
		physKv = append(physKv, types.KV{
			Key:   k,
			Value: int(v.Index)})

		if first {
			fulltextExist = v.FulltextExist
			first = false
		}
	}
	sort.Slice(physKv, func(i, j int) bool {
		return physKv[i].Value < physKv[j].Value
	})

	var physOrder []int64
	var physOrderlabel []string
	var physPage []string
	var physPageKey []string

	var physFulltext []string

	var solrFulltextDocs []types.SolrFulltextDoc

	for _, kv := range physKv {

		var solrFulltextDoc types.SolrFulltextDoc
		if firstPhys {

			// TODO check/solve this eai2 problem (ask juergen)
			if esDoc.Product == "nlh-eai2" || strings.Contains(esDoc.Product, "nlh-ahn") {
				solrDoc.PhysImageFormat = "tif"
			} else {
				solrDoc.PhysImageFormat = esDoc.ESPhysMap[kv.Key].Format
			}
			firstPhys = false
		}

		physOrder = append(physOrder, esDoc.ESPhysMap[kv.Key].Order)
		physOrderlabel = append(physOrderlabel, esDoc.ESPhysMap[kv.Key].Orderlabel)

		physPage = append(physPage, esDoc.ESPhysMap[kv.Key].Page)
		physPageKey = append(physPageKey, esDoc.ESPhysMap[kv.Key].PageKey)

		physFulltext = append(physFulltext, esDoc.ESPhysMap[kv.Key].Fulltext)

		solrFulltextDoc.ID = fmt.Sprintf("%s_page_%v", esDoc.ESPhysMap[kv.Key].Work, esDoc.ESPhysMap[kv.Key].Order)
		solrFulltextDoc.FT = esDoc.ESPhysMap[kv.Key].Fulltext
		solrFulltextDoc.FTPageID = kv.Key
		solrFulltextDoc.FTPageNumber = esDoc.ESPhysMap[kv.Key].Order
		solrFulltextDoc.Doctype = "fulltext"
		// TODO the following two fields are redundant, ask ali which he uses
		solrFulltextDoc.FTOfWork = esDoc.ESPhysMap[kv.Key].Work
		solrFulltextDoc.WorkID = esDoc.ESPhysMap[kv.Key].Work
		solrFulltextDoc.Product = esDoc.Product
		solrFulltextDoc.IsContribution = esDoc.ESPhysMap[kv.Key].Iscontribution

		solrFulltextDoc.LogType = append(solrFulltextDoc.LogType, esDoc.ESPhysMap[kv.Key].Type)
		solrFulltextDocs = append(solrFulltextDocs, solrFulltextDoc)

	}

	solrDoc.PhysOrder = physOrder
	solrDoc.PhysOrderlabel = physOrderlabel
	solrDoc.Page = physPage
	solrDoc.PageKey = physPageKey

	if fulltextExist {
		solrDoc.Fulltext = physFulltext
	}

	if solrDoc.Iswork {
		if tPage == 0 {
			solrDoc.TitlePage = physPageKey[0]
		} else {
			solrDoc.TitlePage = physPageKey[tPage-1]
		}
	}

	// TODO check this
	if pos := firstOccurence(solrDoc.LogType, []string{"titlepage", "title_page", "title-page"}); pos != -1 {
		ppos := solrDoc.LogStartPageIndex[pos]

		if (ppos < 0) || (int(ppos)-1) >= len(physPageKey) {
			log.Errorf("index out of range in %s -> pos: %v, ppos: %v, len(physPageKey): %v\nphysPageKey: %v\nsolrDoc.LogStartPageIndex: %v", solrDoc.ID, pos, ppos, len(physPageKey), physPageKey, solrDoc.LogStartPageIndex)
		} else {
			solrDoc.TitlePage = physPageKey[ppos-1]
		}
	}

	if len(solrLogChildDocs) > 0 {
		solrDoc.SolrLogChildDocs = solrLogChildDocs
	}

	solrDocByteArr, _ := json.Marshal([]types.SolrDocGDZ{solrDoc})

	writeToSolrJobs <- solrDocByteArr

	if fulltextExist {
		solrFulltextDocByteArr, _ := json.Marshal(solrFulltextDocs)
		writeToSolrJobs <- solrFulltextDocByteArr
	}

}

// firstOccurence checks if an element in toCheck occured in arr
// returns the index position of the first occurence of any element of toCheck in arr
// returns -1 if no match is found
func firstOccurence(arr []string, toCheck []string) int {
	for i, a := range arr {
		for _, c := range toCheck {
			if strings.ToLower(a) == c {
				return i
			}
		}
	}
	return -1
}

// WriteToNLHIndex ...
func WriteToNLHIndex(esDoc types.ESDoc) {

	var solrDoc types.SolrDocNLH
	var solrLogChildDocs []types.SolrLogChildDocNLH
	var tPage int64

	firstLog := true

	var logLv []types.KV
	for k, v := range esDoc.ESLogMap {
		logLv = append(logLv, types.KV{
			Key:   k,
			Value: int(v.Index)})
		//Value: int(v.Order)})
	}

	sort.Slice(logLv, func(i, j int) bool {
		return logLv[i].Value < logLv[j].Value
	})

	for _, kv := range logLv {

		if firstLog {

			solrDoc.Bycreator = esDoc.ESLogMap[kv.Key].Bycreator
			solrDoc.Byperson = esDoc.ESLogMap[kv.Key].Byperson
			solrDoc.Bytitle = esDoc.ESLogMap[kv.Key].Bytitle

			for _, creator := range esDoc.ESLogMap[kv.Key].CreatorInfos {
				solrDoc.Creator = append(solrDoc.Creator, creator.Name)
				solrDoc.CreatorType = append(solrDoc.CreatorType, creator.Type)
				solrDoc.CreatorGNDURI = append(solrDoc.CreatorGNDURI, creator.GndURI)
				solrDoc.CreatorDate = append(solrDoc.CreatorDate, creator.Date)
				solrDoc.CreatorDateString = append(solrDoc.CreatorDateString, creator.DateString)
				solrDoc.CreatorRoleterm = append(solrDoc.CreatorRoleterm, creator.Roleterm)
				if creator.Type == "personal" {
					solrDoc.FacetCreatorPersonal = append(solrDoc.FacetCreatorPersonal, creator.Name)
				} else if creator.Type == "corporate" {
					solrDoc.FacetCreatorCorporate = append(solrDoc.FacetCreatorCorporate, creator.Name)
				}
			}

			for _, person := range esDoc.ESLogMap[kv.Key].PersonInfos {
				solrDoc.Person = append(solrDoc.Person, person.Name)
				solrDoc.PersonType = append(solrDoc.PersonType, person.Type)
				solrDoc.PersonGNDURI = append(solrDoc.PersonGNDURI, person.GndURI)
				solrDoc.PersonDate = append(solrDoc.PersonDate, person.Date)
				solrDoc.PersonDateString = append(solrDoc.PersonDateString, person.DateString)
				solrDoc.PersonRoleterm = append(solrDoc.PersonRoleterm, person.Roleterm)
				if person.Type == "personal" {
					solrDoc.FacetPersonPersonal = append(solrDoc.FacetPersonPersonal, person.Name)
				} else if person.Type == "corporate" {
					solrDoc.FacetPersonCorporate = append(solrDoc.FacetPersonCorporate, person.Name)
				}
			}

			for _, curr := range esDoc.ESLogMap[kv.Key].Currentno {
				solrDoc.Currentno = curr
				break
			}
			solrDoc.Currentnosort = esDoc.ESLogMap[kv.Key].Currentnosort
			solrDoc.DateIndexed = esDoc.ESLogMap[kv.Key].DateIndexed
			solrDoc.DateModified = esDoc.ESLogMap[kv.Key].DateModified
			solrDoc.Dc = esDoc.ESLogMap[kv.Key].Dc
			solrDoc.Docstrct = esDoc.ESLogMap[kv.Key].Type   // monograph, volume
			solrDoc.Doctype = esDoc.ESLogMap[kv.Key].Doctype // monograph, volume
			solrDoc.ID = esDoc.ESLogMap[kv.Key].ID
			solrDoc.RecordIdentifier = esDoc.ESLogMap[kv.Key].RecordIdentifier
			solrDoc.Identifier = esDoc.ESLogMap[kv.Key].Identifier
			solrDoc.Issn = esDoc.ESLogMap[kv.Key].Issn
			solrDoc.Isanchor = esDoc.ESLogMap[kv.Key].Isanchor
			solrDoc.Iswork = esDoc.ESLogMap[kv.Key].Iswork
			solrDoc.Lang = esDoc.ESLogMap[kv.Key].Lang
			solrDoc.Scriptterm = esDoc.ESLogMap[kv.Key].Scriptterm
			solrDoc.Sponsor = esDoc.ESLogMap[kv.Key].Sponsor
			solrDoc.Genre = esDoc.ESLogMap[kv.Key].Genre
			solrDoc.NormalizedGenre = esDoc.ESLogMap[kv.Key].NormalizedGenre
			solrDoc.NormalizedPlaceTerm = esDoc.ESLogMap[kv.Key].NormalizedPlaceTerm

			for _, note := range esDoc.ESLogMap[kv.Key].NoteInfos {
				solrDoc.Note = append(solrDoc.Note, note.Value)
				solrDoc.NoteType = append(solrDoc.NoteType, note.Type)
			}

			if esDoc.ESLogMap[kv.Key].PublishInfos != nil {
				solrDoc.Place = esDoc.ESLogMap[kv.Key].PublishInfos.Place
				solrDoc.Publisher = esDoc.ESLogMap[kv.Key].PublishInfos.Publisher
				solrDoc.YearPublishString = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishString
				solrDoc.YearPublish = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublish
				solrDoc.YearPublishStart = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishStart
				solrDoc.YearPublishEnd = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishEnd
				solrDoc.PublishEdition = esDoc.ESLogMap[kv.Key].PublishInfos.Edition
			}

			if esDoc.ESLogMap[kv.Key].DigitizationInfos != nil {
				solrDoc.PlaceDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.PlaceDigitization
				solrDoc.PublisherDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.PublisherDigitization
				solrDoc.YearDigitizationString = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationString
				solrDoc.YearDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitization
				solrDoc.YearDigitizationStart = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationStart
				solrDoc.YearDigitizationEnd = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationEnd
				solrDoc.DigitizationEdition = esDoc.ESLogMap[kv.Key].DigitizationInfos.Edition
			}

			if esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos != nil {
				solrDoc.DigitalOrigin = append(solrDoc.DigitalOrigin, esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos.DigitalOrigin)
				solrDoc.Extent = esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos.Extent
			}

			if esDoc.ESLogMap[kv.Key].Doctype == "work" {
				solrDoc.Purl = fmt.Sprintf(config.PurlURI, esDoc.ESLogMap[kv.Key].Work)
			} else {
				solrDoc.Purl = fmt.Sprintf(config.PurlURI, esDoc.ESLogMap[kv.Key].Collection)
			}

			// TODO remove NLH switch, because the func only processes NLH METS
			// TODO product specific
			if esDoc.ESLogMap[kv.Key].Context == "nlh" {
				for _, ri := range esDoc.ESLogMap[kv.Key].RelatedItemInfos {
					solrDoc.ParentdocID = append(solrDoc.ParentdocID, ri.RelateditemID)
					solrDoc.ParentdocTitle = append(solrDoc.ParentdocTitle, ri.RelateditemTitle)
					solrDoc.ParentdocType = append(solrDoc.ParentdocType, ri.RelateditemType)
					solrDoc.ParentdocNote = append(solrDoc.ParentdocNote, ri.RelateditemNote)
					solrDoc.ParentdocTitlePartname = append(solrDoc.ParentdocTitlePartname, ri.RelateditemTitlePartname)
					solrDoc.ParentdocTitleAbbreviated = append(solrDoc.ParentdocTitleAbbreviated, ri.RelateditemTitleAbbreviated)
				}
			} else {
				for _, ri := range esDoc.ESLogMap[kv.Key].RelatedItemInfos {
					solrDoc.RelateditemID = append(solrDoc.RelateditemID, ri.RelateditemID)
					solrDoc.RelateditemTitle = append(solrDoc.RelateditemTitle, ri.RelateditemTitle)
					solrDoc.RelateditemType = append(solrDoc.RelateditemType, ri.RelateditemType)
					solrDoc.RelateditemNote = append(solrDoc.RelateditemNote, ri.RelateditemNote)
					solrDoc.RelateditemTitlePartname = append(solrDoc.RelateditemTitlePartname, ri.RelateditemTitlePartname)
					solrDoc.RelateditemTitleAbbreviated = append(solrDoc.RelateditemTitleAbbreviated, ri.RelateditemTitleAbbreviated)
				}
				if esDoc.ESLogMap[kv.Key].ParentDivInfo.ParentID != "" {
					solrDoc.ParentdocWork = append(solrDoc.ParentdocWork, esDoc.ESLogMap[kv.Key].ParentDivInfo.ParentID)
					solrDoc.ParentdocLabel = append(solrDoc.ParentdocLabel, esDoc.ESLogMap[kv.Key].ParentDivInfo.ParentLabel)
					solrDoc.ParentdocType = append(solrDoc.ParentdocType, esDoc.ESLogMap[kv.Key].ParentDivInfo.ParentType)
				}
			}

			solrDoc.Owner = esDoc.ESLogMap[kv.Key].RightsInfo.Owner
			solrDoc.OwnerSiteURL = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerSiteURL
			solrDoc.OwnerContact = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerContact
			solrDoc.OwnerLogo = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerLogo
			solrDoc.RightsLicense = esDoc.ESLogMap[kv.Key].RightsInfo.License
			solrDoc.RightsSponsor = esDoc.ESLogMap[kv.Key].RightsInfo.Sponsor

			var geographicSubjectFacet []string
			var topicSubjectFacet []string
			var temporalSubjectFacet []string
			var hierarchicalGeographicSubjectFacet []string

			for _, subject := range esDoc.ESLogMap[kv.Key].SubjectInfos {

				var subjectStr string
				var subjectTypeStr string

				if len(subject.Geographics) > 0 {

					for _, geo := range subject.Geographics {
						v := geo.Value
						t := "geographic"
						subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
						geographicSubjectFacet = append(geographicSubjectFacet, v)
					}
				}

				if len(subject.Topics) > 0 {
					for _, top := range subject.Topics {
						v := top.Value
						t := "topic"
						subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
						topicSubjectFacet = append(topicSubjectFacet, v)
					}
				}
				if len(subject.Temporal) > 0 {
					for _, tempo := range subject.Temporal {
						v := tempo.Value
						t := "temporal"
						subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
						temporalSubjectFacet = append(temporalSubjectFacet, v)
					}
				}

				if len(subject.HierarchicalGeographic) > 0 {
					for _, hgra := range subject.HierarchicalGeographic {
						v := hgra.Country + "/" + hgra.State + "/" + hgra.City
						t := "hierarchicalGeographic"
						subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
						hierarchicalGeographicSubjectFacet = append(hierarchicalGeographicSubjectFacet, v)
					}
				}

				solrDoc.Subject = append(solrDoc.Subject, subjectStr)
				solrDoc.SubjectType = append(solrDoc.SubjectType, subjectTypeStr)
			}

			solrDoc.FacetSubjectGeographic = append(solrDoc.FacetSubjectGeographic, geographicSubjectFacet...)
			solrDoc.FacetSubjectTopic = append(solrDoc.FacetSubjectTopic, topicSubjectFacet...)
			solrDoc.FacetSubjectTemporal = append(solrDoc.FacetSubjectTemporal, temporalSubjectFacet...)
			solrDoc.FacetSubjectHierarchicalGeographic = append(solrDoc.FacetSubjectHierarchicalGeographic, hierarchicalGeographicSubjectFacet...)

			for _, sheflmark := range esDoc.ESLogMap[kv.Key].Shelfmark {
				solrDoc.Shelfmark = append(solrDoc.Shelfmark, sheflmark.Shelfmark)
			}

			if esDoc.ESLogMap[kv.Key].Title != nil {
				solrDoc.Title = append(solrDoc.Title, esDoc.ESLogMap[kv.Key].Title.Title)
				solrDoc.TitleOriginal = append(solrDoc.TitleOriginal, esDoc.ESLogMap[kv.Key].Title.TitleOriginal)
				solrDoc.TitleAbbreviated = append(solrDoc.TitleAbbreviated, esDoc.ESLogMap[kv.Key].Title.TitleAbbreviated)
				solrDoc.TitleAlternative = append(solrDoc.TitleAlternative, esDoc.ESLogMap[kv.Key].Title.TitleAlternative)
				solrDoc.Subtitle = append(solrDoc.Subtitle, esDoc.ESLogMap[kv.Key].Title.Subtitle)
				solrDoc.Sorttitle = append(solrDoc.Sorttitle, esDoc.ESLogMap[kv.Key].Title.Sorttitle)
				solrDoc.Partname = append(solrDoc.Partname, esDoc.ESLogMap[kv.Key].Title.Partname)
				solrDoc.Partnumber = append(solrDoc.Partnumber, esDoc.ESLogMap[kv.Key].Title.Partnumber)

				if helper.Contains(config.MultiPeriodicalProducts, esDoc.Product) {
					if esDoc.ESLogMap[kv.Key].Title.TitleUniform != "" {
						solrDoc.PeriodicalName = append(solrDoc.PeriodicalName, esDoc.ESLogMap[kv.Key].Title.TitleUniform)
					} else {
						solrDoc.PeriodicalName = append(solrDoc.PeriodicalName, esDoc.ESLogMap[kv.Key].Title.TitleOriginal)
					}
				}
			}

			solrDoc.IssueNumber = esDoc.ESLogMap[kv.Key].IssueNumber
			solrDoc.VolumeNumber = esDoc.ESLogMap[kv.Key].VolumeNumber
			solrDoc.Work = esDoc.ESLogMap[kv.Key].Work
			solrDoc.Collection = esDoc.ESLogMap[kv.Key].Collection
			solrDoc.Context = esDoc.ESLogMap[kv.Key].Context
			solrDoc.Product = esDoc.ESLogMap[kv.Key].Product
			solrDoc.Productseries = esDoc.ESLogMap[kv.Key].Productseries
			solrDoc.NaviYear = esDoc.ESLogMap[kv.Key].NaviYear
			solrDoc.NaviMonth = esDoc.ESLogMap[kv.Key].NaviMonth
			solrDoc.NaviDay = esDoc.ESLogMap[kv.Key].NaviDay
			solrDoc.NaviString = esDoc.ESLogMap[kv.Key].NaviString
			solrDoc.NaviDate = esDoc.ESLogMap[kv.Key].NaviDate
			solrDoc.PhysFirstPageIndex = esDoc.ESLogMap[kv.Key].StartPageIndex
			solrDoc.PhysLastPageIndex = esDoc.ESLogMap[kv.Key].EndPageIndex
			solrDoc.LogID = append(solrDoc.LogID, esDoc.ESLogMap[kv.Key].LogID)
			solrDoc.LogType = append(solrDoc.LogType, esDoc.ESLogMap[kv.Key].Type)
			solrDoc.LogLabel = append(solrDoc.LogLabel, esDoc.ESLogMap[kv.Key].Label)
			solrDoc.LogDmdID = append(solrDoc.LogDmdID, esDoc.ESLogMap[kv.Key].DmdID)
			solrDoc.LogLevel = append(solrDoc.LogLevel, esDoc.ESLogMap[kv.Key].Level)
			solrDoc.LogIndex = append(solrDoc.LogIndex, esDoc.ESLogMap[kv.Key].Index)
			solrDoc.LogOrder = append(solrDoc.LogOrder, esDoc.ESLogMap[kv.Key].Order)

			// *_s -> dynamic Fields
			// TODO comment in for highlighting
			if esDoc.ESLogMap[kv.Key].LogFullpage != "" {
				solrDoc.LogFullpage = append(solrDoc.LogFullpage, esDoc.ESLogMap[kv.Key].LogFullpage)
			} else {
				solrDoc.LogFullpage = append(solrDoc.LogFullpage, "")
			}
			if esDoc.ESLogMap[kv.Key].LogFullpageCoord != "" {
				solrDoc.LogFullpageCoord = append(solrDoc.LogFullpageCoord, esDoc.ESLogMap[kv.Key].LogFullpageCoord)
			} else {
				solrDoc.LogFullpageCoord = append(solrDoc.LogFullpageCoord, "")
			}

			if esDoc.ESLogMap[kv.Key].LogFulltextID != "" {
				solrDoc.LogFulltextID = append(solrDoc.LogFulltextID, esDoc.ESLogMap[kv.Key].LogFulltextID)
			} else {
				solrDoc.LogFulltextID = append(solrDoc.LogFulltextID, "")
			}

			if esDoc.ESLogMap[kv.Key].LogFulltext != "" {
				solrDoc.LogFulltext = append(solrDoc.LogFulltext, esDoc.ESLogMap[kv.Key].LogFulltext)
			} else {
				solrDoc.LogFulltext = append(solrDoc.LogFulltext, "")
			}

			solrDoc.LogStartPageIndex = append(solrDoc.LogStartPageIndex, esDoc.ESLogMap[kv.Key].StartPageIndex)
			solrDoc.LogEndPageIndex = append(solrDoc.LogEndPageIndex, esDoc.ESLogMap[kv.Key].EndPageIndex)

			tPage = esDoc.ESLogMap[kv.Key].TitlePageIndex
			firstLog = false
		} else if !esDoc.ESLogMap[kv.Key].Isanchor {

			solrDoc.LogID = append(solrDoc.LogID, esDoc.ESLogMap[kv.Key].LogID)
			solrDoc.LogType = append(solrDoc.LogType, esDoc.ESLogMap[kv.Key].Type)
			solrDoc.LogLabel = append(solrDoc.LogLabel, esDoc.ESLogMap[kv.Key].Label)
			solrDoc.LogDmdID = append(solrDoc.LogDmdID, esDoc.ESLogMap[kv.Key].DmdID)
			solrDoc.LogLevel = append(solrDoc.LogLevel, esDoc.ESLogMap[kv.Key].Level)
			solrDoc.LogIndex = append(solrDoc.LogIndex, esDoc.ESLogMap[kv.Key].Index)
			solrDoc.LogOrder = append(solrDoc.LogOrder, esDoc.ESLogMap[kv.Key].Order)

			// *_s -> dynamic Fields
			// TODO comment in for highlighting
			if esDoc.ESLogMap[kv.Key].LogFullpage != "" {
				solrDoc.LogFullpage = append(solrDoc.LogFullpage, esDoc.ESLogMap[kv.Key].LogFullpage)
			} else {
				solrDoc.LogFullpage = append(solrDoc.LogFullpage, "")
			}

			if esDoc.ESLogMap[kv.Key].LogFullpageCoord != "" {
				solrDoc.LogFullpageCoord = append(solrDoc.LogFullpageCoord, esDoc.ESLogMap[kv.Key].LogFullpageCoord)
			} else {
				solrDoc.LogFullpageCoord = append(solrDoc.LogFullpageCoord, "")
			}

			if esDoc.ESLogMap[kv.Key].LogFulltextID != "" {
				solrDoc.LogFulltextID = append(solrDoc.LogFulltextID, esDoc.ESLogMap[kv.Key].LogFulltextID)
			} else {
				solrDoc.LogFulltextID = append(solrDoc.LogFulltextID, "")
			}

			if esDoc.ESLogMap[kv.Key].LogFulltext != "" {
				solrDoc.LogFulltext = append(solrDoc.LogFulltext, esDoc.ESLogMap[kv.Key].LogFulltext)
			} else {
				solrDoc.LogFulltext = append(solrDoc.LogFulltext, "")
			}

			solrDoc.LogStartPageIndex = append(solrDoc.LogStartPageIndex, esDoc.ESLogMap[kv.Key].StartPageIndex)
			solrDoc.LogEndPageIndex = append(solrDoc.LogEndPageIndex, esDoc.ESLogMap[kv.Key].EndPageIndex)

			// add solr log child doc, also for NLH

			// TODO remove NLH switch, because the func only processes GDZ METS
			// removed because no solr child docs in NLH
			//if esDoc.ESLogMap[kv.Key].Context == "nlh" {
			if false {
				var solrLogChildDoc types.SolrLogChildDocNLH

				solrLogChildDoc.Bycreator = esDoc.ESLogMap[kv.Key].Bycreator
				solrLogChildDoc.Byperson = esDoc.ESLogMap[kv.Key].Byperson
				solrLogChildDoc.Bytitle = esDoc.ESLogMap[kv.Key].Bytitle

				for _, creator := range esDoc.ESLogMap[kv.Key].CreatorInfos {
					solrLogChildDoc.Creator = append(solrLogChildDoc.Creator, creator.Name)
					solrLogChildDoc.CreatorType = append(solrLogChildDoc.CreatorType, creator.Type)
					solrLogChildDoc.CreatorGNDURI = append(solrLogChildDoc.CreatorGNDURI, creator.GndURI)
					solrLogChildDoc.CreatorRoleterm = append(solrLogChildDoc.CreatorRoleterm, creator.Roleterm)
					if creator.Type == "personal" {
						solrLogChildDoc.FacetCreatorPersonal = append(solrLogChildDoc.FacetCreatorPersonal, creator.Name)
					} else if creator.Type == "corporate" {
						solrLogChildDoc.FacetCreatorCorporate = append(solrLogChildDoc.FacetCreatorCorporate, creator.Name)
					}
				}

				for _, person := range esDoc.ESLogMap[kv.Key].PersonInfos {
					solrLogChildDoc.Person = append(solrLogChildDoc.Person, person.Name)
					solrLogChildDoc.PersonType = append(solrLogChildDoc.PersonType, person.Type)
					solrLogChildDoc.PersonGNDURI = append(solrLogChildDoc.PersonGNDURI, person.GndURI)
					solrLogChildDoc.PersonGNDNumber = append(solrLogChildDoc.PersonGNDNumber, person.GndNumber)
					solrLogChildDoc.PersonRoleterm = append(solrLogChildDoc.PersonRoleterm, person.Roleterm)
					if person.Type == "personal" {
						solrLogChildDoc.FacetPersonPersonal = append(solrLogChildDoc.FacetPersonPersonal, person.Name)
					} else if person.Type == "corporate" {
						solrLogChildDoc.FacetPersonCorporate = append(solrLogChildDoc.FacetPersonCorporate, person.Name)
					}
				}

				for _, curr := range esDoc.ESLogMap[kv.Key].Currentno {
					solrLogChildDoc.Currentno = curr
					break
				}
				solrLogChildDoc.Currentnosort = esDoc.ESLogMap[kv.Key].Currentnosort
				solrLogChildDoc.Dc = esDoc.ESLogMap[kv.Key].Dc
				solrLogChildDoc.Doctype = "log"
				solrLogChildDoc.ID = fmt.Sprintf("%s___%v", esDoc.ESLogMap[kv.Key].Work, esDoc.ESLogMap[kv.Key].Log)
				solrLogChildDoc.RecordIdentifier = esDoc.ESLogMap[kv.Key].RecordIdentifier
				solrLogChildDoc.Identifier = esDoc.ESLogMap[kv.Key].Identifier
				solrLogChildDoc.Lang = esDoc.ESLogMap[kv.Key].Lang
				solrLogChildDoc.Scriptterm = esDoc.ESLogMap[kv.Key].Scriptterm
				solrLogChildDoc.Sponsor = esDoc.ESLogMap[kv.Key].Sponsor
				solrLogChildDoc.Genre = esDoc.ESLogMap[kv.Key].Genre
				solrLogChildDoc.NormalizedGenre = esDoc.ESLogMap[kv.Key].NormalizedGenre
				solrLogChildDoc.NormalizedPlaceTerm = esDoc.ESLogMap[kv.Key].NormalizedPlaceTerm
				solrLogChildDoc.IsContribution = esDoc.ESLogMap[kv.Key].Iscontribution

				for _, note := range esDoc.ESLogMap[kv.Key].NoteInfos {
					solrLogChildDoc.Note = append(solrLogChildDoc.Note, note.Value)
					solrLogChildDoc.NoteType = append(solrLogChildDoc.NoteType, note.Type)
				}

				if esDoc.ESLogMap[kv.Key].PublishInfos != nil {
					solrLogChildDoc.Place = esDoc.ESLogMap[kv.Key].PublishInfos.Place
					solrLogChildDoc.Publisher = esDoc.ESLogMap[kv.Key].PublishInfos.Publisher
					solrLogChildDoc.YearPublishString = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishString
					solrLogChildDoc.YearPublish = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublish
					solrLogChildDoc.YearPublishStart = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishStart
					solrLogChildDoc.YearPublishEnd = esDoc.ESLogMap[kv.Key].PublishInfos.YearPublishEnd
					solrLogChildDoc.PublishEdition = esDoc.ESLogMap[kv.Key].PublishInfos.Edition
				}

				if esDoc.ESLogMap[kv.Key].DigitizationInfos != nil {
					solrLogChildDoc.PlaceDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.PlaceDigitization
					solrLogChildDoc.PublisherDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.PublisherDigitization
					solrLogChildDoc.YearDigitizationString = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationString
					solrLogChildDoc.YearDigitization = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitization
					solrLogChildDoc.YearDigitizationStart = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationStart
					solrLogChildDoc.YearDigitizationEnd = esDoc.ESLogMap[kv.Key].DigitizationInfos.YearDigitizationEnd
					solrLogChildDoc.DigitizationEdition = esDoc.ESLogMap[kv.Key].DigitizationInfos.Edition
				}

				if esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos != nil {
					solrLogChildDoc.DigitalOrigin = append(solrLogChildDoc.DigitalOrigin, esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos.DigitalOrigin)
					solrLogChildDoc.Extent = esDoc.ESLogMap[kv.Key].PhysicalDescriptionInfos.Extent
				}

				solrLogChildDoc.Purl = fmt.Sprintf(config.PurlURI, esDoc.ESLogMap[kv.Key].Work, esDoc.ESLogMap[kv.Key].Log)
				solrLogChildDoc.Catalogue = esDoc.ESLogMap[kv.Key].Catalogue

				for _, ri := range esDoc.ESLogMap[kv.Key].RelatedItemInfos {
					solrLogChildDoc.RelateditemID = append(solrLogChildDoc.RelateditemID, ri.RelateditemID)
					solrLogChildDoc.RelateditemTitle = append(solrLogChildDoc.RelateditemTitle, ri.RelateditemTitle)
					solrLogChildDoc.RelateditemType = append(solrLogChildDoc.RelateditemType, ri.RelateditemType)
					solrLogChildDoc.RelateditemNote = append(solrLogChildDoc.RelateditemNote, ri.RelateditemNote)
					solrLogChildDoc.RelateditemTitleAbbreviated = append(solrLogChildDoc.RelateditemTitleAbbreviated, ri.RelateditemTitleAbbreviated)
				}

				for _, ac := range esDoc.ESLogMap[kv.Key].RightsAccessConditionInfos {
					solrLogChildDoc.AccessCondition = ac.Value
					break
				}

				if esDoc.ESLogMap[kv.Key].RightsInfo.Owner == "Digitalisierungszentrum der Niedersächsischen Staats- und Universitätsbibliothek Göttingen" {
					// TODO move to config
					solrLogChildDoc.Owner = "Niedersächsische Staats- und Universitätsbibliothek Göttingen"
					solrLogChildDoc.OwnerSiteURL = "http://gdz.sub.uni-goettingen.de"
					solrLogChildDoc.OwnerContact = "Niedersächsische Staats- und Universitätsbibliothek Göttingen, Platz der Göttinger Sieben 1, 37073 Göttingen"
					solrLogChildDoc.OwnerLogo = "http://gdz.sub.uni-goettingen.de/logo_gdz_dfgv.png"
				} else {
					solrLogChildDoc.Owner = esDoc.ESLogMap[kv.Key].RightsInfo.Owner
					solrLogChildDoc.OwnerSiteURL = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerSiteURL
					solrLogChildDoc.OwnerContact = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerContact
					solrLogChildDoc.OwnerLogo = esDoc.ESLogMap[kv.Key].RightsInfo.OwnerLogo
				}

				solrLogChildDoc.RightsLicense = esDoc.ESLogMap[kv.Key].RightsInfo.License
				solrLogChildDoc.RightsSponsor = esDoc.ESLogMap[kv.Key].RightsInfo.Sponsor

				if esDoc.ESLogMap[kv.Key].License != "" {
					solrLogChildDoc.RightsLicense = esDoc.ESLogMap[kv.Key].License
				}

				var geographicSubjectFacet []string
				var topicSubjectFacet []string
				var temporalSubjectFacet []string
				var hierarchicalGeographicSubjectFacet []string

				for _, subject := range esDoc.ESLogMap[kv.Key].SubjectInfos {

					var subjectStr string
					var subjectTypeStr string

					if len(subject.Geographics) > 0 {

						for _, geo := range subject.Geographics {
							v := geo.Value
							t := "geographic"
							subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
							geographicSubjectFacet = append(geographicSubjectFacet, v)
						}
					}

					if len(subject.Topics) > 0 {
						for _, top := range subject.Topics {
							v := top.Value
							t := "topic"
							subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
							topicSubjectFacet = append(topicSubjectFacet, v)
						}
					}
					if len(subject.Temporal) > 0 {
						for _, tempo := range subject.Temporal {
							v := tempo.Value
							t := "temporal"
							subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
							temporalSubjectFacet = append(temporalSubjectFacet, v)
						}
					}

					if len(subject.HierarchicalGeographic) > 0 {
						for _, hgra := range subject.HierarchicalGeographic {
							v := hgra.Country + "/" + hgra.State + "/" + hgra.City
							t := "hierarchicalGeographic"
							subjectStr, subjectTypeStr = types.ConcatSubject(subjectStr, v, subjectTypeStr, t)
							hierarchicalGeographicSubjectFacet = append(hierarchicalGeographicSubjectFacet, v)
						}
					}

					solrLogChildDoc.Subject = append(solrLogChildDoc.Subject, subjectStr)
					solrLogChildDoc.SubjectType = append(solrLogChildDoc.SubjectType, subjectTypeStr)
				}

				solrLogChildDoc.FacetSubjectGeographic = append(solrLogChildDoc.FacetSubjectGeographic, geographicSubjectFacet...)
				solrLogChildDoc.FacetSubjectTopic = append(solrLogChildDoc.FacetSubjectTopic, topicSubjectFacet...)
				solrLogChildDoc.FacetSubjectTemporal = append(solrLogChildDoc.FacetSubjectTemporal, temporalSubjectFacet...)
				solrLogChildDoc.FacetSubjectHierarchicalGeographic = append(solrLogChildDoc.FacetSubjectHierarchicalGeographic, hierarchicalGeographicSubjectFacet...)

				for _, sheflmark := range esDoc.ESLogMap[kv.Key].Shelfmark {
					solrLogChildDoc.Shelfmark = append(solrLogChildDoc.Shelfmark, sheflmark.Shelfmark)
				}

				if esDoc.ESLogMap[kv.Key].Title != nil {
					solrLogChildDoc.Title = append(solrLogChildDoc.Title, esDoc.ESLogMap[kv.Key].Title.Title)
					solrLogChildDoc.TitleOriginal = append(solrLogChildDoc.TitleOriginal, esDoc.ESLogMap[kv.Key].Title.TitleOriginal)
					solrLogChildDoc.Subtitle = append(solrLogChildDoc.Subtitle, esDoc.ESLogMap[kv.Key].Title.Subtitle)
					solrLogChildDoc.Sorttitle = append(solrLogChildDoc.Sorttitle, esDoc.ESLogMap[kv.Key].Title.Sorttitle)
				}

				solrLogChildDoc.IssueNumber = esDoc.ESLogMap[kv.Key].IssueNumber
				solrLogChildDoc.VolumeNumber = esDoc.ESLogMap[kv.Key].VolumeNumber
				solrLogChildDoc.WorkID = esDoc.ESLogMap[kv.Key].Work
				solrLogChildDoc.Context = esDoc.ESLogMap[kv.Key].Context
				solrLogChildDoc.Product = esDoc.ESLogMap[kv.Key].Product
				solrLogChildDoc.LogID = append(solrLogChildDoc.LogID, esDoc.ESLogMap[kv.Key].LogID)
				solrLogChildDoc.LogType = append(solrLogChildDoc.LogType, esDoc.ESLogMap[kv.Key].Type)
				solrLogChildDoc.LogLabel = append(solrLogChildDoc.LogLabel, esDoc.ESLogMap[kv.Key].Label)
				solrLogChildDoc.LogDmdID = esDoc.ESLogMap[kv.Key].DmdID
				solrLogChildDoc.LogLevel = append(solrLogChildDoc.LogLevel, esDoc.ESLogMap[kv.Key].Level)
				solrLogChildDoc.LogOrder = append(solrLogChildDoc.LogOrder, esDoc.ESLogMap[kv.Key].Order)
				solrLogChildDoc.LogStartPageIndex = append(solrLogChildDoc.LogStartPageIndex, esDoc.ESLogMap[kv.Key].StartPageIndex)

				solrLogChildDocs = append(solrLogChildDocs, solrLogChildDoc)
			}
		}

	}

	var fulltextExist bool
	first := true
	firstPhys := true
	var physKv []types.KV
	for k, v := range esDoc.ESPhysMap {
		physKv = append(physKv, types.KV{
			Key:   k,
			Value: int(v.Index)})

		if first {
			fulltextExist = v.FulltextExist
			first = false
		}
	}

	sort.Slice(physKv, func(i, j int) bool {
		return physKv[i].Value < physKv[j].Value
	})

	var physOrder []int64
	var physOrderlabel []string
	var physPage []string
	var physPageKey []string

	// TODO Added Fulltext, check this
	var physFulltext []string

	var solrFulltextDocs []types.SolrFulltextDoc

	for _, kv := range physKv {

		var solrFulltextDoc types.SolrFulltextDoc
		if firstPhys {

			// TODO solve this eai2 problem in source data
			if esDoc.Product == "nlh-eai2" {
				//if esDoc.Product == "nlh-eai2" || strings.Contains(esDoc.Product, "nlh-ahn") {
				solrDoc.PhysImageFormat = "tif"
			} else {
				solrDoc.PhysImageFormat = esDoc.ESPhysMap[kv.Key].Format
			}
			firstPhys = false
		}

		physOrder = append(physOrder, esDoc.ESPhysMap[kv.Key].Order)
		physOrderlabel = append(physOrderlabel, esDoc.ESPhysMap[kv.Key].Orderlabel)

		physPage = append(physPage, esDoc.ESPhysMap[kv.Key].Page)
		physPageKey = append(physPageKey, esDoc.ESPhysMap[kv.Key].PageKey)

		// TODO Added Fulltext, check this
		physFulltext = append(physFulltext, esDoc.ESPhysMap[kv.Key].Fulltext)
		solrFulltextDoc.ID = fmt.Sprintf("%s_page_%v", esDoc.ESPhysMap[kv.Key].Work, esDoc.ESPhysMap[kv.Key].Order)
		solrFulltextDoc.FT = esDoc.ESPhysMap[kv.Key].Fulltext
		solrFulltextDoc.FTPageID = kv.Key
		solrFulltextDoc.FTPageNumber = esDoc.ESPhysMap[kv.Key].Order
		solrFulltextDoc.Doctype = "fulltext"
		// TODO the following two fields are redundant, ask ali which he uses
		solrFulltextDoc.FTOfWork = esDoc.ESPhysMap[kv.Key].Work
		solrFulltextDoc.WorkID = esDoc.ESPhysMap[kv.Key].Work
		solrFulltextDoc.Product = esDoc.Product
		solrFulltextDoc.IsContribution = esDoc.ESPhysMap[kv.Key].Iscontribution

		solrFulltextDoc.LogType = append(solrFulltextDoc.LogType, esDoc.ESPhysMap[kv.Key].Type)
		solrFulltextDocs = append(solrFulltextDocs, solrFulltextDoc)

	}

	solrDoc.PhysOrder = physOrder
	solrDoc.PhysOrderlabel = physOrderlabel
	solrDoc.Page = physPage
	solrDoc.PageKey = physPageKey

	// TODO Added Fulltext, check this
	if fulltextExist {
		solrDoc.Fulltext = physFulltext
	}

	if solrDoc.Iswork {
		if tPage == 0 {
			solrDoc.TitlePage = physPageKey[0]
		} else {
			solrDoc.TitlePage = physPageKey[tPage-1]
		}
	}

	// TODO check this
	if pos := firstOccurence(solrDoc.LogType, []string{"titlepage", "title_page", "title-page"}); pos != -1 {
		ppos := solrDoc.LogStartPageIndex[pos]

		if (ppos < 0) || (int(ppos)-1) >= len(physPageKey) {
			log.Errorf("index out of range in %s -> pos: %v, ppos: %v, len(physPageKey): %v\nphysPageKey: %v\nsolrDoc.LogStartPageIndex: %v", solrDoc.ID, pos, ppos, len(physPageKey), physPageKey, solrDoc.LogStartPageIndex)
		} else {
			if ppos-1 < 0 {
				solrDoc.TitlePage = physPageKey[0]
			} else {
				solrDoc.TitlePage = physPageKey[ppos-1]
			}
		}
	}

	if len(solrLogChildDocs) > 0 {
		solrDoc.SolrLogChildDocs = solrLogChildDocs
	}
	solrDocByteArr, _ := json.Marshal([]types.SolrDocNLH{solrDoc})

	writeToSolrJobs <- solrDocByteArr

	if fulltextExist {
		solrFulltextDocByteArr, _ := json.Marshal(solrFulltextDocs)
		writeToSolrJobs <- solrFulltextDocByteArr

	}

}

// WriteToIndex ...
func WriteToSolr(esDoc types.ESDoc) {

	// TODO product specific
	if esDoc.Context == "nlh" {
		WriteToNLHIndex(esDoc)
	} else if esDoc.Context == "gdz" {
		WriteToGDZIndex(esDoc)
	}
}

func GetExportDataFromSolrNLH(citationData types.ExportWork) (types.ExportData, error) {

	exportData := types.ExportData{}

	// access index

	soldDoc, err := ReadFromSolrNLH(citationData.WorkID)
	if err != nil {
		return types.ExportData{}, fmt.Errorf("failed to read metadata for %s from solr, due to %s", citationData.WorkID, err.Error())
	}

	// --

	// NLH: newspaper, monograph, issue, 'multipart monograph', volume, periodical
	// GDZ: monograph, volume, multivolume_work, manuscript, map, periodical, file, contained_work, section, folder, bundle, periodicalvolume,
	// DINO:
	// TODO check this
	if soldDoc.Docstrct == "periodical" {
		exportData.IsPeriodical = true
	} else if soldDoc.Docstrct == "volume" {
		exportData.IsVolume = true
	}

	if soldDoc.Isanchor {
		exportData.ID = soldDoc.Collection
	} else {
		exportData.ID = soldDoc.Work
	}

	exportData.Title = soldDoc.Title[0]
	if len(soldDoc.Publisher) > 0 || len(soldDoc.Place) > 0 {
		exportData.Publisher = soldDoc.Publisher
		exportData.PublishingPlace = soldDoc.Place
		exportData.PublishingYear = soldDoc.YearPublish
		exportData.PublishingYearString = soldDoc.YearPublishString
	}

	var series []string
	for pos, riType := range soldDoc.RelateditemType {
		if strings.ToLower(riType) == "series" {
			series = append(series, soldDoc.RelateditemTitle[pos])
		}
	}
	if len(series) > 0 {
		exportData.Series = strings.Join(series[:], ", ")
	}

	if len(soldDoc.Creator) > 0 {
		exportData.Author = soldDoc.Creator
	}

	var editor []string
	for pos, persType := range soldDoc.PersonType {
		if persType == "edt" {
			editor = append(editor, soldDoc.Person[pos])
		}
	}
	if len(editor) > 0 {
		exportData.Editor = strings.Join(editor[:], ", ")
	}

	if soldDoc.Issn != "" {
		exportData.Issn = soldDoc.Issn
	}

	if len(soldDoc.Lang) > 0 {
		exportData.Language = soldDoc.Lang
	}

	var url string

	// TODO product specific
	if soldDoc.Context == "digizeit" {
		url = fmt.Sprintf(config.IiifViewerResolvingURLDigizeit, soldDoc.RecordIdentifier)
	} else if soldDoc.Context == "gdz" {
		url = fmt.Sprintf(config.IiifViewerResolvingURLGDZ, soldDoc.RecordIdentifier)
	} else if soldDoc.Context == "nlh" {
		url = fmt.Sprintf(config.IiifViewerResolvingURLNLH, soldDoc.RecordIdentifier, soldDoc.Product)
	}
	if url != "" {
		exportData.URL = url
	}

	exportData.Volume = soldDoc.Currentno

	return exportData, nil
}

func GetExportDataFromSolrGDZ(citationData types.ExportWork) (types.ExportData, error) {

	exportData := types.ExportData{}

	// access index

	soldDoc, err := ReadFromSolrGDZ(citationData.WorkID)
	if err != nil {
		return types.ExportData{}, fmt.Errorf("failed to read metadata for %s from solr, due to %s", citationData.WorkID, err.Error())
	}

	// --

	// NLH: newspaper, monograph, issue, 'multipart monograph', volume, periodical
	// GDZ: monograph, volume, multivolume_work, manuscript, map, periodical, file, contained_work, section, folder, bundle, periodicalvolume,
	// DINO:
	// TODO check this
	if soldDoc.Docstrct == "periodical" {
		exportData.IsPeriodical = true
	} else if soldDoc.Docstrct == "volume" {
		exportData.IsVolume = true
	}

	if soldDoc.Isanchor {
		exportData.ID = soldDoc.Collection
	} else {
		exportData.ID = soldDoc.Work
	}

	exportData.Title = soldDoc.Title[0]
	if len(soldDoc.Publisher) > 0 || len(soldDoc.Place) > 0 {
		exportData.Publisher = soldDoc.Publisher
		exportData.PublishingPlace = soldDoc.Place
		exportData.PublishingYear = soldDoc.YearPublish
		exportData.PublishingYearString = soldDoc.YearPublishString
	}

	var series []string
	for pos, riType := range soldDoc.RelateditemType {
		if strings.ToLower(riType) == "series" {
			series = append(series, soldDoc.RelateditemTitle[pos])
		}
	}
	if len(series) > 0 {
		exportData.Series = strings.Join(series[:], ", ")
	}

	if len(soldDoc.Creator) > 0 {
		exportData.Author = soldDoc.Creator
	}

	var editor []string
	for pos, persType := range soldDoc.PersonType {
		if persType == "edt" {
			editor = append(editor, soldDoc.Person[pos])
		}
	}
	if len(editor) > 0 {
		exportData.Editor = strings.Join(editor[:], ", ")
	}

	if soldDoc.Issn != "" {
		exportData.Issn = soldDoc.Issn
	}

	if len(soldDoc.Lang) > 0 {
		exportData.Language = soldDoc.Lang
	}

	var url string

	// TODO product specific
	if soldDoc.Context == "digizeit" {
		url = fmt.Sprintf(config.IiifViewerResolvingURLDigizeit, soldDoc.RecordIdentifier)
	} else if soldDoc.Context == "gdz" {
		url = fmt.Sprintf(config.IiifViewerResolvingURLGDZ, soldDoc.RecordIdentifier)
	} else if soldDoc.Context == "nlh" {
		url = fmt.Sprintf(config.IiifViewerResolvingURLNLH, soldDoc.RecordIdentifier, soldDoc.Product)
	}
	if url != "" {
		exportData.URL = url
	}

	exportData.Volume = soldDoc.Currentno

	return exportData, nil
}

func GetParentInfoFromGDZSolr(parentWorkID string) (*types.Parent, error) {

	var parent *types.Parent = new(types.Parent)
	var titleinfo *types.TitleInfo = new(types.TitleInfo)
	var publishInfo *types.PublishInfo = new(types.PublishInfo)

	soldDoc, err := ReadFromSolrGDZ(parentWorkID)

	if err != nil {
		return parent, err
	}

	parent.RecordID = parentWorkID
	parent.ParentID = parentWorkID

	titleinfo.Title = soldDoc.Title[0]
	parent.Title = titleinfo

	parent.Dc = soldDoc.Dc

	publishInfo.Publisher = soldDoc.Publisher
	publishInfo.Place = soldDoc.Place
	publishInfo.YearPublish = soldDoc.YearPublish
	publishInfo.YearPublishString = soldDoc.YearPublishString
	parent.PublishInfos = publishInfo

	parent.Purl = soldDoc.Purl

	return parent, nil
}

func GetParentInfoFromNLHSolr(parentWorkID string) (*types.Parent, error) {

	var parent *types.Parent = new(types.Parent)
	var titleinfo *types.TitleInfo = new(types.TitleInfo)
	var publishInfo *types.PublishInfo = new(types.PublishInfo)

	soldDoc, err := ReadFromSolrNLH(parentWorkID)

	if err != nil {
		return parent, err
	}

	parent.Type = soldDoc.LogType[0]
	parent.RecordID = parentWorkID
	parent.Log = soldDoc.LogID[0]
	parent.ParentID = parentWorkID

	titleinfo.Title = soldDoc.Title[0]
	parent.Title = titleinfo

	parent.Dc = soldDoc.Dc

	publishInfo.Publisher = soldDoc.Publisher
	publishInfo.Place = soldDoc.Place
	publishInfo.YearPublish = soldDoc.YearPublish
	publishInfo.YearPublishString = soldDoc.YearPublishString
	parent.PublishInfos = publishInfo

	parent.Purl = soldDoc.Purl

	return parent, nil
}
