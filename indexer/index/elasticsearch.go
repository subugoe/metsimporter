package index

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"regexp"

	"sort"
	"strconv"
	"strings"
	"time"

	"sync"

	"github.com/go-redis/redis"

	"gitlab.gwdg.de/subugoe/metsimporter/indexer/fulltext"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/helper"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/types"

	elasticsearch "github.com/elastic/go-elasticsearch/v6"
	"github.com/elastic/go-elasticsearch/v6/esapi"
)

var (
	connectionPool    *sync.Pool
	writeToESLogJobs  chan types.ESWriteJob
	writeToESPhysJobs chan types.ESWriteJob
)

func init() {

	writeToESLogJobs = make(chan types.ESWriteJob, 100*config.Concurrency)
	writeToESPhysJobs = make(chan types.ESWriteJob, 100*config.Concurrency)

	for w := 0; w < config.Concurrency*2; w++ {
		netTransp := &http.Transport{
			Dial: (&net.Dialer{
				Timeout:   time.Duration(config.HttpTransportTimeout) * time.Second,
				KeepAlive: time.Duration(config.HttpTransportKeepAlive) * time.Second,
			}).Dial,
			TLSHandshakeTimeout:   time.Duration(config.HttpTransportTslHandshakteTimeout) * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			MaxIdleConns:          config.HttpTransportMaxIdleConns,
			MaxConnsPerHost:       config.HttpTransportMaxIdleConns,
			MaxIdleConnsPerHost:   config.HttpTransportMaxIdleConnsPerHost,
			IdleConnTimeout:       90 * time.Second,
		}

		connectionPool = &sync.Pool{
			New: func() interface{} {
				netClient := &http.Client{
					Timeout:   time.Duration(config.HttpClientTimeout) * time.Second,
					Transport: netTransp,
				}
				return netClient
			},
		}
	}

	for w := 1; w <= 2*config.Concurrency; w++ {
		go WriteToPhysIndex()
		go WriteToLogIndex()
	}

}

// OldSolrDatesMsg ...
type OldSolrDatesMsg struct {
	RecordIdentifier string `json:"record_identifier,omitempty"`
	Datemodified     string `json:"date_indexed,omitempty"`
	Dateindexed      string `json:"date_modified,omitempty"`
}

// OldESDatesMsg ...
type OldESDatesMsg struct {
	ID           string `json:"id"`
	Datemodified string `json:"datemodified,omitempty"`
	Dateindexed  string `json:"dateindexed,omitempty"`
}

// GetESPhysFromPhysStructMap ...
func GetESPhysFromPhysStructMap(
	physicalIDToAttrMap map[string]types.PhysicalAttributes,
	fileIDToAttrMap map[string]types.FileSecAttributes,
	physToLogLinkMap map[string]types.PhysToLogLink,
	logToPhysLinkMap map[string]types.LogToPhysLink,
	esLogMap map[string]types.ESLog,
	metsObject *types.Mets,
	redisClient *redis.Client,
	isExternal bool,
	firstLog string) map[string]types.ESPhys {

	var esPhysMap map[string]types.ESPhys = make(map[string]types.ESPhys)
	var first bool = true
	var firstThumb bool = true
	var fulltextsExists bool = true
	var numberMissingFulltexts = 0

	// sort by order
	sortSlice := make(types.PhysAttrSortSlice, 0, len(physicalIDToAttrMap))
	for _, d := range physicalIDToAttrMap {
		sortSlice = append(sortSlice, d)
	}

	var firstThumbPageRef string
	sort.Sort(sortSlice)
	for _, physMeta := range sortSlice {

		var fileAttr, fulltextFileAttr types.FileSecAttributes
		var allFileAttrs []types.FileSecAttributes

		// TODO product specific
		if metsObject.Context == "ocrd" {

			if physMeta.UseToFileIDMap[metsObject.IndexerJob.ImageFileGrp] != "" {
				fileID := physMeta.UseToFileIDMap[metsObject.IndexerJob.ImageFileGrp]
				fileAttr = fileIDToAttrMap[fileID]
			}
			if physMeta.UseToFileIDMap[metsObject.IndexerJob.FulltextFileGrp] != "" {
				fulltextFileID := physMeta.UseToFileIDMap[metsObject.IndexerJob.FulltextFileGrp]
				fulltextFileAttr = fileIDToAttrMap[fulltextFileID]
			}
			for _, fID := range physMeta.FileIDsArr {
				allFileAttrs = append(allFileAttrs, fileIDToAttrMap[fID])
			}

			llog := physToLogLinkMap[physMeta.ID].LogID
			if isExternal && llog == "" {
				llog = firstLog
			}

			esPhys, fulltextNotLoaded := getPhysStructure(physMeta,
				isExternal,
				first,
				metsObject.Context,
				metsObject.Product,
				metsObject.WorkID,
				metsObject.IndexerJob.Ftype,
				metsObject.IndexerJob.Document,
				metsObject.PrevPID,
				metsObject.PrevPIDs,
				llog,
				logToPhysLinkMap[llog],
				esLogMap[llog],
				fileAttr,
				fulltextFileAttr,
				allFileAttrs,
				redisClient,
				fulltextsExists)

			if fulltextNotLoaded {
				numberMissingFulltexts += 1
				if numberMissingFulltexts > 5 {
					fulltextsExists = false
				}
			}

			esPhys.PID = metsObject.PID

			//--- add semantic labels to logical structure elements

			if physMeta.DMDID != "" {
				if len(metsObject.GtDmdsecs[physMeta.DMDID].MdWrap.GtStates) > 0 {

					var props []string
					for _, state := range metsObject.GtDmdsecs[physMeta.DMDID].MdWrap.GtStates {
						props = append(props, state.Prop)
					}
					esPhys.GtStates = props
				}
			}

			esPhysMap[esPhys.Phys] = esPhys

		} else {

			if physMeta.UseToFileIDMap["PRESENTATION"] != "" {
				fileID := physMeta.UseToFileIDMap["PRESENTATION"]
				fileAttr = fileIDToAttrMap[fileID]
				if fileAttr.Href == "" {
					fileID = physMeta.UseToFileIDMap["DEFAULT"]
					fileAttr = fileIDToAttrMap[fileID]
				}
			} else if physMeta.UseToFileIDMap["DEFAULT"] != "" {
				fileID := physMeta.UseToFileIDMap["DEFAULT"]
				fileAttr = fileIDToAttrMap[fileID]
			} else if physMeta.UseToFileIDMap["THUMBS"] != "" {
				fileID := physMeta.UseToFileIDMap["THUMBS"]
				fileAttr = fileIDToAttrMap[fileID]
			}

			if physMeta.UseToFileIDMap["FULLTEXT"] != "" && !isExternal {
				fulltextFileID := physMeta.UseToFileIDMap["FULLTEXT"]
				fulltextFileAttr = fileIDToAttrMap[fulltextFileID]
			}

			llog := physToLogLinkMap[physMeta.ID].LogID
			if isExternal && llog == "" {
				llog = firstLog
			}

			esPhys, fulltextNotLoaded := getPhysStructure(physMeta,
				isExternal,
				first,
				metsObject.Context,
				metsObject.Product,
				metsObject.WorkID,
				metsObject.IndexerJob.Ftype,
				metsObject.IndexerJob.Document,
				metsObject.PrevPID,
				metsObject.PrevPIDs,
				llog,
				logToPhysLinkMap[llog],
				esLogMap[llog],
				fileAttr,
				fulltextFileAttr,
				[]types.FileSecAttributes{},
				redisClient,
				fulltextsExists)

			if fulltextNotLoaded {
				numberMissingFulltexts += 1
				if numberMissingFulltexts > 5 {
					fulltextsExists = false
				}
			}

			if firstThumb && isExternal {
				fileIDThumb := physMeta.UseToFileIDMap["THUMBS"]
				firstThumbPageRef = fileIDToAttrMap[fileIDThumb].Href
				esPhys.FirstThumbPageHRef = firstThumbPageRef
				firstThumb = false
			}

			esPhys.PID = metsObject.PID

			esPhysMap[esPhys.ID] = esPhys
		}

		first = false
	}

	return esPhysMap
}

func getPhysStructure(physMeta types.PhysicalAttributes,
	isExternal bool,
	isFirst bool,
	ccontext string,
	product string,
	workID string,
	ftype string,
	pid string,
	prevPid string,
	prevPids []string,
	llog string,
	logToPhysLink types.LogToPhysLink,
	esLogMap types.ESLog,
	fileAttr types.FileSecAttributes,
	fulltextFileAttr types.FileSecAttributes,
	allFileAttrs []types.FileSecAttributes,
	redisClient *redis.Client,
	fulltextsExists bool) (types.ESPhys, bool) {

	esPhys := types.ESPhys{}
	esPhys.Index = physMeta.DivPosition
	esPhys.Externalcontent = isExternal
	if isExternal && fileAttr.Href == "" {
		log.Errorf("image reference is empty for externalcontent %s page %s", workID, fileAttr.Page)
	}

	var err error
	// from phys. structMap
	o := helper.GetNumberFromXID(physMeta.ID)
	if o == -1 {
		o, err = strconv.ParseInt(physMeta.Order, 10, 32)
		if err != nil {
			log.Errorf("could not derive order attribute from '%s' or '%s' to int (%s)\n", physMeta.Order, physMeta.ID, workID)
		}
	}

	// TODO product specific
	if strings.Contains(product, "nlh-ahn") {
		esPhys.Order = physMeta.DivPosition
	} else {
		esPhys.Order = o
	}

	esPhys.Orderlabel = physMeta.Orderlabel

	var filename string

	// TODO product specific
	if ccontext == "ocrd" {
		filename = fileAttr.Href
		esPhys.ID = types.Join(product, ":", pid, ":", filename)
	} else {
		filename = fileAttr.Page
		if product == "nlh-ecj" {
			if len(filename) == 6 {
				filename = types.Join("00", filename)
			}
		}
		esPhys.ID = types.Join(product, ":", workID, ":", filename)
	}

	redisResult, err := redisClient.SetNX(esPhys.ID, "locked", 60*time.Second).Result()

	if err == nil {
		if !redisResult {
			log.Errorf("phys. ID '%s' exists in DB (%s), data must be invalid", esPhys.ID, workID)
			return types.ESPhys{}, false
		}
		// go-on
	} else {
		log.Errorf("could not set lock for %s, due to %s", esPhys.ID, err.Error())
		return types.ESPhys{}, false
	}

	esPhys.Context = ccontext
	esPhys.PageKey = esPhys.ID
	esPhys.Page = fileAttr.Page
	esPhys.Phys = physMeta.ID

	if isExternal {
		esPhys.PageHRef = fileAttr.Href
	}

	// TODO product specific
	if ccontext == "ocrd" {
		// "http://localhost:8080/export/file?id=%s&path=%s"
		// esPhys.PageHRef = config.HostBaseURL + fmt.Sprintf(config.OlahdsFileKeyPattern, pid, fileAttr.Href)
		esPhys.PageHRef = fmt.Sprintf(config.OlahdsFileKeyPattern, pid, fileAttr.Href)
	}

	// TODO product specific
	if !isExternal {

		var infojsonkey string
		var imagekey string

		if ccontext == "ocrd" {
			infojsonkey = fmt.Sprintf(config.ExportKeyPatternInfoJson, pid, fileAttr.Page)
			imagekey = fmt.Sprintf(config.ExportKeyPatternImage, pid, fileAttr.Filename) // if OCRD -> URL
		} else {
			infojsonkey = fmt.Sprintf(config.ExportKeyPatternInfoJson, workID, fileAttr.Page)
			imagekey = fmt.Sprintf(config.ExportKeyPatternImage, workID, fileAttr.Filename) // if OCRD -> URL
		}
		exists := helper.ExistInfoJsonInS3(product, infojsonkey)

		var imageInfo *types.ImageInfo
		var err error

		if !exists {
			// create and write to S3
			imageInfo, err = helper.CreateImageInfoAndSaveInfoJsonInS3(product, ccontext, infojsonkey, imagekey, pid, workID, product, fileAttr.Page, fileAttr.Href)
		} else {
			// get
			imageInfo, err = helper.GetImageInfoFromInfoJsonInS3(product, infojsonkey)
			if err != nil || imageInfo.Height == 0 {
				log.Errorf("infojson %s/%s seems corrupted or not available, %s", product, infojsonkey, err.Error())
				imageInfo, err = helper.CreateImageInfoAndSaveInfoJsonInS3(product, ccontext, infojsonkey, imagekey, pid, workID, product, fileAttr.Page, fileAttr.Href)
			}
		}

		// set dimensions
		if err == nil {
			esPhys.PageHeight = imageInfo.Height
			esPhys.PageWidth = imageInfo.Width
		} else {
			// set default value
			esPhys.PageHeight = 300
			esPhys.PageWidth = 300
		}

	}
	esPhys.Log = llog

	// TODO product specific
	if ccontext == "nlh" || ccontext == "gdz" {
		esPhys.LogID = workID
	} else {
		esPhys.LogID = types.Join(workID, "|", llog)
	}
	esPhys.ContentID = physMeta.ContentID

	esPhys.StartPageIndex = logToPhysLink.ESLog.StartPageIndex
	esPhys.EndPageIndex = logToPhysLink.ESLog.EndPageIndex
	esPhys.Type = logToPhysLink.ESLog.Type
	esPhys.Label = logToPhysLink.ESLog.Label
	esPhys.Level = logToPhysLink.ESLog.Level
	esPhys.Title = logToPhysLink.ESLog.Title
	esPhys.PublishInfos = logToPhysLink.ESLog.PublishInfos
	esPhys.CreatorInfos = logToPhysLink.ESLog.CreatorInfos
	esPhys.Bycreator = logToPhysLink.ESLog.Bycreator
	if logToPhysLink.ESLog.RightsAccessConditionInfos != nil {
		esPhys.RightsAccessConditionInfos = logToPhysLink.ESLog.RightsAccessConditionInfos
	} else {
		esPhys.RightsAccessConditionInfos = esLogMap.RightsAccessConditionInfos
	}
	esPhys.Dc = logToPhysLink.ESLog.Dc

	if logToPhysLink.ESLog.Work != "" {
		esPhys.Work = logToPhysLink.ESLog.Work
		esPhys.PID = logToPhysLink.ESLog.PID
		// if logToPhysLink.ESLog.PrevPID != "" {
		// 	esPhys.PrevPID = logToPhysLink.ESLog.PrevPID
		// 	if !helper.Contains(logToPhysLink.ESLog.PrevPIDs, prevPid) {
		// 		esPhys.PrevPIDs = append(logToPhysLink.ESLog.PrevPIDs, prevPid)
		// 	}
		// }
	} else {
		esPhys.Work = workID
		esPhys.PID = pid
		// if prevPid != "" {
		// 	esPhys.PrevPID = prevPid
		// 	if !helper.Contains(prevPids, prevPid) {
		// 		prevPids = append(prevPids, prevPid)
		// 	}
		// 	esPhys.PrevPIDs = prevPids
		// }
	}

	if prevPid != "" {
		esPhys.PrevPID = prevPid
		if !helper.Contains(prevPids, prevPid) {
			prevPids = append(prevPids, prevPid)
		}
		esPhys.PrevPIDs = prevPids
	}

	esPhys.Parent = logToPhysLink.ESLog.Parent
	esPhys.Structrun = logToPhysLink.ESLog.Structrun

	esPhys.DateIndexed = logToPhysLink.ESLog.DateIndexed
	esPhys.DateModified = logToPhysLink.ESLog.DateModified

	// download fulltext
	var fulltextNotLoaded bool = false
	if fulltextsExists {
		if fulltextFileAttr.Page != "" {

			bucket := product
			var key string

			// TODO product specific
			if ccontext == "digizeit" {
				workID := helper.DeriveWork(workID)
				key = "fulltext/" + workID + "/" + fulltextFileAttr.Filename
			} else if ccontext == "ocrd" {
				// example "http://141.5.99.53/api/export/file?id=21.T11998%2F0000-001B-DC0C-F&path=OCR-D-IMG%2FOCR-D-IMG_0001.jpg"
				key = config.HostBaseURL + fmt.Sprintf(config.OlahdsFileKeyPattern, pid, fulltextFileAttr.Href)
			} else { // (indexerJob.Context == "gdz") || (indexerJob.Context == "nlh")

				// TODO change this after correction of nlh_ecj fulltext extension
				if product == "nlh-ecj" {
					filename := strings.ReplaceAll(fulltextFileAttr.Filename, "tei.xml", "html")
					if len(filename) == 11 {
						filename = types.Join("00", filename)
					}
					key = "fulltext/" + workID + "/" + filename
				} else {
					key = "fulltext/" + workID + "/" + fulltextFileAttr.Filename
				}
			}
			if helper.ValidationRun() && ccontext != "ocrd" {
				key = config.FulltextPath + key

			}

			//log.Debugf("bucket: %s, key: %s, fulltextFileAttr.Format: %s, ftype: %s, ccontext: %s", bucket, key, fulltextFileAttr.Format, ftype, ccontext)

			ft, err := fulltext.GetFulltext(bucket, key, fulltextFileAttr.Format, ftype, ccontext)
			if err != nil {
				log.Printf("ERROR %s", err)
				esPhys.FulltextExist = false
				fulltextNotLoaded = true
			} else {
				esPhys.Fulltext = ft
				esPhys.FulltextNotAnalysed = ft
				esPhys.FulltextExist = true

				// TODO product specific
				if ccontext == "ocrd" {
					esPhys.FulltextHRef = key
				}
			}
		}
	}

	var allFileHrefs []string
	// TODO check this: switch required (for OCRD), what about digizeit
	if ccontext == "ocrd" {
		for _, other := range allFileAttrs {
			// TODO this is not consistent, sometimes with "HostBaseURL + ..." and sometimes HostBaseURL is part of the key pattern
			//key := config.HostBaseURL + fmt.Sprintf(config.OlahdsFileKeyPattern, pid, other.Href)
			// OLAHDS_FILE_KEY_PATTERN="{{HOST_BASE_URL}}/export/file?id=%s&path=%s"
			key := fmt.Sprintf(config.OlahdsFileKeyPattern, pid, other.Href)
			allFileHrefs = append(allFileHrefs, key)
		}
	}
	esPhys.AllFileHrefs = allFileHrefs

	esPhys.Mimetype = fileAttr.Mimetype
	esPhys.Filename = fileAttr.Filename
	if fileAttr.Format != "" {
		esPhys.Format = fileAttr.Format
	} else {
		formt := strings.Split(fileAttr.Mimetype, "/")[1]
		esPhys.Format = formt
	}

	esPhys.Iscontribution = logToPhysLink.ESLog.Iscontribution

	esPhys.IsGt = logToPhysLink.ESLog.IsGt
	esPhys.ImporterInstitution = logToPhysLink.ESLog.ImporterInstitution

	return esPhys, fulltextNotLoaded
}

// GetESLogFromDmdsec ...
func GetESLogFromDmdsec(
	nonModsDmdsecIDs []string,
	logDivList []types.Div,
	metsHdr types.MetsHdr,
	dmdsecMap map[string]types.Dmdsec,
	amdsecMap map[string]types.Amdsec,
	logToPhysLinkMap map[string]types.LogToPhysLink,
	physicalIDToAttrMap map[string]types.PhysicalAttributes,
	useMap map[string]bool,
	parentInfo *types.Parent,
	parentDivInfo types.ParentDivInfo,
	dmdidOfFirstPhysDiv string,
	dateIndexed string,
	dateModified string,
	redisClient *redis.Client,
	metsObject *types.Mets,
	prevPIDs []string) (map[string]types.ESLog, string, error) {

	var first bool
	first = true

	var firstLog string

	var now time.Time
	now = time.Now()

	var useTypeList []string = make([]string, 0, len(useMap))
	var esLogMap map[string]types.ESLog = make(map[string]types.ESLog)

	sort.Slice(logDivList[:], func(i, j int) bool {
		return logDivList[i].OOrder < logDivList[j].OOrder
	})

	var titlePageDivID string
	for _, div := range logDivList {
		if (strings.ToLower(div.Type) == "titlepage") || (strings.ToLower(div.Type) == "title_page") {
			titlePageDivID = div.ID
			break
		}
	}

	var i = int64(1)
	var oo = int64(1)

	// TODO parallelize this
	for _, div := range logDivList {

		var ids []string
		if div.DmdID != "" {
			ids = strings.Fields(div.DmdID)
		} else {
			ids = []string{div.DmdID}
		}

		for _, splittedDmdID := range ids {

			if len(nonModsDmdsecIDs) != 0 && helper.Contains(nonModsDmdsecIDs, splittedDmdID) {
				continue
			} else {

				// not-div-related

				// div-related

				parentEsLog := esLogMap[div.ParentLog]

				//--- Div index

				if strings.Contains(metsObject.Product, "nlh-ahn") {
					if div.OOrder > i {
						i = div.OOrder
					} else {
						i = i + 1
					}

				} else {
					i = div.OOrder
				}

				esLog, err := getESLogFromDiv(
					div,
					dmdsecMap[splittedDmdID],
					metsObject.WorkID,
					metsObject.PID,
					prevPIDs,
					metsObject.Doctype,
					splittedDmdID,
					metsObject.Context,
					metsObject.Product,
					parentEsLog,
					parentInfo,
					redisClient,
					first,
					strconv.Itoa(int(i)))

				esLog.Index = i

				if err != nil {
					log.Errorf("could not derive ESLog from Div's in %s, due to %s", metsObject.WorkID, err.Error())
					continue
				}

				// additional shelfmark location from first phys. structMap div
				if first {

					esLog.CreateDate = metsHdr.Createdate

					var agents []types.AgentInfo
					for index, agent := range metsHdr.Agent {
						var agt types.AgentInfo
						agt.Type = agent.Type
						agt.Othertype = agent.Othertype
						agt.Role = agent.Role
						agt.Otherrole = agent.Otherrole
						agt.Order = index

						agt.AgentName = agent.AgentName
						notes := []types.AgentNote{}
						for _, note := range agent.AgentNote {
							notes = append(
								notes,
								types.AgentNote{
									Option: note.Option,
									Value:  note.Value,
								},
							)
						}
						agt.AgentNote = notes
						agents = append(agents, agt)
					}
					esLog.AgentInfo = agents

					firstLog = esLog.Log

					var shelfmarks []types.Shelfmark

					for _, locations := range dmdsecMap[dmdidOfFirstPhysDiv].Locations {

						for _, physDesc := range locations.PhysicalLocationInfos {
							if strings.ToLower(physDesc.Type) == "shelfmark" {
								shelfmarks = append(shelfmarks, types.Shelfmark{Shelfmark: physDesc.Value})
							}

						}

						for _, shelfLoc := range locations.ShelfLocationInfos {
							shelfmarks = append(shelfmarks, types.Shelfmark{Shelfmark: shelfLoc.Value})
						}
					}
					if len(shelfmarks) > 0 {
						esLog.Shelfmark = append(esLog.Shelfmark, shelfmarks...)
					}

					if (parentDivInfo != types.ParentDivInfo{}) {
						esLog.ParentDivInfo = parentDivInfo
					}

					//--- add available file groups to logical structure elements

					if metsObject.Context == "ocrd" {
						for key, _ := range useMap {
							if !belongsToList(key) {
								useTypeList = append(useTypeList, key)
							}
						}
						esLog.FileGrpUseTypes = useTypeList
					}
				} else {
					if metsObject.Context == "ocrd" {
						esLog.FileGrpUseTypes = useTypeList
					}
				}

				//--- DateIndexed, DateModified

				esLog.DateIndexed = dateIndexed
				esLog.DateModified = dateModified

				//--- ParentID, ParentWork

				esLog.ParentID = div.ParentID
				esLog.ParentWork = div.ParentWork
				esLog.ParentLog = div.ParentLog

				if !parentInfo.IsEmpty() {
					esLog.Parent = parentInfo
				}

				if metsObject.Doctype == "work" {
					esLog.Work = div.WorkID
				} else if metsObject.Doctype == "anchor" {
					esLog.Collection = div.WorkID
				}

				//--- Context, Product

				esLog.Context = metsObject.Context

				//--- Doctype (anchor or work)

				esLog.Doctype = metsObject.Doctype

				//--- Structrun

				if first && div.IsPartOfMultivolumework {

					structrun := types.Structrun{}

					structrun.ParentID = parentInfo.ParentID

					structrun.PublishInfos = parentInfo.PublishInfos
					structrun.CreatorInfos = parentInfo.CreatorInfos
					structrun.Title = parentInfo.Title
					structrun.Type = parentInfo.Type
					structrun.Dc = parentInfo.Dc
					structrun.Issn = parentInfo.Issn
					structrun.Zdb = parentInfo.Zdb
					structrun.Purl = parentInfo.Purl
					structrun.Catalogue = parentInfo.Catalogue

					if !structrun.IsEmpty() {
						esLog.Structrun = append(esLog.Structrun, structrun)
					}
				} else {
					structrun := types.Structrun{}

					structrun.ParentID = parentEsLog.LogID

					structrun.PublishInfos = parentEsLog.PublishInfos
					structrun.CreatorInfos = parentEsLog.CreatorInfos
					structrun.Title = parentEsLog.Title
					structrun.Type = parentEsLog.Type
					structrun.RightsAccessConditionInfos = parentEsLog.RightsAccessConditionInfos
					structrun.Dc = parentEsLog.Dc
					structrun.Currentno = parentEsLog.Currentno
					structrun.Currentnosort = parentEsLog.Currentnosort
					structrun.Issn = parentEsLog.Issn
					structrun.Zdb = parentEsLog.Zdb

					if !structrun.IsEmpty() {
						esLog.Structrun = append(parentEsLog.Structrun, structrun)
					}
				}

				//--- phys. Infos

				if metsObject.Doctype == "work" {

					logToPhysLink := logToPhysLinkMap[div.ID]
					min, max, minPhys, maxPhys, error := helper.GetMinMaxStartPhys(logToPhysLink, physicalIDToAttrMap, metsObject.WorkID, metsObject.Product)
					if error != nil {
						return nil, "", error
					}

					esLog.StartPageIndex = min
					esLog.EndPageIndex = max
					esLog.StartPagePhysID = minPhys
					esLog.EndPagePhysID = maxPhys

					if first {
						logToPhysLink := logToPhysLinkMap[titlePageDivID]
						_, _, minPhys, _, error := helper.GetMinMaxStartPhys(logToPhysLink, physicalIDToAttrMap, metsObject.WorkID, metsObject.Product)
						if error != nil {
							return nil, "", error
						}
						esLog.TitlePageIndex = min
						esLog.TitlePagePhysID = minPhys
					}

					logToPhysLink.ESLog = esLog

					logToPhysLinkMap[div.ID] = logToPhysLink
				}

				//--- Order

				o := helper.GetNumberFromXID(div.ID)
				if o == -1 {
					o, err = strconv.ParseInt(div.Order, 10, 32)
					if err != nil {
						log.Errorf("could not derive order attribute from '%s' or '%s' into int (%s)\n", div.Order, div.ID, metsObject.RecordIdentifier)
					}
				}
				if strings.Contains(metsObject.Product, "nlh-ahn") {
					if o+20 > oo {
						oo = o + 20
					} else {
						oo = oo + 1
					}
				} else {
					oo = o
				}
				esLog.Order = oo

				//--- rights_info add to first log (data from Admid)

				var ri types.Rights
				if amdsecMap[div.Amdid].RightsMD.ID != "" {
					ri = amdsecMap[div.Amdid].RightsMD.MdWrap.Rights
				} else {
					ri = parentEsLog.RightsInfo
				}
				if ri.License == "" {
					ri.License = esLog.License
				}
				esLog.RightsInfo = ri

				esLog.IsFirst = first
				esLog.ReindexedAt = now.Format("2006-01-02 15:04:05")

				if esLog.RightsAccessConditionInfos == nil || len(esLog.RightsAccessConditionInfos) == 0 {
					var rac []types.RightAccessCondition
					if len(esLog.Structrun) > 0 {
						l := len(esLog.Structrun)
						for i := range esLog.Structrun {
							rac = esLog.Structrun[l-1-i].RightsAccessConditionInfos
							if rac != nil {
								break
							}
						}
						esLog.RightsAccessConditionInfos = rac
					}
				}

				esLog.PID = metsObject.PID
				esLog.PrevPID = metsObject.PrevPID
				esLog.PrevPIDs = prevPIDs
				esLog.IsGt = metsObject.IsGt

				if metsObject.Context == "ocrd" {

					// TODO cleanup this part

					//esLog.OcrdIdentifier = mets.in
					//esLog.OcrdWorkIdentifier = ocrdWorkIdentifier
					//esLog.ImporterInstitution = importerInstitution
					if esLog.OcrdWorkIdentifier != "" {
						esLog.Identifier = append(esLog.Identifier, esLog.OcrdWorkIdentifier)
					}
					if esLog.OcrdIdentifier != "" {
						esLog.Identifier = append(esLog.Identifier, esLog.OcrdIdentifier)
					}

					// // TODO product specific
					// if indexerJob.Context == "ocrd" {
					// 	OcrdMets = bagInfoTXT.OcrdMets
					// 	OcrdIdentifier = bagInfoTXT.OcrdIdentifier
					// 	OcrdWorkIdentifier = bagInfoTXT.OcrdWorkIdentifier
					// 	Importer = bagInfoTXT.OlahdImporter
					// 	if indexerJob.IsGt == false {
					// 		indexerJob.IsGt = bagInfoTXT.OlahdGT
					// 	}

					// 	Importer = bagInfoTXT.OlahdSearchFulltextFilegrp
					// 	Importer = bagInfoTXT.OlahdSearchFulltextFilegrp

					// mets.PID = indexerJob.Document
					// mets.PrevPID = indexerJob.Prev

					// 	if mets.WorkID == "" {
					// 		mets.WorkID = indexerJob.Document
					// 	}

					// }

				}

				esLogMap[div.ID] = esLog

				if strings.Contains(metsObject.Product, "nlh-ahn") {

					for _, fptr := range div.Fptr {
						//

						var articleAreas types.AreaSortSlice
						var pageAreas types.AreaSortSlice

						for _, seq := range fptr.Par.Seq {
							for index, area := range seq.Area {

								area.Order = int16(index)

								if area.Coords != "" {
									pageAreas = append(pageAreas, area)

								} else {
									articleAreas = append(articleAreas, area)
								}

							}
						}

						sort.Sort(pageAreas)
						sort.Sort(articleAreas)

						for index, page := range pageAreas {
							esPartLog := types.ESLog{}
							var articleID string
							var articleLogID string
							var pageID string

							reg1 := "^FILE_(\\S*)_FULLTEXT$"
							re1 := regexp.MustCompile(reg1)
							match1 := re1.FindStringSubmatch(articleAreas[index].Fileid)
							if len(match1) == 2 {
								articleLogID = fmt.Sprintf("LOG_part_%s", match1[1])
								articleID = match1[1]
							} else {
								log.Errorf("wrong regex pattern %s for FileID %s (%s - %s)", reg1, articleAreas[index].Fileid, metsObject.Product, metsObject.WorkID)
								articleLogID = fmt.Sprintf("%s_part_%d", div.ID, index)
							}
							// "nlh-ahn2:110C5CBF63D399B0:110C5CBFF32AA5E8"
							esPartLog.LogFulltextID = fmt.Sprintf("%s:%s:%s", metsObject.Product, metsObject.WorkID, articleID)
							// "110C5CBFF32AA5E8"

							// LogFulltext ???
							// esPartLog.LogFulltextID = fmt.Sprintf("%s:%s:%s", product, workID, articleID)

							// page ID
							reg2 := "^FILE_(\\S*)_DEFAULT$"
							re2 := regexp.MustCompile(reg2)
							match2 := re2.FindStringSubmatch(page.Fileid)
							if len(match2) == 2 {
								pageID = match2[1]
							} else {
								log.Errorf("wrong regex pattern %s for FileID %s (%s - %s)", reg2, page.Fileid, metsObject.Product, metsObject.WorkID)
							}
							// "nlh-ahn2:110C5CBF63D399B0:110C5CBF6C21DA00“
							if metsObject.Product != "" && metsObject.WorkID != "" && pageID != "" {
								esPartLog.LogFullpage = fmt.Sprintf("%s:%s:%s", metsObject.Product, metsObject.WorkID, pageID)
							}

							// "0,0,2006,642“
							esPartLog.LogFullpageCoord = page.Coords

							// Fulltext:	"...FULLTEXT..."
							// Part of ESLog or ESPhys
							//esPartLog.LogFulltext

							esPartLog.LogID = articleLogID
							esPartLog.Type = "article_part"
							esPartLog.Label = fmt.Sprintf("Part %v", index+1)
							esPartLog.Level = esLog.Level + 1

							i = i + 1
							oo = oo + 1
							esPartLog.Index = i
							esPartLog.Order = oo

							esPartLog.StartPageIndex = getPhysIndexForPageID(physicalIDToAttrMap, pageID)
							esPartLog.EndPageIndex = esPartLog.StartPageIndex

							esLogMap[articleID] = esPartLog
						}

					}
				}

			}
		}

		first = false
	}

	return esLogMap, firstLog, nil
}

func getPhysIndexForPageID(physicalIDToAttrMap map[string]types.PhysicalAttributes, pageID string) int64 {

	for _, physicalElement := range physicalIDToAttrMap {
		if strings.Contains(physicalElement.ID, pageID) {
			return int64(physicalElement.DivPosition)
		}
	}
	return int64(1)
}

// GetESPyhsFromES ...
func GetESPyhsFromES(id string, ccontext string) (map[string]types.ESPhys, error) {
	// "id" : "1047098326_0002"

	var err error
	var esPhysMap map[string]types.ESPhys = make(map[string]types.ESPhys)

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {
		return nil, fmt.Errorf("Elasticsearch connection ERROR: %s", err)
	}

	var query string

	if ccontext == "ocrd" {
		query = fmt.Sprintf(`
		"bool": {
    		"must": [
    	    	{"match": {"pid.keyword": "%s"}}
        	]
    	}`, id)
	} else {
		query = fmt.Sprintf(`
		"bool": {
			"must": [
				{"match": {"work.keyword": "%s"}}
			]
		}`, id)
	}

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, "", 2000)

	// Instantiate a map interface object for storing returned documents
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		log.Fatalf("json.NewEncoder() ERROR:", err)

		// Query is a valid JSON object
	} else {
		// Pass the JSON query to the Golang client's Search() method
		resp, err := client.Search(
			client.Search.WithContext(ctx),
			client.Search.WithIndex(config.PhysIndex),
			client.Search.WithBody(read),
			client.Search.WithTrackTotalHits(true),
		)

		// Check for any errors returned by API call to Elasticsearch
		if err != nil {
			return nil, fmt.Errorf("Elasticsearch Search() API ERROR: %s", err)

			// If no errors are returned, parse esapi.Response object
		} else {

			// Close the result body when the function call is complete
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				return nil, fmt.Errorf("could not read Elasticsearch response, due to %s", err.Error())
			}

			var esPhysResponse *types.ESPhysResponse = new(types.ESPhysResponse)

			if err := json.Unmarshal(body, &esPhysResponse); err != nil {
				panic(err)
			}

			for _, hit := range esPhysResponse.Hits.Hits {
				esPhysMap[hit.ESPhys.Phys] = hit.ESPhys
			}

			return esPhysMap, nil
		}
	}

	return nil, nil
}

// GetESLogFromES ...
func GetESLogFromES(id string, ccontext string) (map[string]types.ESLog, error) {
	// "id" : "1047098326_0002"

	var err error
	var esLogMap map[string]types.ESLog = make(map[string]types.ESLog)

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {
		return nil, fmt.Errorf("Elasticsearch connection ERROR: %s", err)
	}

	var query string
	if ccontext == "ocrd" {
		query = fmt.Sprintf(`
		"bool": {
    		"must": [
    	    	{"match": {"pid.keyword": "%s"}}
        	]
   	 	}`, id)
	} else {
		query = fmt.Sprintf(`
		"bool": {
			"must": [
				{"match": {"work.keyword": "%s"}}
			]
		}`, id)
	}

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, "", 2000)

	// Instantiate a map interface object for storing returned documents
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		return nil, fmt.Errorf("json.NewEncoder() ERROR: %s", err)

		// Query is a valid JSON object
	} else {

		// Pass the JSON query to the Golang client's Search() method
		resp, err := client.Search(
			client.Search.WithContext(ctx),
			client.Search.WithIndex(config.LogIndex),
			client.Search.WithBody(read),
			client.Search.WithTrackTotalHits(true),
		)

		// Check for any errors returned by API call to Elasticsearch
		if err != nil {
			return nil, fmt.Errorf("Elasticsearch Search() API ERROR: %s", err.Error())

			// If no errors are returned, parse esapi.Response object
		} else {

			// Close the result body when the function call is complete
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				return nil, fmt.Errorf("could not read Elasticsearch response, due to %s", err.Error())
			}

			var esLogResponse *types.ESLogResponse = new(types.ESLogResponse)

			if err := json.Unmarshal(body, &esLogResponse); err != nil {
				panic(err)
			}

			for _, hit := range esLogResponse.Hits.Hits {
				esLogMap[hit.ESLog.Log] = hit.ESLog
			}
			return esLogMap, nil
		}
	}
}

// GetPIDForWorkidFromIndex ...
func GetPIDForWorkidFromIndex(workid string) (string, error) {

	var err error

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {
		return "", fmt.Errorf("Elasticsearch connection ERROR: %s", err)
	}

	var fields []string = []string{"pid"}
	source := joinStringArrayToKommaSeparatedString(fields)
	var query = fmt.Sprintf(`
	"bool": {
    	"must": [
    	    {"match": {"work.keyword": "%s"}},
			{"match": {"IsFirst": true}}
        ]
    }`, workid)

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, source, 2000)

	// Instantiate a map interface object for storing returned documents
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		return "", fmt.Errorf("json.NewEncoder() ERROR: %s", err)

		// Query is a valid JSON object
	} else {

		// Pass the JSON query to the Golang client's Search() method
		resp, err := client.Search(
			client.Search.WithContext(ctx),
			client.Search.WithIndex(config.LogIndex),
			client.Search.WithBody(read),
			client.Search.WithTrackTotalHits(true),
		)

		// Check for any errors returned by API call to Elasticsearch
		if err != nil {
			return "", fmt.Errorf("Elasticsearch Search() API ERROR: %s", err.Error())

			// If no errors are returned, parse esapi.Response object
		} else {

			// Close the result body when the function call is complete
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				return "", fmt.Errorf("could not read Elasticsearch response, due to %s", err.Error())
			}

			var esLogResponse *types.ESLogResponse = new(types.ESLogResponse)

			if err := json.Unmarshal(body, &esLogResponse); err != nil {
				panic(err)
			}

			for _, hit := range esLogResponse.Hits.Hits {
				return hit.ESLog.PID, nil
			}

		}
	}

	return "", fmt.Errorf("no pid found in index for workid %s", workid)
}

// GetPIDForWorkidFromIndex_ ...
func GetPIDForWorkidFromIndex_(workid string) (string, error) {

	var err error

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {
		return "", fmt.Errorf("Elasticsearch connection ERROR: %s", err)
	}

	var fields []string = []string{"pid"}
	source := joinStringArrayToKommaSeparatedString(fields)
	var query = fmt.Sprintf(`
	"bool": {
    	"must": [
    	    {"match": {"work.keyword": "%s"}},
			{"match": {"IsFirst": true}}
        ]
    }`, workid)

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, source, 2000)

	// Instantiate a map interface object for storing returned documents
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		return "", fmt.Errorf("json.NewEncoder() ERROR: %s", err)

		// Query is a valid JSON object
	} else {

		// Pass the JSON query to the Golang client's Search() method
		resp, err := client.Search(
			client.Search.WithContext(ctx),
			client.Search.WithIndex(config.LogIndex),
			client.Search.WithBody(read),
			client.Search.WithTrackTotalHits(true),
		)

		// Check for any errors returned by API call to Elasticsearch
		if err != nil {
			return "", fmt.Errorf("Elasticsearch Search() API ERROR: %s", err.Error())

			// If no errors are returned, parse esapi.Response object
		} else {

			// Close the result body when the function call is complete
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				return "", fmt.Errorf("could not read Elasticsearch response, due to %s", err.Error())
			}

			var esLogResponse *types.ESLogResponse = new(types.ESLogResponse)

			if err := json.Unmarshal(body, &esLogResponse); err != nil {
				panic(err)
			}

			for _, hit := range esLogResponse.Hits.Hits {
				return hit.ESLog.PID, nil
			}

		}
	}

	return "", fmt.Errorf("no pid found in index for workid %s", workid)
}

// GetCollectionTitleFromES ...
func GetCollectionTitleFromES(id string) string {

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {

		// TODO logging
		fmt.Printf("Elasticsearch connection ERROR: %s", err)
		return ""
	}

	var fields []string = []string{"title.title"}
	source := joinStringArrayToKommaSeparatedString(fields)
	var query = fmt.Sprintf(`
	"bool": {
    	"must": [
    	    {"match": {"collection.keyword": "%s"}}
        ]
    }`, id)

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, source, 2000)

	// Instantiate a map interface object for storing returned documents
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		return ""

		// Query is a valid JSON object
	} else {

		// Pass the JSON query to the Golang client's Search() method
		resp, err := client.Search(
			client.Search.WithContext(ctx),
			client.Search.WithIndex(config.LogIndex),
			client.Search.WithBody(read),
			client.Search.WithTrackTotalHits(true),
		)

		// Check for any errors returned by API call to Elasticsearch
		if err != nil {
			return ""

			// If no errors are returned, parse esapi.Response object
		} else {

			// Close the result body when the function call is complete
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				return ""
			}

			var esLogResponse *types.ESLogResponse = new(types.ESLogResponse)

			if err := json.Unmarshal(body, &esLogResponse); err != nil {
				log.Fatalf("could not unmarshal elasticsearch response for %s, due to %s", id, err.Error())
			}

			if len(esLogResponse.Hits.Hits) != 0 {
				return esLogResponse.Hits.Hits[0].ESLog.Title.Title
			}

		}
	}
	return ""
}

// GetUpdateDatesFromES ...
func GetUpdateDatesFromES(id string, redisClient *redis.Client) (string, string) {

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {
		return "", ""
	}

	var query = fmt.Sprintf(`
	"bool": {
    	"must": [
    	    {"match": {"record_identifier.keyword": "%s"}}
        ]
    },
	"_source": ["date_indexed","date_modified"]`, id)

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, "", 2000)

	// Instantiate a map interface object for storing returned documents
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		return "", ""

		// Query is a valid JSON object
	} else {

		// Pass the JSON query to the Golang client's Search() method
		resp, err := client.Search(
			client.Search.WithContext(ctx),
			client.Search.WithIndex(config.LogIndex),
			client.Search.WithBody(read),
			client.Search.WithTrackTotalHits(true),
		)

		// Check for any errors returned by API call to Elasticsearch
		if err != nil {
			return "", ""

			// If no errors are returned, parse esapi.Response object
		} else {

			// Close the result body when the function call is complete
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				return "", ""
			}

			var esLogResponse *types.ESLogResponse = new(types.ESLogResponse)

			if err := json.Unmarshal(body, &esLogResponse); err != nil {
				log.Fatalf("could not unmarshal elasticsearch response for %s, due to %s", id, err.Error())
			}

			if len(esLogResponse.Hits.Hits) != 0 {
				return esLogResponse.Hits.Hits[0].ESLog.DateIndexed, esLogResponse.Hits.Hits[0].ESLog.DateModified
			}

			// if not found in ES search in redis (all KV-Pairs are also saved in a redis hash queue)
			redisResult, err := redisClient.HGet(config.OldIndexDates, id).Result()

			if err == nil { // found in redis

				var date OldSolrDatesMsg

				err := json.Unmarshal([]byte(redisResult), &date)
				if err != nil {
					log.Errorf("could not unmarshal redis result %v, due to %s", redisResult, err.Error())
					return "", ""
				}
				return date.Dateindexed, date.Datemodified
			}

			// set new and write to redis

			t := time.Now()
			date := OldSolrDatesMsg{
				id,
				t.UTC().Format(time.RFC3339),
				t.UTC().Format(time.RFC3339),
			}
			b, err := json.Marshal(date)
			if err != nil {
				log.Errorf("could not marshal date %v, due to %s", date, err.Error())
				return "", ""
			}
			redisClient.HSet(config.OldIndexDates, id, b)
			return date.Dateindexed, date.Datemodified
		}
	}
}

// GetUpdateDatesFromSolrNLH ...
func GetUpdateDatesFromSolrNLH(id string, redisClient *redis.Client) (string, string) {

	netClient := connectionPool.Get().(*http.Client)
	defer connectionPool.Put(netClient)

	// TODO move to config
	query := url.QueryEscape(fmt.Sprintf("record_identifier:%s OR collection:%s", id, id))
	apiURL := fmt.Sprintf("/solr/nlh/select?indent=on&q=%s&rows=1&fl=date_indexed,date_modified&wt=json", query)

	reqUrl := config.LiveSolrHost + apiURL

	attempts := 0
	var resp *http.Response
	var err error
	for {
		resp, err = netClient.Get(reqUrl)
		attempts++
		if err != nil {
			time.Sleep(time.Duration(3*attempts) * time.Second)
			if attempts > 4 {
				log.Fatalf("request to Solr failed for %s, due to %s", id, err.Error())
			}
			continue
		}
		break
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("could not read Solr response body for %s, due to %s", id, err.Error())
	}
	_, _ = io.Copy(io.Discard, resp.Body)

	var response types.SolrResponseNLH
	err = json.Unmarshal(body, &response)
	if err != nil {
		log.Fatalf("could not unmarshal solr response for %s, due to %s", id, err.Error())
	}

	if response.Response.NumFound == 1 {
		return response.Response.Docs[0].DateIndexed, response.Response.Docs[0].DateModified
	}

	// if not found in Solr search in redis (all KV-Pairs are also saved in a redis hash queue)
	redisResult, err := redisClient.HGet(config.OldIndexDates, id).Result()

	if err == nil { // found in redis

		var date OldSolrDatesMsg

		err := json.Unmarshal([]byte(redisResult), &date)
		if err != nil {
			log.Errorf("could not unmarshal redis result %s, due to %s", redisResult, err.Error())
			return "", ""
		}
		return date.Dateindexed, date.Datemodified
	}

	// set new and write to redis

	t := time.Now()
	date := OldSolrDatesMsg{
		id,
		t.UTC().Format(time.RFC3339),
		t.UTC().Format(time.RFC3339),
	}
	b, err := json.Marshal(date)
	if err != nil {
		log.Errorf("could not marshal date %v, due to %s", date, err.Error())
		return "", ""
	}
	redisClient.HSet(config.OldIndexDates, id, b)
	return date.Dateindexed, date.Datemodified
}

// GetUpdateDatesFromRedis ...
func GetUpdateDatesFromRedis(recordIdentifier string, redisClient *redis.Client) (string, string) {

	// if not found in Solr search in redis (all KV-Pairs are also saved in a redis hash queue)
	redisResult, err := redisClient.HGet(config.OldIndexDates, recordIdentifier).Result()

	if err == nil { // found in redis

		var date OldSolrDatesMsg

		err := json.Unmarshal([]byte(redisResult), &date)
		if err != nil {
			log.Errorf("could not unmarshal redis result %s, due to %s", redisResult, err.Error())
			return "", ""
		}
		return date.Dateindexed, date.Datemodified
	}

	// set new and write to redis

	t := time.Now()
	date := OldSolrDatesMsg{
		recordIdentifier,
		t.UTC().Format(time.RFC3339),
		t.UTC().Format(time.RFC3339),
	}
	b, err := json.Marshal(date)
	if err != nil {
		log.Errorf("could not marshal date %v, due to %s", date, err.Error())
		return "", ""
	}
	redisClient.HSet(config.OldIndexDates, recordIdentifier, b)
	return date.Dateindexed, date.Datemodified
}

// GetUpdateDatesFromSolrGDZ ...
func GetUpdateDatesFromSolrGDZ(id string, redisClient *redis.Client) (string, string) {

	netClient := connectionPool.Get().(*http.Client)
	defer connectionPool.Put(netClient)

	apiURL := fmt.Sprintf("/solr/gdz/select?indent=on&q=record_identifier:%s&rows=1&fl=date_indexed,date_modified&wt=json", id)

	url := config.LiveSolrHost + apiURL

	attempts := 0
	var resp *http.Response
	var err error
	for {
		resp, err = netClient.Get(url)
		attempts++
		if err != nil {
			time.Sleep(time.Duration(3*attempts) * time.Second)
			if attempts > 4 {
				log.Fatalf("Solr request failed for %s, due to %s", id, err.Error())
			}
			continue
		}
		break
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("could not read Solr response body for %s, due to %s", id, err.Error())
	}
	_, _ = io.Copy(io.Discard, resp.Body)

	var response types.SolrResponseGDZ
	err = json.Unmarshal(body, &response)
	if err != nil {
		log.Fatalf("could not unmarshal solr response for %s, due to %s", id, err.Error())
	}

	if response.Response.NumFound == 1 {
		return response.Response.Docs[0].DateIndexed, response.Response.Docs[0].DateModified
	}

	// if not found in Solr search in redis (all KV-Pairs are also saved in a redis hash queue)
	redisResult, err := redisClient.HGet(config.OldIndexDates, id).Result()

	if err == nil { // found in redis

		var date OldSolrDatesMsg

		err := json.Unmarshal([]byte(redisResult), &date)
		if err != nil {
			log.Errorf("could not unmarshal redis result %s, due to %s", redisResult, err.Error())
			return "", ""
		}
		return date.Dateindexed, date.Datemodified
	}

	// TODO make this persistent for e.g. DINO
	// set new and write to redis

	t := time.Now()
	date := OldSolrDatesMsg{
		id,
		t.UTC().Format(time.RFC3339),
		t.UTC().Format(time.RFC3339),
	}
	b, err := json.Marshal(date)
	if err != nil {
		log.Errorf("could not marshal date %v, due to %s", date, err.Error())
		return "", ""
	}
	redisClient.HSet(config.OldIndexDates, id, b)
	return date.Dateindexed, date.Datemodified
}

// getESLogFromDiv ...
func getESLogFromDiv(
	div types.Div,
	dmdsec types.Dmdsec,
	workID string,
	pid string,
	prevPIDs []string,
	doctype string,
	splittedDmdID string,
	ccontext string,
	product string,
	parentESLog types.ESLog,
	parentInfo *types.Parent,
	redisClient *redis.Client,
	isFirst bool,
	indexAsString string) (types.ESLog, error) {

	esLog := types.ESLog{}

	//--- type (e.g. "PeriodicalVolume")

	var ttype string
	ttype = helper.StrctypeMapping(strings.TrimSpace(div.Type))
	if ttype == "" {
		ttype = div.Type
	}
	esLog.Type = ttype

	esLog.RecordIdentifier = dmdsec.RecordInfo.Recordidentifier.Value
	if dmdsec.RecordInfo.RecordInfoNote.Type == "license" {
		esLog.RightsInfo.License = dmdsec.RecordInfo.RecordInfoNote.Value
	}

	if esLog.RecordIdentifier == "" {
		if pid != "" {
			esLog.RecordIdentifier = pid
		} else if workID != "" {
			esLog.RecordIdentifier = workID
		}
	}

	//--- id and logID (e.g. "PPN345574974_0006|log1")

	// TODO check esLog.ID and esLog.LogID are identical
	// TODO product specific
	//if ccontext == "digizeit" || ccontext == "ocrd" {
	// TODO check this for digizeit

	var id string
	if ccontext == "digizeit" {
		if div.ID != "" {
			esLog.ID = types.Join(workID, "|", div.ID)
			esLog.LogID = types.Join(workID, "|", div.ID)
			id = types.Join(workID, "|", div.ID)
		} else {
			esLog.ID = types.Join(workID, "|", indexAsString)
			esLog.LogID = types.Join(workID, "|", indexAsString)
			id = types.Join(workID, "|", indexAsString)
		}
	} else if ccontext == "ocrd" {
		if div.ID != "" {
			esLog.ID = types.Join(pid, "|", div.ID)
			esLog.LogID = types.Join(pid, "|", div.ID)
			id = types.Join(pid, "|", div.ID)
		} else {
			esLog.ID = types.Join(workID, "|", indexAsString)
			esLog.LogID = types.Join(workID, "|", indexAsString)
			id = types.Join(workID, "|", indexAsString)
		}
	} else if ccontext == "ocrd" {
		if div.ID != "" {
			esLog.ID = types.Join(pid, "|", div.ID)
			esLog.LogID = types.Join(pid, "|", div.ID)
		} else {
			esLog.ID = types.Join(workID, "|", indexAsString)
			esLog.LogID = types.Join(workID, "|", indexAsString)
			id = types.Join(workID, "|", indexAsString)
		}
	} else if ccontext == "ocrd" {
		if div.ID != "" {
			esLog.ID = types.Join(pid, "|", div.ID)
			esLog.LogID = types.Join(pid, "|", div.ID)
			id = types.Join(pid, "|", div.ID)
		} else {
			esLog.ID = types.Join(workID, "|", indexAsString)
			esLog.LogID = types.Join(workID, "|", indexAsString)
			id = types.Join(workID, "|", indexAsString)
		}
	} else { // (ccontext == "nlh") || (ccontext == "gdz")
		//esLog.ID = dmdsec.RecordInfo.Recordidentifier.Value
		esLog.ID = workID
		esLog.LogID = div.ID
		id = esLog.ID + "_" + esLog.LogID
	}

	lock := id
	redisResult, err := redisClient.SetNX(lock, "locked", 60*time.Second).Result()

	if err == nil {
		if !redisResult {
			return types.ESLog{}, fmt.Errorf("logical element %s of %s exists in DB, data must be invalid", lock, workID)
		}
		// go-on
	} else {
		return types.ESLog{}, fmt.Errorf("could not set lock for logical element %s, due to %s", lock, err.Error())
	}

	//--- log (e.g. "log1")
	if div.ID == "" {
		esLog.Log = indexAsString
	} else {
		esLog.Log = div.ID
	}

	//--- dmdid (e.g. "dmdlog1")
	esLog.DmdID = splittedDmdID

	//--- label (e.g. "Juristenzeitung")
	var label string
	label = strings.TrimSpace(div.Label)

	if label == "" {
		label = helper.StrctypeLabelMapping(div.Type)
		if label == "" {
			label = div.Type
		}
	}
	esLog.Label = label

	esLog.Level = div.Level

	if dmdsec.ID != "" {

		//-- Genre

		var genres []string

		for _, genre := range dmdsec.GenresInfos {
			genres = append(genres, genre.Value)
		}

		esLog.Genre = genres

		//--- sponsor

		var sponsors []string

		for _, sponsor := range dmdsec.SponsorshipInfos {
			sponsors = append(sponsors, sponsor.Value)
		}

		esLog.Sponsor = sponsors

		//-- normalized Genre

		var normalizedGenre []string
		normalizedGenre = append(normalizedGenre, dmdsec.NormalizedGenre...)
		esLog.NormalizedGenre = normalizedGenre

		//-- normalized PlaceTerm

		var normalizedPlaceTerm []string
		normalizedPlaceTerm = append(normalizedPlaceTerm, dmdsec.NormalizedPlaceTerm...)
		esLog.NormalizedPlaceTerm = normalizedPlaceTerm

		//--- get CreatorInfo and PersonInfos Bycreator and Byperson

		var pis []types.PersonInfo
		var cis []types.CreatorInfo
		var byc string
		var byp string

		for _, nameInfo := range dmdsec.NameInfos {

			// get creator infos
			if (nameInfo.Roleterm.Value == "cre") || (nameInfo.Roleterm.Value == "aut") {
				var ci types.CreatorInfo

				ci.Type = nameInfo.Type

				if nameInfo.Displayform != "" {
					ci.Name = nameInfo.Displayform
				} else {
					var name map[string]string = make(map[string]string)

					for _, npis := range nameInfo.Namepart {
						name[npis.Type] = npis.Value
					}
					if (name["family"] != "") && (name["given"] != "") {
						ci.Name = types.Join(name["family"], ", ", name["given"])
					} else if name["family"] != "" {
						ci.Name = name["family"]
					} else {
						if name[""] != "" {
							ci.Name = name[""]
						}
						if name["date"] != "" {
							ci.DateString = name["date"]
						}
					}
				}

				if byc == "" {
					byc = ci.Name
				} else {
					byc = types.Join(byc, "; ", ci.Name)
				}

				ci.Roleterm = nameInfo.Roleterm.Value
				ci.RoletermAuthority = nameInfo.Roleterm.Authority
				ci.RoletermType = nameInfo.Roleterm.Type

				if strings.ToLower(nameInfo.Authority) == "gnd" {
					str := nameInfo.ValueURI
					ci.GndURI = str
					i := strings.LastIndex(str, "/")
					ci.GndNumber = str[i+1:]
				}
				ci.Date = nameInfo.Date

				cis = append(cis, ci)
			}

			// get all person infos
			var pi types.PersonInfo

			pi.Type = nameInfo.Type

			if nameInfo.Displayform != "" {
				pi.Name = nameInfo.Displayform
			} else {
				var name map[string]string = make(map[string]string)

				for _, npis := range nameInfo.Namepart {
					name[npis.Type] = npis.Value
				}
				if (name["family"] != "") && (name["given"] != "") {
					pi.Name = types.Join(name["family"], ", ", name["given"])
				} else if name["family"] != "" {
					pi.Name = name["family"]
				} else {
					if name[""] != "" {
						pi.Name = name[""]
					}
					if name["date"] != "" {
						pi.DateString = name["date"]
					}
				}
			}

			if byp == "" {
				byp = pi.Name
			} else {
				byp = types.Join(byp, "; ", pi.Name)
			}

			pi.Roleterm = nameInfo.Roleterm.Value
			pi.RoletermAuthority = nameInfo.Roleterm.Authority
			pi.RoletermType = nameInfo.Roleterm.Type

			if strings.ToLower(nameInfo.Authority) == "gnd" {
				str := nameInfo.ValueURI
				pi.GndURI = str
				i := strings.LastIndex(str, "/")
				pi.GndNumber = str[i+1:]
			}

			pi.Date = nameInfo.Date

			pis = append(pis, pi)

		}

		esLog.CreatorInfos = cis
		esLog.PersonInfos = pis

		esLog.Bycreator = byc
		esLog.Byperson = byp

		//--- Currentno and Currentnosort (from mods>part>detail>number and mods>part[@order])

		esLog.Currentnosort, _ = strconv.ParseInt(dmdsec.PartInfo.Order, 10, 32)

		var nos []string
		for _, dtail := range dmdsec.PartInfo.Detail {
			if dtail.Type == "issue" {
				esLog.IssueNumber = dtail.Number
			} else if dtail.Type == "volume" {
				esLog.VolumeNumber = dtail.Number
			}

			nos = append(nos, dtail.Number)
		}

		esLog.Currentno = nos

		//--- Dc, DcZvdd, DcDDC

		var dc []string
		var dcZvdd []string
		var dcDCC []string
		var dcOther []string
		var dcOtherAuthority []string

		if len(dmdsec.ClassificationInfos) > 0 {
			for _, classi := range dmdsec.ClassificationInfos {

				authority := strings.ToLower(classi.Authority)
				value := classi.Value
				value = strings.ReplaceAll(value, "_", ".")
				value = strings.ReplaceAll(value, " ", ".")
				value = strings.ToLower(value)

				if authority == "ddc" {
					dcDCC = append(dc, value)
				} else if authority == "zvdd" {
					dcZvdd = append(dc, value)
				} else if authority == "dz" {
					dc = append(dc, value)
				} else if authority == "gdz" {
					dc = append(dc, value)
				} else {
					dcOther = append(dcOther, value)
					dcOtherAuthority = append(dcOtherAuthority, authority)
				}
			}

		}

		if len(dmdsec.GatheredIntoInfos) > 0 {
			for _, classi := range dmdsec.GatheredIntoInfos {

				value := classi.Value
				value = strings.ReplaceAll(value, "_", ".")
				value = strings.ReplaceAll(value, " ", ".")
				value = strings.ToLower(value)

				dc = append(dc, value)

			}
		}

		if doctype == "anchor" {
			esLog.Dc = dc
			esLog.DcZvdd = dcZvdd
			esLog.DcDDC = dcDCC
			esLog.DcOther = dcOther
			esLog.DcOtherAuthority = dcOtherAuthority

		} else {
			// children ingerit the dc from the parent
			dc = parentInfo.Dc
			dcDCC = parentInfo.DcDDC
			dcZvdd = parentInfo.DcZvdd
			dcOther = parentInfo.DcOther
			dcOtherAuthority = parentInfo.DcOtherAuthority
		}

		esLog.Dc = dc
		esLog.DcZvdd = dcZvdd
		esLog.DcDDC = dcDCC
		esLog.DcOther = dcOther
		esLog.DcOtherAuthority = dcOtherAuthority

		//--- Hosts, Precedings, Succeedings, Series and RelatedItems

		var hosts []string
		var precedings []string
		var series []string
		var succeedings []string
		var relatedItems []types.RelatedItemInfos

		if len(dmdsec.Relateditems) > 0 {
			for _, ri := range dmdsec.Relateditems {

				ri.RelateditemID = ri.RecordInfo.Recordidentifier.Value

				if ri.Type == "host" {
					hosts = append(hosts, ri.RelateditemID)
				} else if ri.Type == "preceding" {
					precedings = append(precedings, ri.RelateditemID)
				} else if ri.Type == "series" {
					series = append(series, ri.RelateditemID)
				} else if ri.Type == "succeeding" {
					succeedings = append(succeedings, ri.RelateditemID)
				}

				var title string
				var titleAbbreviated string
				var titlePartname string

				if len(ri.TitleInfo) > 0 {
					for _, tinf := range ri.TitleInfo {

						titleInfo := types.ConvertTitleInfo(&tinf)

						if titleInfo.Type == "abbreviated" {
							titleAbbreviated = titleInfo.Title
						} else {
							if title != "" {
								continue
							}
							title = titleInfo.Title
							titlePartname = titleInfo.Partname
						}
					}
				}

				relatedItems = append(relatedItems, types.RelatedItemInfos{
					RelateditemType:             ri.Type,
					RelateditemTitle:            title,
					RelateditemID:               ri.RelateditemID,
					RelateditemTitlePartname:    titlePartname,
					RelateditemTitleAbbreviated: titleAbbreviated,
					RelateditemNote:             ri.RelateditemNote,
				})

			}
		}

		esLog.Hosts = hosts
		esLog.Precedings = precedings
		esLog.Succeedings = succeedings
		esLog.Series = series
		esLog.RelatedItemInfos = relatedItems

		//---

		if doctype == "work" {
			esLog.Isanchor = false
			esLog.Iswork = true
			esLog.Isparent = true

		} else if doctype == "anchor" {
			esLog.Isanchor = true
			esLog.Iswork = false
			esLog.Isparent = true
		}

		//-- identifier ([]string), identifierInfo ([]types.Identifier), Issn, ZDB, Isanchor, Iswork, Isparent

		var ids []string
		var idinfos []types.Identifier

		for _, id := range dmdsec.Identifiers {
			if strings.ToLower(id.Type) == "zdb-id" && esLog.Zdb == "" {
				esLog.Zdb = id.Value
			}
			if strings.ToLower(id.Type) == "issn" && esLog.Issn == "" {

				issn := strings.Replace(id.Value, "ISSN", "", -1)
				issn = strings.Replace(issn, " ", "", -1)
				issn = strings.Replace(issn, "-", "", -1)
				esLog.Issn = strings.ToLower(issn)
			}

			ids = append(ids, id.Value)
			idinfos = append(idinfos, id)
		}

		esLog.Identifier = ids
		esLog.IdentifierInfo = idinfos

		//-- Purl

		// todo switch template for nlh, gdz, digizeit
		// TODO product specific
		if ccontext == "nlh" {
			esLog.Purl = fmt.Sprintf(config.PurlURI, dmdsec.RecordInfo.Recordidentifier.Value)
		} else if ccontext == "ocrd" {
			esLog.Purl = fmt.Sprintf(config.PurlURIOcrd, pid)
		} else {
			if isFirst {
				esLog.Purl = fmt.Sprintf(config.PurlURI, esLog.RecordIdentifier)
			} else {
				esLog.Purl = fmt.Sprintf(config.PurlURI, esLog.ID)
			}
		}

		//-- Catalogue

		if dmdsec.RecordInfo.Recordidentifier.Source == "gbv-ppn" {
			// unAPI is no longer active and the k10plus does not support info for volumes
			// TODO check alternative
		} else if dmdsec.RecordInfo.Recordidentifier.Source == "DE-611" {
			exist := helper.CheckKalliope(esLog.RecordIdentifier)
			if exist {
				esLog.Catalogue = fmt.Sprintf(config.KalliopeURI+config.KalliopePath, esLog.RecordIdentifier)
			}
		}

		//-- Iscontribution

		esLog.Iscontribution = helper.Contains(config.ContributionTypes, strings.ToLower(div.Type))

		//-- Lang, ScriptTerm

		var lang []string
		var scriptterm []string

		for _, l := range dmdsec.LanguageInfos {
			if l.LanguageTerm.Value == "und" && l.ScriptTerm.Value != "" {
				scriptterm = append(scriptterm, l.ScriptTerm.Value)
			} else {
				lang = append(lang, l.LanguageTerm.Value)
			}

		}

		esLog.Lang = lang
		esLog.Scriptterm = scriptterm

		//--- NoteInfos

		esLog.NoteInfos = dmdsec.NoteInfos

		//--- PhysicalDescriptionInfos

		esLog.PhysicalDescriptionInfos = &dmdsec.PhysicalDescriptionInfo

		//--- publishInfo and digitizationInfo. else derived from parent

		var digitizationInfo *types.DigitizationInfo
		var publishInfo *types.PublishInfo

		if len(dmdsec.OriginInfos) > 0 {
			for _, oi := range dmdsec.OriginInfos {

				if (len(oi.DateCaptured) > 0 && len(oi.DateIssued) == 0 && len(oi.DateCreated) == 0) || (oi.EventType == "digitization") || (oi.Edition == "[Electronic ed.]") {
					// digitization -> dateCaptured (separated digitization info)

					digitizationInfo = types.GetDigitizationInfo(oi)

				} else if (len(oi.DateIssued) > 0 || len(oi.DateCreated) > 0) && (len(oi.DateCaptured) == 0) {
					// publish -> dateIssued or dateCreated (separated publishing info)

					publishInfo = types.GetPublishInfo(oi, product, ccontext)

				} else if len(oi.DateCaptured) > 0 && (len(oi.DateIssued) > 0 || len(oi.DateCreated) > 0) {
					// integrated publish and digitization info (integrated digitization and publishing info)

					digitizationInfo = types.GetDigitizationInfoSimplified(oi)
					publishInfo = types.GetPublishInfo(oi, product, ccontext)

				} else if len(oi.Publisher) > 0 || len(oi.Place) > 0 {
					publishInfo = types.GetPublishInfo(oi, product, ccontext)
				}
			}

		} else {
			publishInfo = parentESLog.PublishInfos
			digitizationInfo = parentESLog.DigitizationInfos
		}

		esLog.PublishInfos = publishInfo
		esLog.DigitizationInfos = digitizationInfo

		//--- rightsAccessConditionInfos

		var isExternalContent bool
		var license string
		var licenseForUseAndReproduction []string
		var rac []types.RightAccessCondition
		if len(dmdsec.AccessConditions) > 0 {

			for _, accesscondition := range dmdsec.AccessConditions {

				// TODO product specific
				if (ccontext == "gdz") || (ccontext == "nlh") {
					if accesscondition.Type == "use and reproduction" {

						licenseForUseAndReproduction = append(licenseForUseAndReproduction, accesscondition.Value)

						ac := helper.AccessConditionMapping(accesscondition.Value)

						if ac != "" {
							rac = append(rac, types.RightAccessCondition{
								Value: ac,
								URL:   accesscondition.Value,
							})
						} else {
							rac = append(rac, types.RightAccessCondition{
								URL: accesscondition.Value,
							})
						}
					}
				} else if ccontext == "digizeit" {
					if accesscondition.Type == "dz" {
						if checkIsOA(accesscondition.Value) {
							rac = append(rac, types.RightAccessCondition{
								Type:                "license",
								License:             strings.ToLower(accesscondition.Value),
								LicenseDisplaylabel: accesscondition.DisplayLabel,
								NormalizedPublisher: helper.NomalizePublisherName(accesscondition.Value),
							})

							if esLog.RightsInfo.License == "" {
								license = accesscondition.DisplayLabel
								esLog.RightsInfo.License = license
								esLog.License = license
							}

						} else {
							if accesscondition.Value == "externalcontent" {
								isExternalContent = true
							}

							rac = append(rac, types.RightAccessCondition{
								Value:               strings.ToLower(accesscondition.Value),
								Type:                accesscondition.Type,
								NormalizedPublisher: helper.NomalizePublisherName(accesscondition.Value),
							})

						}
					} else if accesscondition.Type == "copyright" {
						rac = append(rac, types.RightAccessCondition{
							Copyright: strings.ToLower(accesscondition.Value),
							Type:      accesscondition.Type,
						})
					}
				}
			}
		} else if parentESLog.RightsAccessConditionInfos != nil && len(parentESLog.RightsAccessConditionInfos) > 0 {
			rac = parentESLog.RightsAccessConditionInfos
		}
		esLog.RightsAccessConditionInfos = rac

		if isExternalContent {
			esLog.IsExternalContent = isExternalContent
		} else {
			// if parent is external, inherit the value
			esLog.IsExternalContent = parentESLog.IsExternalContent
		}

		if len(licenseForUseAndReproduction) > 0 {
			esLog.LicenseForUseAndReproduction = licenseForUseAndReproduction
		}

		//--- Shelfmark from dmdsec (in addition to the alternative shelfmark location, see: "from first phys. structMap div")

		var shelfmarks []types.Shelfmark

		for _, locations := range dmdsec.Locations {

			for _, physDesc := range locations.PhysicalLocationInfos {
				if strings.ToLower(physDesc.Type) == "shelfmark" {
					shelfmarks = append(shelfmarks, types.Shelfmark{Shelfmark: physDesc.Value})
				}

			}

			for _, shelfLoc := range locations.ShelfLocationInfos {
				shelfmarks = append(shelfmarks, types.Shelfmark{Shelfmark: shelfLoc.Value})
			}
		}

		esLog.Shelfmark = shelfmarks

		//--- subjectInfos

		if dmdsec.SubjectInfos != nil {
			esLog.SubjectInfos = dmdsec.SubjectInfos
		} else {
			// derive from parent
			esLog.SubjectInfos = parentESLog.SubjectInfos

		}

		//--- Title, Bytitle

		var bytitle string
		var titleAbbreviated string
		var titleAlternative string
		var titleUniform string
		var titleInfo *types.TitleInfo

		for _, ti := range dmdsec.TitleInfos {

			if ti.Type == "uniform" {
				titleUniform = ti.Title
			} else if ti.Type == "abbreviated" {
				titleAbbreviated = ti.Title
			} else if ti.Type == "alternative" {
				titleAlternative = ti.Title
			} else {
				titleInfo = types.ConvertTitleInfo(&ti)
				esLog.Title = titleInfo

				if bytitle == "" {
					bytitle = titleInfo.Sorttitle
				} else {
					bytitle = types.Join(bytitle, "; ", titleInfo.Sorttitle)
				}
			}
		}

		if titleInfo != nil {
			if titleAbbreviated != "" {
				esLog.Title.TitleAbbreviated = titleAbbreviated
			}

			if titleAlternative != "" {
				esLog.Title.TitleAlternative = titleAlternative
			}

			space := regexp.MustCompile(`\s+`)

			if titleUniform != "" {
				esLog.Title.TitleUniform = space.ReplaceAllString(titleUniform, " ")
				esLog.Title.TitleOriginal = space.ReplaceAllString(titleUniform, " ")
			} else {
				esLog.Title.TitleOriginal = space.ReplaceAllString(titleInfo.Title, " ")
			}

			esLog.Bytitle = bytitle

		}

		if strings.Contains(product, "nlh-ahn") {
			esLog.Productseries = product
			esLog.Product = "nlh-ahn"
		} else {
			esLog.Productseries = product
			esLog.Product = product
		}

		// TODO product specific
		if ccontext == "nlh" {

			if esLog.Iswork {
				if helper.Contains(config.DatebasedProducts, esLog.Product) {

					var match []string
					re := regexp.MustCompile("\\S*(\\d{4}).(\\d{2}|\\w{3})[\\S]?(\\d{2})")

					if esLog.Product == "nlh-mms" || strings.Contains(esLog.Product, "nlh-ahn") {
						// special case nlh-mms: 	1854-12-01, 	info comes from originInfo>dateIssued
						// special case nlh-ahn:	1805-04-09, 	info comes from originInfo>dateIssued OR part>detail>number
						if esLog.PublishInfos.YearPublishString != "" {
							match = re.FindStringSubmatch(esLog.PublishInfos.YearPublishString)
						} else if esLog.IssueNumber != "" {
							match = re.FindStringSubmatch(esLog.IssueNumber)
						}
					} else {
						// 0FFO-1785-0630 OR 0FFO-1785-AUG30
						match = re.FindStringSubmatch(workID)

					}

					if len(match) != 4 {
						log.Printf("INFO navi* cannot be determined, because Work %v doesn't match pattern /\\S*(\\d{4}).(\\d{2}|\\w{3})[\\S]?(\\d{2})/", div.WorkID)
					} else {
						esLog.NaviYear, _ = strconv.ParseInt(match[1], 10, 64)
						esLog.NaviMonth, _ = strconv.ParseInt(helper.MonthMapping(match[2]), 10, 64)
						esLog.NaviDay, _ = strconv.ParseInt(match[3], 10, 64)
						esLog.NaviString = fmt.Sprintf("%s-%s-%s", match[1], helper.MonthMapping(match[2]), match[3])
						t, _ := time.Parse("2006-01-02", esLog.NaviString)
						esLog.NaviDate = t.UTC().Format(time.RFC3339)

						esLog.Title.Title = esLog.Title.Title + " (" + esLog.NaviString + ")"
					}

				}
			}

		}
	} else {
		esLog.Product = product
		esLog.Productseries = product
		esLog.Context = ccontext

		if ccontext == "ocrd" {
			esLog.ID = types.Join(pid, "|", div.ID)
		} else {
			esLog.ID = types.Join(workID, "|", div.ID)
		}

		esLog.Type = div.Type
		esLog.Label = div.Label
	}

	return esLog, nil
}

func checkIsOA(accesscondition string) bool {
	ac := strings.ToLower(accesscondition)
	re := regexp.MustCompile(`\s{2,}`)

	s := re.ReplaceAllString(ac, " ")

	oaPrefixes := strings.Split(config.OAPrefixes, ",")
	for _, a := range oaPrefixes {
		if strings.Contains(s, a) {
			return true
		}
	}
	return false
}

// WriteToElasticSearch ...
func WriteToElasticSearch(esDoc types.ESDoc) {

	first := true

	// sort by order
	sortSlice := make(types.ESLogSortSlice, 0, len(esDoc.ESLogMap))
	for _, d := range esDoc.ESLogMap {
		sortSlice = append(sortSlice, d)
	}

	sort.Sort(sortSlice)

	for _, logObj := range sortSlice {
		var id string

		if first {
			if logObj.Context != "ocrd" {
				DeleteFromElasticSearch(logObj.Work, logObj.Collection, logObj.PID, logObj.Doctype)
			} else {
				if logObj.PrevPID != "" {
					DeleteFromElasticSearch(logObj.Work, logObj.Collection, logObj.PrevPID, "pid")
				}
				// New Object, do not delete
				// else {
				// 	DeleteFromElasticSearch(logObj.Work, logObj.Collection, logObj.PID, "pid")
				// }
			}

			first = false
		}
		requestBody, err := json.Marshal(logObj)
		if err != nil {
			log.Errorf("could not marshal log object for %s, due to %s", logObj.Work, err.Error())
		}

		// todo check ID vs PID
		if logObj.Context != "ocrd" {
			if logObj.Doctype == "anchor" {
				id = logObj.Collection
			} else if logObj.Doctype == "work" {
				id = logObj.LogID
			}
		} else {
			id = logObj.LogID
		}

		writeToESLogJobs <- types.ESWriteJob{
			ID:      id,
			Type:    "logical",
			Content: requestBody}

	}

	for _, physObj := range esDoc.ESPhysMap {
		var id string

		if len(esDoc.ESLogMap) == 0 {
			if first {
				if physObj.Context == "ocrd" {

					if physObj.PrevPID != "" {
						DeleteFromElasticSearch(physObj.Work, "", physObj.PrevPID, "pid")
					}
					// New Object, do not delete
					// else {
					// 	DeleteFromElasticSearch(physObj.Work, "", physObj.PID, "pid")
					// }
				}

				first = false
			}
		}
		requestBody, err := json.Marshal(physObj)
		if err != nil {
			// TODO check if fatal is to strong
			log.Fatalf("could not marshal phys object for %s, due to %s", physObj.Work, err.Error())
		}

		if physObj.Context != "ocrd" {
			id = physObj.Work
		} else {
			id = physObj.ID
		}

		writeToESPhysJobs <- types.ESWriteJob{
			ID:      id,
			Type:    "physical",
			Content: requestBody,
		}

	}

}

// WriteToLogIndex ...
func WriteToLogIndex() {

	netClient := connectionPool.Get().(*http.Client)
	defer connectionPool.Put(netClient)

	apiResource := fmt.Sprintf("%s/_doc/", config.LogIndex)
	url := config.ElasticsearchHost + apiResource

	for writeToESLogJob := range writeToESLogJobs {

		attempts := 1
		var resp *http.Response
		var err error

		for {
			if netClient == nil {
				netClient = connectionPool.Get().(*http.Client)
			}
			resp, err = netClient.Post(url, "application/json", bytes.NewBuffer(writeToESLogJob.Content))
			attempts++
			if err != nil {
				time.Sleep(time.Duration(30*attempts) * time.Second)
				if attempts > 5 {
					log.Errorf("could not post logical index document %s to elasticsearch, due to %s", writeToESLogJob.ID, err.Error())
					time.Sleep(2 * time.Second)
					return
				}
				log.Errorf("could not post logical index document %s to elasticsearch (%v), due to %s", writeToESLogJob.ID, 30*attempts, err.Error())
				continue
			}
			break
		}

		if resp.StatusCode > 300 {
			log.Errorf("indexing of logical document %s failed, due to %s", writeToESLogJob.ID, resp.Status)
		}
		resp.Body.Close()
	}

}

// WriteToPhysIndex ...
func WriteToPhysIndex() {

	netClient := connectionPool.Get().(*http.Client)
	defer connectionPool.Put(netClient)

	apiResource := fmt.Sprintf("%s/_doc/", config.PhysIndex)
	url := config.ElasticsearchHost + apiResource

	for writeToESPhysJob := range writeToESPhysJobs {

		attempts := 1
		var resp *http.Response
		var err error

		for {
			if netClient == nil {
				netClient = connectionPool.Get().(*http.Client)
			}
			resp, err = netClient.Post(url, "application/json", bytes.NewBuffer(writeToESPhysJob.Content))
			attempts++
			if err != nil {
				time.Sleep(time.Duration(30*attempts) * time.Second)
				if attempts > 5 {
					log.Errorf("could not post physical index document %s to elasticsearch, due to %s", writeToESPhysJob.ID, err.Error())
					time.Sleep(2 * time.Second)
					return
				}
				log.Errorf("could not post physical index document %s to elasticsearch (%v), due to %s", writeToESPhysJob.ID, 30*attempts, err.Error())
				continue
			}
			break
		}

		if resp.StatusCode > 300 {
			log.Errorf("indexing of physical document %s failed, due to %s\nurl: %s\nresp: %v", writeToESPhysJob.ID, resp.Status, url, resp)
		}

		resp.Body.Close()
	}
}

// DeletePIDQuery ...
type DeletePIDQuery struct {
	DeletePIDMatch DeletePIDMatch `json:"query"`
}

// DeletePIDMatch ...
type DeletePIDMatch struct {
	DeletePID DeletePID `json:"match"`
}

// DeletePID ...
type DeletePID struct {
	PID string `json:"pid.keyword"`
}

// DeleteWorkQuery ...
type DeleteWorkQuery struct {
	DeleteWorkMatch DeleteWorkMatch `json:"query"`
}

// DeleteWorkMatch ...
type DeleteWorkMatch struct {
	DeleteWork DeleteWork `json:"match"`
}

// DeleteWork ...
type DeleteWork struct {
	Work string `json:"work.keyword"`
}

// DeleteAnchorQuery ...
type DeleteAnchorQuery struct {
	DeleteAnchorMatch DeleteAnchorMatch `json:"query"`
}

// DeleteAnchorMatch ...
type DeleteAnchorMatch struct {
	DeleteCollection DeleteCollection `json:"match"`
}

// DeleteCollection ...
type DeleteCollection struct {
	Collection string `json:"collection.keyword"`
}

// DeleteFromElasticSearch ...
func DeleteFromElasticSearch(work string, collection string, pid string, doctype string) {

	var apiResource string

	var deletePIDQuery DeletePIDQuery
	var deleteWorkQuery DeleteWorkQuery
	var deleteAnchorQuery DeleteAnchorQuery

	apiURL := config.ElasticsearchHost

	if doctype == "anchor" {
		// TODO move to config
		apiResource = fmt.Sprintf("%s/_delete_by_query", config.LogIndex)
		deleteAnchorQuery = DeleteAnchorQuery{
			DeleteAnchorMatch: DeleteAnchorMatch{
				DeleteCollection: DeleteCollection{
					Collection: collection}}}
	} else if doctype == "work" {
		// TODO move to config
		apiResource = fmt.Sprintf("%s,%s/_delete_by_query", config.LogIndex, config.PhysIndex)
		deleteWorkQuery = DeleteWorkQuery{
			DeleteWorkMatch: DeleteWorkMatch{
				DeleteWork: DeleteWork{
					Work: work}}}
	} else {
		// TODO move to config
		apiResource = fmt.Sprintf("%s,%s/_delete_by_query", config.LogIndex, config.PhysIndex)
		deletePIDQuery = DeletePIDQuery{
			DeletePIDMatch: DeletePIDMatch{
				DeletePID: DeletePID{
					PID: pid}}}
	}

	u, _ := url.ParseRequestURI(apiURL)
	u.Path = apiResource

	urlStr := fmt.Sprintf("%v", u)

	var requestBody []byte
	var err error

	if doctype == "anchor" {
		requestBody, err = json.Marshal(deleteAnchorQuery)
	} else if doctype == "work" {
		requestBody, err = json.Marshal(deleteWorkQuery)
	} else {
		requestBody, err = json.Marshal(deletePIDQuery)
	}
	if err != nil {
		log.Fatalln(err)
	}

	resp, err := http.Post(urlStr, "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()
}

func GetExportDataFromElastictSearch(citationData types.ExportWork) (types.ExportData, error) {

	exportData := types.ExportData{}

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {
		return types.ExportData{}, fmt.Errorf("Elasticsearch connection ERROR: %s", err)
	}

	var query = fmt.Sprintf(`
	"bool": {
    	"must": [
    	    {"match": {"record_identifier.keyword": "%s"}}
        ]
    }`, citationData.WorkID)

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, "", 2000)

	// Instantiate a map interface object for storing returned documents
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		log.Fatalf("json.NewEncoder() ERROR:", err)

		// Query is a valid JSON object
	} else {
		// Pass the JSON query to the Golang client's Search() method
		resp, err := client.Search(
			client.Search.WithContext(ctx),
			client.Search.WithIndex(config.LogIndex),
			client.Search.WithBody(read),
			client.Search.WithTrackTotalHits(true),
		)

		// Check for any errors returned by API call to Elasticsearch
		if err != nil {
			return types.ExportData{}, fmt.Errorf("Elasticsearch Search() API ERROR: %s", err)

			// If no errors are returned, parse esapi.Response object
		} else {

			// Close the result body when the function call is complete
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				return types.ExportData{}, fmt.Errorf("could not read Elasticsearch response, due to %s", err.Error())
			}

			var esLogResponse *types.ESLogResponse = new(types.ESLogResponse)

			if err := json.Unmarshal(body, &esLogResponse); err != nil {
				panic(err)
			}

			if len(esLogResponse.Hits.Hits) == 0 {
				return types.ExportData{}, fmt.Errorf("no matching entry in elasticsearch for %s", citationData.WorkID)
			}

			var esDoc types.ESLog = esLogResponse.Hits.Hits[0].ESLog

			// NLH: newspaper, monograph, issue, 'multipart monograph', volume, periodical
			// GDZ: monograph, volume, multivolume_work, manuscript, map, periodical, file, contained_work, section, folder, bundle, periodicalvolume,
			// DINO:
			// TODO check this
			if esDoc.Doctype == "periodical" {
				exportData.IsPeriodical = true
			} else if esDoc.Doctype == "volume" {
				exportData.IsVolume = true
			}

			if esDoc.Isanchor {
				exportData.ID = esDoc.Collection
			} else {
				exportData.ID = esDoc.Work
			}

			exportData.Title = esDoc.Title.Title

			if len(esDoc.PublishInfos.Publisher) > 0 || len(esDoc.PublishInfos.Place) > 0 {
				exportData.Publisher = esDoc.PublishInfos.Publisher
				exportData.PublishingPlace = esDoc.PublishInfos.Place
				exportData.PublishingYear = esDoc.PublishInfos.YearPublish
				exportData.PublishingYearString = esDoc.PublishInfos.YearPublishString
			}

			var series []string
			for _, riInfo := range esDoc.RelatedItemInfos {
				if strings.ToLower(riInfo.RelateditemType) == "series" {
					series = append(series, riInfo.RelateditemTitle)
				}
			}
			if len(series) > 0 {
				exportData.Series = strings.Join(series[:], ", ")
			}

			var authors []string
			for _, creInfor := range esDoc.CreatorInfos {
				authors = append(authors, creInfor.Name)
			}
			if len(authors) > 0 {
				exportData.Author = authors
			}

			var editors []string
			for _, persInfo := range esDoc.PersonInfos {
				if persInfo.Type == "edt" {
					editors = append(editors, persInfo.Name)
				}
			}
			if len(editors) > 0 {
				exportData.Editor = strings.Join(editors[:], ", ")
			}

			if esDoc.Issn != "" {
				exportData.Issn = esDoc.Issn
			}

			if len(esDoc.Lang) > 0 {
				exportData.Language = esDoc.Lang
			}

			var url string
			// TODO check ocrd?
			// TODO product specific
			if esDoc.Context == "digizeit" {
				url = fmt.Sprintf(config.IiifViewerResolvingURLDigizeit, esDoc.RecordIdentifier)
			} else if esDoc.Context == "gdz" {
				url = fmt.Sprintf(config.IiifViewerResolvingURLGDZ, esDoc.RecordIdentifier)
			} else if esDoc.Context == "nlh" {
				url = fmt.Sprintf(config.IiifViewerResolvingURLNLH, esDoc.RecordIdentifier, esDoc.Product)
			}
			if url != "" {
				exportData.URL = url
			}

			exportData.Volume = strings.Join(esDoc.Currentno[:], ", ")

			return exportData, nil
		}
	}
	return types.ExportData{}, nil
}

// func GetPrevPIDsFromES(prevPID string) (*types.PrevInfo, error) {
func GetPrevPIDsFromES(prevPID string, index string) ([]string, error) {

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {
		return nil, fmt.Errorf("elasticsearch connection ERROR: %s", err)
	}

	var query = fmt.Sprintf(`
	"bool": {
    	"must": [
    	    {"match": {"pid.keyword": "%s"}}
        ]
    }`, prevPID)

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, "\"prevPIDs\"", 1)

	// Instantiate a map interface object for storing returned documents
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		return nil, fmt.Errorf("json.NewEncoder() ERROR: %s", err)

		// Query is a valid JSON object
	} else {
		var resp *esapi.Response
		var err error
		// Pass the JSON query to the Golang client's Search() method
		if index == "log" {
			resp, err = client.Search(
				client.Search.WithContext(ctx),
				client.Search.WithIndex(config.LogIndex),
				client.Search.WithBody(read),
				client.Search.WithTrackTotalHits(true),
			)

			// Check for any errors returned by API call to Elasticsearch
			if err != nil {
				return nil, fmt.Errorf("elasticsearch Search() API ERROR: %s", err)

				// If no errors are returned, parse esapi.Response object
			} else {

				// Close the result body when the function call is complete
				defer resp.Body.Close()

				body, err := io.ReadAll(resp.Body)
				if err != nil {
					return nil, fmt.Errorf("could not read Elasticsearch response, due to %s", err.Error())
				}

				var esLogResponse *types.ESLogResponse = new(types.ESLogResponse)

				if err := json.Unmarshal(body, &esLogResponse); err != nil {
					panic(err)
				}

				if len(esLogResponse.Hits.Hits) == 0 {
					return nil, fmt.Errorf("no index document available for  PID %s", prevPID)
				}

				result := esLogResponse.Hits.Hits[0].ESLog

				return result.PrevPIDs, nil
			}

		} else {
			resp, err = client.Search(
				client.Search.WithContext(ctx),
				client.Search.WithIndex(config.PhysIndex),
				client.Search.WithBody(read),
				client.Search.WithTrackTotalHits(true),
			)

			// Check for any errors returned by API call to Elasticsearch
			if err != nil {
				return nil, fmt.Errorf("elasticsearch Search() API ERROR: %s", err)

				// If no errors are returned, parse esapi.Response object
			} else {

				// Close the result body when the function call is complete
				defer resp.Body.Close()

				body, err := io.ReadAll(resp.Body)
				if err != nil {
					return nil, fmt.Errorf("could not read Elasticsearch response, due to %s", err.Error())
				}

				var esPhysResponse *types.ESPhysResponse = new(types.ESPhysResponse)

				if err := json.Unmarshal(body, &esPhysResponse); err != nil {
					panic(err)
				}

				if len(esPhysResponse.Hits.Hits) == 0 {
					return nil, fmt.Errorf("no index document available for  PID %s", prevPID)
				}

				result := esPhysResponse.Hits.Hits[0].ESPhys

				return result.PrevPIDs, nil
			}

		}
	}

}

func GetParentInfoFromES(parentRecordID string) (*types.Parent, error) {

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {
		return nil, fmt.Errorf("elasticsearch connection ERROR: %s", err)
	}

	var query = fmt.Sprintf(`
	"bool": {
    	"must": [
    	    {"match": {"collection.keyword": "%s"}}
        ]
    }`, parentRecordID)

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, "", 2000)

	// Instantiate a map interface object for storing returned documents
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		return nil, fmt.Errorf("json.NewEncoder() ERROR: %s", err.Error())

		// Query is a valid JSON object
	} else {
		// Pass the JSON query to the Golang client's Search() method
		resp, err := client.Search(
			client.Search.WithContext(ctx),
			client.Search.WithIndex(config.LogIndex),
			client.Search.WithBody(read),
			client.Search.WithTrackTotalHits(true),
		)

		// Check for any errors returned by API call to Elasticsearch
		if err != nil {
			return nil, fmt.Errorf("elasticsearch Search() API ERROR: %s", err)

			// If no errors are returned, parse esapi.Response object
		} else {

			// Close the result body when the function call is complete
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				return nil, fmt.Errorf("could not read Elasticsearch response, due to %s", err.Error())
			}

			var esLogResponse *types.ESLogResponse = new(types.ESLogResponse)

			if err := json.Unmarshal(body, &esLogResponse); err != nil {
				panic(err)
			}

			if len(esLogResponse.Hits.Hits) == 0 {
				return nil, fmt.Errorf("logical element %s doesn't exists in Eslasticsearch", parentRecordID)
			}

			var parent *types.Parent = new(types.Parent)

			result := esLogResponse.Hits.Hits[0].ESLog

			parent.Type = result.Type
			parent.RecordID = parentRecordID
			parent.RecordIdentifier = result.RecordIdentifier
			parent.Log = result.Log
			parent.ParentID = result.LogID
			parent.RelatedItemInfos = result.RelatedItemInfos

			parent.Title = result.Title
			parent.Zdb = result.Zdb
			parent.Issn = result.Issn
			parent.Dc = result.Dc
			parent.DcDDC = result.DcDDC
			parent.DcZvdd = result.DcZvdd
			parent.DcOther = result.DcOther
			parent.DcOtherAuthority = result.DcOtherAuthority

			parent.SubjectInfos = result.SubjectInfos
			parent.PublishInfos = result.PublishInfos
			parent.RightsAccessConditionInfos = result.RightsAccessConditionInfos

			parent.Purl = result.Purl
			parent.Catalogue = result.Catalogue
			return parent, nil
		}
	}

	return nil, nil

}

func constructQuery(q string, source string, size int) *strings.Reader {
	if source == "" {
		source = "\"*\""
	}
	// Build a query string from string passed to function
	var query = `{"query": {`

	// Concatenate query string with string passed to method call
	query = query + q

	// Use the strconv.Itoa() method to convert int to string
	query = query + `}, "_source": [` + source + `], "size": ` + strconv.Itoa(size) + `}`

	// Check for JSON errors
	isValid := json.Valid([]byte(query)) // returns bool

	// Default query is "{}" if JSON is invalid
	if !isValid {
		log.Errorf("using the default query match_all, due to incorrect query: %s", query)
		query = "{}"
	}

	// Build a new string from JSON query
	var b strings.Builder
	b.WriteString(query)

	// Instantiate a *strings.Reader object from string
	read := strings.NewReader(b.String())

	// Return a *strings.Reader object
	return read
}

func joinStringArrayToKommaSeparatedString(strArr []string) string {
	builder := strings.Builder{}
	for i, el := range strArr {
		if i < len(strArr)-1 {
			builder.WriteString("\"")
			builder.WriteString(el)
			builder.WriteString("\"")
			builder.WriteString(",")
		} else {
			builder.WriteString("\"")
			builder.WriteString(el)
			builder.WriteString("\"")

		}
	}
	return builder.String()
}

// TODO move list to config
func belongsToList(lookup string) bool {
	list := []string{
		"DEFAULT",
		"MAX",
		"MIN",
		"PRESENTATION",
		"THUMB",
		"GDZ_OCR",
	}
	for _, val := range list {
		if val == lookup {
			return true
		}
	}
	return false
}
