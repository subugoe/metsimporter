package index

import (
	"fmt"
	"strings"
	"unicode/utf8"

	"gitlab.gwdg.de/subugoe/metsimporter/indexer/types"
)

var ()

func init() {

}

func GetExportDataFromIndex(citationData types.ExportWork) types.ExportData {

	var exportData types.ExportData

	var err error

	// TODO product specific
	if citationData.Context == "digizeit" {
		exportData, err = GetExportDataFromElastictSearch(citationData)
	} else if citationData.Context == "gdz" {
		exportData, err = GetExportDataFromSolrGDZ(citationData)
	} else if citationData.Context == "nlh" {
		exportData, err = GetExportDataFromSolrNLH(citationData)
	}

	if err != nil {
		log.Errorf("Could not create export data, %s", err.Error())
	}

	return exportData
}

func GetExportDataFromEsLog(esLog types.ESLog) (types.ExportData, error) {

	exportData := types.ExportData{}

	// NLH: newspaper, monograph, issue, 'multipart monograph', volume, periodical
	// GDZ: monograph, volume, multivolume_work, manuscript, map, periodical, file, contained_work, section, folder, bundle, periodicalvolume,
	// DINO:
	// TODO check this
	if esLog.Doctype == "periodical" {
		exportData.IsPeriodical = true
	} else if esLog.Doctype == "volume" {
		exportData.IsVolume = true
	}

	if esLog.Isanchor {
		exportData.ID = esLog.Collection
	} else {
		exportData.ID = esLog.Work
	}

	// from unicode to ascii
	if esLog.Title.Title != "" {
		exportData.Title = getAsciiFromUnicode(esLog.Title.Title)
	}

	if esLog.PublishInfos != nil {
		if len(esLog.PublishInfos.Publisher) > 0 || len(esLog.PublishInfos.Place) > 0 {
			exportData.Publisher = esLog.PublishInfos.Publisher
			exportData.PublishingPlace = esLog.PublishInfos.Place
			exportData.PublishingYear = esLog.PublishInfos.YearPublish
			exportData.PublishingYearString = esLog.PublishInfos.YearPublishString
		}
	}

	var series []string
	for _, riInfo := range esLog.RelatedItemInfos {
		if strings.ToLower(riInfo.RelateditemType) == "series" {
			series = append(series, riInfo.RelateditemTitle)
		}
	}
	if len(series) > 0 {
		exportData.Series = strings.Join(series[:], ", ")
	}

	var authors []string
	for _, creInfor := range esLog.CreatorInfos {
		authors = append(authors, getAsciiFromUnicode(creInfor.Name))
	}
	if len(authors) > 0 {
		exportData.Author = authors
	}

	var editors []string
	for _, persInfo := range esLog.PersonInfos {
		if persInfo.Type == "edt" {
			editors = append(editors, getAsciiFromUnicode(persInfo.Name))
		}
	}
	if len(editors) > 0 {
		exportData.Editor = strings.Join(editors[:], ", ")
	}

	if esLog.Issn != "" {
		exportData.Issn = esLog.Issn
	}

	if len(esLog.Lang) > 0 {
		exportData.Language = esLog.Lang
	}

	var url string

	// TODO product specific
	if esLog.Context == "digizeit" {
		url = fmt.Sprintf(config.IiifViewerResolvingURLDigizeit, esLog.LogID)
	} else if esLog.Context == "gdz" {
		url = fmt.Sprintf(config.IiifViewerResolvingURLGDZ, esLog.LogID)
	} else if esLog.Context == "nlh" {
		url = fmt.Sprintf(config.IiifViewerResolvingURLNLH, esLog.Work, esLog.LogID)
	}
	if url != "" {
		exportData.URL = url
	}

	exportData.Volume = strings.Join(esLog.Currentno[:], ", ")

	return exportData, nil
}

func getAsciiFromUnicode(str string) string {
	t := make([]byte, utf8.RuneCountInString(str))
	i := 0
	for _, r := range str {
		t[i] = byte(r)
		i++
	}
	return string(t)
}
