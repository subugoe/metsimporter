package helper

import (
	"strings"
)

var (
	publisherMap = map[string]string{
		// internal
		"abtherwegeninstitutmarialaach":    "AbtHerwegenInstitutMariaLaach",
		"alber":                            "Alber",
		"archivfürliturgiewissenschaften":  "Archiv für Liturgiewissenschaften",
		"archivfuerliturgiewissenschaften": "Archiv für Liturgiewissenschaften",
		"baynm":                            "BayNM",
		"bibliothekundwissenschaft":        "BibliothekUndWissenschaft",
		"buske":                            "Buske",
		"bärenreiter":                      "Bärenreiter",
		"baerenreiter":                     "Bärenreiter",
		"birkhäuser":                       "Birkhäuser",
		"birkhaeuser":                      "Birkhäuser",
		"böhlau":                           "Böhlau",
		"boehlau":                          "Böhlau",
		"böhlauwien":                       "BöhlauWien",
		"boehlauwien":                      "BöhlauWien",
		"brill":                            "Brill",
		"carl":                             "Carl",
		"degruyter":                        "DeGruyter",
		"deutscherkunstverlag":             "DeutscherKunstverlag",
		"doi":                              "DOI",
		"dunckerhumblot":                   "DunckerHumblot",
		"editionsigma":                     "editionsigma",
		"finanzarchiv":                     "Finanzarchiv",
		"fink":                             "Fink",
		"gau":                              "GAU",
		"gesellschaftfürerdkundezuberlin":  "GesellschaftFürErdkundeZuBerlin",
		"gesellschaftfuererdkundezuberlin": "GesellschaftFürErdkundeZuBerlin",
		"gfhwkev":                          "GfHWKeV",
		"gutenberggesellschaft":            "GutenbergGesellschaft",
		"gutenbergjahrbuch":                "GutenbergJahrbuch",
		"harrassowitz":                     "Harrassowitz",
		"heinsius":                         "Heinsius",
		"hesse":                            "Hesse",
		"hirzel":                           "Hirzel",
		"histanhomm":                       "HistanHomm",
		"institutfürweltwirtschaft":        "InstitutFürWeltwirtschaft",
		"institutfuerweltwirtschaft":       "InstitutFürWeltwirtschaft",
		"juventa":                          "Juventa",
		"kginstpum":                        "KGInstPUM",
		"kamp":                             "Kamp",
		"klartextverlag":                   "KlartextVerlag",
		"klett":                            "Klett",
		"kröner":                           "Kröner",
		"kroener":                          "Kröner",
		"klostermann":                      "Klostermann",
		"liber":                            "Liber",
		"luciuslucius":                     "LuciusLucius",
		"mpgkgrom":                         "MPGKGRom",
		"mann":                             "Mann",
		"meiner":                           "Meiner",
		"metzler":                          "Metzler",
		"mgh":                              "MGH",
		"mohrsiebeck":                      "MohrSiebeck",
		"mohrsiebeckschnupper":             "MohrSiebeck",
		"niemeyer":                         "Niemeyer",
		"oldenbourg":                       "Oldenbourg",
		"sua":                              "SUA",
		"saur":                             "Saur",
		"schäfferpoeschelmetzler":          "SchaefferPoeschelMetzler",
		"schaefferpoeschelmetzler":         "SchaefferPoeschelMetzler",
		"shakespearegesellschaft":          "Shakespeare Gesellschaft",
		"schott":                           "Schott",
		"simion":                           "Simion",
		"springer":                         "Springer",
		"stso20ja":                         "StSo20Ja",
		"statistischesbundesamtwiesbaden":  "StatistischesBundesamtWiesbaden",
		"steiner":                          "Steiner",
		"unbekannt":                        "unbekannt",
		"vsev":                             "VSeV",
		"vandenhoeckruprecht":              "VandenhoeckRuprecht",
		"vereinwkg":                        "VereinWKG",
		"wasmuth":                          "Wasmuth",
		"winter":                           "Winter",

		// external

		"dipfbbfberlin": "DIPFBBFBerlin",
		"sbberlin":      "sbberlin",
		"ubfrankfurt":   "ubfrankfurt",
		"ubheidelberg":  "ubheidelberg",
		"ubmannheim":    "ubmannheim",
		"ubtuebingen":   "UBTuebingen",
		"ubweimar":      "UBWeimar",
	}
)

// AccessConditionMapping ...
func AccessConditionMapping(accessCondition string) string {
	accessConditionMap := map[string]string{
		"http://creativecommons.org/publicdomain/mark/1.0/": "Public Domain Mark 1.0 (PDM)",
		"http://rightsstatements.org/vocab/InC/1.0/":        "In Copyright (InC)",
		"http://rightsstatements.org/vocab/InC-OW-EU/1.0/":  "In Copyright - EU Orphan Work (InC-EU-OW)",
		"http://rightsstatements.org/vocab/CNE/1.0/":        "Copyright Not Evaluated (CNE)",
	}

	str := accessConditionMap[accessCondition]
	if str != "" {
		return accessCondition
	}
	return str

}

// ImageFileFormatMapping ...
func ImageFileFormatMapping(format string) string {
	imageFileFormatMap := map[string]string{
		"tiff": "image/tiff",
		"tif":  "image/tiff",
		"jpg":  "image/jpg",
		"jpeg": "image/jpg",
		"gif":  "image/gif",
		"giff": "image/gif",
		"png":  "image/png",
	}

	str := imageFileFormatMap[format]
	if str == "" {
		return format
	}
	return str
}

// ClassifcationMappingDE ...
func ClassifcationMappingDE(classifcation string) string {
	classifcationMap := map[string]string{
		"020.librarianship":         "Buch- / Bibliothekswesen",
		"100.philosophy":            "Philosophie",
		"200.religion":              "Religion",
		"300.sociology":             "Soziologie",
		"330.economics":             "Wirtschaftswissenschaften",
		"340.law":                   "Rechtswissenschaften",
		"370.education":             "Erziehungswissenschaften",
		"400.philology":             "Philologie",
		"420.english.languages":     "Anglistik",
		"430.germanic.languages":    "Germanistik",
		"440.romance.languages":     "Romanistik",
		"500.sciences":              "Naturwissenschaften",
		"510.mathematics":           "Mathematik",
		"550.geology":               "Geowissenschaften",
		"609.history.of.technology": "Technikgeschichte",
		"700.arts":                  "Kunst",
		"780.musicology":            "Musikwissenschaft",
		"930.archaeology":           "Archäologie",
		"900.history":               "Geschichtswissenschaften",
		"953.oriental.studies":      "Orientalistik ",
		"962.egyptology":            "Ägyptologie und Koptologie",
	}

	str := classifcationMap[classifcation]
	if str == "" {
		return classifcation
	}
	return str

}

func classifcationMappingEN(classifcation string) string {
	classifcationMap := map[string]string{
		"020.librarianship":         "Librarianship",
		"100.philosophy":            "Philosophy",
		"200.religion":              "Religion",
		"300.sociology":             "Sociology",
		"330.economics":             "Economics",
		"340.law":                   "Law",
		"370.education":             "Education",
		"400.philology":             "Philology",
		"420.english.languages":     "English",
		"430.germanic.languages":    "Germanic",
		"440.romance.languages":     "Romance",
		"500.sciences":              "Sciences",
		"510.mathematics":           "Mathematics",
		"550.geology":               "Geology",
		"609.history.of.technology": "History of Technology",
		"700.arts":                  "Arts",
		"780.musicology":            "Music",
		"930.archaeology":           "Archaeology",
		"900.history":               "History",
		"953.oriental.studies":      "Oriental Studies",
		"962.egyptology":            "Egyptology",
	}

	str := classifcationMap[classifcation]
	if str == "" {
		return classifcation
	}
	return str

}

// MonthMapping ...
func MonthMapping(month string) string {
	monthMap := map[string]string{
		"JAN": "01",
		"FEB": "02",
		"MAR": "03",
		"APR": "04",
		"MAY": "05",
		"JUN": "06",
		"JUL": "07",
		"AUG": "08",
		"SEP": "09",
		"OCT": "10",
		"NOV": "11",
		"DEC": "12",
		"01":  "01",
		"02":  "02",
		"03":  "03",
		"04":  "04",
		"05":  "05",
		"06":  "06",
		"07":  "07",
		"08":  "08",
		"09":  "09",
		"10":  "10",
		"11":  "11",
		"12":  "12",
	}
	str := monthMap[month]
	if str == "" {
		return month
	}
	return str

}

// StrctypeLabelMapping ...
func StrctypeLabelMapping(strctypeLabel string) string {
	// see: https://dfg-viewer.de/strukturdatenset/
	strctypeLabelMap := map[string]string{
		"abstract":                "Abstrakt",
		"acknowledgment":          "Danksagung",
		"addendum":                "Addendum",
		"additional":              "Beilage",
		"address":                 "Anrede",
		"advertising":             "Werbung",
		"act":                     "Urkunde",
		"album":                   "Album",
		"annotation":              "Annotation",
		"appendix":                "Appendix",
		"article":                 "Artikel",
		"atlas":                   "Atlas",
		"attachedwork":            "Beigefügtes Werk",
		"bachelor thesis":         "Bachelorarbeit",
		"bachelor_thesis":         "Bachelorarbeit",
		"bachelorthesis":          "Bachelorarbeit",
		"binding":                 "Einband",
		"bookplate":               "Exlibris",
		"bundle":                  "Nachlass",
		"cartulary":               "Kartular",
		"chapter":                 "Kapitel",
		"collation":               "Bogensignatur",
		"colophon":                "Kolophon",
		"contained work":          "Enthaltenes Werk",
		"contained_work":          "Enthaltenes Werk",
		"containedwork":           "Enthaltenes Werk",
		"contents":                "Inhaltsverzeichnis",
		"corrigenda":              "Errata",
		"cover":                   "Deckel",
		"front_cover":             "Vorderdeckel",
		"front cover":             "Vorderdeckel",
		"frontcover":              "Vorderdeckel",
		"back_cover":              "Rückdeckel",
		"back cover":              "Rückdeckel",
		"backcover":               "Rückdeckel",
		"day":                     "Tag",
		"diploma thesis":          "Diplomarbeit",
		"diploma_thesis":          "Diplomarbeit",
		"diplomathesis":           "Diplomarbeit",
		"doctoral thesis":         "Dissertation",
		"doctoral_thesis":         "Dissertation",
		"doctoralthesis":          "Dissertation",
		"document":                "Dokument",
		"dossier":                 "Vorgang",
		"dedication":              "Widmung",
		"edge":                    "Schnitt",
		"endsheet":                "Vorsatz",
		"engraved titlepage":      "Kupfertitel",
		"engraved_titlepage":      "Kupfertitel",
		"engravedtitlepage":       "Kupfertitel",
		"entry":                   "Eintrag",
		"ephemera":                "Konzertprogramm",
		"epilogue":                "Epilogue",
		"errata":                  "Errata",
		"fascicle":                "Faszikel",
		"figure":                  "Abbildung",
		"file":                    "Akte",
		"folder":                  "Mappe",
		"fragment":                "Fragment",
		"ground plan":             "Grundriss",
		"ground_plan":             "Grundriss",
		"groundplan":              "Grundriss",
		"habilitation thesis":     "Habilitation",
		"habilitation_thesis":     "Habilitation",
		"habilitationthesis":      "Habilitation",
		"headword":                "Stichwort",
		"illustration":            "Illustration",
		"illustrationdescription": "Bildbeschreibung",
		"image":                   "Bild",
		"imprint":                 "Impressum",
		//"index":                   "Index",
		"index":              "Register",
		"indexabbreviations": "Abkürzungsverzeichnis",
		//"indexauthor":           "Index of authors",
		"indexauthors":        "Autorenverzeichnis",
		"indexchronological":  "Chronologisches verzeichnis",
		"indexfigures":        "Abbildungsverzeichnis",
		"indexlocation":       "Ortsregister",
		"indexlocations":      "Ortsregister",
		"indexnames":          "Namensregister",
		"indexofchronology":   "Index (chronologisch)",
		"indexoverall":        "Gesamtindex",
		"indexpersons":        "Personenindex",
		"indexspecial":        "Spezialverzeichnis",
		"indexsubject":        "Fachbegriffsregister",
		"indextables":         "Tabellenverzeichnis",
		"indexvolume":         "Bandindex",
		"initial decoration":  "Initialschmuck",
		"initial_decoration":  "Initialschmuck",
		"initialdecoration":   "Initialschmuck",
		"introduction":        "Einleitung",
		"issue":               "Heft",
		"inventory":           "Bestand",
		"judgement":           "Urteil",
		"lang":                "Sprache|Sprachen",
		"land register":       "Grundbuch",
		"land_register":       "Grundbuch",
		"landregister":        "Grundbuch",
		"leaflet":             "Flugblatt",
		"lecture":             "Vorlesung",
		"legalcomment":        "Rechtskommentar",
		"legalnorm":           "Rechtsnorm",
		"letter":              "Brief",
		"lettertoeditor":      "Brief an den Editor",
		"list":                "Liste",
		"listofillustrations": "Liste der Abbildungen",
		"listofmaps":          "Liste der Karten",
		"listofpublications":  "Publikationsverzeichnis",
		"listoftables":        "Liste der Tabellen",
		"magister thesis":     "Magisterarbeit",
		"magister_thesis":     "Magisterarbeit",
		"magisterthesis":      "Magisterarbeit",
		"manuscript":          "Handschrift",
		"map":                 "Karte",
		"message":             "Nachricht",
		"master thesis":       "Masterarbeit",
		"master_thesis":       "Masterarbeit",
		"masterthesis":        "Masterarbeit",
		"misc":                "Verschiedenes",
		"miscelle":            "Miszelle",
		"monograph":           "Monographie",
		"month":               "Monat",
		"multivolume_work":    "Mehrbändiges Werk",
		"multivolume work":    "Mehrbändiges Werk",
		"multivolumework":     "Mehrbändiges Werk",
		"music":               "Musik",
		"musical_notation":    "Musiknotation",
		"musical notation":    "Musiknotation",
		"musicalnotation":     "Musiknotation",
		"newspaper":           "Zeitung",
		"note":                "Vermerk",
		//"notes":                 "Noten",
		"obituary":              "Nachruf",
		"official notification": "Bescheid",
		"official_notification": "Bescheid",
		"officialnotification":  "Bescheid",
		"ornament":              "Buchschmuck",
		"other":                 "Sonstiges",
		"otherdocstrct":         "Sonstiges",
		"paper":                 "Vortrag",
		"partofwork":            "Teilwerk",
		"part":                  "Teil",
		"paste down":            "Spiegel",
		"paste_down":            "Spiegel",
		"pastedown":             "Spiegel",
		//"periodical":              "Periodica",
		"periodical":            "Zeitschrift",
		"periodicalissue":       "Zeitschriftenheft",
		"periodicalpart":        "Zeitschriftenteil",
		"periodicalvolume":      "Zeitschriftenband",
		"photograph":            "Fotografie",
		"plan":                  "Plan",
		"poem":                  "Gedicht",
		"poster":                "Plakat",
		"preface":               "Vorwort",
		"prepage":               "Deckblatt",
		"preprint":              "Vorabdruck",
		"printed archives":      "Druckerzeugnis (Archivale)",
		"printed_archives":      "Druckerzeugnis (Archivale)",
		"printedarchives":       "Druckerzeugnis (Archivale)",
		"printers mark":         "Druckermarke",
		"printers_mark":         "Druckermarke",
		"printersmark":          "Druckermarke",
		"privileges":            "Privilegien",
		"proceeding":            "Tagungsband",
		"provenance":            "Besitznachweis",
		"register":              "Amtsbuch",
		"remarks":               "Anmerkungen",
		"report":                "report",
		"research paper":        "Forschungsarbeit",
		"research_paper":        "Forschungsarbeit",
		"researchpaper":         "Forschungsarbeit",
		"review":                "Rezension",
		"scheme":                "Schema",
		"seal":                  "Siegel",
		"section":               "Abschnitt",
		"sheetmusic":            "Musik",
		"singlemap":             "Einzelne Karte",
		"spine":                 "Rücken",
		"stamp":                 "Stempel",
		"study":                 "Studie",
		"subinventory":          "Unterbestand",
		"supplement":            "Beilage",
		"table":                 "Tabelle",
		"tabledescription":      "Tabelle der Beschreibungen",
		"tablelist":             "Tabelle, Liste",
		"tableofabbreviations":  "Abkürzungsverzeichnis",
		"tableofcontents":       "Inhaltsverzeichnis",
		"table of contents":     "Inhaltsverzeichnis",
		"table_of_contents":     "Inhaltsverzeichnis",
		"tableofliteraturerefs": "Literaturverzeichnis",
		"text":                  "Text",
		"textsection":           "Textabschnitt",
		"theses":                "Dissertation",
		"title_page":            "Titelblatt",
		"title page":            "Titelblatt",
		"titlepage":             "Titelblatt",
		"unit":                  "Teil",
		"verse":                 "Verse",
		"volume":                "Band",
		"werk_beigefuegt":       "Beigefügtes Werk",
		"werk":                  "Werk",
		"year":                  "Jahr",
		"genre":                 "Genre|Genres",
		"announcement":          "Ankündigung",
		"beigefuegt":            "Beigefügt",
		"bibliography":          "Bibliographie",
		"collectivework":        "Sammelwerk",
		"comment":               "Kommentar",
		"courtdecision":         "Rechtsentscheidung",
		"curriculumvitae":       "Lebenslauf",
	}

	str := strctypeLabelMap[strings.ToLower(strctypeLabel)]
	if str == "" {
		return strctypeLabel
	}
	return str

}

// StrctypeMapping ...
func StrctypeMapping(strctype string) string {

	strctypeMap := map[string]string{
		"abstract":                   "section",
		"acknowledgment":             "section",
		"addendum":                   "section",
		"additional":                 "additional",
		"advertising":                "section",
		"announcement_advertisement": "section",
		"announcement":               "section",
		"announcementadvertisement":  "section",
		"appendix":                   "section",
		"article":                    "article",
		"attached_work":              "contained_work",
		"attachedwork":               "contained_work",
		"bibliography":               "bibliography",
		"binding":                    "binding",
		"bundle":                     "bundle",
		"chapter":                    "chapter",
		"colophon":                   "colophon",
		"contained_work":             "contained_work",
		"containedwork":              "contained_work",
		"contents":                   "contents",
		"corrigenda":                 "corrigenda",
		"cover":                      "cover",
		"courtdecision":              "courtdecision",
		"curriculum_vitae":           "section",
		"curriculumvitae":            "section",
		"dedication_foreword_intro":  "section",
		"dedication":                 "dedication",
		"dedicationforewordintro":    "section",
		"entry":                      "section",
		"epilogue":                   "section",
		"errata":                     "corrigenda",
		"figure":                     "illustration",
		"file":                       "file",
		"folder":                     "folder",
		"headword":                   "section",
		"illustration_description":   "illustration",
		"illustration":               "illustration",
		"illustrationdescription":    "illustration",
		"imprint_colophon":           "colophon",
		"imprint":                    "section",
		"imprintcolophon":            "colophon",
		"index_author":               "index",
		"index_location":             "index",
		"index_overall":              "index",
		"index_persons":              "index",
		"index_special":              "index",
		"index_subject":              "index",
		"index":                      "index",
		"indexauthor":                "index",
		"indexlocation":              "index",
		"indexoverall":               "index",
		"indexpersons":               "index",
		"indexspecial":               "index",
		"indexsubject":               "index",
		"introduction":               "section",
		"issue":                      "issue",
		"journal":                    "periodical",
		"legalcomment":               "legalcomment",
		"legalnorm":                  "legalnorm",
		"letter":                     "letter",
		"list_of_publications":       "section",
		"list":                       "section",
		"listofpublications":         "section",
		"manuscript":                 "manuscript",
		"map":                        "map",
		"message":                    "section",
		"misc":                       "misc",
		"miscella":                   "miscelle",
		"miscelle":                   "miscelle",
		"miscellany":                 "section",
		"multivolume_work":           "multivolume_work",
		"multivolumework":            "multivolume_work",
		"music":                      "musical_notation",
		"musical_notation":           "musical_notation",
		"musicalnotation":            "musical_notation",
		"obituary":                   "section",
		"other_doc_strct":            "section",
		"other_doc_struct":           "section",
		"other":                      "section",
		"otherdocstrct":              "section",
		"otherdocstruct":             "section",
		"part_of_work":               "section",
		"partofwork":                 "section",
		"periodical_issue":           "issue",
		"periodical_part":            "part",
		"periodical_volume":          "volume",
		"periodical":                 "periodical",
		"periodicalissue":            "issue",
		"periodicalpart":             "part",
		"periodicalvolume":           "volume",
		"preface":                    "preface",
		"prepage":                    "section",
		"remarks":                    "section",
		"review":                     "review",
		"section":                    "section",
		"sheet_music":                "musical_notation",
		"sheetmusic":                 "musical_notation",
		"supplement":                 "section",
		"table_description":          "table",
		"table_list":                 "section",
		"table_of_abbreviations":     "table",
		"table_of_contents":          "contents",
		"table_of_literature_refs":   "section",
		"table":                      "table",
		"tabledescription":           "table",
		"tablelist":                  "section",
		"tableofabbreviations":       "table",
		"tableofcontents":            "contents",
		"tableofliteraturerefs":      "table",
		"text_section":               "section",
		"textsection":                "section",
		"theses":                     "section",
		"title_page":                 "title_page",
		"title page":                 "title_page",
		"title":                      "title_page",
		"titlepage":                  "title_page",
		"unit":                       "section",
		"verse":                      "section",
		"volume":                     "volume",
		"annotation":                 "annotation",
		"atlas":                      "atlas",
		"monograph":                  "monograph",
		"photograph":                 "photograph",
		"research_paper":             "section",
		// "act" : "act", // DFGViewer datenset element not used in Goobi
		// "address" : "address", // DFGViewer datenset element not used in Goobi
		// "album" : "album", // DFGViewer datenset element not used in Goobi
		// "bachelor_thesis" : "section", // DFGViewer datenset element not used in Goobi
		// "bookplate" : "bookplate", // DFGViewer datenset element not used in Goobi
		// "cartulary" : "cartulary", // DFGViewer datenset element not used in Goobi
		// "collation" : "collation", // DFGViewer datenset element not used in Goobi
		// "cover_back" : "cover", // DFGViewer datenset element not used in Goobi
		// "cover_front" : "cover", // DFGViewer datenset element not used in Goobi
		// "day" : "day", // DFGViewer datenset element not used in Goobi
		// "diploma_thesis" : "section", // DFGViewer datenset element not used in Goobi
		// "doctoral_thesis" : "section", // DFGViewer datenset element not used in Goobi
		// "document" : "document", // DFGViewer datenset element not used in Goobi
		// "dossier" : "dossier", // DFGViewer datenset element not used in Goobi
		// "edge" : "edge", // DFGViewer datenset element not used in Goobi
		// "endsheet" : "endsheet", // DFGViewer datenset element not used in Goobi
		// "engraved_titlepage" : "engraved_titlepage", // DFGViewer datenset element not used in Goobi
		// "engraved_titlepage" : "engraved_titlepage", // DFGViewer datenset element not used in Goobi
		// "engravedtitlepage" : "engraved_titlepage", // DFGViewer datenset element not used in Goobi
		// "fascicle" : "fascicle", // DFGViewer datenset element not used in Goobi
		// "file" : "file", // DFGViewer datenset element not used in Goobi
		// "fragment" : "fragment", // DFGViewer datenset element not used in Goobi
		// "ground_plan" : "ground_plan", // DFGViewer datenset element not used in Goobi
		// "habilitation_thesis" : "section", // DFGViewer datenset element not used in Goobi
		// "image" : "image", // DFGViewer datenset element not used in Goobi
		// "initial_decoration" : "initial_decoration", // DFGViewer datenset element not used in Goobi
		// "judgement" : "judgement", // DFGViewer datenset element not used in Goobi
		// "land_register" : "land_register", // DFGViewer datenset element not used in Goobi
		// "leaflet" : "leaflet", // DFGViewer datenset element not used in Goobi
		// "lecture" : "lecture", // DFGViewer datenset element not used in Goobi
		// "magister_thesis" : "section", // DFGViewer datenset element not used in Goobi
		// "master_thesis" : "section", // DFGViewer datenset element not used in Goobi
		// "month" : "month", // DFGViewer datenset element not used in Goobi
		// "newspaper" : "newspaper", // DFGViewer datenset element not used in Goobi
		// "note" : "note", // DFGViewer datenset element not used in Goobi
		// "official_notification" : "official_notification", // DFGViewer datenset element not used in Goobi
		// "ornament" : "ornament", // DFGViewer datenset element not used in Goobi
		// "paper" : "paper", // DFGViewer datenset element not used in Goobi
		// "paste_down" : "paste_down", // DFGViewer datenset element not used in Goobi
		// "plan" : "plan", // DFGViewer datenset element not used in Goobi
		// "poster" : "poster", // DFGViewer datenset element not used in Goobi
		// "preprint" : "preprint", // DFGViewer datenset element not used in Goobi
		// "printed_archives" : "printed_archives", // DFGViewer datenset element not used in Goobi
		// "printers_mark" : "printers_mark", // DFGViewer datenset element not used in Goobi
		// "privileges" : "privileges", // DFGViewer datenset element not used in Goobi
		// "proceeding" : "proceeding", // DFGViewer datenset element not used in Goobi
		// "provenance" : "provenance", // DFGViewer datenset element not used in Goobi
		// "register" : "register", // DFGViewer datenset element not used in Goobi
		// "report" : "report", // DFGViewer datenset element not used in Goobi
		// "scheme" : "scheme", // DFGViewer datenset element not used in Goobi
		// "seal" : "seal", // DFGViewer datenset element not used in Goobi
		// "spine" : "spine", // DFGViewer datenset element not used in Goobi
		// "stamp" : "stamp", // DFGViewer datenset element not used in Goobi
		// "study" : "study", // DFGViewer datenset element not used in Goobi
		// "text" : "text", // DFGViewer datenset element not used in Goobi
		// "year" : "year" // DFGViewer datenset element not used in Goobi
	}
	return strctypeMap[strings.ToLower(strctype)]
}

// NomalizePublisherName ...
func NomalizePublisherName(publisherName string) string {

	publisherName = strings.ReplaceAll(publisherName, " ", "")
	str := publisherMap[strings.ToLower(publisherName)]

	if str == "" {
		// TODO logging
		//log.Errorf("No mapping defined for publisher %s", publisherName)
		return ""
	}
	return str

}
