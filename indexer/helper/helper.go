package helper

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"sort"

	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis"

	"gitlab.gwdg.de/subugoe/metsimporter/indexer/types"
)

var ()

func init() {
}

// DeriveWork ...
func DeriveWork(id string) string {
	if strings.HasPrefix(id, "PPN") {
		return id[3:]
	}
	return id
}

// CheckIfPaused ...
func CheckIfPaused(retries int, redisClient *redis.Client) (bool, error) {

	redisResult, err := redisClient.Get(config.RedisStopIndexerKey).Result()
	if err == redis.Nil {
		// key does not exist
		return false, nil
	} else if err != nil {
		if retries < 5 {
			time.Sleep(2 * time.Second)
			return CheckIfPaused(retries+1, redisClient)
		}
		return false, fmt.Errorf("could not read from readis, due to %s", err.Error())

	} else {
		if redisResult == "locked" {
			// services are paused (lock exists)
			return true, nil
		}
		return false, nil
	}

}

// Contains ... check if the slice contains the value
func Contains(arr []string, el string) bool {
	for _, a := range arr {
		if a == el {
			return true
		}
	}
	return false
}

// ListContainsStringItem ...
func ListContainsStringItem(item string) bool {
	list := []string{"?", "!", ";", ":", ".", ",", "(", ")", "{", "}", "[", "]", "+", "+", "#", "'", "´", "`", "="}
	for _, v := range list {
		if v == item {
			return true
		}
	}
	return false
}

// Prettyprint ...
func Prettyprint(b []byte) ([]byte, error) {
	var out bytes.Buffer
	err := json.Indent(&out, b, "", "  ")
	return out.Bytes(), err
}

func retry(attempts int, sleep time.Duration, f func() error) (err error) {
	for i := 0; ; i++ {
		err = f()
		if err == nil {
			return
		}

		if i >= (attempts - 1) {
			break
		}
		time.Sleep(sleep)
	}
	return fmt.Errorf("execution failed after %d attempts, %s", attempts, err)
}

func filterNewLines(s string) string {
	return strings.Map(func(r rune) rune {
		switch r {
		case 0x000A, 0x000B, 0x000C, 0x000D, 0x0085, 0x2028, 0x2029:
			return -1
		default:
			return r
		}
	}, s)
}

// GetMinMaxStartPhys ...
func GetMinMaxStartPhys(logToPhysLink types.LogToPhysLink, physicalIDToAttrMap map[string]types.PhysicalAttributes, workID string, product string) (int64, int64, string, string, error) {
	ffirst := true
	min := int64(1)
	max := int64(1)
	minPhys := ""
	maxPhys := ""
	if len(logToPhysLink.ToPhysIDs) > 0 {
		for _, physID := range logToPhysLink.ToPhysIDs {

			if physicalIDToAttrMap[physID.To].Order == "" {
				continue
			}

			var to int64
			var err error

			// TODO product specific
			if strings.Contains(product, "nlh-ahn") {
				to = physicalIDToAttrMap[physID.To].DivPosition
			} else {
				to, err = strconv.ParseInt(physicalIDToAttrMap[physID.To].Order, 10, 32)
			}
			//to, err := strconv.ParseInt(physicalIDToAttrMap[physID.To].Order, 10, 32)
			if err != nil {
				log.Infof("could not convert attribute order '%s' of element '%s' in %s to an int", physicalIDToAttrMap[physID.To].Order, physID.To, workID)
				continue
			}

			if to == 0 {
				to = 1
			}

			if ffirst {
				min = to
				max = to
				minPhys = physID.To
				maxPhys = physID.To
			} else {

				if to > max {
					max = to
					maxPhys = physID.To
				}

				if to < min {
					min = to
					minPhys = physID.To
				}

			}

			ffirst = false

		}
	} else {
		sortSlice := make(types.PhysAttrSortSlice, 0, len(physicalIDToAttrMap))
		for _, d := range physicalIDToAttrMap {
			sortSlice = append(sortSlice, d)
		}
		if len(sortSlice) > 0 {
			sort.Sort(sortSlice)

			min = sortSlice[0].OrderValue
			max = sortSlice[len(sortSlice)-1].OrderValue
			minPhys = sortSlice[0].ID
			maxPhys = sortSlice[len(sortSlice)-1].ID
		} else {
			return min, max, minPhys, maxPhys, fmt.Errorf("inconsistency in METS 'structLink' and/or 'structMap (physical)'")
		}
	}

	if minPhys == "" || maxPhys == "" {
		return min, max, minPhys, maxPhys, fmt.Errorf("inconsistency in METS 'structLink' and/or 'structMap (physical)'")
	} else {
		return min, max, minPhys, maxPhys, nil
	}

}

// GetNumberFromXID ...
func GetNumberFromXID(id string) int64 {

	// e.g. "P_00000001."
	if strings.HasPrefix(strings.ToLower(id), "p_") {
		re := regexp.MustCompile("^(P_|p_)(\\S*)\\.$")
		o := re.FindStringSubmatch(id)
		if len(o) == 0 {
			return -1
		}

		if o[2] == "" {
			return 0
		}

		number, err := strconv.ParseInt(o[2], 10, 32)
		if err != nil {
			return -1
		}
		return number
	}

	// else
	re := regexp.MustCompile("^(phys_|phys|PHYS_|log_|log|LOG_)(\\S*)$")
	o := re.FindStringSubmatch(id)
	if len(o) == 0 {
		return -1
	}

	if o[2] == "" {
		return 0
	}

	number, err := strconv.ParseInt(o[2], 10, 32)
	if err != nil {
		return -1
	}
	return number

}

func findOrderForStructType(index int64, divs []types.Div, id string) int64 {

	for _, div := range divs {
		if div.ID == id {
			return index
		}
		if len(div.Divs) > 0 {
			index += 1
			return findOrderForStructType(index, div.Divs, id)
		}
		index += 1
	}
	return -1
}

func getOrderPosition(structMapType string, structElementID string, metsObject *types.Mets) int64 {

	if structMapType == "log" {
		for _, structMap := range metsObject.Structmaps {
			if strings.ToLower(structMap.Type) == "logical" {
				return findOrderForStructType(0, structMap.Divs, structElementID)
			} else {
				continue
			}
		}
	} else {
		for _, structMap := range metsObject.Structmaps {
			if strings.ToLower(structMap.Type) == "physical" {
				return findOrderForStructType(0, structMap.Divs, structElementID)
			} else {
				continue
			}
		}
	}
	return -1
}

// GetLogToXLinkMap ...
// func GetLogToXLinkMap(structlink types.Structlink, recordIdentifier string) (map[string]types.LogToPhysLink, map[string]types.PhysToLogLink) {
func GetLogToXLinkMap(metsObject *types.Mets) (map[string]types.LogToPhysLink, map[string]types.PhysToLogLink) {

	var logToPhysLinkMap map[string]types.LogToPhysLink = make(map[string]types.LogToPhysLink)

	var physToLogLinkMap map[string]types.PhysToLogLink = make(map[string]types.PhysToLogLink)

	for _, smlink := range metsObject.Structlink.Smlink {

		logPosition := GetNumberFromXID(smlink.From)
		if logPosition == -1 {
			logPosition = getOrderPosition("log", smlink.From, metsObject)
			if logPosition == -1 {
				log.Errorf("could not derive smlink.From %s for Record %s", smlink.From, metsObject.RecordIdentifier)
			}
		}

		physPosition := GetNumberFromXID(smlink.To)
		if physPosition == -1 {
			physPosition = getOrderPosition("phys", smlink.To, metsObject)
			if physPosition == -1 {
				log.Errorf("could not derive smlink.To %s for Record %s", smlink.To, metsObject.RecordIdentifier)
			}
		}

		// --- log -> phys
		if logToPhysLinkMap[smlink.From].FromLogID == "" {

			var logToPhysLink types.LogToPhysLink

			logToPhysLink.FromLogID = smlink.From
			logToPhysLink.ToPhysIDs = []types.Phys{{Order: physPosition, To: smlink.To}}

			logToPhysLinkMap[smlink.From] = logToPhysLink

		} else {
			current := logToPhysLinkMap[smlink.From]
			current.ToPhysIDs = append(current.ToPhysIDs,
				types.Phys{Order: physPosition, To: smlink.To})
			logToPhysLinkMap[smlink.From] = current
		}

		// --- phys -> log
		if physToLogLinkMap[smlink.To].PhysID == "" {

			var physToLogLink types.PhysToLogLink

			physToLogLink.PhysID = smlink.To
			physToLogLink.LogID = smlink.From

			physToLogLinkMap[smlink.To] = physToLogLink

		} else {
			// TODO check remove for DigiZeit
			current := physToLogLinkMap[smlink.To]

			current.PhysID = smlink.To
			current.LogID = smlink.From

			physToLogLinkMap[smlink.To] = current
		}

	}

	return logToPhysLinkMap, physToLogLinkMap
}

// GetNumberFromPPNID ...
func GetNumberFromPPNID(recordID string) string {

	str := strings.ToLower(recordID)
	if strings.HasPrefix(str, "ppn") {
		i := strings.Index(str, "_")
		if i != -1 {
			return recordID[3:(i)]
		}
		return recordID[3:]
	}
	return recordID
}

// CheckOpac ...
func CheckOpac(id string) bool { //,error) {

	url := fmt.Sprintf(config.UnapiURI+config.UnapiPath, id)

	response, err := http.Get(url)
	if err != nil {
		log.Errorf("HTTP request to %s failed, due to %s", url, err.Error())
		return false
	}

	if strings.HasPrefix(response.Status, "2") {
		_, err := io.ReadAll(response.Body)
		if err != nil {
			log.Errorf("could not read response body of OPAC request, due to %s", err.Error())
		}
		_, _ = io.Copy(io.Discard, response.Body)

		return err == nil
	}
	return false

}

// CheckKalliope ...
func CheckKalliope(id string) bool {

	url := fmt.Sprintf(config.KalliopeURI+config.KalliopeSruPath, id)

	response, err := http.Get(url)
	if err != nil {
		log.Errorf("HTTP request to %s failed, due to %s", url, err.Error())
		return false
	}

	if strings.HasPrefix(response.Status, "2") {
		data, err := io.ReadAll(response.Body)
		if err != nil {
			log.Errorf("could not read response body of Kalliope request, due to %s", err.Error())
		}
		_, _ = io.Copy(io.Discard, response.Body)

		var kalliopeResponse types.KalliopeResponse
		xml.Unmarshal(data, &kalliopeResponse)

		return kalliopeResponse.NumberOfRecords > 0
	}
	return false

}

// ValidationRun...
//
//	returns true if validation is running, false in the indexing
func ValidationRun() bool {
	return config.ValidationRun
}
