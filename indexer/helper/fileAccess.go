package helper

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"

	"os"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/go-redis/redis"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/templating"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/types"

	//"gopkg.in/gographics/imagick.v2/imagick"
	"gopkg.in/gographics/imagick.v3/imagick"
)

var (
	s3Client *s3.S3
)

func init() {
	imagick.Initialize()
}

// GetS3Client ...
func GetS3Client() *s3.S3 {

	if s3Client != nil {
		return s3Client
	}

	createS3Client()

	return s3Client
}

// DownloadFile ...
func DownloadFile(url string) (string, error) {

	b := bytes.Buffer{}

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if _, err := io.Copy(&b, resp.Body); err != nil {
		if _, err := io.Copy(&b, resp.Body); err != nil {
			return "", err
		}
	}

	str := fmt.Sprintf("%s", b.Bytes())

	return str, nil
}

// createS3Client ...
func createS3Client() {

	// Readded from pull request
	log.Printf("INFO Waiting for S3 Storage %s...", config.StorageEndpoint)

	for {
		resp, err := http.Get(config.StorageEndpoint)
		if err != nil {
			log.Printf("ERROR S3 %s GET Request failed, due to: due to %s", config.StorageEndpoint, err)
			time.Sleep(60 * time.Second)
			continue
		}
		defer resp.Body.Close()
		log.Printf("INFO S3 %s response with status code %v", config.StorageEndpoint, resp.StatusCode)
		if resp.StatusCode >= 500 {
			time.Sleep(60 * time.Second)
			continue
		} else {
			log.Infof("Request successful, S3 service ready")
			break
		}

	}

	creds := credentials.NewStaticCredentials(config.StorageKey, config.StorageSecret, "")
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(config.StorageRegion),
		Credentials: creds,
		Endpoint:    aws.String(config.StorageEndpoint),
		// true results for nlh-usc in problems
		S3ForcePathStyle: aws.Bool(true),
		//S3ForcePathStyle: aws.Bool(false),
		MaxRetries: aws.Int(3),

		HTTPClient: &http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   time.Duration(config.HttpTransportTimeout) * time.Second,
					KeepAlive: time.Duration(config.HttpTransportKeepAlive) * time.Second,
				}).DialContext,
				TLSHandshakeTimeout:   time.Duration(config.HttpTransportTslHandshakteTimeout) * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
				MaxIdleConns:          config.HttpTransportMaxIdleConns,
				MaxIdleConnsPerHost:   config.HttpTransportMaxIdleConnsPerHost,
				IdleConnTimeout:       90 * time.Second,
			}},
	})
	if err != nil {
		log.Fatalf("failed to create a new session, due to %s ", err.Error())
	}

	//Create S3 service client
	s3Client = s3.New(sess)
}

// GetXMLFrom ...
func GetXMLFrom(bucket string, key string, context string, doctype string) (string, error) {

	b := bytes.Buffer{}

	if ValidationRun() {

		f, err := os.Open(key)

		if err != nil {
			return "", err
		}

		if _, err := io.Copy(&b, f); err != nil {
			if _, err := io.Copy(&b, f); err != nil {
				return "", err
			}
		}

	} else {
		svc := GetS3Client()

		var result *s3.GetObjectOutput
		var err error
		attempts := 0
		for {
			attempts++

			result, err = svc.GetObject(&s3.GetObjectInput{
				Bucket: aws.String(bucket),
				Key:    aws.String(key),
			})
			if err != nil {
				if strings.Contains(err.Error(), "NoSuchKey") {
					return "", fmt.Errorf("NoSuchKey %s", key)
				}

				if attempts > 5 {
					time.Sleep(time.Duration(6*attempts) * time.Second)
					return "", fmt.Errorf("unable to load %s, %s", key, err)
				}
				continue
			}
			break
		}

		defer result.Body.Close()
		if _, err := io.Copy(&b, result.Body); err != nil {
			return "", fmt.Errorf("copy %s failed, %s", key, err)
		}

		str := fmt.Sprintf("%s", b.Bytes())
		return str, nil
	}

	return "", nil
}

// GetFileFromOLAHDS ...
func GetFileFromOLAHDS(url string) (string, error) {

	str, err := DownloadFile(url)
	if err != nil {
		return "", err
	} else {
		return str, nil
	}

	return "", nil
}

// TODO not yet implemented
// GetImageDimensionFromOLAHDS ...
func GetImageDimensionFromOLAHDS(bucket string, key string) (*types.ImageInfo, error) {
	// url := config.OlahdsServiceEndpointBasepath + key
	// str, err := DownloadFile(url)
	// if err != nil {
	// 	return *types.ImageInfo{
	// 		Width:  0,
	// 		Height: 0,
	// 	}, err
	// } else {
	// 	return types.ImageInfo{
	// 		Width:  0,
	// 		Height: 0,
	// 	}, nil
	// }

	return new(types.ImageInfo), nil
}

// CreateImageInfoAndSaveInfoJsonInS3 ...
func CreateImageInfoAndSaveInfoJsonInS3(bucket string, context string, infojsonkey string, imagekey string, pid string, document string, product string, page string, href string) (*types.ImageInfo, error) {

	b := bytes.Buffer{}
	var result *s3.GetObjectOutput
	var err error
	mw := imagick.NewMagickWand()
	attempts := 1

	for {
		attempts++
		if context != "ocrd" {
			result, err = s3Client.GetObject(&s3.GetObjectInput{
				Bucket: aws.String(bucket),
				Key:    aws.String(imagekey),
			})

			if err != nil {
				if strings.Contains(err.Error(), "NoSuchKey") {
					// go on: NoSuchKey
					break
				}

				if attempts > 5 {
					// any error, retry
					time.Sleep(time.Duration(6*attempts) * time.Second)
				}
				continue
			} else {
				defer result.Body.Close()
				if _, err := io.Copy(&b, result.Body); err != nil {
					if _, err := io.Copy(&b, result.Body); err != nil {
						return nil, fmt.Errorf("failed to copy response body for %s/%s, %s", bucket, imagekey, err.Error())
					}
				}
			}

		} else {
			url := config.HostBaseURL + fmt.Sprintf(config.OlahdsFileKeyPattern, pid, href)
			result, err := DownloadFile(url)
			if err != nil {
				if attempts < 6 {
					// any error, retry
					time.Sleep(time.Duration(5*attempts) * time.Second)
					continue
				} else {
					log.Errorf("failed to load info.json %s, %s", url, err.Error())
					break
				}
			} else {
				// write string to bytes.Buffer{}
				if _, err := b.WriteString(result); err != nil {
					return nil, fmt.Errorf("failed to copy info.json %s, %s", url, err.Error())
				}
			}
		}

		break
	}

	defer mw.Destroy()
	err = mw.ReadImageBlob(b.Bytes())
	if err != nil {
		return nil, fmt.Errorf("Failed to create image object %s/%s, due to %s\n", bucket, imagekey, err.Error())
	}

	var x, y float64
	size, _ := mw.GetImageLength()
	depth := mw.GetImageDepth()
	var height uint
	var width uint

	if size > 0 {
		height = mw.GetImageHeight()
		width = mw.GetImageWidth()
		x, y, _ = mw.GetImageResolution()
	} else {
		height = 300
		width = 300
	}

	var imageInfo templating.ImageInfo

	if context != "ocrd" {
		imageInfo = templating.ImageInfo{
			Depth:    depth,
			Height:   int32(height),
			Width:    int32(width),
			Size:     int32(size),
			X:        x,
			Y:        y,
			Document: document,
			Product:  product,
			Page:     page,
		}
	} else {
		imageInfo = templating.ImageInfo{
			Depth:    depth,
			Height:   int32(height),
			Width:    int32(width),
			Size:     int32(size),
			X:        x,
			Y:        y,
			Document: pid,
			Product:  product,
			Page:     page,
		}
	}

	infoJsonString := templating.Create(imageInfo)

	err = UploadTo(product, infojsonkey, []byte(infoJsonString))
	if err != nil {
		log.Errorf("Could not upload infoJson for %s/%s to S3, due to %s", bucket, infojsonkey, err.Error())
	}

	log.Infof("Upload infoJson for %s/%s to S3 completed", bucket, infojsonkey)

	return &types.ImageInfo{
		Width:  int32(width),
		Height: int32(height),
	}, nil
}

// ExistInfoJsonInS3 ...
func ExistInfoJsonInS3(bucket string, key string) bool {

	s3Client := GetS3Client()

	query := &s3.HeadObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}

	_, err := s3Client.HeadObject(query)
	if err != nil {
		if strings.Contains(err.Error(), "NoSuchKey") {
			// TODO logging
			//log.Errorf("NoSuchKey %s/%s", bucket, key)
			return false // go on: NoSuchKey
		} else {
			// TODO logging
			//log.Errorf("unable to load %s/%s, %s", bucket, key, err)
			return false // go on: Unable to get object
		}
	}
	return true
}

// GetImageInfoFromInfoJsonInS3 ...
func GetImageInfoFromInfoJsonInS3(bucket string, infojsonkey string) (*types.ImageInfo, error) {

	svc := GetS3Client()

	var result *s3.GetObjectOutput
	var err error
	attempts := 0
	for {
		attempts++

		result, err = svc.GetObject(&s3.GetObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(infojsonkey),
		})
		if err != nil {
			if strings.Contains(err.Error(), "NoSuchKey") {
				return nil, fmt.Errorf("NoSuchKey %s", infojsonkey)
			}

			if attempts > 5 {
				time.Sleep(time.Duration(6*attempts) * time.Second)
				return nil, fmt.Errorf("unable to load %s, %s", infojsonkey, err)
			}
			continue
		}
		break
	}

	defer result.Body.Close()
	body, err := ioutil.ReadAll(result.Body)
	if err != nil {
		return nil, fmt.Errorf("could not read S3 response for object s3://%s/%s, due to %s", bucket, infojsonkey, err.Error())
	}

	var imageInfo *types.ImageInfo = new(types.ImageInfo)
	if err := json.Unmarshal(body, &imageInfo); err != nil {
		return nil, fmt.Errorf("could not unmarshal object s3://%s/%s , due to %s", bucket, infojsonkey, err.Error())
	}

	return imageInfo, nil
}

// CheckKeyExistInS3 checks if a key exists in S3, it receives a S3 client and in
// addition the bucket name and key.
// return true,nil if the key exist
// return false,error if the key doesn't exist or an error occured
func CheckKeyExistInS3(s3Client *s3.S3, bucket string, key string) (bool, error) {

	query := &s3.ListObjectsV2Input{
		Bucket:  aws.String(bucket),
		Prefix:  aws.String(key),
		MaxKeys: aws.Int64(int64(1)),
	}

	resp, err := s3Client.ListObjectsV2(query)
	if err != nil {
		return false, fmt.Errorf("Reject processing of %s, due to %s", key, err.Error())
	}

	if len(resp.Contents) > 0 {
		return true, nil
	}
	return false, fmt.Errorf("Reject processing of s3://%s/%s, due to no images found", bucket, key)

}

// CheckFileExistInRedisHSET checks if a key exists in redis HSET, which has prefilled
// it receives a redis client and in addition the key (filename) and the redis queue name.
// return true,nil if the key exist
// return false,error if the key doesn't exist or an error occured
func CheckFileExistInRedisHSET(redisClient *redis.Client, key string, redisFileHSETQueue string) (bool, error) {

	exist, err := redisClient.HExists(redisFileHSETQueue, key).Result()
	if err != nil {
		return false, fmt.Errorf("call HExists returns an ERROR, due to %s", err.Error())
	}
	if !exist {
		return false, fmt.Errorf("key %s doesn't exist in HSET, due to %s", key, err.Error())
	}
	return true, nil

}

// UploadTo ...
func UploadTo(bucket string, key string, content []byte) error {

	svc := GetS3Client()

	_, err := svc.PutObject((&s3.PutObjectInput{}).
		SetBucket(bucket).
		SetKey(key).
		SetBody(bytes.NewReader(content)),
	)

	return err
}
