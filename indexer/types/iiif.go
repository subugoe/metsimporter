package types

var ()

// IiifManifest ...
type IiifManifest struct {
	//Version     string
	Comment          string          `json:"__comment"`
	ID               string          `json:"@id"`      // identifier (URI) of the Manifest
	Type             string          `json:"@type"`    //  always set to "sc:Manifest"
	Context          string          `json:"@context"` // always with the value "http://iiif.io/api/presentation/2/context.json"
	ViewingDirection string          `json:"viewingDirection"`
	ViewingHint      string          `json:"viewingHint"`
	NavDate          string          `json:"navDate"` // issue date of the item
	Within           string          `json:"within,omitempty"`
	Label            string          `json:"label,omitempty"`       // title of the Item
	Description      *Description    `json:"description,omitempty"` // description of the Item
	Metadata         []Metadata      `json:"metadata"`              // list of metadata values
	Thumbnail        *AnnotationBody `json:"thumbnail,omitempty"`   // thumbnail (preview) of the record
	Attribution      string          `json:"attribution"`           // human readable label that must be displayed when the item is displayed or used, presenting the copyright or ownership statements and an acknowledgement of the owning and/or publishing institution
	License          string          `json:"license,omitempty"`     // right statement (URI) which defines the copyright, usage and access rights that apply to this digital object
	Logo             Logo            `json:"logo,omitempty"`        // logo URL
	Rendering        []Rendering     `json:"rendering"`             // rendering
	Related          []Related       `json:"related,omitempty"`     // rendering
	SeeAlso          []Dataset       `json:"seeAlso,omitempty"`
	Sequences        []Sequence      `json:"sequences"`
	Structures       []Structure     `json:"structures"`
}

// Description ...
type Description struct {
	Value    string `json:"@value,omitempty"`    // name of a metadata property
	Language string `json:"@language,omitempty"` // value of the metadata property, possibly as link
}

// Metadata ...
type Metadata struct {
	Label string   `json:"label,omitempty"` // name of a metadata property
	Value []string `json:"value"`           // value of the metadata property, possibly as link
}

// Logo ...
type Logo struct {
	ID      string       `json:"@id,omitempty"`     // URL to the metadata in a specific format
	Service *IiifService `json:"service,omitempty"` //
}

// Dataset ...
type Dataset struct {
	Label   string `json:"label,omitempty"`
	ID      string `json:"@id"`     // URL to the metadata in a specific format
	Format  string `json:"format"`  // mimetype of the format
	Profile string `json:"profile"` // url of the profile
}

// Sequence ...
type Sequence struct {
	Type             string   `json:"@type"`           // always set to "sc:Sequence"
	ID               string   `json:"@id"`             // identifier (URI) of the Sequence
	Label            string   `json:"label,omitempty"` // label for the sequence, always set to "Current Page Order"
	StartCanvas      string   `json:"startCanvas"`     // URI of the first canvas to be displayed
	ViewingDirection string   `json:"viewingDirection"`
	ViewingHint      string   `json:"viewingHint"`
	Canvases         []Canvas `json:"canvases"` // ordered list of Canvases
}

// Structure ..
type Structure struct {
	Type             string      `json:"@type"`           // always set to "sc:Range"
	ID               string      `json:"@id"`             // identifier (URI) of the Structure
	Label            string      `json:"label,omitempty"` // label for the Structure, e.g. "Table of Contents"
	ViewingDirection string      `json:"viewingDirection,omitempty"`
	ViewingHint      string      `json:"viewingHint,omitempty"` // e.g. "top"
	Metadata         []Metadata  `json:"metadata"`              // list of metadata values
	Rendering        []Rendering `json:"rendering,omitempty"`   // ordered list of related Canvases
	Within           string      `json:"within,omitempty"`
	SeeAlso          []Dataset   `json:"seeAlso,omitempty"`
	Canvases         []string    `json:"canvases"` // ordered list of related Canvases
}

// RangeJSON ..
type RangeJSON struct {
	Type        string   `json:"@type"` // always set to "sc:Range"
	ID          string   `json:"@id"`
	Label       string   `json:"label,omitempty"`
	ViewingHint string   `json:"viewingHint,omitempty"`
	Members     []string `json:"members"`
}

// Rendering ..
type Rendering struct {
	ID     string `json:"@id,omitempty"`    // URL to the metadata in a specific format
	Label  string `json:"label,omitempty"`  // label, e.g. "PDF download"
	Format string `json:"format,omitempty"` // mimetype of the resource
}

// Related ..
type Related struct {
	ID     string `json:"@id"`             // URL to the metadata in a specific format
	Label  string `json:"label,omitempty"` // label, e.g. "PDF download"
	Format string `json:"format"`          // mimetype of the resource
}

// Canvas ..
type Canvas struct {
	Type         string         `json:"@type"`                  // type of the resource, always set to "sc:Canvas"
	ID           string         `json:"@id"`                    // identifier (URI) of the Canvas
	Label        string         `json:"label,omitempty"`        // label for the canvas
	Height       int32          `json:"height,omitempty"`       // height of the canvas (corresponds to height of the image)
	Width        int32          `json:"width,omitempty"`        // width of the canvas (corresponds to width of the image)
	Attribution  string         `json:"attribution,omitempty"`  // human readable label that must be displayed when the item is displayed or used, presenting the copyright or ownership statements and an acknowledgement of the owning and/or publishing institution
	License      string         `json:"license,omitempty"`      // (URI) defines the copyright, usage and access rights that apply to this digital object
	Images       []Annotation   `json:"images"`                 // list of one annotation that represents the projection of the image into the canvas where it is displayed
	OtherContent []OtherContent `json:"otherContent,omitempty"` // reference to the page fulltext todo
}

// Annotation ..
type Annotation struct {
	Context    string         `json:"@context"`
	Type       string         `json:"@type"`      // type of the resource, always set to "oa:Annotation"
	ID         string         `json:"@id"`        // identifier (URI) of the Annotation
	Motivation string         `json:"motivation"` // motivation for the annotation, ie. "sc:painting" means image will be projected on to the Canvas
	Resource   AnnotationBody `json:"resource"`   // image resource being projected into the Canvas
	On         string         `json:"on"`         // identifier (URI) of the Canvas on which the image will be projected
}

// AnnotationBody ..
type AnnotationBody struct {
	Type    string       `json:"@type,omitempty"`   // type of the resource, always set to "dctypes:Image"
	ID      string       `json:"@id,omitempty"`     // URL of the image
	Format  string       `json:"format,omitempty"`  // mimetype of the resource
	Service *IiifService `json:"service,omitempty"` // Optional. Service hosting and delivering the IIIF resource
	Width   int32        `json:"width,omitempty"`   // width of the image
	Height  int32        `json:"height,omitempty"`  // height of the image
}

// IiifService ..
type IiifService struct {
	ID      string `json:"@id,omitempty"`      // URL of the IIIF service hosting/serving the resource
	Context string `json:"@context,omitempty"` // URL of the JSON-LD context, always with the value "http://iiif.io/api/image/2/context.json"
	Profile string `json:"profile,omitempty"`  // URI of the version supported by the service
}

// OtherContent ..
type OtherContent struct {
	ID string `json:"@id"` // URL of the page fulltext
}
