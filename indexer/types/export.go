package types

import "encoding/xml"

// Data ..
type Data struct {
	DataFields []string
}

// ExportData ...
type ExportData struct {
	ID                   string
	Author               []string
	Title                string
	Subtitle             string
	Publisher            []string
	Editor               string
	PublishingPlace      []string
	PublishingYearString string
	PublishingYear       int64
	Issn                 string
	ParentIssn           string
	Series               string
	URL                  string
	LogURL               string
	Language             []string
	Journal              string
	Periodical           string
	Volume               string
	Number               string
	Month                string
	Edition              string
	Pages                int64
	Product              string
	Note                 string
	IsPeriodical         bool
	IsVolume             bool
	Log                  string
	IsFirst              bool
}

// EndnoteData ...
type EndnoteData struct {
	XMLName xml.Name `xml:"records"`
	Records []Record `xml:"record"`
}

// Record ...
type Record struct {
	XMLName        xml.Name      `xml:"record"`
	EndnoteRefType RefType       `xml:"ref-type,omitempty"` // 6->book, 17->journal article
	Contributors   []Contributor `xml:"contributors,omitempty"`
	Titles         *Title        `xml:"titles,omitempty"`
	Periodical     *Periodical   `xml:"periodical,omitempty"`
	Pages          string        `xml:"pages,omitempty"`
	Volume         string        `xml:"volume,omitempty"`
	Number         string        `xml:"number,omitempty"`
	Edition        string        `xml:"edition,omitempty"`
	Dates          *Date         `xml:"dates,omitempty"`
	PubLocation    []string      `xml:"pub-location,omitempty"`
	Publisher      []string      `xml:"publisher,omitempty"`
	ISSN           string        `xml:"issn,omitempty"`
	Language       string        `xml:"language,omitempty"`
	URLs           URL           `xml:"urls,omitempty"`
}

// RefType ...
type RefType struct {
	XMLName xml.Name `xml:"ref-type"`
	Name    string   `xml:"name,attr"`
	Value   string   `xml:",chardata"`
}

// Contributor ...
type Contributor struct {
	XMLName xml.Name `xml:"contributors"`
	Authors []Author `xml:"authors"`
}

// Author ...
type Author struct {
	XMLName xml.Name `xml:"author"`
	Value   string   `xml:",chardata"`
}

// Title ...
type Title struct {
	XMLName        xml.Name `xml:"titles"`
	Title          string   `xml:"title,omitempty"`
	SecondaryTitle string   `xml:"secondary-title,omitempty"`
}

// Periodical ...
type Periodical struct {
	XMLName   xml.Name `xml:"periodical"`
	FullTitle string   `xml:"full-title,omitempty"`
	Abbr1     string   `xml:"abbr-1,omitempty"`
}

// Date ...
type Date struct {
	XMLName  xml.Name  `xml:"dates"`
	Year     int64     `xml:"year,omitempty"`
	PubDates []PubDate `xml:"pub-dates,omitempty"`
}

// PubDate ...
type PubDate struct {
	XMLName xml.Name `xml:"pub-dates"`
	Date    string   `xml:"date"`
}

// URL ...
type URL struct {
	XMLName xml.Name `xml:"urls"`
	URL     string   `xml:"text-url>url"`
}
