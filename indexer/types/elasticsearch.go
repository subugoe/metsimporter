package types

type ESWriteJob struct {
	ID      string
	Type    string
	Content []byte
}

// ESDoc ...
type ESDoc struct {
	ESLogMap  map[string]ESLog
	ESPhysMap map[string]ESPhys
	Product   string
	Context   string
}

type ESLogSortSlice []ESLog

// Len is part of sort.Interface.
func (d ESLogSortSlice) Len() int {
	return len(d)
}

// Swap is part of sort.Interface.
func (d ESLogSortSlice) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less is part of sort.Interface. We use count as the value to sort by
func (d ESLogSortSlice) Less(i, j int) bool {
	//return d[i].Order < d[j].Order
	return d[i].Index < d[j].Index
}

// ESLog ...
type ESLog struct {
	CreateDate string      `json:"CREATEDATE,omitempty"`
	AgentInfo  []AgentInfo `json:"agentinfo,omitempty"`

	IsFirst           bool
	IsExternalContent bool
	ReindexedAt       string        `json:"reindexedat,omitempty"`
	Bycreator         string        `json:"bycreator,omitempty"`
	Byperson          string        `json:"byperson,omitempty"`
	Bytitle           string        `json:"bytitle,omitempty"`
	Catalogue         string        `json:"catalogue,omitempty"`
	ContentID         string        `json:"contentid,omitempty"`
	Context           string        `json:"context,omitempty"`
	CreatorInfos      []CreatorInfo `json:"creator_infos,omitempty"`
	Currentno         []string      `json:"currentno,omitempty"`
	Currentnosort     int64         `json:"currentnosort,omitempty"`
	DateIndexed       string        `json:"date_indexed,omitempty"`
	DateModified      string        `json:"date_modified,omitempty"`
	Dc                []string      `json:"dc,omitempty"`
	DcZvdd            []string      `json:"dc_zvdd,omitempty"`
	DcDDC             []string      `json:"dc_ddc,omitempty"`
	DcOther           []string      `json:"dc_other,omitempty"`
	DcOtherAuthority  []string      `json:"dc_other_authority,omitempty"`
	DmdID             string        `json:"dmdid,omitempty"`
	Doctype           string        `json:"doctype,omitempty"`

	ID                  string   `json:"id,omitempty"`
	PID                 string   `json:"pid,omitempty"`
	Work                string   `json:"work,omitempty"`
	Collection          string   `json:"collection,omitempty"`
	PrevPID             string   `json:"prevPID,omitempty"`
	PrevPIDs            []string `json:"prevPIDs,omitempty"`
	IsGt                bool     `json:"IsGt,omitempty"`
	OcrdIdentifier      string   `json:"OcrdIdentifier,omitempty"`
	OcrdWorkIdentifier  string   `json:"OcrdWorkIdentifier,omitempty"`
	ImporterInstitution string   `json:"ImporterInstitution,omitempty"`
	Log                 string   `json:"log,omitempty"`
	LogID               string   `json:"log_id,omitempty"`
	LogFullpage         string   `json:"log_fullpage_s,omitempty"`
	LogFullpageCoord    string   `json:"log_fullpage_coord_s,omitempty"`
	LogFulltextID       string   `json:"log_fulltext_id_s,omitempty"`
	LogFulltext         string   `json:"log_fulltext_s,omitempty"`

	Hosts               []string     `json:"host,omitempty"`
	Precedings          []string     `json:"preceding,omitempty"`
	Series              []string     `json:"series,omitempty"`
	Succeedings         []string     `json:"succeeding,omitempty"`
	Identifier          []string     `json:"identifier,omitempty"`
	IdentifierInfo      []Identifier `json:"identifier_info,omitempty"`
	Isanchor            bool         `json:"isanchor"`
	Iscontribution      bool         `json:"iscontribution"`
	Isparent            bool         `json:"isparent"`
	Issn                string       `json:"issn,omitempty"`
	Iswork              bool         `json:"iswork"`
	Label               string       `json:"label,omitempty"`
	Lang                []string     `json:"lang,omitempty"`
	Genre               []string     `json:"genre,omitempty"`
	Sponsor             []string     `json:"sponsor,omitempty"`
	NormalizedGenre     []string     `json:"facet_normalized_genre,omitempty"`
	NormalizedPlaceTerm []string     `json:"facet_normalized_place_term,omitempty"`

	FileGrpUseTypes []string `json:"filegrp_use_types,omitempty"`

	Scriptterm []string `json:"scriptterm,omitempty"`
	Level      int8     `json:"level,omitempty"`

	// copy-field Metadata string  `json:"metadata"`

	NoteInfos  []NoteInfo `json:"note_infos,omitempty"`
	Order      int64      `json:"order,omitempty"`
	Parent     *Parent    `json:"parent,omitempty"`
	ParentID   string     `json:"parent_id,omitempty"`
	ParentWork string     `json:"parent_work,omitempty"`
	ParentLog  string     `json:"parent_log,omitempty"`

	// todo, fill from mods>NameInfo (all)
	PersonInfos              []PersonInfo             `json:"person_infos,omitempty"`
	PhysicalDescriptionInfos *PhysicalDescriptionInfo `json:"physical_description_infos,omitempty"`
	Productseries            string                   `json:"productseries_s,omitempty"`
	Product                  string                   `json:"product,omitempty"`
	PublishInfos             *PublishInfo             `json:"publish_infos,omitempty"`
	DigitizationInfos        *DigitizationInfo        `json:"digitization_infos,omitempty"`
	Purl                     string                   `json:"purl,omitempty"`
	RecordIdentifier         string                   `json:"record_identifier,omitempty"`
	License                  string                   `json:"license,omitempty"`

	// indexing simplified
	RelatedItemInfos []RelatedItemInfos `json:"related_items_infos,omitempty"`
	ParentDivInfo    ParentDivInfo      `json:"parent_div_info,omitempty"`

	LicenseForMetadata           []string `json:"license_for_metadata,omitempty"`
	LicenseForUseAndReproduction []string `json:"license_for_use_and_reproduction,omitempty"`

	RightsAccessConditionInfos []RightAccessCondition `json:"rights_access_condition_infos,omitempty"`
	RightsInfo                 Rights                 `json:"rights_info,omitempty"`
	Shelfmark                  []Shelfmark            `json:"shelfmark_infos,omitempty"`
	Structrun                  []Structrun            `json:"structrun,omitempty"`
	SubjectInfos               []SubjectInfo          `json:"subject_infos,omitempty"`
	Title                      *TitleInfo             `json:"title,omitempty"`
	Type                       string                 `json:"type,omitempty"`
	IssueNumber                string                 `json:"issue_number,omitempty"`
	VolumeNumber               string                 `json:"volume_number,omitempty"`
	NaviYear                   int64                  `json:"navi_year,omitempty"`
	NaviMonth                  int64                  `json:"navi_month,omitempty"`
	NaviDay                    int64                  `json:"navi_day,omitempty"`
	NaviString                 string                 `json:"navi_string,omitempty"`
	NaviDate                   string                 `json:"navi_date,omitempty"`
	Zdb                        string                 `json:"zdb,omitempty"`
	Index                      int64                  `json:"index,omitempty"`
	StartPageIndex             int64                  `json:"start_page_index,omitempty"`
	StartPagePhysID            string                 `json:"start_page_phys_id,omitempty"`
	EndPageIndex               int64                  `json:"end_page_index,omitempty"`
	EndPagePhysID              string                 `json:"end_page_phys_id,omitempty"`
	TitlePageIndex             int64                  `json:"title_page_index,omitempty"`
	TitlePagePhysID            string                 `json:"title_page_phys_id,omitempty"`

	// TODO still required?
	// IsExportDataCreated bool `json:"export_data_created"`
}

type ESPhysSortSlice []ESPhys

// Len is part of sort.Interface.
func (d ESPhysSortSlice) Len() int {
	return len(d)
}

// Swap is part of sort.Interface.
func (d ESPhysSortSlice) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less is part of sort.Interface. We use count as the value to sort by
func (d ESPhysSortSlice) Less(i, j int) bool {
	//return d[i].Order < d[j].Order
	return d[i].Index < d[j].Index
}

// ESPhys ...
type ESPhys struct {
	Context                    string
	Bycreator                  string   `json:"bycreator,omitempty"`
	ContentID                  string   `json:"content_id,omitempty"`
	ContentIDChangedAt         string   `json:"content_id_changed_at,omitempty"`
	Currentno                  []string `json:"currentno,omitempty"`
	Currentnosort              int64    `json:"currentnosort,omitempty"`
	DateIndexed                string   `json:"date_indexed,omitempty"`
	DateModified               string   `json:"date_modified,omitempty"`
	Dc                         []string `json:"dc,omitempty"`
	DcZvdd                     []string `json:"dc_zvdd,omitempty"`
	DcDDC                      []string `json:"dc_ddc,omitempty"`
	FulltextExist              bool
	Fulltext                   string                 `json:"fulltext,omitempty"`
	FulltextHRef               string                 `json:"fulltext_href,omitempty"`
	FulltextNotAnalysed        string                 `json:"fulltext_not_analysed,omitempty"`
	AllFileHrefs               []string               `json:"all_file_hrefs,omitempty"`
	ID                         string                 `json:"id,omitempty"`
	PID                        string                 `json:"pid,omitempty"`
	PrevPID                    string                 `json:"prevPID,omitempty"`
	PrevPIDs                   []string               `json:"prevPIDs,omitempty"`
	Iscontribution             bool                   `json:"iscontribution"`
	Label                      string                 `json:"label,omitempty"`
	Level                      int8                   `json:"level,omitempty"`
	Log                        string                 `json:"log,omitempty"`
	LogID                      string                 `json:"log_id,omitempty"`
	Order                      int64                  `json:"order,omitempty"`
	Orderlabel                 string                 `json:"orderlabel,omitempty"`
	Filename                   string                 `json:"filename,omitempty"`
	Format                     string                 `json:"format"`
	Page                       string                 `json:"page,omitempty"`
	PageKey                    string                 `json:"page_key,omitempty"`
	PageHRef                   string                 `json:"page_href,omitempty"`
	FirstThumbPageHRef         string                 `json:"first_thumb_page_href,omitempty"`
	PageHeight                 int32                  `json:"page_height,omitempty"`
	PageWidth                  int32                  `json:"page_width,omitempty"`
	Parent                     *Parent                `json:"parent,omitempty"`
	Phys                       string                 `json:"phys,omitempty"`
	PublishInfos               *PublishInfo           `json:"publish_infos,omitempty"`
	CreatorInfos               []CreatorInfo          `json:"creator_infos,omitempty"`
	RightsAccessConditionInfos []RightAccessCondition `json:"rights_access_condition_infos,omitempty"`
	StartPageIndex             int64                  `json:"start_page_index"`
	EndPageIndex               int64                  `json:"end_page_index"`
	Structrun                  []Structrun            `json:"structrun,omitempty"`
	Title                      *TitleInfo             `json:"title,omitempty"`
	Type                       string                 `json:"type,omitempty"`
	Work                       string                 `json:"work,omitempty"`
	Mimetype                   string                 `json:"mimetype,omitempty"`
	Externalcontent            bool
	Index                      int64

	IsGt                bool     `json:"IsGt"`
	ImporterInstitution string   `json:"importerInstitution"`
	GtStates            []string `json:"gt_states,omitempty"`
}

// ESLogResponse ..
type ESLogResponse struct {
	Hits ESLogHits `json:"hits"`
}

// ESPhysResponse ..
type ESPhysResponse struct {
	Hits ESPhysHits `json:"hits"`
}

// ESLogHits ...
type ESLogHits struct {
	Total int64             `json:"total"`
	Hits  []ESLogSourceHits `json:"hits"`
}

// ESPhysHits ...
type ESPhysHits struct {
	Total int64              `json:"total"`
	Hits  []ESPhysSourceHits `json:"hits"`
}

// ESLogSourceHits ...
type ESLogSourceHits struct {
	ESLog ESLog `json:"_source"`
}

// ESPhysSourceHits ...
type ESPhysSourceHits struct {
	ESPhys ESPhys `json:"_source"`
}

// PrevInfo ...
type PrevInfo struct {
	Work     string   `json:"work"`
	PID      string   `json:"pid"`
	PrevPIDs []string `json:"prevPIDs"`
}
