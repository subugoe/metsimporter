package types

// KV ...
type KV struct {
	Key   string
	Value int
}

// SolrResponseNLH ...
type SolrResponseNLH struct {
	ResponseHeader SolrResponseHeader  `json:"responseHeader"`
	Response       SolrResponseBodyNLH `json:"response"`
}

// SolrResponseGDZ ...
type SolrResponseGDZ struct {
	ResponseHeader SolrResponseHeader  `json:"responseHeader"`
	Response       SolrResponseBodyGDZ `json:"response"`
}

// SolrResponseHeader ..
type SolrResponseHeader struct {
	Status int64   `json:"status"`
	QTime  float64 `json:"QTime"`
	Params Params  `json:"params"`
}

// SolrResponseBodyNLH ...
type SolrResponseBodyNLH struct {
	NumFound int64        `json:"numFound"`
	Start    int64        `json:"start"`
	Docs     []SolrDocNLH `json:"docs"`
}

// SolrResponseBodyGDZ ...
type SolrResponseBodyGDZ struct {
	NumFound int64        `json:"numFound"`
	Start    int64        `json:"start"`
	Docs     []SolrDocGDZ `json:"docs"`
}

// Params ..
type Params struct {
	Q    string `json:"q"`
	Fl   string `json:"fl"`
	Rows string `json:"rows"`
	Wt   string `json:"wt"`
}

// SolrDocGDZ ..
type SolrDocGDZ struct {
	// logical elements
	Bycreator              string   `json:"bycreator,omitempty"`
	Byperson               string   `json:"byperson,omitempty"`
	Bytitle                string   `json:"bytitle,omitempty"`
	Creator                []string `json:"creator,omitempty"`
	CreatorType            []string `json:"creator_type,omitempty"`
	CreatorGNDURI          []string `json:"creator_gndURI,omitempty"`
	CreatorGNDNumber       []string `json:"creator_gndNumber,omitempty"`
	CreatorRoleterm        []string `json:"creator_roleterm,omitempty"`
	FacetCreatorPersonal   []string `json:"facet_creator_personal,omitempty"`
	FacetCreatorCorporate  []string `json:"facet_creator_corporate,omitempty"`
	Person                 []string `json:"person,omitempty"`
	PersonType             []string `json:"person_type,omitempty"`
	PersonGNDURI           []string `json:"person_gndURI,omitempty"`
	PersonGNDNumber        []string `json:"person_gndNumber,omitempty"`
	PersonRoleterm         []string `json:"person_roleterm,omitempty"`
	FacetPersonPersonal    []string `json:"facet_person_personal,omitempty"`
	FacetPersonCorporate   []string `json:"facet_person_corporate,omitempty"`
	Currentno              string   `json:"currentno,omitempty"`
	Currentnosort          int64    `json:"currentnosort,omitempty"`
	DateIndexed            string   `json:"date_indexed,omitempty"`
	DateModified           string   `json:"date_modified,omitempty"`
	Dc                     []string `json:"dc,omitempty"`
	DcAuthority            []string `json:"dc_authority,omitempty"`
	Docstrct               string   `json:"docstrct,omitempty"` // monograph, volume
	Doctype                string   `json:"doctype,omitempty"`  // work, collection, log, anchor
	LogID                  []string `json:"log_id,omitempty"`
	LogType                []string `json:"log_type,omitempty"`
	LogLabel               []string `json:"log_label,omitempty"`
	LogDmdID               []string `json:"log_dmdid,omitempty"`
	LogAdmid               []string `json:"log_admid,omitempty"`
	LogLevel               []int8   `json:"log_level,omitempty"`
	LogIndex               []int64  `json:"log_index,omitempty"` // todo add to schema.xml
	LogOrder               []int64  `json:"log_order,omitempty"` // todo add to schema.xml
	LogStartPageIndex      []int64  `json:"log_start_page_index,omitempty"`
	LogEndPageIndex        []int64  `json:"log_end_page_index,omitempty"`
	LogPartProduct         []string `json:"log_part_product,omitempty"`
	LogPartWork            []string `json:"log_part_work,omitempty"`
	LogPartKey             []string `json:"log_part_key,omitempty"`
	TitlePage              string   `json:"title_page,omitempty"` // todo add to schema.xml
	ID                     string   `json:"id,omitempty"`
	RecordIdentifier       string   `json:"record_identifier,omitempty"`
	Identifier             []string `json:"identifier,omitempty"`
	Issn                   string   `json:"issn,omitempty"`
	Isanchor               bool     `json:"isanchor"`
	Iswork                 bool     `json:"iswork"`
	Sponsor                []string `json:"sponsor,omitempty"`
	Genre                  []string `json:"genre,omitempty"`
	NormalizedGenre        []string `json:"facet_normalized_genre,omitempty"`
	NormalizedPlaceTerm    []string `json:"facet_normalized_place_term,omitempty"`
	Lang                   []string `json:"lang,omitempty"`
	Scriptterm             []string `json:"scriptterm,omitempty"`
	Note                   []string `json:"note,omitempty"`
	NoteType               []string `json:"note_type,omitempty"`
	Place                  []string `json:"place_publish,omitempty"`
	Publisher              []string `json:"publisher,omitempty"`
	YearPublishString      string   `json:"year_publish_string,omitempty"`
	YearPublish            int64    `json:"year_publish,omitempty"`
	YearPublishStart       int64    `json:"year_publish_start,omitempty"`
	YearPublishEnd         int64    `json:"year_publish_end,omitempty"`
	PublishEdition         string   `json:"edition,omitempty"`
	PlaceDigitization      []string `json:"place_digitization,omitempty"`
	PublisherDigitization  []string `json:"publisher_digitization,omitempty"`
	YearDigitizationString string   `json:"year_digitization_string,omitempty"`
	YearDigitization       int64    `json:"year_digitization,omitempty"`
	YearDigitizationStart  int64    `json:"year_digitization_start,omitempty"`
	YearDigitizationEnd    int64    `json:"year_digitization_end,omitempty"`
	DigitizationEdition    string   `json:"edition_digitization,omitempty"`
	DigitalOrigin          []string `json:"phys_desc_digitalOrigin,omitempty"`
	Extent                 []string `json:"phys_desc_extent,omitempty"`
	Purl                   string   `json:"purl,omitempty"`
	Catalogue              string   `json:"catalogue,omitempty"`

	// TODO this must be standardised across the projects. this has to be discussed with ali
	// GDZ referenced via relateditem_*, NLH references via parentdoc_*
	RelateditemID                      []string `json:"relateditem_id,omitempty"`
	RelateditemTitle                   []string `json:"relateditem_title,omitempty"`
	RelateditemTitleAbbreviated        []string `json:"relateditem_title_abbreviated,omitempty"`
	RelateditemNote                    []string `json:"relateditem_note,omitempty"`
	RelateditemType                    []string `json:"relateditem_type,omitempty"`
	ParentdocID                        []string `json:"parentdoc_id,omitempty"`
	ParentdocTitle                     []string `json:"parentdoc_title,omitempty"`
	ParentdocTitleAbbreviated          []string `json:"parentdoc_title_abbreviated,omitempty"`
	ParentdocTitlePartname             []string `json:"parentdoc_title_partname,omitempty"`
	ParentdocNote                      []string `json:"parentdoc_note,omitempty"`
	ParentdocType                      []string `json:"parentdoc_type,omitempty"`
	ParentdocWork                      []string `json:"parentdoc_work,omitempty"`
	ParentdocLabel                     []string `json:"parentdoc_label,omitempty"`
	AccessCondition                    string   `json:"rights_access_condition,omitempty"`
	Owner                              string   `json:"rights_owner,omitempty"`
	OwnerSiteURL                       string   `json:"rights_owner_site_url,omitempty"`
	OwnerContact                       string   `json:"rights_owner_contact,omitempty"`
	OwnerLogo                          string   `json:"rights_owner_logo,omitempty"`
	RightsLicense                      string   `json:"rights_license,omitempty"`
	RightsReference                    string   `json:"rights_reference,omitempty"`
	RightsSponsor                      string   `json:"rights_sponsor,omitempty"`
	LicenseForMetadata                 []string `json:"license_for_metadata,omitempty"`
	LicenseForUseAndReproduction       []string `json:"license_for_use_and_reproduction,omitempty"`
	Subject                            []string `json:"subject,omitempty"`
	SubjectType                        []string `json:"subject_type,omitempty"`
	FacetSubjectGeographic             []string `json:"facet_subject_geographic,omitempty"`
	FacetSubjectTopic                  []string `json:"facet_subject_topic,omitempty"`
	FacetSubjectTemporal               []string `json:"facet_subject_temporal,omitempty"`
	FacetSubjectHierarchicalGeographic []string `json:"facet_subject_hierarchicalGeographic,omitempty"`
	Shelfmark                          []string `json:"shelfmark,omitempty"`
	Title                              []string `json:"title,omitempty"`
	Subtitle                           []string `json:"subtitle,omitempty"`
	Sorttitle                          []string `json:"sorttitle,omitempty"`
	IssueNumber                        string   `json:"issue_number,omitempty"`
	VolumeNumber                       string   `json:"volume_number,omitempty"`
	Work                               string   `json:"work,omitempty"`
	Collection                         string   `json:"collection,omitempty"`
	Context                            string   `json:"context,omitempty"`
	Product                            string   `json:"product,omitempty"`
	Productseries                      string   `json:"productseries_s,omitempty"`

	//NaviYear   int64  `json:"navi_year,omitempty"`     // todo add to schema.xml
	//NaviMonth  int64  `json:"navi_month,omitempty"`   // todo add to schema.xml
	//NaviDay    int64  `json:"navi_day,omitempty"`       // todo add to schema.xml
	//NaviString string `json:"navi_string,omitempty"` // todo add to schema.xml
	//NaviDate   string `json:"navi_date,omitempty"`     // todo add to schema.xml

	// physical elements
	PhysImageFormat    string               `json:"image_format,omitempty"`
	PhysFirstPageIndex int64                `json:"phys_first_page_index,omitempty"`
	PhysLastPageIndex  int64                `json:"phys_last_page_index,omitempty"`
	PhysOrder          []int64              `json:"phys_order,omitempty"`
	PhysOrderlabel     []string             `json:"phys_orderlabel,omitempty"`
	Page               []string             `json:"page,omitempty"`
	PageKey            []string             `json:"page_key,omitempty"`
	Fulltext           []string             `json:"fulltext,omitempty"`
	SolrLogChildDocs   []SolrLogChildDocGDZ `json:"_childDocuments_,omitempty"`
}

// SolrLogChildDocGDZ ..
type SolrLogChildDocGDZ struct {
	Bycreator              string   `json:"bycreator,omitempty"`
	Byperson               string   `json:"byperson,omitempty"`
	Bytitle                string   `json:"bytitle,omitempty"`
	Creator                []string `json:"creator,omitempty"`
	CreatorType            []string `json:"creator_type,omitempty"`
	CreatorGNDURI          []string `json:"creator_gndURI,omitempty"`
	CreatorGNDNumber       []string `json:"creator_gndNumbber,omitempty"`
	CreatorRoleterm        []string `json:"creator_roleterm,omitempty"`
	FacetCreatorPersonal   []string `json:"facet_creator_personal,omitempty"`
	FacetCreatorCorporate  []string `json:"facet_creator_corporate,omitempty"`
	Person                 []string `json:"person,omitempty"`
	PersonType             []string `json:"person_type,omitempty"`
	PersonGNDURI           []string `json:"person_gndURI,omitempty"`
	PersonGNDNumber        []string `json:"person_gndNumber,omitempty"`
	PersonRoleterm         []string `json:"person_roleterm,omitempty"`
	FacetPersonPersonal    []string `json:"facet_person_personal,omitempty"`
	FacetPersonCorporate   []string `json:"facet_person_corporate,omitempty"`
	Currentno              string   `json:"currentno,omitempty"`
	Currentnosort          int64    `json:"currentnosort,omitempty"`
	Dc                     []string `json:"dc,omitempty"`
	DcAuthority            []string `json:"dc_authority,omitempty"`
	Doctype                string   `json:"doctype,omitempty"` // work, collection, log, anchor
	LogID                  []string `json:"log_id,omitempty"`
	LogType                []string `json:"log_type,omitempty"`
	LogLabel               []string `json:"log_label,omitempty"`
	LogDmdID               string   `json:"log_dmdid,omitempty"`
	LogAdmid               []string `json:"log_admid,omitempty"`
	LogLevel               []int8   `json:"log_level,omitempty"`
	LogOrder               []int64  `json:"log_order,omitempty"` // todo add to schema.xml
	LogStartPageIndex      []int64  `json:"log_start_page_index,omitempty"`
	LogEndPageIndex        []int64  `json:"log_end_page_index,omitempty"`
	LogPartProduct         []string `json:"log_part_product,omitempty"`
	LogPartWork            []string `json:"log_part_work,omitempty"`
	LogPartKey             []string `json:"log_part_key,omitempty"`
	ID                     string   `json:"id,omitempty"`
	RecordIdentifier       string   `json:"record_identifier,omitempty"`
	Identifier             []string `json:"identifier,omitempty"`
	Sponsor                []string `json:"sponsor,omitempty"`
	Genre                  []string `json:"genre,omitempty"`
	NormalizedGenre        []string `json:"facet_normalized_genre,omitempty"`
	NormalizedPlaceTerm    []string `json:"normalized_place_term,omitempty"`
	IsContribution         bool     `json:"iscontribution,omitempty"`
	Lang                   []string `json:"lang,omitempty"`
	Scriptterm             []string `json:"scriptterm,omitempty"`
	Note                   []string `json:"note,omitempty"`
	NoteType               []string `json:"note_type,omitempty"`
	Place                  []string `json:"place_publish,omitempty"`
	Publisher              []string `json:"publisher,omitempty"`
	YearPublishString      string   `json:"year_publish_string,omitempty"`
	YearPublish            int64    `json:"year_publish,omitempty"`
	YearPublishStart       int64    `json:"year_publish_start,omitempty"`
	YearPublishEnd         int64    `json:"year_publish_end,omitempty"`
	PublishEdition         string   `json:"edition,omitempty"`
	PlaceDigitization      []string `json:"place_digitization,omitempty"`
	PublisherDigitization  []string `json:"publisher_digitization,omitempty"`
	YearDigitizationString string   `json:"year_digitization_string,omitempty"`
	YearDigitization       int64    `json:"year_digitization,omitempty"`
	YearDigitizationStart  int64    `json:"year_digitization_start,omitempty"`
	YearDigitizationEnd    int64    `json:"year_digitization_end,omitempty"`
	DigitizationEdition    string   `json:"edition_digitization,omitempty"`
	DigitalOrigin          []string `json:"phys_desc_digitalOrigin,omitempty"`
	Extent                 []string `json:"phys_desc_extent,omitempty"`
	Purl                   string   `json:"purl,omitempty"`
	Catalogue              string   `json:"catalogue,omitempty"`

	// TODO this must be standardised across the projects. this has to be discussed with ali
	// GDZ referenced via relateditem_*, NLH references via parentdoc_*
	RelateditemID                      []string `json:"relateditem_id,omitempty"`
	RelateditemTitle                   []string `json:"relateditem_title,omitempty"`
	RelateditemTitleAbbreviated        []string `json:"relateditem_title_abbreviated,omitempty"`
	RelateditemNote                    []string `json:"relateditem_note,omitempty"`
	RelateditemType                    []string `json:"relateditem_type,omitempty"`
	ParentdocID                        []string `json:"parentdoc_id,omitempty"`
	ParentdocTitle                     []string `json:"parentdoc_title,omitempty"`
	ParentdocTitleAbbreviated          []string `json:"parentdoc_title_abbreviated,omitempty"`
	ParentdocTitlePartname             []string `json:"parentdoc_title_partname,omitempty"`
	ParentdocNote                      []string `json:"parentdoc_note,omitempty"`
	ParentdocType                      []string `json:"parentdoc_type,omitempty"`
	ParentdocWork                      []string `json:"parentdoc_work,omitempty"`
	ParentdocLabel                     []string `json:"parentdoc_label,omitempty"`
	AccessCondition                    string   `json:"rights_access_condition,omitempty"`
	Owner                              string   `json:"rights_owner,omitempty"`
	OwnerSiteURL                       string   `json:"rights_owner_site_url,omitempty"`
	OwnerContact                       string   `json:"rights_owner_contact,omitempty"`
	OwnerLogo                          string   `json:"rights_owner_logo,omitempty"`
	RightsLicense                      string   `json:"rights_license,omitempty"`
	RightsReference                    string   `json:"rights_reference,omitempty"`
	RightsSponsor                      string   `json:"rights_sponsor,omitempty"`
	Subject                            []string `json:"subject,omitempty"`
	SubjectType                        []string `json:"subject_type,omitempty"`
	FacetSubjectGeographic             []string `json:"facet_subject_geographic,omitempty"`
	FacetSubjectTopic                  []string `json:"facet_subject_topic,omitempty"`
	FacetSubjectTemporal               []string `json:"facet_subject_temporal,omitempty"`
	FacetSubjectHierarchicalGeographic []string `json:"facet_subject_hierarchicalGeographic,omitempty"`
	Shelfmark                          []string `json:"shelfmark,omitempty"`
	Title                              []string `json:"title,omitempty"`
	Subtitle                           []string `json:"subtitle,omitempty"`
	Sorttitle                          []string `json:"sorttitle,omitempty"`
	IssueNumber                        string   `json:"issue_number,omitempty"`
	VolumeNumber                       string   `json:"volume_number,omitempty"`
	WorkID                             string   `json:"work_id,omitempty"`
	Context                            string   `json:"context,omitempty"`
	Product                            string   `json:"product,omitempty"`
	Productseries                      string   `json:"productseries_s,omitempty"`

	//NaviYear   int64  `json:"navi_year,omitempty"`     // todo add to schema.xml
	//NaviMonth  int64  `json:"navi_month,omitempty"`   // todo add to schema.xml
	//NaviDay    int64  `json:"navi_day,omitempty"`       // todo add to schema.xml
	//NaviString string `json:"navi_string,omitempty"` // todo add to schema.xml
}

// SolrDocNLH ..
type SolrDocNLH struct {
	// logical elements
	Bycreator             string   `json:"bycreator,omitempty"`
	Byperson              string   `json:"byperson,omitempty"`
	Bytitle               string   `json:"bytitle,omitempty"`
	Creator               []string `json:"creator,omitempty"`
	CreatorType           []string `json:"creator_type,omitempty"`
	CreatorGNDURI         []string `json:"creator_gndURI,omitempty"`
	CreatorDate           []string `json:"creator_date,omitempty"`
	CreatorDateString     []string `json:"creator_date_string,omitempty"`
	CreatorRoleterm       []string `json:"creator_roleterm,omitempty"`
	FacetCreatorPersonal  []string `json:"facet_creator_personal,omitempty"`
	FacetCreatorCorporate []string `json:"facet_creator_corporate,omitempty"`
	Person                []string `json:"person,omitempty"`
	PersonType            []string `json:"person_type,omitempty"`
	PersonGNDURI          []string `json:"person_gndURI,omitempty"`
	PersonDate            []string `json:"person_date,omitempty"`
	PersonDateString      []string `json:"person_date_string,omitempty"`
	PersonRoleterm        []string `json:"person_roleterm,omitempty"`
	FacetPersonPersonal   []string `json:"facet_person_personal,omitempty"`
	FacetPersonCorporate  []string `json:"facet_person_corporate,omitempty"`
	Currentno             string   `json:"currentno,omitempty"`
	Currentnosort         int64    `json:"currentnosort,omitempty"`
	DateIndexed           string   `json:"date_indexed,omitempty"`
	DateModified          string   `json:"date_modified,omitempty"`
	Dc                    []string `json:"dc,omitempty"`
	DcAuthority           []string `json:"dc_authority,omitempty"`
	Docstrct              string   `json:"docstrct,omitempty"` // monograph, volume
	Doctype               string   `json:"doctype,omitempty"`  // work, collection, log, anchor
	LogID                 []string `json:"log_id,omitempty"`
	LogType               []string `json:"log_type,omitempty"`
	LogLabel              []string `json:"log_label,omitempty"`
	LogDmdID              []string `json:"log_dmdid,omitempty"`
	LogAdmid              []string `json:"log_admid,omitempty"`
	LogLevel              []int8   `json:"log_level,omitempty"`
	LogIndex              []int64  `json:"log_index,omitempty"` // todo add to schema.xml
	LogOrder              []int64  `json:"log_order,omitempty"` // todo add to schema.xml
	LogStartPageIndex     []int64  `json:"log_start_page_index,omitempty"`
	LogEndPageIndex       []int64  `json:"log_end_page_index,omitempty"`
	LogPartProduct        []string `json:"log_part_product,omitempty"`
	LogPartWork           []string `json:"log_part_work,omitempty"`
	LogPartKey            []string `json:"log_part_key,omitempty"`

	LogFullpage      []string `json:"log_fullpage_s,omitempty"`
	LogFullpageCoord []string `json:"log_fullpage_coord_s,omitempty"`
	LogFulltextID    []string `json:"log_fulltext_id_s,omitempty"`
	LogFulltext      []string `json:"log_fulltext_s,omitempty"`

	TitlePage              string   `json:"title_page,omitempty"` // todo add to schema.xml
	ID                     string   `json:"id,omitempty"`
	RecordIdentifier       string   `json:"record_identifier,omitempty"`
	Identifier             []string `json:"identifier,omitempty"`
	Issn                   string   `json:"issn,omitempty"`
	Isanchor               bool     `json:"isanchor"`
	Iswork                 bool     `json:"iswork"`
	Sponsor                []string `json:"sponsor,omitempty"`
	Genre                  []string `json:"genre,omitempty"`
	NormalizedGenre        []string `json:"facet_normalized_genre,omitempty"`
	NormalizedPlaceTerm    []string `json:"facet_normalized_place_term,omitempty"`
	Lang                   []string `json:"lang,omitempty"`
	Scriptterm             []string `json:"scriptterm,omitempty"`
	Note                   []string `json:"note,omitempty"`
	NoteType               []string `json:"note_type,omitempty"`
	Place                  []string `json:"place_publish,omitempty"`
	Publisher              []string `json:"publisher,omitempty"`
	YearPublishString      string   `json:"year_publish_string,omitempty"`
	YearPublish            int64    `json:"year_publish,omitempty"`
	YearPublishStart       int64    `json:"year_publish_start,omitempty"`
	YearPublishEnd         int64    `json:"year_publish_end,omitempty"`
	PublishEdition         string   `json:"edition,omitempty"`
	PlaceDigitization      []string `json:"place_digitization,omitempty"`
	PublisherDigitization  []string `json:"publisher_digitization,omitempty"`
	YearDigitizationString string   `json:"year_digitization_string,omitempty"`
	YearDigitization       int64    `json:"year_digitization,omitempty"`
	YearDigitizationStart  int64    `json:"year_digitization_start,omitempty"`
	YearDigitizationEnd    int64    `json:"year_digitization_end,omitempty"`
	DigitizationEdition    string   `json:"edition_digitization,omitempty"`
	DigitalOrigin          []string `json:"phys_desc_digitalOrigin,omitempty"`
	Extent                 []string `json:"phys_desc_extent,omitempty"`
	Purl                   string   `json:"purl,omitempty"`

	// TODO this must be standardised across the projects. this has to be discussed with ali
	// GDZ referenced via relateditem_*, NLH references via parentdoc_*
	RelateditemID                      []string `json:"relateditem_id,omitempty"`
	RelateditemTitle                   []string `json:"relateditem_title,omitempty"`
	RelateditemTitleAbbreviated        []string `json:"relateditem_title_abbreviated,omitempty"`
	RelateditemTitlePartname           []string `json:"relateditem_title_partname,omitempty"`
	RelateditemNote                    []string `json:"relateditem_note,omitempty"`
	RelateditemType                    []string `json:"relateditem_type,omitempty"`
	ParentdocID                        []string `json:"parentdoc_id,omitempty"`
	ParentdocTitle                     []string `json:"parentdoc_title,omitempty"`
	ParentdocTitleAbbreviated          []string `json:"parentdoc_title_abbreviated,omitempty"`
	ParentdocTitlePartname             []string `json:"parentdoc_title_partname,omitempty"`
	ParentdocNote                      []string `json:"parentdoc_note,omitempty"`
	ParentdocType                      []string `json:"parentdoc_type,omitempty"`
	ParentdocWork                      []string `json:"parentdoc_work,omitempty"`
	ParentdocLabel                     []string `json:"parentdoc_label,omitempty"`
	Owner                              string   `json:"rights_owner,omitempty"`
	OwnerSiteURL                       string   `json:"rights_owner_site_url,omitempty"`
	OwnerContact                       string   `json:"rights_owner_contact,omitempty"`
	OwnerLogo                          string   `json:"rights_owner_logo,omitempty"`
	RightsLicense                      string   `json:"rights_license,omitempty"`
	RightsReference                    string   `json:"rights_reference,omitempty"`
	RightsSponsor                      string   `json:"rights_sponsor,omitempty"`
	Subject                            []string `json:"subject,omitempty"`
	SubjectType                        []string `json:"subject_type,omitempty"`
	FacetSubjectGeographic             []string `json:"facet_subject_geographic,omitempty"`
	FacetSubjectTopic                  []string `json:"facet_subject_topic,omitempty"`
	FacetSubjectTemporal               []string `json:"facet_subject_temporal,omitempty"`
	FacetSubjectHierarchicalGeographic []string `json:"facet_subject_hierarchicalGeographic,omitempty"`
	Shelfmark                          []string `json:"shelfmark,omitempty"`
	Title                              []string `json:"title,omitempty"`
	TitleOriginal                      []string `json:"title_original,omitempty"`
	Subtitle                           []string `json:"subtitle,omitempty"`
	Sorttitle                          []string `json:"sorttitle,omitempty"`
	Partname                           []string `json:"partname,omitempty"`
	Partnumber                         []int64  `json:"partnumber,omitempty"`
	TitleAbbreviated                   []string `json:"title_abbreviated,omitempty"`
	TitleAlternative                   []string `json:"title_alternative,omitempty"`
	PeriodicalName                     []string `json:"periodical_name,omitempty"`
	IssueNumber                        string   `json:"issue_number,omitempty"`
	VolumeNumber                       string   `json:"volume_number,omitempty"`
	Work                               string   `json:"work,omitempty"`
	Collection                         string   `json:"collection,omitempty"`
	Context                            string   `json:"context,omitempty"`
	Product                            string   `json:"product,omitempty"`
	Productseries                      string   `json:"productseries_s,omitempty"`

	NaviYear   int64  `json:"navi_year,omitempty"`   // todo add to schema.xml
	NaviMonth  int64  `json:"navi_month,omitempty"`  // todo add to schema.xml
	NaviDay    int64  `json:"navi_day,omitempty"`    // todo add to schema.xml
	NaviString string `json:"navi_string,omitempty"` // todo add to schema.xml
	NaviDate   string `json:"navi_date,omitempty"`   // todo add to schema.xml

	// physical elements
	PhysImageFormat    string               `json:"image_format,omitempty"`
	PhysFirstPageIndex int64                `json:"phys_first_page_index,omitempty"`
	PhysLastPageIndex  int64                `json:"phys_last_page_index,omitempty"`
	PhysOrder          []int64              `json:"phys_order,omitempty"`
	PhysOrderlabel     []string             `json:"phys_orderlabel,omitempty"`
	Page               []string             `json:"page,omitempty"`
	PageKey            []string             `json:"page_key,omitempty"`
	Fulltext           []string             `json:"fulltext,omitempty"`
	SolrLogChildDocs   []SolrLogChildDocNLH `json:"_childDocuments_,omitempty"`
}

// SolrLogChildDocNLH ..
type SolrLogChildDocNLH struct {
	Bycreator              string   `json:"bycreator,omitempty"`
	Byperson               string   `json:"byperson,omitempty"`
	Bytitle                string   `json:"bytitle,omitempty"`
	Creator                []string `json:"creator,omitempty"`
	CreatorType            []string `json:"creator_type,omitempty"`
	CreatorGNDURI          []string `json:"creator_gndURI,omitempty"`
	CreatorGNDNumber       []string `json:"creator_gndNumbber,omitempty"`
	CreatorDate            []string `json:"creator_date,omitempty"`
	CreatorDateString      []string `json:"creator_date_string,omitempty"`
	CreatorRoleterm        []string `json:"creator_roleterm,omitempty"`
	FacetCreatorPersonal   []string `json:"facet_creator_personal,omitempty"`
	FacetCreatorCorporate  []string `json:"facet_creator_corporate,omitempty"`
	Person                 []string `json:"person,omitempty"`
	PersonType             []string `json:"person_type,omitempty"`
	PersonGNDURI           []string `json:"person_gndURI,omitempty"`
	PersonGNDNumber        []string `json:"person_gndNumber,omitempty"`
	PersonDate             []string `json:"person_date,omitempty"`
	PersonDateString       []string `json:"person_date_string,omitempty"`
	PersonRoleterm         []string `json:"person_roleterm,omitempty"`
	FacetPersonPersonal    []string `json:"facet_person_personal,omitempty"`
	FacetPersonCorporate   []string `json:"facet_person_corporate,omitempty"`
	Currentno              string   `json:"currentno,omitempty"`
	Currentnosort          int64    `json:"currentnosort,omitempty"`
	Dc                     []string `json:"dc,omitempty"`
	DcAuthority            []string `json:"dc_authority,omitempty"`
	Doctype                string   `json:"doctype,omitempty"` // work, collection, log, anchor
	LogID                  []string `json:"log_id,omitempty"`
	LogType                []string `json:"log_type,omitempty"`
	LogLabel               []string `json:"log_label,omitempty"`
	LogDmdID               string   `json:"log_dmdid,omitempty"`
	LogAdmid               []string `json:"log_admid,omitempty"`
	LogLevel               []int8   `json:"log_level,omitempty"`
	LogIndex               []int64  `json:"log_index,omitempty"` // todo add to schema.xml
	LogOrder               []int64  `json:"log_order,omitempty"` // todo add to schema.xml
	LogStartPageIndex      []int64  `json:"log_start_page_index,omitempty"`
	LogEndPageIndex        []int64  `json:"log_end_page_index,omitempty"`
	LogPartProduct         []string `json:"log_part_product,omitempty"`
	LogPartWork            []string `json:"log_part_work,omitempty"`
	LogPartKey             []string `json:"log_part_key,omitempty"`
	LogTitlePageIndex      int64    `json:"log_title_page_index,omitempty"` // todo add to schema.xml
	ID                     string   `json:"id,omitempty"`
	RecordIdentifier       string   `json:"record_identifier,omitempty"`
	Identifier             []string `json:"identifier,omitempty"`
	Sponsor                []string `json:"sponsor,omitempty"`
	Genre                  []string `json:"genre,omitempty"`
	NormalizedGenre        []string `json:"facet_normalized_genre,omitempty"`
	NormalizedPlaceTerm    []string `json:"facet_normalized_place_term,omitempty"`
	IsContribution         bool     `json:"iscontribution,omitempty"`
	Lang                   []string `json:"lang,omitempty"`
	Scriptterm             []string `json:"scriptterm,omitempty"`
	Note                   []string `json:"note,omitempty"`
	NoteType               []string `json:"note_type,omitempty"`
	Place                  []string `json:"place_publish,omitempty"`
	Publisher              []string `json:"publisher,omitempty"`
	YearPublishString      string   `json:"year_publish_string,omitempty"`
	YearPublish            int64    `json:"year_publish,omitempty"`
	YearPublishStart       int64    `json:"year_publish_start,omitempty"`
	YearPublishEnd         int64    `json:"year_publish_end,omitempty"`
	PublishEdition         string   `json:"edition,omitempty"`
	PlaceDigitization      []string `json:"place_digitization,omitempty"`
	PublisherDigitization  []string `json:"publisher_digitization,omitempty"`
	YearDigitizationString string   `json:"year_digitization_string,omitempty"`
	YearDigitization       int64    `json:"year_digitization,omitempty"`
	YearDigitizationStart  int64    `json:"year_digitization_start,omitempty"`
	YearDigitizationEnd    int64    `json:"year_digitization_end,omitempty"`
	DigitizationEdition    string   `json:"edition_digitization,omitempty"`
	DigitalOrigin          []string `json:"phys_desc_digitalOrigin,omitempty"`
	Extent                 []string `json:"phys_desc_extent,omitempty"`
	Purl                   string   `json:"purl,omitempty"`
	Catalogue              string   `json:"catalogue,omitempty"`

	// TODO this must be standardised across the projects. this has to be discussed with ali
	// GDZ referenced via relateditem_*, NLH references via parentdoc_*
	RelateditemID                      []string `json:"relateditem_id,omitempty"`
	RelateditemTitle                   []string `json:"relateditem_title,omitempty"`
	RelateditemTitleAbbreviated        []string `json:"relateditem_title_abbreviated,omitempty"`
	RelateditemTitlePartname           []string `json:"relateditem_title_partname,omitempty"`
	RelateditemNote                    []string `json:"relateditem_note,omitempty"`
	RelateditemType                    []string `json:"relateditem_type,omitempty"`
	ParentdocID                        []string `json:"parentdoc_id,omitempty"`
	ParentdocTitle                     []string `json:"parentdoc_title,omitempty"`
	ParentdocTitleAbbreviated          []string `json:"parentdoc_title_abbreviated,omitempty"`
	ParentdocTitlePartname             []string `json:"parentdoc_title_partname,omitempty"`
	ParentdocNote                      []string `json:"parentdoc_note,omitempty"`
	ParentdocType                      []string `json:"parentdoc_type,omitempty"`
	ParentdocWork                      []string `json:"parentdoc_work,omitempty"`
	ParentdocLabel                     []string `json:"parentdoc_label,omitempty"`
	AccessCondition                    string   `json:"rights_access_condition,omitempty"`
	Owner                              string   `json:"rights_owner,omitempty"`
	OwnerSiteURL                       string   `json:"rights_owner_site_url,omitempty"`
	OwnerContact                       string   `json:"rights_owner_contact,omitempty"`
	OwnerLogo                          string   `json:"rights_owner_logo,omitempty"`
	RightsLicense                      string   `json:"rights_license,omitempty"`
	RightsReference                    string   `json:"rights_reference,omitempty"`
	RightsSponsor                      string   `json:"rights_sponsor,omitempty"`
	Subject                            []string `json:"subject,omitempty"`
	SubjectType                        []string `json:"subject_type,omitempty"`
	FacetSubjectGeographic             []string `json:"facet_subject_geographic,omitempty"`
	FacetSubjectTopic                  []string `json:"facet_subject_topic,omitempty"`
	FacetSubjectTemporal               []string `json:"facet_subject_temporal,omitempty"`
	FacetSubjectHierarchicalGeographic []string `json:"facet_subject_hierarchicalGeographic,omitempty"`
	Shelfmark                          []string `json:"shelfmark,omitempty"`
	Title                              []string `json:"title,omitempty"`
	TitleOriginal                      []string `json:"title_original,omitempty"`
	Subtitle                           []string `json:"subtitle,omitempty"`
	Sorttitle                          []string `json:"sorttitle,omitempty"`
	Partname                           []string `json:"partname,omitempty"`
	Partnumber                         []int64  `json:"partnumber,omitempty"`
	TitleAbbreviated                   []string `json:"title_abbreviated,omitempty"`
	TitleAlternative                   []string `json:"title_alternative,omitempty"`
	IssueNumber                        string   `json:"issue_number,omitempty"`
	VolumeNumber                       string   `json:"volume_number,omitempty"`
	WorkID                             string   `json:"work_id,omitempty"`
	Context                            string   `json:"context,omitempty"`
	Product                            string   `json:"product,omitempty"`
	Productseries                      string   `json:"productseries_s,omitempty"`

	//NaviYear   int64  `json:"navi_year,omitempty"`     // todo add to schema.xml
	//NaviMonth  int64  `json:"navi_month,omitempty"`   // todo add to schema.xml
	//NaviDay    int64  `json:"navi_day,omitempty"`       // todo add to schema.xml
	//NaviString string `json:"navi_string,omitempty"` // todo add to schema.xml
}

// SolrFulltextDoc ..
type SolrFulltextDoc struct {
	ID             string   `json:"id,omitempty"`
	FT             string   `json:"ft,omitempty"`
	FTOfWork       string   `json:"ft_of_work,omitempty"`
	FTPageNumber   int64    `json:"ft_page_number,omitempty"`
	FTPageID       string   `json:"ft_page_id_ss,omitempty"`
	Doctype        string   `json:"doctype,omitempty"`
	WorkID         string   `json:"work_id,omitempty"`
	Product        string   `json:"product,omitempty"`
	Productseries  string   `json:"productseries_s,omitempty"`
	LogType        []string `json:"log_type,omitempty"`
	IsContribution bool     `json:"iscontribution,omitempty"`
}
