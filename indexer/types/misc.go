package types

import "time"

// ParentDivInfo ...
type ParentDivInfo struct {
	ParentID    string `json:"ParentID,omitempty"`
	ParentLabel string `json:"ParentLabel,omitempty"`
	ParentType  string `json:"ParentType,omitempty"`
}

// PageMeta ...
type PageMeta struct {
	Format     string `json:"format"`
	Page       string `json:"page"`
	PageKey    string `json:"page_key"`
	PageWidth  int32  `json:"page_width"`
	PageHeight int32  `json:"page_height"`
}

// ExportWork ...
type ExportWork struct {
	WorkID   string    `json:"workid"`
	PID      string    `json:"pid"`
	RangeID  string    `json:"rangeid"`
	Context  string    `json:"context"`
	Product  string    `json:"product"`
	Time     time.Time `json:"time"`
	Attempts int32     `json:"attempts"`
}

// ImageInfo ...
type ImageInfo struct {
	// s3://dzeit/info/020194994_0001/00000161.json

	Width  int32 `json:"width"`
	Height int32 `json:"height"`
}
