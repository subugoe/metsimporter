package types

import (
	"encoding/xml"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"
)

// IndexerJob ...
type IndexerJob struct {
	BaseDir         string    `json:"basedir"`
	FulltextDir     string    `json:"fulltextdir"`
	Document        string    `json:"document"` // document == ID
	Prev            string    `json:"prev"`
	Context         string    `json:"context"`
	Key             string    `json:"inkey"`
	Product         string    `json:"product"` // product == bucket
	Reindex         bool      `json:"reindex"`
	Starttime       time.Time `json:"starttime"`
	Filename        string    `json:"filename"`
	Path            string    `json:"path"`
	Ftype           string    `xml:"ftype" json:"ftype"`                     // fulltext type HTML, TXT_1, TXT_2, TEI_2
	IsGt            bool      `xml:"isgt" json:"isgt"`                       // true if import contains full-text
	ImageFileGrp    string    `xml:"imageFileGrp" json:"imageFileGrp"`       // name of filegroup to use for image presentation in a viewer
	FulltextFileGrp string    `xml:"fulltextFileGrp" json:"fulltextFileGrp"` // name of filegroup to use for full-text indexing
	BagInfoTXT      *BagInfoTXT

	// OcrdMets           string `xml:"ocrdMets" json:"ocrdMets"`                     // name of METS file, if not metx.xml (default)
	// OcrdIdentifier     string `xml:"ocrdIdentifier" json:"ocrdIdentifier"`         // prefixed work identifier (e.g. with the ISIL: 'DE-7_PPN...')
	// OcrdWorkIdentifier string `xml:"ocrdWorkIdentifier" json:"ocrdWorkIdentifier"` // PPN...
	// PrevPID            string `xml:"prevPID" json:"prevPID"`                       // PID of previous import (version)
	// //PrevPIDs            []string `xml:"prevPIDs" json:"prevPIDs"`
	// ImporterInstitution string `xml:"importerInstitution" json:"importerInstitution"` // name of the importer institurion

	// GtDmdsecs           map[string]DmdsecCheck

	// --- commint from mets type
	//PrevPID string
	//IsGt    bool

	// TODO stil unused, check if and where required
	//OcrdIdentifier      string
	//OcrdWorkIdentifier  string
	//OcrdMets            string
	//ImporterInstitution string
}

type BagInfoTXT struct {
	BagSoftwareAgent       string `mapstructure:"Bag-Software-Agent"`
	BagItProfileIdentifier string `mapstructure:"BagIt-Profile-Identifier"`
	BaggingDate            string `mapstructure:"Bagging-Date"`
	PayloadOxum            string `mapstructure:"Payload-Oxum"`

	OcrdBaseVersionChecksum    string `mapstructure:"Ocrd-Base-Version-Checksum"`
	OcrdManifestationDepth     string `mapstructure:"Ocrd-Manifestation-Depth"`
	OcrdMets                   string `mapstructure:"Ocrd-Mets"`
	OcrdIdentifier             string `mapstructure:"Ocrd-Identifier"`
	OcrdWorkIdentifier         string `mapstructure:"Ocrd-Work-Identifier"`
	OlahdSearchPrevPID         string `mapstructure:"Olahd-Search-Prev-PID"`
	OlahdSearchImageFilegrp    string `mapstructure:"Olahd-Search-Image-Filegrp"`
	OlahdSearchFulltextFilegrp string `mapstructure:"Olahd-Search-Fulltext-Filegrp"`
	OlahdSearchFulltextFtype   string `mapstructure:"Olahd-Search-Fulltext-Ftype"`
	OlahdGT                    bool   `mapstructure:"Olahd-GT"`
	OlahdImporter              string `mapstructure:"Olahd-Importer"`
}

// JsonUploadObject ...
type JsonUploadObject struct {
	Bucket  string
	Key     string
	Content []byte
}

// ReturnDmdsecMap ...
type ReturnDmdsecMap struct {
	DmdsecMap map[string]Dmdsec
	Size      int32
}

// Mets ..
type Mets struct {
	Context          string
	Product          string
	PID              string
	RecordIdentifier string
	WorkID           string
	Doctype          string
	XMLName          xml.Name    `xml:"mets"`
	MetsHdr          MetsHdr     `xml:"metsHdr"`
	Dmdsecs          []Dmdsec    `xml:"dmdSec"`
	Amdsecs          []Amdsec    `xml:"amdSec"`     // required, repeatable
	Filesec          Filesec     `xml:"fileSec"`    // (required), not repeatable
	Structmaps       []Structmap `xml:"structMap"`  // required, repeatable (for TYPE: logical), not repeatable for physical structMap
	Structlink       Structlink  `xml:"structLink"` // (required), not repeatable
	NonModsDmdsecIDs []string

	IndexerJob IndexerJob
	BagInfoTXT BagInfoTXT
	IsGt       bool
	PrevPID    string

	// // TODO stil unused, check if and where required
	// OcrdIdentifier      string
	// OcrdWorkIdentifier  string
	// OcrdMets            string
	// ImporterInstitution string
	GtDmdsecs map[string]DmdsecCheck
	PrevPIDs  []string
}

// MetsHdr ...
type MetsHdr struct {
	XMLName    xml.Name    `xml:"metsHdr"`
	Createdate string      `xml:"CREATEDATE,attr"`
	Agent      []AgentInfo `xml:"agent"` // Processor chain, one element for each processor
}

// AgentInfo ...
type AgentInfo struct {
	XMLName   xml.Name `xml:"agent"`
	Role      string   `xml:"ROLE,attr" json:"agent_role,omitempty"`
	Type      string   `xml:"TYPE,attr" json:"agent_type,omitempty"`
	Otherrole string   `xml:"OTHERROLE,attr" json:"agent_otherrole,omitempty"`
	Othertype string   `xml:"OTHERTYPE,attr" json:"agent_othertype,omitempty"`
	Order     int      `json:"order,omitempty"`

	AgentName string      `xml:"name" json:"agent_name,omitempty"` // Name and version of the processor, e.g. the name of the executable
	AgentNote []AgentNote `xml:"note" json:"agent_note,omitempty"` // additional parameter in relation to the processor, e.g. input fileGrp, output fileGrp, parameter list
}

// AgentNote ...
type AgentNote struct {
	XMLName xml.Name `xml:"note"`
	Option  string   `xml:"ocrd:option,attr" json:"option,omitempty"` // values: input-file-grp,  output-file-grp, parameter, page-id
	Value   string   `xml:",chardata" json:"value,omitempty"`
}

// Structlink ...
type Structlink struct {
	XMLName xml.Name `xml:"structLink"`
	ID      string   `xml:"ID,attr"`
	Smlink  []Smlink `xml:"smLink"` // required, repeatable
}

// LogToPhysLink ...
type LogToPhysLink struct {
	FromLogID string
	ToPhysIDs []Phys
	ESLog     ESLog
	LogLabel  string
}

// PhysToLogLink ...
type PhysToLogLink struct {
	PhysID      string
	LogID       string
	LogPosition int64
	LogType     string
}

// Phys ...
type Phys struct {
	Order int64
	To    string
}

// Smlink ..
type Smlink struct {
	XMLName xml.Name `xml:"smLink"`
	From    string   `xml:"from,attr"`
	To      string   `xml:"to,attr"`
}

// Structmap ...
type Structmap struct {
	XMLName xml.Name `xml:"structMap"`
	Type    string   `xml:"TYPE,attr"`
	Divs    []Div    `xml:"div"` // required, repeatable
}

type PhysAttrSortSlice []PhysicalAttributes

// Len is part of sort.Interface.
func (d PhysAttrSortSlice) Len() int {
	return len(d)
}

// Swap is part of sort.Interface.
func (d PhysAttrSortSlice) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less is part of sort.Interface. We use count as the value to sort by
func (d PhysAttrSortSlice) Less(i, j int) bool {
	return d[i].OrderValue < d[j].OrderValue
}

// PhysicalAttributes ...
type PhysicalAttributes struct {
	ID             string
	DMDID          string
	AMDID          string
	Order          string
	OrderValue     int64
	Orderlabel     string
	Type           string
	ContentID      string
	UseToFileIDMap map[string]string
	FileIDsArr     []string
	DivPosition    int64
}

// FileSecAttributes ...
type FileSecAttributes struct {
	FileID       string
	Use          string
	Mimetype     string
	Loctype      string
	OtherLoctype string
	Href         string
	Page         string
	Filename     string
	Format       string
}

// Div ...
type Div struct {
	XMLName                 xml.Name `xml:"div"`
	OOrder                  int64
	IsPartOfMultivolumework bool

	// logical and physical
	ID         string `xml:"ID,attr"`
	Type       string `xml:"TYPE,attr"`
	DmdID      string `xml:"DMDID,attr"`
	Amdid      string `xml:"ADMID,attr"`
	Divs       []Div  `xml:"div"` // required, repeatable
	ParentID   string
	ParentWork string
	ParentLog  string
	WorkID     string

	// physical tags
	Order      string `xml:"ORDER,attr"`      // required
	Orderlabel string `xml:"ORDERLABEL,attr"` // optional
	ContentIDs string `xml:"CONTENTIDS,attr"` // optionnal, identifizierende PURL und/oder URN mit Leerzeichen getrennt

	Fptr []Fptr `xml:"fptr"` // required, repeatable

	// logical tags
	Label string `xml:"LABEL,attr"`

	Mptr Mptr `xml:"mptr"` // optional, not repeatable

	DivPosition int64
	Level       int8
}

// Fptr ..
type Fptr struct {
	// nicht teil des DFG-Viewer Anwend.prof.
	XMLName xml.Name `xml:"fptr"`
	Fileid  string   `xml:"FILEID,attr"`

	Par Par `xml:"par"` // optional, not repeatable
}

// Par ...
type Par struct {
	XMLName xml.Name `xml:"par"`

	Seq []Seq `xml:"seq"` // optional, not repeatable
}

// Seq ...
type Seq struct {
	XMLName xml.Name `xml:"seq"`
	Order   int16
	Type    string

	Area []Area `xml:"area"` // optional, not repeatable
}

type AreaSortSlice []Area

// Len is part of sort.Interface.
func (d AreaSortSlice) Len() int {
	return len(d)
}

// Swap is part of sort.Interface.
func (d AreaSortSlice) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less is part of sort.Interface. We use count as the value to sort by
func (d AreaSortSlice) Less(i, j int) bool {
	return d[i].Order < d[j].Order
}

// Area ...
type Area struct {
	XMLName xml.Name `xml:"area"`
	Fileid  string   `xml:"FILEID,attr"`
	Shape   string   `xml:"SHAPE,attr"`
	Coords  string   `xml:"COORDS,attr"`
	Order   int16
}

// Mptr ...
type Mptr struct {
	XMLName xml.Name `xml:"mptr"`
	Loctype string   `xml:"LOCTYPE,attr"`
	Href    string   `xml:"href,attr"`
}

// Filesec ...
type Filesec struct {
	XMLName  xml.Name  `xml:"fileSec"`
	Filegrps []Filegrp `xml:"fileGrp"` // required, repeatable
}

// Filegrp ...
type Filegrp struct {
	XMLName xml.Name `xml:"fileGrp"`
	Use     string   `xml:"USE,attr"`
	Files   []File   `xml:"file"` // required, repeatable
}

// File ...
type File struct {
	XMLName  xml.Name `xml:"file"`
	ID       string   `xml:"ID,attr"`
	Mimetype string   `xml:"MIMETYPE,attr"`
	FLocat   []FLocat `xml:"FLocat"` // required, not repeatable
}

// FLocat ...
type FLocat struct {
	XMLName      xml.Name `xml:"FLocat"`
	Loctype      string   `xml:"LOCTYPE,attr"`
	OtherLoctype string   `xml:"OTHERLOCTYPE,attr"`
	OTHERMDTYPE  string   `xml:"OTHERMDTYPE,attr"`
	Href         string   `xml:"href,attr"`
}

// Amdsec ...
type Amdsec struct {
	XMLName    xml.Name   `xml:"amdSec"`
	ID         string     `xml:"ID,attr"`
	RightsMD   RightsMD   `xml:"rightsMD"`   // required, not repeatable
	DigiprovMD DigiprovMD `xml:"digiprovMD"` // required, not repeatable
}

// RightsMD ...
type RightsMD struct {
	ID     string `xml:"ID,attr"`
	MdWrap MdWrap `xml:"mdWrap"` // required, not repeatable
}

// Rights ...
type Rights struct {
	Owner             string `xml:"owner" json:"rights_owner,omitempty"`                     // required, not repeatable
	OwnerLogo         string `xml:"ownerLogo" json:"rights_owner_logo,omitempty"`            // required, not repeatable
	OwnerSiteURL      string `xml:"ownerSiteURL" json:"rights_owner_site_url,omitempty"`     // required, not repeatable
	OwnerContact      string `xml:"ownerContact" json:"rights_owner_contact,omitempty"`      // required, not repeatable
	Aggregator        string `xml:"aggregator" json:"aggregator,omitempty"`                  // optional, not repeatable
	AggregatorLogo    string `xml:"aggregatorLogo" json:"aggregator_logo,omitempty"`         // optional, not repeatable
	AggregatorSiteURL string `xml:"aggregatorSiteURL" json:"aggregator_site_url,omitempty"`  // optional, not repeatable
	Sponsor           string `xml:"sponsor" json:"rights_sponsor,omitempty"`                 // optional, not repeatable
	SponsorLogo       string `xml:"sponsorLogo" json:"rights_sponsor_logo,omitempty"`        // optional, not repeatable
	SponsorSiteURL    string `xml:"sponsorSiteURL" json:"rights_sponsor_site_url,omitempty"` // optional, not repeatable
	License           string `xml:"license" json:"rights_license,omitempty"`                 // optional, not repeatable
}

// Shelfmark ...
type Shelfmark struct {
	Shelfmark string `json:"shelfmark,omitempty"` // ???
}

// Structrun ...
type Structrun struct {
	Currentno                  []string               `json:"currentno,omitempty"`
	Currentnosort              int64                  `json:"currentnosort,omitempty"`
	ParentID                   string                 `json:"parent_id,omitempty"`
	PublishInfos               *PublishInfo           `json:"publish_infos,omitempty"`
	CreatorInfos               []CreatorInfo          `json:"creator_infos,omitempty"`
	RightsAccessConditionInfos []RightAccessCondition `json:"rights_access_condition_infos,omitempty"`
	Title                      *TitleInfo             `json:"title,omitempty"` // ???
	Type                       string                 `json:"type,omitempty"`
	Dc                         []string               `json:"dc,omitempty"`
	Issn                       string                 `json:"issn,omitempty"`
	Zdb                        string                 `json:"zdb,omitempty"`
	Purl                       string                 `json:"purl,omitempty"`
	Catalogue                  string                 `json:"catalogue,omitempty"`
}

// Equal ...
func (s Structrun) Equal(o Structrun) bool {
	if (len(s.Currentno) == 0) &&
		(s.Currentnosort == o.Currentnosort) &&
		(s.ParentID == o.ParentID) &&
		(s.PublishInfos == o.PublishInfos) &&
		(len(s.RightsAccessConditionInfos) == 0) &&
		(s.Title == o.Title) &&
		(len(s.Dc) == 0) &&
		(s.Issn == o.Issn) &&
		(s.Zdb == o.Zdb) {
		return true
	}

	return false
}

// IsEmpty ...
func (s Structrun) IsEmpty() bool {
	return s.Equal(Structrun{})
}

// DigiprovMD ...
type DigiprovMD struct {
	ID     string `xml:"ID,attr"`
	MdWrap MdWrap `xml:"mdWrap"` // required, not repeatable
}

// Links ...
type Links struct {
	Reference    []Reference `xml:"reference",omitempty`    // required, repeatable
	Presentation string      `xml:"presentation",omitempty` // optional, not repeatable
	Sru          string      `xml:"sru",omitempty`          // optional, not repeatable
	Iiif         string      `xml:"iiif",omitempty`         // optional, not repeatable
}

// Reference ...
type Reference struct {
	Linktext string `xml:"linktext,attr"`
	Value    string `xml:",chardata" json:"value,omitempty"`
}

// DmdsecCheck ...
type DmdsecCheck struct {
	XMLName xml.Name `xml:"dmdSec"`
	ID      string   `xml:"ID,attr"`
	MdWrap  MdWrap   `xml:"mdWrap"`
}

// Dmdsec ...
type Dmdsec struct {
	XMLName                 xml.Name                `xml:"dmdSec"`
	ID                      string                  `xml:"ID,attr"`
	Identifiers             []Identifier            `xml:"mdWrap>xmlData>mods>identifier"`
	TitleInfos              []TitleInfo             `xml:"mdWrap>xmlData>mods>titleInfo" json:"title"` // required for anchor, repeatable
	OriginInfos             []OriginInfo            `xml:"mdWrap>xmlData>mods>originInfo"`             // required for root, repeatable
	NameInfos               []NameInfo              `xml:"mdWrap>xmlData>mods>name"`
	Locations               []LocationInfo          `xml:"mdWrap>xmlData>mods>location"`
	GenresInfos             []GenreInfo             `xml:"mdWrap>xmlData>mods>genre"`
	ClassificationInfos     []ClassificationInfo    `xml:"mdWrap>xmlData>mods>classification"`
	GatheredIntoInfos       []GatheredIntoInfo      `xml:"mdWrap>xmlData>mods>extension>edm>isGatheredInto"`
	SponsorshipInfos        []SponsorshipInfo       `xml:"mdWrap>xmlData>mods>extension>gdz>sponsorship"`
	NormalizedPlaceTerm     []string                `xml:"mdWrap>xmlData>mods>extension>placeTerm"`
	NormalizedGenre         []string                `xml:"mdWrap>xmlData>mods>extension>genre"`
	LanguageInfos           []LanguageInfo          `xml:"mdWrap>xmlData>mods>language"`
	PhysicalDescriptionInfo PhysicalDescriptionInfo `xml:"mdWrap>xmlData>mods>physicalDescription"` // optinal, not repeatable
	NoteInfos               []NoteInfo              `xml:"mdWrap>xmlData>mods>note"`                // optional, repeatable
	SubjectInfos            []SubjectInfo           `xml:"mdWrap>xmlData>mods>subject"`             // optional, repeatable
	Relateditems            []Relateditem           `xml:"mdWrap>xmlData>mods>relatedItem"`         // required for anchor, repeatable
	PartInfo                PartInfo                `xml:"mdWrap>xmlData>mods>part"`                // (required), not repeatable
	RecordInfo              RecordInfo              `xml:"mdWrap>xmlData>mods>recordInfo"`          // required, not repeatable
	AccessConditions        []AccessCondition       `xml:"mdWrap>xmlData>mods>accessCondition"`     // required for DigiZeit, repeatable
}

// MdWrap ...
type MdWrap struct {
	XMLName     xml.Name  `xml:"mdWrap"`
	MDTYPE      string    `xml:"MDTYPE,attr"`
	OTHERMDTYPE string    `xml:"OTHERMDTYPE,attr"`
	Rights      Rights    `xml:"xmlData>rights" json:"Rights,omitempty"` // required, not repeatable
	Links       Links     `xml:"xmlData>links" json:"Links,omitempty"`   // required, not repeatable
	GtStates    []GtState `xml:"xmlData>gt>state" json:"GtStates,omitempty"`
	Value       string    `xml:",chardata" json:"Value,omitempty"`
}

// GtState ...
type GtState struct {
	XMLName xml.Name `xml:"state"`
	Prop    string   `xml:"prop,attr" json:"prop"`
}

// DmdsecRecordID ...
type DmdsecRecordID struct {
	XMLName xml.Name `xml:"dmdSec"`
	ID      string   `xml:"ID,attr" json:"id"`
}

// SponsorshipInfo ...
type SponsorshipInfo struct {
	Value string `xml:",chardata" json:"Value,omitempty"`
}

// DigitizationInfo ...
type DigitizationInfo struct {
	// digitization -> date_captured
	EventType               string   `json:"event_type,omitempty"`                //
	PlaceDigitization       []string `json:"place_digitization,omitempty"`        //
	PlaceInfo               []Place  `json:"place_info,omitempty"`                //
	PublisherDigitization   []string `json:"publisher_digitization,omitempty"`    //
	PublisherInfo           []string `json:"publisher_info,omitempty"`            //
	PublisherInfoNormalized []string `json:"publisher_info_normalized,omitempty"` //
	YearDigitizationString  string   `json:"year_digitization_string,omitempty"`  //
	YearDigitization        int64    `json:"year_digitization,omitempty"`         //
	YearDigitizationStart   int64    `json:"year_digitization_start,omitempty"`   //
	YearDigitizationEnd     int64    `json:"year_digitization_end,omitempty"`     //
	Edition                 string   `json:"edition_digitization,omitempty"`      //
}

// PublishInfo ...
type PublishInfo struct {
	// publish -> date_issued or date_created
	EventType           string   `json:"event_type,omitempty"`           //
	Place               []string `json:"place_publish,omitempty"`        //
	PlaceInfo           []Place  `json:"place_info,omitempty"`           //
	Publisher           []string `json:"publisher,omitempty"`            //
	PublisherNormalized []string `json:"publisher_normalized,omitempty"` //
	YearPublishString   string   `json:"year_publish_string,omitempty"`  //
	YearPublish         int64    `json:"year_publish,omitempty"`         //
	YearPublishStart    int64    `json:"year_publish_start,omitempty"`   //
	YearPublishEnd      int64    `json:"year_publish_end,omitempty"`     //
	Edition             string   `json:"edition,omitempty"`              //
}

// OriginInfo ...
type OriginInfo struct {
	EventType    string         `xml:"eventType,attr" json:"event_type,omitempty"`  // values: {production|publication|digitization|distribution}
	Place        []Place        `xml:"place" json:"place,omitempty"`                // optinal, repeatable
	Publisher    []string       `xml:"publisher" json:"publisher,omitempty"`        // optinal, repeatable
	DateIssued   []DateIssued   `xml:"dateIssued" json:"date_issued,omitempty"`     // (required), repeatable
	DateCreated  []DateCreated  `xml:"dateCreated" json:"date_created,omitempty"`   // (required), repeatable
	DateCaptured []DateCaptured `xml:"dateCaptured" json:"date_captured,omitempty"` // optional, repeatable
	DateOther    []DateOther    `xml:"dateOther" json:"date_other,omitempty"`       // optional, repeatable
	Edition      string         `xml:"edition" json:"edition,omitempty"`            // optional, not repeatable
}

// DateIssued ...
type DateIssued struct {
	Encoding   string `xml:"encoding,attr" json:"encoding,omitempty"`   //
	Point      string `xml:"point,attr" json:"point,omitempty"`         //
	KeyDate    string `xml:"keyDate,attr" json:"key_date,omitempty"`    //
	Qualifier  string `xml:"qualifier,attr" json:"qualifier,omitempty"` //
	DateIssued string `xml:",chardata" json:"value,omitempty"`          //
}

// DateCreated ...
type DateCreated struct {
	Encoding    string `xml:"encoding,attr" json:"encoding,omitempty"`   //
	Point       string `xml:"point,attr" json:"point,omitempty"`         //
	KeyDate     string `xml:"keyDate,attr" json:"key_date,omitempty"`    //
	Qualifier   string `xml:"qualifier,attr" json:"qualifier,omitempty"` //
	DateCreated string `xml:",chardata" json:"value,omitempty"`          //
}

// DateCaptured ...
type DateCaptured struct {
	Encoding     string `xml:"encoding,attr" json:"encoding,omitempty"`   //
	Point        string `xml:"point,attr" json:"point,omitempty"`         //
	KeyDate      string `xml:"keyDate,attr" json:"key_date,omitempty"`    //
	Qualifier    string `xml:"qualifier,attr" json:"qualifier,omitempty"` //
	DateCaptured string `xml:",chardata" json:"value,omitempty"`          //
}

// DateOther ...
type DateOther struct {
	Encoding  string `xml:"encoding,attr" json:"encoding,omitempty"`   //
	Point     string `xml:"point,attr" json:"point,omitempty"`         //
	KeyDate   string `xml:"keyDate,attr" json:"key_date,omitempty"`    //
	Qualifier string `xml:"qualifier,attr" json:"qualifier,omitempty"` //
	DateOther string `xml:",chardata" json:"value,omitempty"`          //
}

// Place ...
type Place struct {
	PlaceTerm []PlaceTerm `xml:"placeTerm" json:"place_term,omitempty"` // // required, repeatabley
}

// PlaceTerm ...
type PlaceTerm struct {
	Type         string `xml:"type,attr" json:"type,omitempty"`                 // values: {text|code}
	Authority    string `xml:"authority,attr" json:"authority,omitempty"`       //
	AuthorityURI string `xml:"authorityURI,attr" json:"authorityURI,omitempty"` //
	ValueURI     string `xml:"valueURI,attr" json:"valueURI,omitempty"`         //
	Value        string `xml:",chardata" json:"value,omitempty"`                //
}

// TitleInfo ...
type TitleInfo struct {
	Type             string `xml:"type,attr" json:"type,omitempty"`
	AuthorityURI     string `xml:"authorityURI,attr" json:"authority_URI,omitempty"` // URI der Normdatei
	ValueURI         string `xml:"valueURI,attr" json:"ValueURI,omitempty"`          // URI des Sachtitels
	Title            string `xml:"title" json:"title,omitempty"`                     // required, not repeatable
	TitleOriginal    string `json:"title_original,omitempty"`                        // required, not repeatable
	Subtitle         string `xml:"subTitle" json:"subtitle,omitempty"`               // optinal, not repeatable
	Nonsort          string `xml:"nonSort" json:"-"`                                 // optinal, repeatable
	Sorttitle        string `json:"sorttitle,omitempty"`                             // optinal, repeatable
	Partname         string `xml:"partName" json:"partname,omitempty"`               // optinal, repeatable
	PartnumberOrig   string `xml:"partNumber" json:"-"`                              // optinal, repeatable
	Partnumber       int64  `json:"partnumber,omitempty"`                            // optinal, repeatable
	TitleAbbreviated string `json:"title_abbreviated,omitempty"`                     // required, not repeatable
	TitleAlternative string `json:"title_alternative,omitempty"`                     // required, not repeatable
	TitleUniform     string `json:"title_uniform,omitempty"`                         // required, not repeatable
}

// NameInfo ...
type NameInfo struct {
	Type        string         `xml:"type,attr"`
	Displayform string         `xml:"displayForm"`
	Namepart    []NamePartInfo `xml:"namePart"`
	Date        string         `xml:"date"`
	Authority   string         `xml:"authority,attr"`
	ValueURI    string         `xml:"valueURI,attr"`
	Roleterm    RoletermInfo   `xml:"role>roleTerm"`
}

// PersonInfo ...
type PersonInfo struct {
	Type              string `json:"type,omitempty"`
	Name              string `json:"name,omitempty"`
	Roleterm          string `json:"roleterm,omitempty"`
	RoletermAuthority string `json:"roleterm_authority,omitempty"`
	RoletermType      string `json:"roleterm_type,omitempty"`
	GndURI            string `json:"gndURI,omitempty"`
	GndNumber         string `json:"gndNumber,omitempty"`
	Date              string `json:"date,omitempty"`
	DateString        string `json:"date_string,omitempty"`
}

// CreatorInfo ...
type CreatorInfo struct {
	Type              string `json:"type,omitempty"`
	Name              string `json:"name,omitempty"`
	Roleterm          string `json:"roleterm,omitempty"`
	RoletermAuthority string `json:"roleterm_authority,omitempty"`
	RoletermType      string `json:"roleterm_type,omitempty"`
	GndURI            string `json:"gndURI,omitempty"`
	GndNumber         string `json:"gndNumber,omitempty"`
	Date              string `json:"date,omitempty"`
	DateString        string `json:"date_string,omitempty"`
}

// NamePartInfo ...
type NamePartInfo struct {
	Value string `xml:",chardata" json:"Value,omitempty"`
	Type  string `xml:"type,attr"`
}

// RoletermInfo ...
type RoletermInfo struct {
	Value     string `xml:",chardata" json:"Value,omitempty"`
	Authority string `xml:"authority,attr" json:"Authority,omitempty"`
	Type      string `xml:"type,attr" json:"Type,omitempty"`
}

// LocationInfo ...
type LocationInfo struct {
	PhysicalLocationInfos []PhysicalLocationInfo `xml:"physicalLocation"`
	ShelfLocationInfos    []ShelfLocationInfo    `xml:"shelfLocator"`
}

// PhysicalLocationInfo ...
type PhysicalLocationInfo struct {
	Value string `xml:",chardata" json:"Value,omitempty"`
	Type  string `xml:"type,attr" json:"Type,omitempty"`
}

// ShelfLocationInfo ...
type ShelfLocationInfo struct {
	Value string `xml:",chardata" json:"Value,omitempty"`
	Type  string `xml:"type,attr" json:"Type,omitempty"` // not really required
}

// GenreInfo ...
type GenreInfo struct {
	Value     string `xml:",chardata" json:"Value,omitempty"`
	Authority string `xml:"authority,attr" json:"Authority,omitempty"`
}

// ClassificationInfo ...
type ClassificationInfo struct {
	Value     string `xml:",chardata" json:"Value,omitempty"`
	Authority string `xml:"authority,attr" json:"Authority,omitempty"`
}

// GatheredIntoInfo ...
type GatheredIntoInfo struct {
	Value string `xml:",chardata" json:"Value,omitempty"`
}

// LanguageInfo ...
type LanguageInfo struct {
	LanguageTerm LanguageTerm `xml:"languageTerm"`
	ScriptTerm   ScriptTerm   `xml:"scriptTerm"`
}

// LanguageTerm ...
type LanguageTerm struct {
	Value     string `xml:",chardata" json:"Value,omitempty"`
	Authority string `xml:"authority,attr" json:"Authority,omitempty"`
	Type      string `xml:"type,attr" json:"Type,omitempty"`
}

// ScriptTerm ...
type ScriptTerm struct {
	Value string `xml:",chardata" json:"Value,omitempty"`
	Type  string `xml:"type,attr" json:"Type,omitempty"`
}

// PhysicalDescriptionInfo ...
type PhysicalDescriptionInfo struct {
	DigitalOrigin string   `xml:"digitalOrigin" json:"digitalOrigin,omitempty"` // optional, not repeatable
	Extent        []string `xml:"extent" json:"extent,omitempty"`               // optional, repeatable
	Scripttype    string   `xml:"scripttype" json:"scripttype,omitempty"`       // optional, not repeatable
}

// NoteInfo ...
type NoteInfo struct {
	Value string `xml:",chardata" json:"Value,omitempty"`
	Type  string `xml:"type,attr" json:"type,omitempty"`
	ID    string `xml:"ID,attr" json:"id,omitempty"`
}

// SubjectInfo ...
type SubjectInfo struct {
	Type                   string                   `json:"type,omitempty"`
	Value                  string                   `xml:",chardata" json:"subject,omitempty"`
	Authority              string                   `xml:"authority,attr" json:"authority,omitempty"`
	AuthorityURI           string                   `xml:"authorityURI,attr" json:"authorityURI,omitempty"`
	Topics                 []Topic                  `xml:"topic" json:"topic,omitempty"`                                   // optional, repeatable
	HierarchicalGeographic []HierarchicalGeographic `xml:"hierarchicalGeographic" json:"hierarchicalGeographic,omitempty"` // optional, repeatable
	Geographics            []Geographic             `xml:"geographic" json:"geographic,omitempty"`                         // optional, repeatable
	Temporal               []Temporal               `xml:"temporal" json:"temporal,omitempty"`                             // optional, repeatable
	TitleInfo              []TitleInfo              `xml:"titleInfo" json:"titleInfo,omitempty"`                           // optional, repeatable
	NameInfo               []NameInfo               `xml:"name" json:"name,omitempty"`                                     // optional, repeatable
}

// Topic ...
type Topic struct {
	Value    string `xml:",chardata" json:"Value,omitempty"`
	ValueURI string `xml:"valueURI,attr" json:"ValueURI,omitempty"`
}

// Geographic ...
type Geographic struct {
	Value    string `xml:",chardata" json:"Value,omitempty"`
	ValueURI string `xml:"valueURI,attr" json:"ValueURI,omitempty"`
}

// HierarchicalGeographic ...
type HierarchicalGeographic struct {
	Country string `xml:"country" json:"country,omitempty"`
	State   string `xml:"state" json:"state,omitempty"`
	City    string `xml:"city" json:"city,omitempty"`
}

// Temporal ...
type Temporal struct {
	Value    string `xml:",chardata" json:"Value,omitempty"`
	ValueURI string `xml:"valueURI,attr" json:"ValueURI,omitempty"`
}

// Relateditem ...
type Relateditem struct {
	Type             string      `xml:"type,attr" json:"relateditem_type,omitempty"`
	TitleInfo        []TitleInfo `xml:"titleInfo" json:"titleInfo,omitempty"`
	RelateditemTitle string      `json:"relateditem_title,omitempty"`
	RecordInfo       RecordInfo  `xml:"recordInfo" json:"recordInfo,omitempty"` // (required), not repeatable
	RelateditemID    string      `json:"relateditem_id,omitempty"`              // (required), not repeatable
	PartInfo         *PartInfo   `xml:"part"`                                   // (required), not repeatable
	RelateditemNote  string      `xml:"note" json:"note,omitempty"`
}

// RelatedItemInfos ...
type RelatedItemInfos struct {
	RelateditemType             string `json:"relateditem_type,omitempty"`
	RelateditemTitle            string `json:"relateditem_title,omitempty"`
	RelateditemID               string `json:"relateditem_id,omitempty"`
	RelateditemTitlePartname    string `json:"relateditem_title_partname,omitempty"`
	RelateditemTitleAbbreviated string `json:"relateditem_title_abbreviated,omitempty"`
	RelateditemNote             string `json:"relateditem_note,omitempty"`
}

// RightAccessCondition ...
type RightAccessCondition struct {
	Type                string `json:"type,omitempty"`
	Value               string `json:"access_condition,omitempty"`
	URL                 string `json:"url,omitempty"`
	Copyright           string `json:"copyright,omitempty"`
	License             string `json:"license,omitempty"`
	LicenseDisplaylabel string `json:"license_displaylabel,omitempty"`
	NormalizedPublisher string `json:"normalized_publisher,omitempty"`
}

// Extension ...
type Extension struct {
	PlaceTerm string   `xml:"placeTerm" json:"PlaceTerm,omitempty"` // required, not repeatable
	Genre     []string `xml:"genre" json:"Genre,omitempty"`         // required, not repeatable
}

// RecordInfo ...
type RecordInfo struct {
	Recordidentifier    Recordidentifier `xml:"recordIdentifier" json:"Recordidentifier,omitempty"`       // required, not repeatable
	RecordInfoNote      RecordInfoNote   `xml:"recordInfoNote" json:"RecordInfoNote,omitempty"`           // required, not repeatable
	DescriptionStandard string           `xml:"descriptionStandard" json:"DescriptionStandard,omitempty"` // optional, not repeatable
}

// RecordInfoNote ...
type RecordInfoNote struct {
	Type  string `xml:"type,attr" json:"Type,omitempty"`
	Value string `xml:",chardata" json:"Value,omitempty"`
}

// Recordidentifier ...
type Recordidentifier struct {
	Source string `xml:"source,attr" json:"Source,omitempty"`
	Value  string `xml:",chardata" json:"Value,omitempty"`
}

// PartInfo ...
type PartInfo struct {
	Order  string   `xml:"order,attr" json:"order,omitempty"`
	Detail []Detail `xml:"detail" json:"detail,omitempty"` // required, repeatable
}

// Detail ...
type Detail struct {
	Type   string `xml:"type,attr" json:"type,omitempty"`
	Number string `xml:"number" json:"number,omitempty"` // required, not repeatable
}

// AccessCondition ...
type AccessCondition struct {
	Type         string `xml:"type,attr" json:"type,omitempty"`
	DisplayLabel string `xml:"displayLabel,attr"  json:"displayLabel,omitempty"`
	Value        string `xml:",chardata" json:"Value,omitempty"`
}

// ID ...
type ID struct {
	IsRecordID bool
	Type       string
	Value      string
}

// Identifier ...
type Identifier struct {
	Type    string `xml:"type,attr" json:"type,omitempty"`
	Value   string `xml:",chardata" json:"id,omitempty"`
	InvalID string `xml:"invalid,attr" json:"-"`
}

// Parent ...
type Parent struct {
	ParentID                   string                 `json:"parent_id,omitempty"`
	RecordID                   string                 `json:"record_id,omitempty"`
	RecordIdentifier           string                 `json:"record_identifier,omitempty"`
	Log                        string                 `json:"log,omitempty"`
	Title                      *TitleInfo             `json:"title,omitempty"`
	Type                       string                 `json:"type,omitempty"`
	Issn                       string                 `json:"issn,omitempty"`
	Zdb                        string                 `json:"zdb,omitempty"`
	Dc                         []string               `json:"dc,omitempty"`
	DcOther                    []string               `json:"dc_other,omitempty"`
	DcOtherAuthority           []string               `json:"dc_other_authority,omitempty"`
	DcZvdd                     []string               `json:"dc_zvdd,omitempty"`
	DcDDC                      []string               `json:"dc_ddc,omitempty"`
	PublishInfos               *PublishInfo           `json:"publish_infos,omitempty"`
	CreatorInfos               []CreatorInfo          `json:"pcretor_infos,omitempty"`
	RightsAccessConditionInfos []RightAccessCondition `json:"rights_access_condition_infos,omitempty"`
	SubjectInfos               []SubjectInfo          `json:"subject_infos,omitempty"`
	Purl                       string                 `json:"purl,omitempty"`
	Catalogue                  string                 `json:"catalogue,omitempty"`
	RelatedItemInfos           []RelatedItemInfos     `json:"related_items_infos,omitempty"`
}

// Equal ...
func (s Parent) Equal(o Parent) bool {
	if (s.ParentID == o.ParentID) &&
		(s.RecordID == o.RecordID) &&
		(s.Log == o.Log) &&
		(s.Title == o.Title) &&
		(s.Type == o.Type) &&
		(s.Issn == o.Issn) &&
		(s.Zdb == o.Zdb) &&
		(len(s.Dc) == 0) &&
		(len(s.DcZvdd) == 0) &&
		(len(s.DcDDC) == 0) &&
		(s.PublishInfos == o.PublishInfos) &&
		(len(s.RightsAccessConditionInfos) == 0) {
		return true
	}

	return false
}

// IsEmpty ...
func (s Parent) IsEmpty() bool {
	return s.Equal(Parent{})
}

// KalliopeResponse ...
type KalliopeResponse struct {
	XMLName         xml.Name `xml:"searchRetrieveResponse"`
	Version         string   `xml:"version"`
	NumberOfRecords int      `xml:"numberOfRecords"`
}

// Order ...
type Order struct {
	Value int64
}

// CurrentValue ...
func (order Order) CurrentValue() int64 {
	return order.Value
}

// Increment ...
func (order *Order) Increment() {
	order.Value++
}

// ConvertTitleInfo ...
func ConvertTitleInfo(titleInfo *TitleInfo) *TitleInfo {

	tinfo := new(TitleInfo)

	var title string

	if titleInfo.Nonsort != "" {
		title = titleInfo.Nonsort + " " + titleInfo.Title
	} else {
		title = titleInfo.Title
	}

	re := regexp.MustCompile("^([ .:,*+\"'`´]*)([\\S\\s]*)")
	ti := re.FindStringSubmatch(titleInfo.Title)[2]

	var sorttitleFirst string
	var sorttitleEnd string

	for i, s := range ti {
		if i == 0 {
			sorttitleFirst = string(unicode.ToUpper(s))
		} else {
			sorttitleEnd = Join(sorttitleEnd, string(s))
		}
	}

	tinfo.Sorttitle = sorttitleFirst + sorttitleEnd
	tinfo.Title = title

	if titleInfo.PartnumberOrig == "" && titleInfo.Partname != "" {
		s := StripNonIntFloat(titleInfo.Partname)
		v, err := strconv.ParseInt(s, 10, 32)
		if err == nil {
			tinfo.Partnumber = v
		}
	} else if titleInfo.PartnumberOrig != "" {
		s := StripNonIntFloat(titleInfo.PartnumberOrig)
		v, err := strconv.ParseInt(s, 10, 32)
		if err == nil {
			tinfo.Partnumber = v
		}

		if titleInfo.Partname == "" {
			tinfo.Partname = titleInfo.PartnumberOrig
		}
	}
	tinfo.Subtitle = titleInfo.Subtitle
	tinfo.Type = titleInfo.Type
	tinfo.AuthorityURI = titleInfo.AuthorityURI
	tinfo.ValueURI = titleInfo.ValueURI

	return tinfo
}

// GetDigitizationInfo ...
func GetDigitizationInfo(oi OriginInfo) *DigitizationInfo {

	var digitizationInfo *DigitizationInfo = new(DigitizationInfo)

	digitizationInfo.EventType = oi.EventType
	digitizationInfo.Edition = oi.Edition

	if len(oi.Publisher) > 0 {
		str := strings.TrimSpace(oi.Publisher[0])
		digitizationInfo.PublisherDigitization = append(digitizationInfo.PublisherDigitization, str)
		digitizationInfo.PublisherInfo = oi.Publisher
	}
	if len(oi.Place) > 0 {
		digitizationInfo.PlaceDigitization = append(digitizationInfo.PlaceDigitization, oi.Place[0].PlaceTerm[0].Value)
		digitizationInfo.PlaceInfo = oi.Place
	}
	for _, date := range oi.DateCaptured {

		if strings.ToLower(date.Point) == "start" {
			digitizationInfo.YearDigitizationStart, _ = strconv.ParseInt(date.DateCaptured, 10, 64)
		} else if strings.ToLower(date.Point) == "end" {
			digitizationInfo.YearDigitizationEnd, _ = strconv.ParseInt(date.DateCaptured, 10, 64)
		}

		if strings.ToLower(date.KeyDate) == "yes" {
			digitizationInfo.YearDigitization, _ = strconv.ParseInt(date.DateCaptured, 10, 64)
		}
		if strings.ToLower(date.KeyDate) == "" && strings.ToLower(date.Point) == "" {
			digitizationInfo.YearDigitizationString = date.DateCaptured
		}

		if digitizationInfo.YearDigitizationString == "" {
			digitizationInfo.YearDigitizationString = date.DateCaptured
		}

		if digitizationInfo.YearDigitization == 0 {
			digitizationInfo.YearDigitization = digitizationInfo.YearDigitizationStart
		}

	}

	return digitizationInfo
}

// GetDigitizationInfoSimplified ...
func GetDigitizationInfoSimplified(oi OriginInfo) *DigitizationInfo {

	var digitizationInfo *DigitizationInfo = new(DigitizationInfo)

	digitizationInfo.EventType = oi.EventType
	digitizationInfo.Edition = oi.Edition

	for _, date := range oi.DateCaptured {

		if strings.ToLower(date.Point) == "start" {
			digitizationInfo.YearDigitizationStart, _ = strconv.ParseInt(date.DateCaptured, 10, 64)
		} else if strings.ToLower(date.Point) == "end" {
			digitizationInfo.YearDigitizationEnd, _ = strconv.ParseInt(date.DateCaptured, 10, 64)
		}

		if strings.ToLower(date.KeyDate) == "yes" {
			digitizationInfo.YearDigitization, _ = strconv.ParseInt(date.DateCaptured, 10, 64)
		}
		if strings.ToLower(date.KeyDate) == "" && strings.ToLower(date.Point) == "" {
			digitizationInfo.YearDigitizationString = date.DateCaptured
		}

		if digitizationInfo.YearDigitizationString == "" {
			digitizationInfo.YearDigitizationString = date.DateCaptured
		}

		if digitizationInfo.YearDigitization == 0 {
			digitizationInfo.YearDigitization = digitizationInfo.YearDigitizationStart
		}
	}

	return digitizationInfo
}

// GetPublishInfo ...
func GetPublishInfo(oi OriginInfo, product string, ccontext string) *PublishInfo {

	var publishInfo *PublishInfo = new(PublishInfo)

	publishInfo.EventType = oi.EventType
	publishInfo.Edition = oi.Edition

	if len(oi.Publisher) > 0 {
		str := strings.TrimSpace(oi.Publisher[0])
		publishInfo.Publisher = append(publishInfo.Publisher, str)
	}
	if len(oi.Place) > 0 {
		for _, place := range oi.Place {
			for _, placeterm := range place.PlaceTerm {
				if placeterm.Type == "text" {
					l := len(strings.Trim(placeterm.Value, " "))
					if l > 1 {
						publishInfo.Place = append(publishInfo.Place, placeterm.Value)
					}
				}
				publishInfo.PlaceInfo = append(publishInfo.PlaceInfo, place)
				//publishInfo.PlaceInfo = append(publishInfo.PlaceInfo, oi.Place...)
			}
		}
	}
	for _, date := range oi.DateIssued {
		dIssued := date.DateIssued

		// TODO product specific
		if ccontext == "nlh" {
			date.DateIssued = CheckDate(date.DateIssued, product)
		}

		if strings.ToLower(date.Point) == "start" {
			publishInfo.YearPublishStart, _ = strconv.ParseInt(date.DateIssued, 10, 64)
		} else if strings.ToLower(date.Point) == "end" {
			publishInfo.YearPublishEnd, _ = strconv.ParseInt(date.DateIssued, 10, 64)
		}

		if strings.ToLower(date.KeyDate) == "yes" {
			publishInfo.YearPublish, _ = strconv.ParseInt(date.DateIssued, 10, 64)
		}
		if strings.ToLower(date.KeyDate) == "" && strings.ToLower(date.Point) == "" {
			publishInfo.YearPublishString = date.DateIssued
		}

		if publishInfo.YearPublishString == "" {
			publishInfo.YearPublishString = dIssued
			//publishInfo.YearPublishString = date.DateIssued
		}

		if publishInfo.YearPublish == 0 {
			publishInfo.YearPublish = publishInfo.YearPublishStart
		}
	}
	for _, date := range oi.DateCreated {
		dCreated := date.DateCreated

		// TODO product specific
		if ccontext == "nlh" {
			date.DateCreated = CheckDate(date.DateCreated, product)
		}

		if strings.ToLower(date.Point) == "start" {
			publishInfo.YearPublishStart, _ = strconv.ParseInt(date.DateCreated, 10, 64)
		} else if strings.ToLower(date.Point) == "end" {
			publishInfo.YearPublishEnd, _ = strconv.ParseInt(date.DateCreated, 10, 64)
		}

		if strings.ToLower(date.KeyDate) == "yes" {
			publishInfo.YearPublish, _ = strconv.ParseInt(date.DateCreated, 10, 64)
		}
		if strings.ToLower(date.KeyDate) == "" && strings.ToLower(date.Point) == "" {
			publishInfo.YearPublishString = dCreated
		}

		if publishInfo.YearPublishString == "" {
			publishInfo.YearPublishString = date.DateCreated
		}

		if publishInfo.YearPublish == 0 {
			publishInfo.YearPublish = publishInfo.YearPublishStart
		}
	}

	return publishInfo
}

// ConcatSubject ...
func ConcatSubject(subjectStr string, v string, subjectTypeStr string, t string) (string, string) {
	if len(subjectStr) == 0 {
		subjectStr = v
		subjectTypeStr = t
	} else {
		subjectStr = Join(subjectStr, "/", v)
		subjectTypeStr = Join(subjectTypeStr, "/", t)
	}
	return subjectStr, subjectTypeStr
}

// Join ...
func Join(strs ...string) string {
	var sb strings.Builder
	for _, str := range strs {
		sb.WriteString(str)
	}
	return sb.String()
}

// StripNonIntFloat ...
func StripNonIntFloat(s string) string {
	f := func(c rune) bool {
		return !unicode.IsNumber(c) && (c != 46)
	}
	output := strings.FieldsFunc(s, f)
	if len(output) > 0 {
		return output[0]
	}

	return ""
}

// CheckDate ...
func CheckDate(date string, product string) string {

	// [1804], [1804?]
	re := regexp.MustCompile("^\\[(\\d{4})[?]*\\]$")
	match := re.FindStringSubmatch(date)
	if len(match) == 2 {
		return match[1]
	}

	// [1795-1807], [1795-96]
	re1 := regexp.MustCompile("^\\[(\\d{4})-\\d{2,4}\\]$")
	match1 := re1.FindStringSubmatch(date)
	if len(match1) == 2 {
		return match1[1]
	}

	// MDCCXCI. [1791], MDCCLXXI. [1771]-93
	re2 := regexp.MustCompile("^\\S* \\[(\\d{4})[?]*\\]\\S*$")
	match2 := re2.FindStringSubmatch(date)
	if len(match2) == 2 {
		return match2[1]
	}

	// 17uu
	re3 := regexp.MustCompile("^(\\d{2})uu$")
	match3 := re3.FindStringSubmatch(date)
	if len(match3) == 2 {
		return match3[1] + "00"
	}

	// 171u
	re4 := regexp.MustCompile("^(\\d{3})u$")
	match4 := re4.FindStringSubmatch(date)
	if len(match4) == 2 {
		return match4[1] + "0"
	}

	// c2007, 2007-, -2007, 1986-01-17
	re5 := regexp.MustCompile("^\\S*(\\d{4})\\S*$")
	match5 := re5.FindStringSubmatch(date)
	if len(match5) == 2 {
		return match5[1]
	}

	// 1712[i.e.1721], 1709-[1710], 1719-20 [i.e. 1720], 1728 [1729]
	re6 := regexp.MustCompile("^(\\d{4}).*\\[\\S*.*\\S*\\]$")
	match6 := re6.FindStringSubmatch(date)
	if len(match6) == 2 {
		return match6[1]
	}

	// 17191720
	re7 := regexp.MustCompile("^(\\d{4})\\d{4}$")
	match7 := re7.FindStringSubmatch(date)
	if len(match7) == 2 {
		return match7[1]
	}

	return date
}
