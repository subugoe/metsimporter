package mets

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"

	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.gwdg.de/subugoe/metsimporter/indexer/helper"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/index"

	"gitlab.gwdg.de/subugoe/metsimporter/indexer/types"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/util"

	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	config                 util.Config
	log                    logrus.Logger
	metsProcessingJobsChan chan *types.Mets
)

func init() {

	config, log = util.LoadConfig()

	metsProcessingJobsChan = make(chan *types.Mets, 2*config.Concurrency)
}

func getDmdsecMap(dmdsec []types.Dmdsec) map[string]types.Dmdsec {

	var dmdsecMap map[string]types.Dmdsec = make(map[string]types.Dmdsec)

	for _, dmd := range dmdsec {

		dmdsecMap[dmd.ID] = dmd
	}

	return dmdsecMap
}

func getParentRecordIDFromRelatedItem(dmdsec types.Dmdsec) string {

	for _, ri := range dmdsec.Relateditems {

		if ri.Type == "host" {
			return ri.RecordInfo.Recordidentifier.Value
		}
		continue
	}
	return ""
}

// TODO product specific
func getParentRecordIDFromFirstLogDiv(div types.Div, ccontext string, workID string) types.ParentDivInfo { //string {

	var parentRecordID, parentLabel string

	parentRecordID = ""

	if div.Label == "" {
		parentLabel = helper.StrctypeLabelMapping(div.Type)
		if parentLabel == "" {
			parentLabel = div.Type
		}
	} else {
		parentLabel = div.Label
	}

	parentType := div.Type

	if div.Mptr.Href != "" {
		href := div.Mptr.Href
		href = strings.ReplaceAll(href, " ", "")

		// TODO product specifics
		// TODO move regex string to config

		// nlh
		re1 := regexp.MustCompile("^(\\S*)/(\\S*):(\\S*).(mets).(xml)$")
		re2 := regexp.MustCompile("^(\\S*)/(\\S*)_(\\S*).(mets).(xml)$")

		// gdz and SUB digizeit
		re4 := regexp.MustCompile("^(\\S*)/mets/(\\S*).xml$")

		// external digizeit

		// http://goobiweb.bbf.dipf.de/viewer/metsresolver?id=020194994
		re5 := regexp.MustCompile("^(\\S*)/metsresolver\\?id=(\\S*)$")

		// http://digital.staatsbibliothek-berlin.de/dms/metsresolver/?PPN=PPN1046226541
		re6 := regexp.MustCompile("^(\\S*)/metsresolver/\\?PPN=(\\S*)$")

		// not usable
		// http://sammlungen.ub.uni-frankfurt.de/botanik/oai/?verb=GetRecord\u0026metadataPrefix=mets\u0026identifier=2165395
		//re7 := regexp.MustCompile("^(\\S*)/metsresolver/\\?PPN=(\\S*)$")

		// TODO product specific
		if ccontext == "nlh" {
			match := re1.FindStringSubmatch(href)
			if len(match) != 6 {
				log.Errorf("HRef %s doesn't match pattern %s", href, re1.String())
				return types.ParentDivInfo{}
			}
			parentRecordID = match[3]
			if parentRecordID == "" {
				match := re2.FindStringSubmatch(href)
				if len(match) != 6 {
					log.Errorf("HRef %s doesn't match pattern %s", href, re2.String())
					return types.ParentDivInfo{}
				}
				parentRecordID = match[3]
			}

		} else if (ccontext == "digizeit") || (ccontext == "gdz") {
			// TODO check ocrd?

			if strings.Contains(strings.ToLower(href), "digi.ub.uni-heidelberg.de") {
				// e.g. in s3://dzeit/mets/oai:digi.ub.uni-heidelberg.de:3224.xml
				// href="https://digi.ub.uni-heidelberg.de/diglit/jugend/mets"
				parentRecordID = ""
			} else if strings.Contains(strings.ToLower(href), "idb.ub.uni-tuebingen.de") {
				// http://idb.ub.uni-tuebingen.de/diglit/dzcw/mets
				parentRecordID = ""
			} else if strings.Contains(strings.ToLower(href), "goobiweb") {
				match := re5.FindStringSubmatch(href)
				if len(match) != 3 {
					log.Errorf("HRef %s doesn't match pattern %s", href, re5.String())
					return types.ParentDivInfo{}
				}
				parentRecordID = match[2]
			} else if strings.Contains(strings.ToLower(href), "digital.staatsbibliothek-berlin") {
				match := re6.FindStringSubmatch(href)
				if len(match) != 3 {
					log.Errorf("HRef %s doesn't match pattern %s", href, re6.String())
					return types.ParentDivInfo{}
				}
				parentRecordID = match[2]
			} else if strings.Contains(strings.ToLower(href), "sammlungen.ub.uni-frankfurt.de") {
				parentRecordID = ""
			} else {
				match := re4.FindStringSubmatch(href)
				if len(match) != 3 {
					log.Errorf("HRef %s doesn't match pattern %s (%s)", href, re4.String(), workID)
					return types.ParentDivInfo{}
				}
				parentRecordID = match[2]
			}
		}

	} else {
		return types.ParentDivInfo{}
	}

	return types.ParentDivInfo{
		ParentID:    helper.DeriveWork(parentRecordID),
		ParentLabel: parentLabel,
		ParentType:  parentType,
	}
}

func getMapFromFileSec(fileSec types.Filesec, context string, isExternal bool, workID string) (map[string]types.FileSecAttributes, map[string]bool) {

	var fileIDToAttrMap map[string]types.FileSecAttributes = make(map[string]types.FileSecAttributes)
	var uniqueUseNames map[string]bool = make(map[string]bool)

	for _, fileGrp := range fileSec.Filegrps {

		var re *regexp.Regexp
		var re2 *regexp.Regexp

		var reString string
		var reString2 string

		uniqueUseNames[fileGrp.Use] = true

		// TODO move regex string to config
		if !isExternal {

			// TODO product specific
			if (context == "gdz") || (context == "digizeit") {
				if (strings.ToLower(fileGrp.Use) == "default") || (strings.ToLower(fileGrp.Use) == "presentation") || (strings.ToLower(fileGrp.Use) == "thumbs") {

					// TODO check this
					reString = "^([\\W\\S]*)/(\\S*).(tif|TIF|gif|GIF|jpg|JPG|png|PNG)$"
					re = regexp.MustCompile(reString)

				} else if strings.ToLower(fileGrp.Use) == "gdzocr" {
					reString = "^([\\W\\S]*)/(\\S*).(xml|txt|html)$"
					re = regexp.MustCompile(reString)
				} else {
					continue
				}
			} else if context == "nlh" {
				// {scheme}://{server}{/prefix}/{identifier}/{region}/{size}/{rotation}/{quality}.{format}
				if (strings.ToLower(fileGrp.Use) == "default") || (strings.ToLower(fileGrp.Use) == "presentation") || (strings.ToLower(fileGrp.Use) == "thumbs") {
					reString = "^(\\S*)/\\S*:\\S*:(\\S*)/\\S*/\\S*/\\S*/\\S*.(tif|TIF|gif|GIF|jpg|JPG)$$"
					re = regexp.MustCompile(reString)
				} else if strings.ToLower(fileGrp.Use) == "fulltext" {
					// https://nl.sub.uni-goettingen.de/fulltext/ahn:102B478E71658745:102B478F399BFD88.txt
					reString = "^(\\S*)/\\S*:\\S*:(\\S*).(tei.xml|txt.xml|html|txt)$"
					re = regexp.MustCompile(reString)
				} else {
					continue
				}
			} else if context == "ocrd" {
				// "OCR-D-GT-SEG-PAGE/OCR-D-GT-SEG-PAGE_0001.xml"
				// DEFAULT/00000011.tif
				// OCR-D-BIN/OCR-D-BIN_00000001..xml
				reString = "^(\\S*)/(\\S*)\\.(\\S*)$"
				re = regexp.MustCompile(reString)
			}
		} else {

			if (strings.ToLower(fileGrp.Use) == "default") || (strings.ToLower(fileGrp.Use) == "presentation") || (strings.ToLower(fileGrp.Use) == "thumbs") {
				reString = "^\\S*/((\\S*).(tif|TIF|gif|GIF|jpg|JPG|png|PNG))$"
				re = regexp.MustCompile(reString)

				reString2 = "^([\\W\\S]*)/([\\d]*)/(\\S*)$"
				re2 = regexp.MustCompile(reString2)

			} else {
				continue
			}
		}

		for _, file := range fileGrp.Files {

			if isExternal {
				var arr []string

				for _, flocat := range file.FLocat {

					if flocat.Loctype == "URL" {

						if strings.HasPrefix(flocat.Href, "http://sammlungen.ub.uni-frankfurt.de/botanik/download/webcache/") ||
							(strings.HasPrefix(flocat.Href, "https://sammlungen.ub.uni-frankfurt.de/botanik/download/webcache/")) {
							arr = re2.FindStringSubmatch(strings.Replace(flocat.Href, " ", "", -1))

							if len(arr) != 4 {
								log.Errorf("URL %s doesn't match pattern %s (%s)", flocat.Href, reString, workID)
							} else {
								fileIDToAttrMap[file.ID] = types.FileSecAttributes{
									FileID:   file.ID,
									Use:      fileGrp.Use,
									Mimetype: file.Mimetype,
									Loctype:  flocat.Loctype,
									Href:     flocat.Href,
									Page:     arr[3],
									Filename: arr[3],
									Format:   "",
								}
							}
						} else {
							arr = re.FindStringSubmatch(strings.Replace(flocat.Href, " ", "", -1))

							if len(arr) != 4 {
								log.Errorf("URL %s doesn't match pattern %s (%s)", flocat.Href, reString, workID)
							} else {
								fileIDToAttrMap[file.ID] = types.FileSecAttributes{
									FileID:   file.ID,
									Use:      fileGrp.Use,
									Mimetype: file.Mimetype,
									Loctype:  flocat.Loctype,
									Href:     flocat.Href,
									Page:     arr[2],
									Filename: arr[1],
									Format:   arr[3],
								}
							}
						}
					}
				}
			} else {

				if context == "ocrd" {
					for _, flocat := range file.FLocat {

						if flocat.OtherLoctype == "FILE" {
							var arr []string = re.FindStringSubmatch(strings.Replace(flocat.Href, " ", "", -1))
							if len(arr) != 4 {
								log.Errorf("URL %s doesn't match pattern %s", flocat.Href, reString)
							} else {
								fileIDToAttrMap[file.ID] = types.FileSecAttributes{
									FileID:       file.ID,
									Use:          fileGrp.Use,
									Mimetype:     file.Mimetype,
									Loctype:      flocat.Loctype,
									OtherLoctype: flocat.OtherLoctype,
									Href:         flocat.Href,
									Page:         arr[2],
									Filename:     arr[2] + "." + arr[3],
									Format:       arr[3],
								}
							}
						}
						// else if flocat.Loctype == "URL" {
						// 	var arr []string = re.FindStringSubmatch(strings.Replace(flocat.Href, " ", "", -1))
						// 	if len(arr) != 4 {
						// 		log.Errorf("URL %s doesn't match pattern %s", flocat.Href, reString)
						// 	} else {
						// 		fileIDToAttrMap[file.ID] = types.FileSecAttributes{
						// 			FileID:       file.ID,
						// 			Use:          fileGrp.Use,
						// 			Mimetype:     file.Mimetype,
						// 			Loctype:      flocat.Loctype,
						// 			OtherLoctype: flocat.OtherLoctype,
						// 			Href:         flocat.Href,
						// 			Page:         arr[2],
						// 			Filename:     arr[2] + "." + arr[3],
						// 			Format:       arr[3],
						// 		}
						// 	}
						// }
					}
				} else {
					for _, flocat := range file.FLocat {

						if flocat.Loctype == "URL" {
							var arr []string = re.FindStringSubmatch(strings.Replace(flocat.Href, " ", "", -1))
							if len(arr) != 4 {
								log.Errorf("URL %s doesn't match pattern %s", flocat.Href, reString)
							} else {
								fileIDToAttrMap[file.ID] = types.FileSecAttributes{
									FileID:       file.ID,
									Use:          fileGrp.Use,
									Mimetype:     file.Mimetype,
									Loctype:      flocat.Loctype,
									OtherLoctype: flocat.OtherLoctype,
									Href:         flocat.Href,
									Page:         arr[2],
									Filename:     arr[2] + "." + arr[3],
									Format:       arr[3],
								}
							}
						}
					}

				}
			}
		}
	}

	//log.Debugf("fileIDToAttrMap '%v'", fileIDToAttrMap)
	return fileIDToAttrMap, uniqueUseNames
}

func getMapFromPhysDivs(physDivs []types.Div, workID string, context string, imageFileGrp string, fulltextFileGrp string) map[string]types.PhysicalAttributes {

	var physicalIDToAttrMap map[string]types.PhysicalAttributes = make(map[string]types.PhysicalAttributes)

	var useToFileIDMap map[string]string

	for index, div := range physDivs {

		useToFileIDMap = make(map[string]string)
		var fileIDsArr []string

		for _, fptr := range div.Fptr {

			// TODO product specific
			if context == "ocrd" {
				if strings.Contains(strings.ToLower(fptr.Fileid), strings.ToLower(imageFileGrp)) {
					useToFileIDMap[imageFileGrp] = fptr.Fileid
				} else if strings.Contains(strings.ToLower(fptr.Fileid), strings.ToLower(fulltextFileGrp)) {
					useToFileIDMap[fulltextFileGrp] = fptr.Fileid
				}
				fileIDsArr = append(fileIDsArr, fptr.Fileid)
			} else {
				if strings.Contains(strings.ToLower(fptr.Fileid), "default") {
					useToFileIDMap["DEFAULT"] = fptr.Fileid
				} else if strings.Contains(strings.ToLower(fptr.Fileid), "presentation") {
					useToFileIDMap["PRESENTATION"] = fptr.Fileid
				} else if strings.Contains(strings.ToLower(fptr.Fileid), "gdzocr") {
					useToFileIDMap["FULLTEXT"] = fptr.Fileid
				} else if strings.Contains(strings.ToLower(fptr.Fileid), "fulltext") {
					useToFileIDMap["FULLTEXT"] = fptr.Fileid
				} else if strings.Contains(strings.ToLower(fptr.Fileid), "thumb") {
					useToFileIDMap["THUMBS"] = fptr.Fileid
				}
			}
		}

		o, err := strconv.ParseInt(div.Order, 10, 32)
		if err != nil {
			// e.g. div.Order == ""
			o = int64(index + 1)
			log.Errorf("physical div.Order '%s' not convertible to integer, use div index position '%d' (%s)\n", div.Order, o, workID)
		}

		physicalIDToAttrMap[div.ID] = types.PhysicalAttributes{
			ID:             div.ID,
			DMDID:          div.DmdID,
			AMDID:          div.Amdid,
			Order:          div.Order,
			OrderValue:     o,
			Orderlabel:     div.Orderlabel,
			Type:           div.Type,
			ContentID:      div.ContentIDs,
			UseToFileIDMap: useToFileIDMap,
			FileIDsArr:     fileIDsArr,
			DivPosition:    int64(index + 1),
		}

	}

	return physicalIDToAttrMap
}

func getLogDivList(order *types.Order, divs []types.Div, logDivList *[]types.Div, level int8, workID string, parentWorkID string, parentLogID string, isanchor bool, multivolumework bool, context string) {

	for _, div := range divs {

		order.Increment()

		div.OOrder = order.CurrentValue()
		div.WorkID = workID

		if isanchor {
			div.Level = level
			*logDivList = append(*logDivList, div)
			break
		}

		if (helper.Contains(config.MultivolumeworkTypes, strings.ToLower(div.Type))) || (level == 1 && div.Mptr.Href != "") {
			multivolumework = true
		}

		if multivolumework {

			div.IsPartOfMultivolumework = true

			// volume eg. PPN345574974_0006
			if level == 1 {
				getLogDivList(order, div.Divs, logDivList, level+1, workID, parentWorkID, parentLogID, isanchor, multivolumework, context)

				//  volume eg. PPN345574974_0006 || monograph eg. PPN234999721
			} else if level == 2 {

				// TODO product specific
				if context == "nlh" {
					div.ParentID = parentWorkID
				} else {
					div.ParentID = parentWorkID + "|" + parentLogID
				}

				div.ParentWork = parentWorkID
				div.ParentLog = parentLogID

				getLogDivList(order, div.Divs, logDivList, level+1, workID, parentWorkID, div.ID, isanchor, multivolumework, context)
				//  volume eg. PPN345574974_0006 || monograph eg. PPN234999721
			} else if level > 2 {

				// TODO product specific
				if context == "nlh" {
					div.ParentID = workID
				} else {
					div.ParentID = workID + "|" + parentLogID
				}

				div.ParentWork = workID
				div.ParentLog = parentLogID

				getLogDivList(order, div.Divs, logDivList, level+1, workID, parentWorkID, div.ID, isanchor, multivolumework, context)
			}
		} else if !multivolumework {

			if level == 1 {
				if len(div.Divs) == 0 {

					// TODO product specific
					if context == "nlh" {
						div.ParentID = workID
					} else {
						div.ParentID = workID + "|" + parentLogID
					}

					div.ParentWork = workID
					div.ParentLog = parentLogID

				} else {
					div.ParentID = ""
					div.ParentWork = ""
					div.ParentLog = ""

					getLogDivList(order, div.Divs, logDivList, level+1, workID, parentWorkID, div.ID, isanchor, multivolumework, context)
				}

				//  volume eg. PPN345574974_0006 || monograph eg. PPN234999721
			} else if level >= 2 {

				// TODO product specific
				if context == "nlh" {
					div.ParentID = workID
				} else {
					div.ParentID = workID + "|" + parentLogID
				}

				div.ParentWork = workID
				div.ParentLog = parentLogID

				getLogDivList(order, div.Divs, logDivList, level+1, workID, parentWorkID, div.ID, isanchor, multivolumework, context)
			}

		}

		if level > 1 || !multivolumework {
			div.Level = level
			*logDivList = append(*logDivList, div)
		}
	}

}

func getRedisClient() *redis.Client {

	return redis.NewClient(&redis.Options{
		Addr:         config.RedisAdr,
		DB:           config.RedisDB, // use default DB
		MaxRetries:   config.RedisMaxRetries,
		MinIdleConns: config.RedisMinIDConns,
		DialTimeout:  time.Minute,
		ReadTimeout:  time.Minute,
	})
}

// ProcessMetsWorker ...
func ProcessMetsWorker(mythread int) {

	redisClient := getRedisClient()
	defer redisClient.Close()

	for metsObject := range metsProcessingJobsChan {

		start := time.Now()

		var dmdidOfFirstPhysDiv string

		var logToPhysLinkMap map[string]types.LogToPhysLink = make(map[string]types.LogToPhysLink)
		var physToLogLinkMap map[string]types.PhysToLogLink = make(map[string]types.PhysToLogLink)

		if metsObject.Doctype != "anchor" {
			logToPhysLinkMap, physToLogLinkMap = helper.GetLogToXLinkMap(metsObject)
		}

		dmdsecMap := getDmdsecMap(metsObject.Dmdsecs)

		var logStructMap types.Structmap
		var physStructMap types.Structmap
		var parentDivInfo types.ParentDivInfo

		for _, structmap := range metsObject.Structmaps {
			if structmap.Type == "LOGICAL" {
				logStructMap = structmap
				parentDivInfo = getParentRecordIDFromFirstLogDiv(structmap.Divs[0], metsObject.Context, metsObject.WorkID)
			} else if structmap.Type == "PHYSICAL" {
				dmdidOfFirstPhysDiv = structmap.Divs[0].DmdID
				physStructMap = structmap
			}

		}

		var parentInfo *types.Parent = new(types.Parent)

		parentWorkID := ""
		var isanchor bool

		var isExternal bool
		if len(metsObject.Dmdsecs) > 0 {
			for _, ac := range metsObject.Dmdsecs[0].AccessConditions {
				if strings.ToLower(ac.Value) == "externalcontent" {
					isExternal = true
				}
			}
		}

		var prevPIDs []string
		var err error
		// get prevPIDs from log index
		if metsObject.PrevPID != "" {
			prevPIDs, err = index.GetPrevPIDsFromES(metsObject.PID, "log")

			if (err != nil) || len(prevPIDs) == 0 {
				// get prevPIDs from phys index, in some cases we only have a phys index
				prevPIDs, err = index.GetPrevPIDsFromES(metsObject.PID, "phys")
				if err != nil {
					log.Errorf("Indexing rejected for PID %s, due to %s", metsObject.PID, err.Error())
					continue
				}
			}

			metsObject.PrevPIDs = prevPIDs
		}

		// get parentInfo

		// TODO product specific
		if metsObject.Context != "ocrd" {

			// TODO get parentInfo from DB instead file
			if metsObject.Doctype != "anchor" {
				isanchor = false

				// TODO check ocrd?
				// TODO product specific
				if metsObject.Context == "digizeit" {
					parentWorkID = helper.DeriveWork(getParentRecordIDFromRelatedItem(metsObject.Dmdsecs[0]))
				} else {
					parentWorkID = getParentRecordIDFromRelatedItem(metsObject.Dmdsecs[0])
				}

				if parentWorkID != "" {

					if parentDivInfo.ParentID == "" {
						parentDivInfo.ParentID = parentWorkID
					}

					var parentKey string

					// TODO product specific
					if metsObject.Context == "nlh" {
						parentKey = "mets/" + parentWorkID + ".mets.xml"
					} else {
						parentKey = "mets/" + parentWorkID + ".xml"
					}

					var err error

					res, _ := redisClient.Get(parentKey).Result()
					if res == "nosuchkey" {
						log.Errorf("reject processing of %s, due to parent %s doesn't exist", metsObject.IndexerJob.Document, parentKey)
						continue
					}

					if !helper.ValidationRun() {

						// TODO product specific
						if metsObject.Context == "nlh" {
							parentInfo, err = index.GetParentInfoFromNLHSolr(parentWorkID)
						} else if metsObject.Context == "gdz" {
							// TODO check, if RelatedItemInfos of parent is also required (preceding, succeeding)
							parentInfo, err = index.GetParentInfoFromGDZSolr(parentWorkID)
						} else {
							// RelatedItemInfos added to get info about preceding, succeeding
							parentInfo, err = index.GetParentInfoFromES(parentWorkID)
						}

						if err != nil {
							parentInfo = nil

							log.Infof("parent %s for child %s is not yet indexed, parent will be added to the queue", parentWorkID, metsObject.IndexerJob.Document)

							if strings.Contains(metsObject.Product, "nlh-ahn") {
								continue
							}

							// unlock key (current)
							_, err := redisClient.Del(metsObject.IndexerJob.Key).Result()
							if err != nil {
								log.Errorf("could not delete key %s from redis, due to %s", metsObject.IndexerJob.Key, err.Error())
							}

							msg := types.IndexerJob{
								Document: parentWorkID,
								Context:  metsObject.IndexerJob.Context,
								Product:  metsObject.IndexerJob.Product,
								Reindex:  metsObject.IndexerJob.Reindex,
								Key:      parentKey,
								Path:     metsObject.IndexerJob.Path,
								Filename: "",
							}

							// add parent to processing queue (at the queue start, process read from right "R")
							parentJsonData, err := json.Marshal(msg)
							if err != nil {
								log.Errorf("could not marshal IndexerJob %v, due to %s", msg, err.Error())
							}
							_, err = redisClient.RPush(config.RedisIndexQueue, string(parentJsonData)).Result()
							if err != nil {
								log.Errorf("could not push indexing job for %s, due to %s", parentWorkID, err.Error())
							}

							// add child again to processing queue (at the queue end, process read from right "R")
							metsObject.IndexerJob.Starttime = time.Now()
							childJsonData, err := json.Marshal(metsObject.IndexerJob)
							if err != nil {
								log.Errorf("could not marshal IndexerJob %v, due to %s", metsObject.IndexerJob, err.Error())
							}
							_, err = redisClient.LPush(config.RedisIndexQueue, string(childJsonData)).Result()
							if err != nil {
								log.Errorf("could not push indexing job for %s, due to %s", metsObject.IndexerJob.Document, err.Error())
							}

							// continue if any error occured
							continue
						}
					}
				}
			} else {
				isanchor = true
			}
		}

		var logDivList []types.Div = make([]types.Div, 0)

		order := types.Order{Value: 1}

		amdsecMap := getAmdsecMap(metsObject.Amdsecs)

		var physicalIDToAttrMap map[string]types.PhysicalAttributes
		var fileIDToAttrMap map[string]types.FileSecAttributes
		var useMap map[string]bool
		if metsObject.Doctype == "work" {
			// TODO limit to the important fileGrps
			fileIDToAttrMap, useMap = getMapFromFileSec(metsObject.Filesec, metsObject.Context, isExternal, metsObject.WorkID)

			// Set ImageFileGrp to a existing value if empty
			if metsObject.Context == "ocrd" && metsObject.IndexerJob.ImageFileGrp == "" {
				if _, exists := useMap["PRESENTATION"]; exists {
					metsObject.IndexerJob.ImageFileGrp = "PRESENTATION"
				} else if _, exists := useMap["DEFAULT"]; exists {
					metsObject.IndexerJob.ImageFileGrp = "DEFAULT"
				} else if _, exists := useMap["OCR-D-IMG"]; exists {
					metsObject.IndexerJob.ImageFileGrp = "OCR-D-IMG"
				}
			}
			physicalIDToAttrMap = getMapFromPhysDivs(physStructMap.Divs[0].Divs, metsObject.WorkID, metsObject.Context, metsObject.IndexerJob.ImageFileGrp, metsObject.IndexerJob.FulltextFileGrp)
		}

		var dateIndexed, dateModified string

		// TODO product specific
		if metsObject.Context == "digizeit" {
			dateIndexed, dateModified = index.GetUpdateDatesFromRedis(metsObject.RecordIdentifier, redisClient)
		} else if metsObject.Context == "nlh" {
			dateIndexed, dateModified = index.GetUpdateDatesFromSolrNLH(metsObject.RecordIdentifier, redisClient)
		} else if metsObject.Context == "gdz" {
			dateIndexed, dateModified = index.GetUpdateDatesFromSolrGDZ(metsObject.RecordIdentifier, redisClient)
		}

		// handle if parentInfo is nil
		getLogDivList(&order, logStructMap.Divs, &logDivList, 1, metsObject.WorkID, parentWorkID, parentInfo.Log, isanchor, false, metsObject.Context)

		// TODO paralelisieren via channel
		esLogMap, firstLog, error := index.GetESLogFromDmdsec(
			metsObject.NonModsDmdsecIDs,
			logDivList,
			metsObject.MetsHdr,
			dmdsecMap,
			amdsecMap,
			logToPhysLinkMap,
			physicalIDToAttrMap,
			useMap,
			parentInfo,
			parentDivInfo,
			dmdidOfFirstPhysDiv,
			dateIndexed,
			dateModified,
			redisClient,
			metsObject,
			prevPIDs) // TODO check prevPIDs, where/how created

		if error != nil {
			log.Errorf("Indexing rejected for %s, due to %s", metsObject.WorkID, error.Error())
			continue
		}
		var esPhysMap = make(map[string]types.ESPhys)

		if !isanchor {
			esPhysMap = index.GetESPhysFromPhysStructMap(
				physicalIDToAttrMap,
				fileIDToAttrMap,
				physToLogLinkMap,
				logToPhysLinkMap,
				esLogMap,
				metsObject,
				redisClient,
				isExternal,
				firstLog)
		}

		// TODO product specific
		if !helper.ValidationRun() {
			if metsObject.Context == "digizeit" || metsObject.Context == "ocrd" {

				index.WriteToElasticSearch(types.ESDoc{
					ESLogMap:  esLogMap,
					ESPhysMap: esPhysMap,
					Product:   metsObject.Product,
					Context:   metsObject.Context})

			} else {
				index.WriteToSolr(types.ESDoc{
					ESLogMap:  esLogMap,
					ESPhysMap: esPhysMap,
					Product:   metsObject.Product,
					Context:   metsObject.Context})
			}

			if metsObject.Context != "ocrd" {

				citationData := types.ExportWork{
					WorkID:  metsObject.WorkID,
					Context: metsObject.Context,
					Product: metsObject.Product,
					Time:    time.Now(),
				}

				citatioJson, err := json.Marshal(citationData)
				if err != nil {
					log.Errorf("could not marshal citation data %v, due to %s", citatioJson, err)
					continue
				}

				_, err = redisClient.LPush(config.RedisCitationQueue, string(citatioJson)).Result()
				if err != nil {
					log.Errorf("could not push citation data to redis queue '%s', due to %s", config.RedisCitationQueue, err)
				}
			}

			if !isanchor {
				var manifestData types.ExportWork

				if metsObject.Context == "ocrd" {
					manifestData = types.ExportWork{
						WorkID:  metsObject.PID,
						PID:     metsObject.PID,
						Context: metsObject.Context,
						Product: metsObject.Product,
						Time:    time.Now(),
					}
				} else {
					manifestData = types.ExportWork{
						WorkID:  metsObject.WorkID,
						PID:     metsObject.PID,
						Context: metsObject.Context,
						Product: metsObject.Product,
						Time:    time.Now(),
					}
				}

				// TODO same as above
				manifestJson, err := json.Marshal(manifestData)
				if err != nil {
					log.Errorf("could not marshal manifest data %v, due to %s", manifestJson, err)
					continue
				}

				// TODO write to redis failed ??????????
				_, err = redisClient.LPush(config.RedisIiifManifestQueue, string(manifestJson)).Result()
				if err != nil {
					log.Errorf("could not push iiif manifest job to redis queue '%s', due to %s", config.RedisIiifManifestQueue, err)
				}

				_, err = redisClient.LPush(config.RedisInfoJsonQueue, string(manifestJson)).Result()
				if err != nil {
					log.Errorf("could not push infojson manifest job to redis queue '%s', due to %s", config.RedisInfoJsonQueue, err)
				}
			}

		}

		elapsed := time.Since(start)

		log.Infof("Finish indexing of %s (%s)", metsObject.IndexerJob.Document, elapsed)
	}
}

func getMetsStructFromXML(recordIdentifier types.ID, indexerJob types.IndexerJob, decoder *xml.Decoder) *types.Mets {

	mets := &types.Mets{}

	mets.RecordIdentifier = recordIdentifier.Value
	mets.IndexerJob = indexerJob

	mets.Context = indexerJob.Context
	mets.Product = indexerJob.Product

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Errorf("failed to get token, due to %v", tokenErr.Error())
				return nil
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "mets" {

				decErr := decoder.DecodeElement(mets, &startElem)
				if decErr != nil {
					log.Errorf("failed to decode file, due to %v", decErr.Error())
					return nil
				}
			}
		case xml.EndElement:
			//
		}

	}

	filesec := mets.Filesec
	var physStructmapExist = false
	for _, physStructmap := range mets.Structmaps {
		if physStructmap.Type == "PHYSICAL" {
			physStructmapExist = true
		}
	}

	if len(filesec.Filegrps) == 0 || !physStructmapExist {
		mets.Doctype = "anchor"
	} else {
		mets.Doctype = "work"

		// TODO product specific
		if mets.Context == "ocrd" {
			if indexerJob.IsGt != true {
				for _, fileGrp := range filesec.Filegrps {
					fg := strings.ToLower(fileGrp.Use)
					if strings.Contains(fg, "-gt-") || strings.Contains(fg, "_gt_") {
						// TODO duplicated information
						mets.IsGt = true
						indexerJob.IsGt = true
						break
					}
				}
			}
		}
	}

	if strings.ToLower(mets.Context) == "nlh" {
		for _, id := range mets.Dmdsecs[0].Identifiers {
			if strings.ToLower(id.Type) == "work" {

				// TODO ask juergen to correct the eha recordIdentifier
				if strings.Contains(id.Value, "eha_") {
					mets.WorkID = strings.ReplaceAll(id.Value, "eha_", "")
				} else {
					mets.WorkID = id.Value
				}
			}
		}

		if mets.WorkID == "" {
			mets.WorkID = recordIdentifier.Value
		}

	} else {

		// TODO product specific
		if mets.Context == "digizeit" {
			mets.WorkID = helper.DeriveWork(recordIdentifier.Value)
		} else {
			mets.WorkID = recordIdentifier.Value
		}

	}

	return mets
}

func getNonModsDmdsecIDs(decoder *xml.Decoder, key string) ([]string, map[string]types.DmdsecCheck) {

	var nonModsDmdsecIDs []string
	var gtDmdsecs map[string]types.DmdsecCheck
	gtDmdsecs = make(map[string]types.DmdsecCheck)

	for {
		dmdsecCheck := &types.DmdsecCheck{}
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Errorf("failed to read token in %s, due to %s", key, tokenErr.Error())
				return []string{}, map[string]types.DmdsecCheck{}
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if startElem.Name.Local == "dmdSec" {

				decErr := decoder.DecodeElement(dmdsecCheck, &startElem)
				if decErr != nil {
					log.Errorf("could not decode dmdSec in %s, due to %s", key, decErr.Error())
					continue
				}

				mdtype := strings.TrimSpace(dmdsecCheck.MdWrap.MDTYPE)
				othermdtype := strings.TrimSpace(dmdsecCheck.MdWrap.OTHERMDTYPE)

				if strings.ToLower(mdtype) == "mods" {
					continue
				} else if strings.ToLower(mdtype) == "other" && strings.ToLower(othermdtype) == "gt" {
					gtDmdsecs[dmdsecCheck.ID] = types.DmdsecCheck{
						ID:     dmdsecCheck.ID,
						MdWrap: dmdsecCheck.MdWrap,
					}
					nonModsDmdsecIDs = append(nonModsDmdsecIDs, dmdsecCheck.ID)
				} else {
					nonModsDmdsecIDs = append(nonModsDmdsecIDs, dmdsecCheck.ID)
				}

			}
		case xml.EndElement:
			//
		}
	}

	return nonModsDmdsecIDs, gtDmdsecs
}

func addToIDMap(ids map[string][]types.ID, recordIdentifier *types.Recordidentifier, identifier *types.Identifier, IsRecordID bool) {

	if IsRecordID {
		s := strings.ReplaceAll(recordIdentifier.Source, " ", "")
		idSource := strings.ToLower(s)
		if len(ids[idSource]) == 0 {
			ids[idSource] = []types.ID{
				{IsRecordID: IsRecordID,
					Type:  idSource,
					Value: strings.TrimSpace(recordIdentifier.Value)}}
		} else {
			ids[idSource] = append(ids[idSource],
				types.ID{
					IsRecordID: IsRecordID,
					Type:       idSource,
					Value:      strings.TrimSpace(recordIdentifier.Value)})
		}
	} else {
		t := strings.ReplaceAll(identifier.Type, " ", "")
		idType := strings.ToLower(t)
		if len(ids[idType]) == 0 {
			ids[idType] = []types.ID{
				{IsRecordID: IsRecordID,
					Type:  idType,
					Value: strings.TrimSpace(identifier.Value)}}
		} else {
			ids[idType] = append(ids[idType], types.ID{
				IsRecordID: IsRecordID,
				Type:       idType,
				Value:      strings.TrimSpace(identifier.Value)})
		}
	}

}

func getRecordIdentifier(ids map[string][]types.ID, ccontext string) types.ID {

	// TODO product specific
	if ccontext == "nlh" {
		if len(ids["recordidentifier"]) != 0 {
			for _, id := range ids["recordidentifier"] {
				if id.IsRecordID {
					return id
				}
			}
		}
		if len(ids["cengagegale"]) != 0 {
			for _, id := range ids["cengagegale"] {
				if id.IsRecordID {
					return id
				}
			}
		}

		if len(ids["gale"]) != 0 {
			for _, id := range ids["gale"] {
				if id.IsRecordID {
					return id
				}
			}
		}
		if len(ids["readex"]) != 0 {
			for _, id := range ids["readex"] {
				if id.IsRecordID {
					return id
				}
			}
		}
	} else {
		if len(ids["urn"]) != 0 {
			for _, id := range ids["urn"] {
				if id.IsRecordID {
					return id
				}
			}
		}
		if len(ids["urn"]) != 0 {
			for _, id := range ids["urn"] {
				if !id.IsRecordID {
					return id
				}
			}
		}
		if len(ids["gbv-ppn"]) != 0 {
			for _, id := range ids["gbv-ppn"] {
				if !id.IsRecordID {
					return id
				}
			}

		}
		if len(ids["gbv-ppn"]) != 0 {
			for _, id := range ids["gbv-ppn"] {
				if id.IsRecordID {
					return id
				}
			}

		}
		if len(ids["recordidentifier"]) != 0 {
			for _, id := range ids["recordidentifier"] {
				if id.IsRecordID {
					return id
				}
			}
		}
		if len(ids["swb-ppn"]) != 0 {
			for _, id := range ids["swb-ppn"] {
				if id.IsRecordID {
					return id
				}
			}
		}
		if len(ids["spo-id"]) != 0 {
			for _, id := range ids["spo-id"] {
				if id.IsRecordID {
					return id
				}
			}
		}
		if len(ids["zdb-id"]) != 0 {
			for _, id := range ids["zdb-id"] {
				if id.IsRecordID {
					return id
				}
			}
		}
		if len(ids["oai"]) != 0 {
			for _, id := range ids["oai"] {
				if !id.IsRecordID {
					return id
				}
			}
		}
		if len(ids["de-611"]) != 0 {
			for _, id := range ids["de-611"] {
				if id.IsRecordID {
					return id
				}
			}
		}
		// SUB Regelsatz für Kopialbücher
		if len(ids["arcinsys.niedersachsen"]) != 0 {
			for _, id := range ids["arcinsys.niedersachsen"] {
				if id.IsRecordID {
					return id
				}
			}
		}

	}
	return types.ID{}
}

func getRecordIdentifierFromXML(decoder *xml.Decoder, ccontext string, key string) types.ID {

	var isRelatedItem bool
	var firstDmdsec bool
	var ids map[string][]types.ID

	for {
		tok, tokenErr := decoder.Token()

		if tokenErr != nil {
			if tokenErr == io.EOF {
				break
			} else {
				log.Errorf("failed to read token in %s, due to %s", key, tokenErr.Error())
				return types.ID{}
			}
		}

		switch startElem := tok.(type) {
		case xml.StartElement:
			if isRelatedItem {
				continue
			}

			if startElem.Name.Local == "relatedItem" {
				isRelatedItem = true
			} else if startElem.Name.Local == "dmdSec" {

				firstDmdsec = true

				ids = make(map[string][]types.ID)
			} else if startElem.Name.Local == "identifier" {
				identifier := &types.Identifier{}
				decErr := decoder.DecodeElement(identifier, &startElem)
				if decErr != nil {
					log.Errorf("could not decode identifier in %s, due to %s", key, decErr.Error())
					continue
				}

				addToIDMap(ids, nil, identifier, false)

			} else if startElem.Name.Local == "recordIdentifier" {
				recordIdentifier := &types.Recordidentifier{}
				decErr := decoder.DecodeElement(recordIdentifier, &startElem)
				if decErr != nil {
					log.Errorf("could not decode recordidentifier in %s, due to %s", key, decErr.Error())
					continue
				}

				// add dummy source, if nil
				if recordIdentifier.Source == "" {
					recordIdentifier.Source = "recordidentifier"
				}

				addToIDMap(ids, recordIdentifier, nil, true)

			}
		case xml.EndElement:
			if startElem.Name.Local == "relatedItem" {
				isRelatedItem = false
			} else if startElem.Name.Local == "dmdSec" {

				if firstDmdsec {
					rid := getRecordIdentifier(ids, ccontext)

					return rid
				}

			}
		}
	}
	return types.ID{}
}

// ParseMetsWorker ...
func ParseMetsWorker(mythread int) {

	redisClient := getRedisClient()
	defer redisClient.Close()

	// TODO remove this
	// TODO product specific
	if config.CContext != "ocrd" {
		for {
			// waiting for collector to finish work
			val, err := redisClient.HGet(config.RedisStatusHsetQueue, config.RedisFinishMetsKeyResolving).Result()
			if err != nil {
				log.Errorf("could not get value from redis, due to %s", err.Error())
			} else {
				if val == "1" {
					break
				}
			}

			log.Infoln("preprocessing stil running, waiting for indexing end")
			time.Sleep(20 * time.Second)
		}
	}

	// TODO update this e.g. to 3
	var attempts int = 1
	var redisIndexingFinishedKey bool = false

	log.Info("Start parsing loop...")

	for {

		var err error
		var result string
		var resultArr []string

		// TODO check ocrd
		// TODO product specific
		if config.CContext == "digizeit" {
			resultArr, err = redisClient.BRPop(3600*time.Second, config.RedisIndexQueue).Result()

			attempts = attempts - 1
			if err == redis.Nil {
				// queue is empty
				if attempts > 0 {
					continue
				}

				if !redisIndexingFinishedKey {
					// required for pre-populated index
					// sets flag RedisIndexingFinishedKey in RedisStatusHsetQueue to notify watching processes
					_, err := redisClient.HSet(config.RedisStatusHsetQueue, config.RedisIndexingFinishedKey, true).Result()
					if err != nil {
						log.Errorf("could not write to redis HSET, due to %s", err)
					} else {
						redisIndexingFinishedKey = true
					}
				}

				continue

			} else if err != nil {
				log.Errorf("indexer could not request redis, due to %s", err.Error())
				continue
			}

			result = resultArr[1]

		} else {
			resultArr, err = redisClient.BRPop(3600*time.Second, config.RedisIndexQueue).Result()
			if err == redis.Nil {
				continue
			} else if err != nil {
				log.Errorf("indexer could not request redis, due to %s", err.Error())
				continue
			}
			result = resultArr[1]
		}

		indexerJob := types.IndexerJob{}
		json.Unmarshal([]byte(result), &indexerJob)

		if indexerJob.Document == "" || indexerJob.Context == "" {
			continue
		}

		// TODO check to replace KEy with Document (are the same?)
		if indexerJob.Key == "" {
			indexerJob.Key = indexerJob.Document
		}

		// TODO check if stil required
		// TODO product specific
		if !helper.ValidationRun() && indexerJob.Context != "ocrd" {
			if time.Duration(time.Since(indexerJob.Starttime).Seconds()) < 40 {
				// add child again to processing queue (at the queue end, process read from right "R")
				_, err := redisClient.LPush(config.RedisIndexQueue, result).Result()
				if err != nil {
					log.Errorf("could not push indexing job for %s, due to %s", indexerJob.Document, err.Error())
				}
				time.Sleep(20 * time.Second)
				continue
			}
		}

		// TODO patch for EHA title ECON-1928-1201
		//		fulltexts have to be corrected by MET OR REP (top XML type TXT_4)
		// 		the full texts for this title is plaintext, should be xml
		if indexerJob.Document == "ECON-1928-1201" {
			indexerJob.Ftype = "TXT"
		}

		// check if set in previous runs
		res, _ := redisClient.Get(indexerJob.Key).Result()
		if res == "nosuchkey" {
			log.Errorf("key %s marked as not available in S3 (nosuchkey)", indexerJob.Key)
			continue
		}

		if !helper.ValidationRun() {
			// lock key:	returns true if new, false if already set
			redisResult, err := redisClient.SetNX(indexerJob.Key, "locked", 30*time.Second).Result()
			if err == nil {
				if !redisResult {
					log.Errorf("could not set lock for (%s), due to redis connection can not be established", indexerJob.Key)
					continue
				}
				// go on

			} else {
				log.Errorf("could not set lock for %s, due to %s", indexerJob.Key, err.Error())
				continue
			}
		}

		// load METS from S3 or FS

		var xmlString string

		var recordIdentifier types.ID

		// TODO product specific
		if indexerJob.Context == "ocrd" {

			// Load missing information from bag-info.txt,
			//      request parameters take precedence
			indexerJob.BagInfoTXT = getBagInfoTXT(indexerJob.Document)

			if indexerJob.BagInfoTXT.OcrdIdentifier != "" {
				recordIdentifier.Value = indexerJob.BagInfoTXT.OcrdIdentifier
				recordIdentifier.Type = "recordidentifier"
				recordIdentifier.IsRecordID = true
			}

			// example "http://.../api/export/mets?id=21.T11998/0000-001B-DC0C-F"
			// HOST_BASE_URL=https://.../api + OLAHDS_METS_KEY_PATTERN="/export/mets?id=%s"
			url := config.HostBaseURL + fmt.Sprintf(config.OlahdsMetsKeyPattern, indexerJob.Key)
			xmlString, err = helper.GetFileFromOLAHDS(url)

			if !strings.Contains(xmlString, "mets:mets") {
				log.Errorf("loaded METS file does not appear to be in METS format for (%s) %s", indexerJob.Product, url)
				continue
			}

			// TODO Exception handling if ID is not valid?
		} else {
			xmlString, err = helper.GetXMLFrom(indexerJob.Product, indexerJob.Key, indexerJob.Context, "mets")
			if !strings.Contains(xmlString, "mets:mets") {
				log.Errorf("loaded METS file does not appear to be in METS format for (%s/%s)", indexerJob.Product, indexerJob.Key)
				continue
			}
			indexerJob.BagInfoTXT = nil
		}

		if err != nil {
			if strings.Contains(err.Error(), "NoSuchKey") {
				_, _ = redisClient.Set(indexerJob.Key, "nosuchkey", 600*time.Second).Result()
			}
			log.Errorf("could not load METS file (%s/%s), due to %s", indexerJob.Product, indexerJob.Key, err.Error())
			continue
		}

		xmlFile := strings.NewReader(xmlString)

		buf, err := io.ReadAll(xmlFile)
		if err != nil {
			log.Errorf("could not read xml file for %s, due to %s", indexerJob.Key, err.Error())
			continue
		}

		decoder1 := xml.NewDecoder(bytes.NewBuffer(buf))
		decoder2 := xml.NewDecoder(bytes.NewBuffer(buf))
		decoder3 := xml.NewDecoder(bytes.NewBuffer(buf))

		if recordIdentifier.Value == "" {
			recordIdentifier = getRecordIdentifierFromXML(decoder1, indexerJob.Context, indexerJob.Key)
		}

		// // TODO product specific
		// if indexerJob.Context == "ocrd" {

		// 	// OcrdIdentifier

		// 	// OcrdWorkIdentifier

		// 	// PrevPID

		// 	// TODO are these comming from the index? Then remove it here
		// 	// PrevPIDs

		// 	// OcrdMets

		// 	// ImageFileGrp

		// 	// FulltextFileGrp

		// 	// IsGt

		// 	// Ftype

		// 	// ImporterInstitution

		// 	// GtDmdsecs

		// 	if indexerJob.Ftype == "" {
		// 		indexerJob.Ftype = config.OlahdsDefaulFtype
		// 	}

		// 	if indexerJob.ImageFileGrp == "" {
		// 		indexerJob.ImageFileGrp = config.OlahdsDefaulImgFilegrp
		// 	}

		// 	if indexerJob.FulltextFileGrp == "" {
		// 		indexerJob.FulltextFileGrp = config.OlahdsDefaulFulltextFilegrp
		// 	}

		// 	if indexerJob.Key == "" {
		// 		indexerJob.Key = indexerJob.Document
		// 	}
		// }

		// TODO ask juergen to correct the eha recordIdentifier
		if strings.Contains(recordIdentifier.Value, "eha_") {
			recordIdentifier.Value = strings.ReplaceAll(recordIdentifier.Value, "eha_", "")
		}

		// get valid dmdsec id's
		nonModsDmdsecIDs, gtDmdsecs := getNonModsDmdsecIDs(decoder2, indexerJob.Key)

		// parse METS in one step
		mets := getMetsStructFromXML(recordIdentifier, indexerJob, decoder3)
		if mets == nil {
			continue
		}

		mets.NonModsDmdsecIDs = nonModsDmdsecIDs
		mets.GtDmdsecs = gtDmdsecs

		// TODO check: still required?
		if config.ProcessDoctype != "both" {
			if mets.Doctype != config.ProcessDoctype {
				log.Infof("config excludes doctype %s from processing (%s)", config.ProcessDoctype, indexerJob.Document)
				continue
			}
		}

		// TODO product specific
		if indexerJob.Context == "ocrd" {
			mets.PID = indexerJob.Document
			mets.PrevPID = indexerJob.Prev

			if mets.WorkID == "" {
				mets.WorkID = indexerJob.Document
			}

			if recordIdentifier.Value == "" {
				recordIdentifier.Value = indexerJob.Document
			}
		}

		// put the mets to the processing queue
		metsProcessingJobsChan <- mets
	}
}

// getAmdsecMap ...
func getAmdsecMap(amdsecs []types.Amdsec) map[string]types.Amdsec {
	var amdsecMap map[string]types.Amdsec = make(map[string]types.Amdsec)

	for _, amdsec := range amdsecs {
		amdsecMap[amdsec.ID] = amdsec
	}

	return amdsecMap
}

// TODO check if still required
// getBagInfoTXT ...
func getBagInfoTXT(key string) *types.BagInfoTXT {

	var bagInfoTXT types.BagInfoTXT

	// TODO move to config and replace with config variable
	bagInfoTxtURL := fmt.Sprintf("%s/search/indexer-info?pid=%s", config.HostBaseURL, key)
	bagInfoTxt, err := helper.GetFileFromOLAHDS(bagInfoTxtURL)
	if err != nil {
		log.Printf("Could not load bag-ino.txt from OLA-HD, due to %v", err)
		return &types.BagInfoTXT{}
	}

	viper.SetConfigType("properties")
	viper.ReadConfig(bytes.NewBuffer([]byte(bagInfoTxt)))

	err = viper.Unmarshal(&bagInfoTXT)
	if err != nil {
		log.Printf("Could not Unmarshal bag-ino.txt, due to %v", err)
		return &types.BagInfoTXT{}
	}

	return &bagInfoTXT

}
