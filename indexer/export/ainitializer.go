package export

import (
	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/util"
)

var (
	config      util.Config
	log         logrus.Logger
	redisClient *redis.Client
)

func init() {
	config, log = util.LoadConfig()
	log.Debugf("config.RedisAdr: %s, config.RedisDB: %v", config.RedisAdr, config.RedisDB)

	redisClient = redis.NewClient(&redis.Options{
		Addr:       config.RedisAdr,
		DB:         config.RedisDB,
		MaxRetries: 3,
	})
}
