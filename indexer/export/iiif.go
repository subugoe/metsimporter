package export

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/go-redis/redis"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/helper"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/index"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/templating"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/types"

	//"gopkg.in/gographics/imagick.v2/imagick"
	"gopkg.in/gographics/imagick.v3/imagick"
)

type InfoJsonRetrievalJob struct {
	Document       string `json:"document"`
	Context        string `json:"context"`
	Product        string `json:"product"`
	Page           string `json:"page"`
	Format         string `json:"format"`
	Key            string `json:"key"`
	InfoJsonKey    string `json:"infojsonkey"`
	PageKey        string `json:"pagekey"`
	ForceOverwrite bool   `json:"forceoverwrite"`
}

var (
	jsonUploadChan chan types.JsonUploadObject
	intervals      []int32
)

func init() {
	if config.TestRun && config.TestRunLimit == 0 {
		config.TestRunLimit = 5
	}

	jsonUploadChan = make(chan types.JsonUploadObject, 30)
	for w := 1; w <= 2; w++ {
		go uploadJsonToS3()
	}

	// increase 7 times (30s, 90s, 180s, 300s, 3600, 3600s, 7200)
	intervals = []int32{30, 90, 180, 300, 3600, 3600, 7200, 21600}

}

// --- iiif manifest types (2.1)
//
// URI pattern:	 {scheme}://{host}/{prefix}/{identifier}/manifest
// https://manifests.sub.uni-goettingen.de/iiif/presentation/[RECORD_ID]/manifest

func CreateIiifManifest(mythread int) {

	var err error
	var result string
	var resultArr []string

	if config.CContext != "ocrd" {
		for {
			// waiting for indexer to finish work
			val, err := redisClient.HGet(config.RedisStatusHsetQueue, config.RedisIndexingFinishedKey).Result()
			if err != nil {
				log.Errorf("could not get value from redis, due to %s", err.Error())
			} else {
				if val == "1" {
					break
				}
			}
			time.Sleep(30 * time.Second)
		}
	}

	for {
		resultArr, err = redisClient.BRPop(3600*time.Second, config.RedisIiifManifestQueue).Result()

		if err == redis.Nil { // redis queue empty
			continue
		} else if err != nil {
			log.Errorf("indexer could not request redis, due to %s", err.Error())
			time.Sleep(20 * time.Second)
			continue
		}
		result = resultArr[1]

		manifestData := types.ExportWork{}
		json.Unmarshal([]byte(result), &manifestData)

		if elapsed := time.Since(manifestData.Time); elapsed < (time.Second * time.Duration(intervals[manifestData.Attempts])) {

			// put back to queue
			_, err = redisClient.LPush(config.RedisIiifManifestQueue, resultArr[1]).Result()
			if err != nil {
				log.Errorf("could not push manifest data to redis queue '%s', due to %s", config.RedisIiifManifestQueue, err)
			}

			time.Sleep(25 * time.Second)

			continue
		}

		var esPhysMap map[string]types.ESPhys
		esPhysMap, err = index.GetESPyhsFromES(manifestData.WorkID, manifestData.Context)
		if err != nil {
			log.Errorf("could not get physical info from index for %s, due to %s", manifestData.WorkID, err.Error())
			continue
		}

		if len(esPhysMap) == 0 {
			log.Errorf("physical info for %s not yet in the index", manifestData.WorkID)

			if manifestData.Attempts >= 7 {
				log.Errorf("failed 8 times to create iiif Manifest %v (%v), job will not enqueued again and debugging is required", manifestData.PID, manifestData.WorkID)
				continue
			}

			manifestData.Attempts = manifestData.Attempts + 1
			manifestJson, err := json.Marshal(manifestData)
			if err != nil {
				log.Errorf("could not marshal manifest data %v, due to %s", manifestJson, err)
				continue
			}

			// write to queue failed
			_, err = redisClient.LPush(config.RedisIiifManifestQueue, string(manifestJson)).Result()

			if err != nil {
				log.Errorf("could not push manifest data to redis queue '%s', due to %s", config.RedisIiifManifestQueue, err)
			}

			continue
		}

		var esLogMap map[string]types.ESLog
		esLogMap, err = index.GetESLogFromES(manifestData.WorkID, manifestData.Context)
		if err != nil {
			log.Errorf("could not get logical info from index for %s, due to %s", manifestData.WorkID, err.Error())
			continue
		}

		if len(esLogMap) == 0 {
			log.Errorf("logical info for %s not yet in the index", manifestData.WorkID)

			if manifestData.Attempts >= 7 {
				log.Errorf("failed 8 times to create iiif Manifest %v (%v), debugging required", manifestData.PID, manifestData.WorkID)
				continue
			}

			manifestData.Attempts = manifestData.Attempts + 1
			manifestJson, err := json.Marshal(manifestData)
			if err != nil {
				log.Errorf("could not marshal manifest data %v, due to %s", manifestJson, err)
				continue
			}

			// write to queue failed
			_, err = redisClient.LPush(config.RedisIiifManifestQueue, string(manifestJson)).Result()
			if err != nil {
				log.Errorf("could not push manifest data to redis queue '%s', due to %s", config.RedisIiifManifestQueue, err)
			}

			continue
		}

		iiifManifest := types.IiifManifest{}
		var physIDToPageElementMetaMap map[string]types.PageMeta = make(map[string]types.PageMeta)

		for _, esPhys := range esPhysMap {
			physIDToPageElementMetaMap[esPhys.Phys] = types.PageMeta{
				Format:     esPhys.Format,
				Page:       esPhys.Page,
				PageKey:    esPhys.PageKey,
				PageWidth:  esPhys.PageWidth,
				PageHeight: esPhys.PageHeight,
			}
		}

		// ---- TODO start here ----

		var startPageCanvasURL string
		var structures []types.Structure = make([]types.Structure, 0)

		isExternalContent := false
		first := true
		var ccontext string

		// sort logical elements by order
		sortSliceLogical := make(types.ESLogSortSlice, 0, len(esLogMap))
		for _, d := range esLogMap {
			sortSliceLogical = append(sortSliceLogical, d)
		}
		sort.Sort(sortSliceLogical)

		// sort physical elements by order
		sortSlicePhysical := make(types.ESPhysSortSlice, 0, len(esPhysMap))
		for _, d := range esPhysMap {
			sortSlicePhysical = append(sortSlicePhysical, d)
		}
		sort.Sort(sortSlicePhysical)

		var firstLog string

		for _, esLog := range sortSliceLogical {

			var physID = esLog.TitlePagePhysID
			if esLog.TitlePagePhysID != "" {
				physID = esLog.TitlePagePhysID
			} else {
				physID = esLog.StartPagePhysID
			}
			pageMeta := physIDToPageElementMetaMap[physID]

			// get main meta

			if first {

				firstLog = esLog.Log
				isExternalContent = esLog.IsExternalContent
				ccontext = strings.ToLower(esLog.Context)

				iiifManifest.ID = fmt.Sprintf(config.IiifManifestURLDigizeit, manifestData.WorkID)

				iiifManifest.Type = config.IiifManifestType
				iiifManifest.Context = config.IiifPresentationContext // "http://iiif.io/api/presentation/2/context.json"
				iiifManifest.ViewingDirection = "left-to-right"
				iiifManifest.ViewingHint = "paged"

				// https://dev.digizeitschriften.de/iiif/presentation/PPN511864582_0045|log1/manifest

				// TODO esLog.Parent.RecordID is possibly nil, if parent is not yet indexed
				// TODOD check this
				if esLog.Parent == nil {
					log.Errorf("parent is nil for work %s", manifestData.WorkID)
				} else {
					iiifManifest.Within = fmt.Sprintf(config.IiifManifestURLDigizeit, esLog.Parent.RecordID)
				}

				var arr []string
				arr = make([]string, 0)
				if len(esLog.Currentno) > 0 {
					arr = append(arr, esLog.Currentno...)
				}

				// replacement for the preceding
				if esLog.Title != nil {
					iiifManifest.Label = esLog.Title.Title
				} else {
					iiifManifest.Label = esLog.Label
				}

				// nlh doesn't support iiif
				// else if ccontext == "nlh" {
				//
				// }

				// --- []metadata
				var metadata []types.Metadata

				//--- []metadata -> editor
				/* remove editor
				for _, pers := range esLog.PersonInfos {
					if strings.ToLower(pers.Roleterm) == "edt" {
						metadata = append(metadata, types.Metadata{
							Label: "Herausgeber",
							Value: []string{pers.Name},
						})
					}
				}
				*/

				//--- []metadata -> publisher, year published
				var publishers []string
				if esLog.PublishInfos != nil {

					if len(esLog.PublishInfos.Publisher) > 0 {
						publishers = esLog.PublishInfos.Publisher
					}

					var pd string

					a := esLog.PublishInfos.YearPublishString
					b := esLog.PublishInfos.YearPublishStart
					c := esLog.PublishInfos.YearPublish

					if a != "" {
						pd = a
					} else if b != 0 {
						pd = fmt.Sprint(b)
					} else if c != 0 {
						pd = fmt.Sprint(c)
					}
					if pd != "" {
						metadata = append(metadata, types.Metadata{
							Label: "Jahr",
							Value: []string{pd},
						})
					}

					if b != 0 {
						const shortForm = "2006"
						t, _ := time.Parse(shortForm, strconv.FormatInt(c, 10))
						iiifManifest.NavDate = fmt.Sprintf(t.UTC().Format(time.RFC3339))
					} else if c != 0 {
						const shortForm = "2006"
						t, _ := time.Parse(shortForm, strconv.FormatInt(c, 10))
						iiifManifest.NavDate = fmt.Sprintf(t.UTC().Format(time.RFC3339))
					}

				}

				//--- []metadata -> volume number
				if len(esLog.Currentno) > 0 {
					//metadata = append(metadata, Metadata{"Band", []string{strconv.FormatInt(esLog.Currentno[0], 10)}})
					metadata = append(metadata, types.Metadata{
						Label: "Band",
						Value: []string{esLog.Currentno[0]},
					})
				}

				//--- []metadata -> place published
				if (esLog.PublishInfos != nil) && len(esLog.PublishInfos.Place) > 0 {
					metadata = append(metadata, types.Metadata{
						Label: "Ort",
						Value: esLog.PublishInfos.Place,
					})
				}

				//--- add publisher identified above
				var pbs []string
				// TODO check if additional nil checks are required
				if esLog.PublishInfos != nil {
					for _, pubs := range esLog.PublishInfos.Publisher {
						str := strings.TrimSpace(pubs)
						pbs = append(pbs, str)
					}
					if len(publishers) > 0 {
						metadata = append(metadata, types.Metadata{
							Label: "Verlag",
							Value: pbs,
						})
					}
				}

				//--- []metadata -> volume PURL
				if esLog.Purl != "" {
					metadata = append(metadata, types.Metadata{
						Label: "PURL",
						Value: []string{esLog.Purl},
					})
				}

				//--- []metadata -> classification
				if len(esLog.Dc) > 0 {
					var dcs []string
					for _, dc := range esLog.Dc {
						str := helper.ClassifcationMappingDE(strings.ToLower(dc))

						if str != "" {
							dcs = append(dcs, str)
						} else {
							dcs = append(dcs, dc)
						}
					}
					metadata = append(metadata, types.Metadata{
						Label: "Fachgebiete",
						Value: dcs,
					})
				}

				//--- []metadata -> host
				var ri string
				if esLog.Parent != nil {
					tt := esLog.Parent.Title.Title
					id := esLog.Parent.RecordID
					str := fmt.Sprintf(config.IiifSearchByIdURLDigizeit, id)
					ri = fmt.Sprintf("<a href=\"%s\">%s</a>", str, tt)
					metadata = append(metadata, types.Metadata{
						Label: "Übergeordnetes Werk",
						Value: []string{ri},
					})
				}

				var precedings []string
				var succeedings []string
				var series []string
				var relatedID string

				if esLog.Parent != nil {
					for _, riInfo := range esLog.Parent.RelatedItemInfos {

						if strings.HasPrefix(strings.ToLower(riInfo.RelateditemID), "ppn") {
							relatedID = riInfo.RelateditemID[3:len(riInfo.RelateditemID)]
						}
						//--- []metadata -> preceding
						if riInfo.RelateditemType == "preceding" {
							tt := riInfo.RelateditemTitle
							id := riInfo.RelateditemID
							if tt == "" {
								tt = index.GetCollectionTitleFromES(relatedID)
								if tt == "" {
									tt = id
								}
							}
							str := fmt.Sprintf(config.IiifSearchByIdURLDigizeit, id)
							prec := fmt.Sprintf("<a href=\"%s\">%s</a>", str, tt)
							precedings = append(precedings, prec)

							//--- []metadata -> succeeding
						} else if riInfo.RelateditemType == "succeeding" {
							tt := riInfo.RelateditemTitle
							id := riInfo.RelateditemID
							if tt == "" {
								tt = index.GetCollectionTitleFromES(relatedID)
								if tt == "" {
									tt = id
								}
							}
							str := fmt.Sprintf(config.IiifSearchByIdURLDigizeit, id)
							succ := fmt.Sprintf("<a href=\"%s\">%s</a>", str, tt)
							succeedings = append(succeedings, succ)

							//--- []metadata -> series
						} else if riInfo.RelateditemType == "series" {
							tt := riInfo.RelateditemTitle
							id := riInfo.RelateditemID
							if tt == "" {
								tt = index.GetCollectionTitleFromES(relatedID)
								if tt == "" {
									tt = id
								}
							}
							str := fmt.Sprintf(config.IiifSearchByIdURLDigizeit, id)
							ser := fmt.Sprintf("<a href=\"%s\">%s</a>", str, tt)
							series = append(series, ser)
						}

					}
				}
				if len(precedings) > 0 {
					metadata = append(metadata, types.Metadata{
						Label: "Vorgänger Zeitschrift(en)",
						Value: precedings,
					})
				}
				if len(succeedings) > 0 {
					metadata = append(metadata, types.Metadata{
						Label: "Nachfolger Zeitschrift(en)",
						Value: succeedings,
					})
				}
				if len(series) > 0 {
					metadata = append(metadata, types.Metadata{
						Label: "Zeitschriftenreihe(n)",
						Value: series,
					})
				}

				//--- []metadata -> ISSN
				if esLog.Issn != "" {
					metadata = append(metadata, types.Metadata{
						Label: "ISSN",
						Value: []string{esLog.Issn},
					})
				} else if esLog.Parent != nil {
					if esLog.Parent.Issn != "" {
						metadata = append(metadata, types.Metadata{
							Label: "ISSN",
							Value: []string{esLog.Parent.Issn},
						})
					}
				}

				//--- []metadata -> ZDB-ID
				if esLog.Zdb != "" {
					metadata = append(metadata, types.Metadata{
						Label: "ZDB-ID",
						Value: []string{esLog.Zdb},
					})
				}

				//--- year digitization
				/* remove year of digitization
				if esLog.DigitizationInfos != nil {
					var dd string
					if esLog.DigitizationInfos.YearDigitizationString != "" {
						dd = esLog.DigitizationInfos.YearDigitizationString
					} else if esLog.DigitizationInfos.YearDigitizationStart != 0 {
						dd = fmt.Sprint(esLog.DigitizationInfos.YearDigitizationStart)
					} else if esLog.DigitizationInfos.YearDigitization != 0 {
						dd = fmt.Sprint(esLog.DigitizationInfos.YearDigitization)
					}
					if dd != "" {
						metadata = append(metadata, types.Metadata{
							Label: "Digitalisierungsdatum",
							Value: []string{dd},
						})
					}
				}
				*/

				//--- []metadata -> shelfmark
				/* remove shelfmark
				if len(esLog.Shelfmark) > 0 {
					var shelfmarks []string
					for _, shelfmark := range esLog.Shelfmark {
						shelfmarks = append(shelfmarks, shelfmark.Shelfmark)
					}
					metadata = append(metadata, types.Metadata{
						Label: "Signatur",
						Value: shelfmarks,
					})
				}
				*/

				//--- []metadata -> rights
				if esLog.RightsInfo.License != "" {
					var value string
					if esLog.RightsInfo.License == "CC BY 4.0" {
						value = fmt.Sprintf("<a href=\"%s\">%s</a>", config.CcBy40LicenseLink, esLog.RightsInfo.License)
					} else {
						value = esLog.RightsInfo.License
					}

					metadata = append(metadata, types.Metadata{
						Label: "Lizenz",
						Value: []string{value},
					})
				}

				// --- []metadata -> languages
				/* remove languages
				if len(esLog.Lang) > 0 {
					metadata = append(metadata, types.Metadata{
						Label: "Sprache",
						Value: esLog.Lang,
					})
				}
				*/

				iiifManifest.Metadata = metadata

				// --- Attribution
				iiifManifest.Attribution = esLog.RightsInfo.Owner

				// --- Logo
				//iiifManifest.Logo.ID = esLog.RightsInfo.OwnerLogo   // todo check replacement

				// TODO add NLH
				// TODO product specific
				if ccontext == "digizeit" {
					iiifManifest.Logo.ID = config.LogoDigizeit
				} else if ccontext == "gdz" {
					iiifManifest.Logo.ID = config.LogoGDZ
				} else if ccontext == "ocrd" {
					//TODO check Logo URL
					//iiifManifest.Logo.ID = config.LogoOcrd
				}

				// nlh doesn't support iiif
				// else if ccontext == "nlh" {
				//
				// }

				// TODO add NLH
				// TODO product specific
				if iiifManifest.Logo.ID != "" {
					var serviceID string
					if ccontext == "digizeit" {
						serviceID = config.IiifBaseURLDigizeit
					} else if ccontext == "gdz" {
						serviceID = config.IiifBaseURLGDZ
					} else if ccontext == "ocrd" {
						serviceID = config.IiifBaseURL
					}
					iiifManifest.Logo.Service = &types.IiifService{
						ID:      serviceID,
						Context: config.IiifImageContext,
						Profile: config.IiifImageProfile,
					}
				}

				// --- License license_for_use_and_reproduction
				if len(esLog.LicenseForUseAndReproduction) > 0 {
					metadata = append(metadata, types.Metadata{
						Label: "Lizenz (Nutzung und Wiederverwendung)",
						Value: esLog.LicenseForUseAndReproduction,
					})
				}

				// --- License license_for_metadata
				if len(esLog.LicenseForMetadata) > 0 {
					metadata = append(metadata, types.Metadata{
						Label: "Lizenz (Metadaten)",
						Value: esLog.LicenseForMetadata,
					})
				}

				// --- add previous PID (OCR-D only)
				if ccontext == "ocrd" && esLog.PrevPID != "" {
					metadata = append(metadata, types.Metadata{
						Label: "previous PID",
						Value: []string{esLog.PrevPID},
					})
				}

				if ccontext == "ocrd" && len(esLog.PrevPID) > 0 {
					metadata = append(metadata, types.Metadata{
						Label: "previous PIDs",
						Value: esLog.PrevPIDs,
					})
				}

				// --- first Rendering
				var renderings []types.Rendering

				// TODO add NLH
				// TODO product specific
				if ccontext == "digizeit" {

					// not yet required for DigiZeitschriften
					/*
						r := types.Rendering{
							ID:     fmt.Sprintf(config.PdfDownloadURLDigizeit, manifestData.WorkID, manifestData.WorkID),
							Format: config.PdfFormat,
							Label:  config.PdfDownloadLabel,
						}
						renderings = append(renderings, r)
					*/

					r := types.Rendering{
						ID:     fmt.Sprintf(config.DfgViewerBaseURL, fmt.Sprintf(config.MetsDownloadURLDigizeit, manifestData.WorkID)),
						Format: config.DfgViewerFormat,
						Label:  config.DfgViewerLabel,
					}
					renderings = append(renderings, r)

				} else if ccontext == "gdz" {

					r := types.Rendering{
						ID:     fmt.Sprintf(config.PdfDownloadURLGDZ, manifestData.WorkID, manifestData.WorkID),
						Format: config.PdfFormat,
						Label:  config.PdfDownloadLabel,
					}
					renderings = append(renderings, r)

					r = types.Rendering{
						ID:     fmt.Sprintf(config.DfgViewerBaseURL, fmt.Sprintf(config.MetsDownloadURLGDZ, manifestData.WorkID)),
						Format: config.DfgViewerFormat,
						Label:  config.DfgViewerLabel,
					}
					renderings = append(renderings, r)

				} else if ccontext == "ocrd" {

					// r := types.Rendering{
					// 	ID:     fmt.Sprintf(config.PdfDownloadURL, manifestData.WorkID, manifestData.WorkID),
					// 	Format: config.PdfFormat,
					// 	Label:  config.PdfDownloadLabel,
					// }
					// renderings = append(renderings, r)

					r := types.Rendering{
						ID: fmt.Sprintf(config.DfgViewerBaseURL, fmt.Sprintf(config.OlahdsMetsKeyPattern, esLog.PID)),

						Format: config.DfgViewerFormat,
						Label:  config.DfgViewerLabel,
					}
					renderings = append(renderings, r)

				}

				// nlh doesn't support iiif
				// else if ccontext == "nlh" {
				//
				// }

				if len(renderings) > 0 {
					iiifManifest.Rendering = renderings
				}

				// --- SeeAlso todo bibtex, ris, endnote
				var seealso []types.Dataset = getSeeAlso(ccontext, manifestData.WorkID, esLog.Log, true)

				if len(seealso) > 0 {
					iiifManifest.SeeAlso = seealso
				}

				// --- Catalogue
				if esLog.Catalogue != "" {

					re := regexp.MustCompile("^(\\S*)\\W*(\\S*)$")

					match := re.FindStringSubmatch(esLog.Catalogue)
					if len(match) == 3 {

						if match[1] == "OPAC" {
							var arr []types.Related
							related := types.Related{
								ID:     match[2],
								Label:  "OPAC",
								Format: "text/html",
							}
							arr = append(arr, related)
							iiifManifest.Related = arr
						} else if match[1] == "Kalliope" {
							var arr []types.Related
							related := types.Related{
								ID:     match[2],
								Label:  "Kalliope",
								Format: "text/html",
							}
							arr = append(arr, related)
							iiifManifest.Related = arr
						}

					}
				}

				// TODO add NLH
				// TODO product specific
				if ccontext == "gdz" {
					startPageCanvasURL = fmt.Sprintf(config.IiifViewerCanvasResolvingURLGDZ, manifestData.WorkID, esLog.Product, manifestData.WorkID, pageMeta.Page)
				} else if ccontext == "digizeit" {
					startPageCanvasURL = fmt.Sprintf(config.IiifViewerCanvasResolvingURLDigizeit, manifestData.WorkID, esLog.Product, manifestData.WorkID, pageMeta.Page)
				} else if ccontext == "ocrd" {
					startPageCanvasURL = fmt.Sprintf(config.IiifViewerCanvasResolvingURL, manifestData.WorkID, esLog.Product, manifestData.WorkID, pageMeta.Page)
				}

			} // if first

			// --- Structures

			var structure types.Structure

			if !first {
				structure.ID = fmt.Sprintf(config.IiifViewerLogResolvingDigizeit, manifestData.WorkID, esLog.Log)
				structure.Type = config.IiifStructureType

				var metadata []types.Metadata

				start := strconv.FormatInt(esLog.StartPageIndex, 10)
				end := strconv.FormatInt(esLog.EndPageIndex, 10)

				if start == end {
					/*metadata = append(metadata, types.Metadata{
						Label: "Seitenzahl",
						Value: []string{start},
					})*/

					metadata = append(metadata, types.Metadata{
						Label: "Gescannte Seiten",
						Value: []string{fmt.Sprintf("%v ", 1)},
					})

				} else {
					/*metadata = append(metadata, types.Metadata{
						Label: "Seitenzahl",
						Value: []string{fmt.Sprintf("%s - %s", start, end)},
					})*/

					metadata = append(metadata, types.Metadata{
						Label: "Gescannte Seiten",
						Value: []string{fmt.Sprintf("%v ", (esLog.EndPageIndex - esLog.StartPageIndex + 1))},
					})
				}

				// --- []metadata -> identifier
				/* remove identifier
				if len(esLog.Identifier) > 0 {
					metadata = append(metadata, types.Metadata{
						Label: "Identifier",
						Value: esLog.Identifier,
					})
				}
				*/

				// --- []metadata -> author
				if esLog.Bycreator != "" {
					metadata = append(metadata, types.Metadata{
						Label: "Autor(n)",
						Value: []string{esLog.Bycreator},
					})
				}

				// --- []metadata -> title
				/* used is label, value is the same
				if esLog.Title != nil && esLog.Title.Title != "" {
					metadata = append(metadata, types.Metadata{
						Label: "Titel",
						Value: []string{esLog.Title.Title},
					})
				}*/

				// --- []metadata -> citations as metadata
				var seeAlos []types.Dataset

				if esLog.Iscontribution {
					if iiifManifest.Label != "" {
						var sal []string
						seeAlos = getSeeAlso(ccontext, manifestData.WorkID, esLog.Log, false)
						for _, sa := range seeAlos {
							sal = append(sal, fmt.Sprintf("<a href=\"%s\">%s</a>", sa.ID, sa.Label))
						}

						metadata = append(metadata, types.Metadata{
							Label: "Zitationen",
							Value: sal,
						})
					}
				}

				structure.Metadata = metadata

				structure.Label = esLog.Label

				structure.ViewingHint = "top"

				var renderings []types.Rendering

				// TODO product specific
				if ccontext == "digizeit" && !isExternalContent {

					r := types.Rendering{
						ID:     fmt.Sprintf(config.PdfDownloadURLDigizeit, manifestData.WorkID, esLog.Log),
						Format: config.PdfFormat,
						Label:  config.PdfDownloadLabel,
					}
					renderings = append(renderings, r)

				} else if ccontext == "gdz" {

					r := types.Rendering{
						ID:     fmt.Sprintf(config.PdfDownloadURLGDZ, manifestData.WorkID, esLog.Log),
						Format: config.PdfFormat,
						Label:  config.PdfDownloadLabel,
					}
					renderings = append(renderings, r)

				}

				if len(renderings) > 0 {
					structure.Rendering = renderings
				}

				if firstLog != esLog.ParentLog {

					// TODO product specific
					if ccontext == "digizeit" {
						structure.Within = fmt.Sprintf(config.IiifViewerLogResolvingDigizeit, esLog.ParentWork, esLog.ParentLog)
					} else if ccontext == "gdz" {
						structure.Within = fmt.Sprintf(config.IiifViewerLogResolvingGDZ, esLog.ParentWork, esLog.ParentLog)
					} else if ccontext == "ocrd" {
						structure.Within = fmt.Sprintf(config.IiifViewerLogResolving, esLog.ParentWork, esLog.ParentLog)
					}
				}

				structure.Canvases = []string{}

				firstPhys := true
				for _, esPhys := range sortSlicePhysical {

					if firstPhys {

						// --- Thumbnail
						thumbnailType := config.IiifImageType
						thumbnailServiceID := ""
						thumbnailID := ""
						thumbnailFormat := ""
						var thumbnailHeight int32
						var thumbnailWidth int32
						thumbnailService := &types.IiifService{}

						// TODO add NLH
						// TODO product specific
						if ccontext == "gdz" {
							thumbnailID = fmt.Sprintf(config.IiifThumbTemplateGDZ, esLog.Context, manifestData.WorkID, pageMeta.Page, esPhys.Format)
							thumbnailServiceID = fmt.Sprintf(config.IiifManifestURLGDZ, manifestData.WorkID)
						} else if ccontext == "digizeit" {
							if !isExternalContent {
								thumbnailID = fmt.Sprintf(config.IiifThumbTemplateDigizeit, esLog.Context, manifestData.WorkID, pageMeta.Page, esPhys.Format)
								thumbnailServiceID = fmt.Sprintf(config.IiifManifestURLDigizeit, manifestData.WorkID)
								thumbnailHeight = 128
								thumbnailWidth = 91

							} else {
								thumbnailID = esPhys.FirstThumbPageHRef
								thumbnailServiceID = ""
								thumbnailWidth = 150
							}
						} else if ccontext == "ocrd" {
							keyArray := strings.Split(esPhys.PageKey, ":")
							//first := keyArray[0] // -> olahds
							//middle := keyArray[1 : len(keyArray)-2] // -> [urn nbn de kobv]
							page := keyArray[len(keyArray)-1] // -> OCR-D-IMG/OCR-D-IMG_0001.jpg

							thumbnailID = fmt.Sprintf(config.IiifImageConverterURL, esPhys.PID, page)
							thumbnailServiceID = fmt.Sprintf(config.IiifManifestURL, manifestData.WorkID)
						}

						thumbnailFormat = helper.ImageFileFormatMapping(pageMeta.Format) // result.Mimetype

						if thumbnailServiceID != "" {
							thumbnailService = &types.IiifService{
								ID:      thumbnailServiceID,
								Context: config.IiifImageContext,
								Profile: config.IiifImageProfile,
							}
							iiifManifest.Thumbnail = &types.AnnotationBody{
								Type:    thumbnailType,
								ID:      thumbnailID,
								Format:  thumbnailFormat,
								Service: thumbnailService,
								Width:   thumbnailWidth,
								Height:  thumbnailHeight,
							}
						} else {
							iiifManifest.Thumbnail = &types.AnnotationBody{
								Type:    thumbnailType,
								ID:      thumbnailID,
								Format:  thumbnailFormat,
								Service: nil,
								Width:   thumbnailWidth,
								Height:  thumbnailHeight,
							}
						}

						firstPhys = false
					}

					if (esPhys.Index >= esLog.StartPageIndex) && (esPhys.Index <= esLog.EndPageIndex) {
						structure.Canvases = append(structure.Canvases, fmt.Sprintf(config.IiifViewerCanvasResolvingURLDigizeit, manifestData.WorkID, manifestData.Product, manifestData.WorkID, esPhys.Page))
						//structure.Canvases = append(structure.Canvases, fmt.Sprintf(config.IiifViewerCanvasResolvingURLDigizeit, manifestData.WorkID, config.Product, manifestData.WorkID, esPhys.Page))
					}

					if esPhys.Index > esLog.EndPageIndex {
						break
					}

				}

				var rangeObject types.RangeJSON
				rangeObject.ID = structure.ID
				rangeObject.Type = config.IiifStructureType
				rangeObject.Label = structure.Label
				rangeObject.ViewingHint = "top"
				rangeObject.Members = structure.Canvases

				rangeObjectJSON, _ := json.Marshal(rangeObject)
				// iiif/{{ id }}/range/{{ range_id }}.json
				key := fmt.Sprintf(config.ExportKeyPatternIiifRange, manifestData.WorkID, esLog.Log)
				jsonUploadChan <- types.JsonUploadObject{
					Bucket: manifestData.Product,
					//Bucket:  config.Product,
					Key:     key,
					Content: rangeObjectJSON,
				}

			} // if !first

			if structure.ID != "" {
				structures = append(structures, structure)
			}

			first = false
		}

		iiifManifest.Structures = structures

		// --- Sequences
		var sequences []types.Sequence
		sequences = make([]types.Sequence, 0)

		var canvases []types.Canvas
		canvases = make([]types.Canvas, 0)

		var sequence types.Sequence
		sequence.ID = fmt.Sprintf(config.IiifViewerSequenceResolvingURLDigizeit, manifestData.WorkID)
		sequence.Type = config.IiifSequenceType
		sequence.Label = "Current Page Order"
		sequence.ViewingDirection = "left-to-right"
		sequence.ViewingHint = "paged"
		sequence.StartCanvas = startPageCanvasURL

		for _, esPhys := range sortSlicePhysical {

			var canvas types.Canvas
			var serviceID string
			var imageID string
			var canvasID string
			var annotationID string

			// TODO add NLH
			// TODO product specific
			if ccontext == "digizeit" {
				canvasID = fmt.Sprintf(config.IiifViewerCanvasResolvingURLDigizeit, manifestData.WorkID, manifestData.Product, manifestData.WorkID, esPhys.Page)
				annotationID = fmt.Sprintf(config.IiifViewerAnnotationResolvingURLDigizeit, manifestData.WorkID, manifestData.Product, manifestData.WorkID, esPhys.Page)
				canvas.ID = canvasID

				if isExternalContent {
					serviceID = ""
					imageID = esPhys.PageHRef
				} else {
					serviceID = fmt.Sprintf(config.IiifImageConverterURLDigizeit, esPhys.PageKey)
					imageID = fmt.Sprintf(config.IiifImageConverterURLDigizeit, esPhys.PageKey)
				}
			} else if ccontext == "gdz" {
				canvasID = fmt.Sprintf(config.IiifViewerCanvasResolvingURLGDZ, manifestData.WorkID, manifestData.Product, manifestData.WorkID, esPhys.Page)
				annotationID = fmt.Sprintf(config.IiifViewerAnnotationResolvingURLGDZ, manifestData.WorkID, manifestData.Product, manifestData.WorkID, esPhys.Page)
				canvas.ID = canvasID
				serviceID = fmt.Sprintf(config.IiifImageConverterURLGDZ, esPhys.PageKey)
				imageID = fmt.Sprintf(config.IiifImageConverterURLGDZ, esPhys.PageKey)
			} else if ccontext == "ocrd" {
				canvasID = fmt.Sprintf(config.IiifViewerCanvasResolvingURL, manifestData.WorkID, manifestData.Product, manifestData.WorkID, esPhys.Page)
				annotationID = fmt.Sprintf(config.IiifViewerAnnotationResolvingURL, manifestData.WorkID, manifestData.Product, manifestData.WorkID, esPhys.Page)
				canvas.ID = canvasID
				keyArray := strings.Split(esPhys.PageKey, ":")
				// first := keyArray[0] // -> olahds
				// middle := keyArray[1 : len(keyArray)-2] // -> [urn nbn de kobv]
				pagename := keyArray[len(keyArray)-1] // -> OCR-D-IMG/OCR-D-IMG_0001.jpg
				imageID = fmt.Sprintf(config.IiifImageConverterURL, esPhys.PID, pagename)
				// pageArray := strings.Split(pagename, ".")
				// page := pageArray[0] // -> OCR-D-IMG/OCR-D-IMG_0001.jpg
				// serviceID = fmt.Sprintf(config.IiifImageConverterServiceURL, manifestData.WorkID, page)
			}

			canvas.Type = config.IiifCanvasType
			canvas.Label = esPhys.Orderlabel
			canvas.Height = esPhys.PageHeight // 400
			canvas.Width = esPhys.PageWidth   // 300

			var images []types.Annotation
			images = make([]types.Annotation, 0)
			var image types.Annotation

			image.ID = annotationID
			image.Type = config.IiifAnnotationType
			image.Context = config.IiifPresentationContext
			image.Motivation = config.IiifImageMotivationType
			image.On = canvasID

			if serviceID != "" {
				image.Resource = types.AnnotationBody{
					ID:     imageID,
					Type:   config.IiifAnnotationBodyType,
					Format: esPhys.Mimetype, // esPhys.Mimetype
					Height: esPhys.PageHeight,
					Width:  esPhys.PageWidth,

					Service: &types.IiifService{
						ID:      serviceID,
						Context: config.IiifImageContext,
						Profile: config.IiifImageProfile,
					},
				}
			} else {
				image.ID = imageID
				image.Resource = types.AnnotationBody{
					ID:     imageID,
					Type:   config.IiifAnnotationBodyType,
					Format: esPhys.Mimetype, // esPhys.Mimetype
					Height: esPhys.PageHeight,
					Width:  esPhys.PageWidth,
				}
			}

			images = append(images, image)
			canvas.Images = images
			canvases = append(canvases, canvas)

		}
		t := time.Now()
		formatted := fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d",
			t.Year(), t.Month(), t.Day(),
			t.Hour(), t.Minute(), t.Second())
		iiifManifest.Comment = fmt.Sprintf("Created by 'metsimporter' (%s)", formatted)

		sequence.Canvases = canvases
		sequences = append(sequences, sequence)
		iiifManifest.Sequences = sequences

		iiifManifestJSON, _ := JSONMarshal(iiifManifest)

		key := fmt.Sprintf(config.ExportKeyPatternIiif, manifestData.WorkID)
		err := helper.UploadTo(manifestData.Product, key, iiifManifestJSON)
		if err != nil {
			log.Errorf("Could not upload iiif manifest for %s to S3, due to %s", key, err.Error())
		}

		log.Infof("Finish iiif manifest creation for %s", manifestData.WorkID)
	}
}

func JSONMarshal(t interface{}) ([]byte, error) {
	buffer := &bytes.Buffer{}
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)
	err := encoder.Encode(t)
	return buffer.Bytes(), err
}

func uploadJsonToS3() {

	for jsonUploadObject := range jsonUploadChan {
		err := helper.UploadTo(jsonUploadObject.Bucket, jsonUploadObject.Key, jsonUploadObject.Content)
		if err != nil {
			log.Errorf("Could not upload iiif manifest for %s to S3, due to %s", jsonUploadObject.Key, err.Error())
		}
	}
}

func getSeeAlso(ccontext string, workID string, logID string, withMets bool) []types.Dataset {
	var seealso []types.Dataset
	var ds types.Dataset
	var bibID, risID, endnoteID string

	// TODO add NLH
	// TODO product specific
	// TODO check OCRD
	if ccontext == "digizeit" {

		if withMets {
			ds = types.Dataset{
				ID:      fmt.Sprintf(config.MetsDownloadURLDigizeit, workID),
				Format:  "text/xml",
				Profile: config.MetsProfile,
			}
			seealso = append(seealso, ds)
		}

		if logID == "" {
			// 020406282_0004.bib
			bibID = fmt.Sprintf(config.BibDownloadURLDigizeit, workID, workID)
			risID = fmt.Sprintf(config.RisDownloadURLDigizeit, workID, workID)
			endnoteID = fmt.Sprintf(config.EndnoteDownloadURLDigizeit, workID, workID)
		} else {
			// 020406282_0004_LOG_0009.bib
			bibID = fmt.Sprintf(config.BibDownloadURLDigizeit, workID, workID+"_"+logID)
			risID = fmt.Sprintf(config.RisDownloadURLDigizeit, workID, workID+"_"+logID)
			endnoteID = fmt.Sprintf(config.EndnoteDownloadURLDigizeit, workID, workID+"_"+logID)
		}

		ds = types.Dataset{
			Label:   "BibTex",
			ID:      bibID,
			Format:  config.BibFormat,
			Profile: config.BibProfile,
		}
		seealso = append(seealso, ds)

		ds = types.Dataset{
			Label:   "RIS",
			ID:      risID,
			Format:  config.RisFormat,
			Profile: config.RisProfile,
		}
		seealso = append(seealso, ds)

		ds = types.Dataset{
			Label:   "EndNote",
			ID:      endnoteID,
			Format:  config.EndnoteFormat,
			Profile: config.EndnoteProfile,
		}
		seealso = append(seealso, ds)

	} else if ccontext == "gdz" {
		if withMets {
			ds = types.Dataset{
				ID:      fmt.Sprintf(config.MetsDownloadURLGDZ, workID),
				Format:  "text/xml",
				Profile: config.MetsProfile,
			}
			seealso = append(seealso, ds)
		}

		if logID == "" {
			// 020406282_0004.bib
			bibID = fmt.Sprintf(config.BibDownloadURLGDZ, workID)
			risID = fmt.Sprintf(config.RisDownloadURLGDZ, workID)
			endnoteID = fmt.Sprintf(config.EndnoteDownloadURLGDZ, workID)
		} else {
			// 020406282_0004_LOG_0009.bib
			bibID = fmt.Sprintf(config.BibDownloadURLGDZ, workID+"_"+logID)
			risID = fmt.Sprintf(config.RisDownloadURLGDZ, workID+"_"+logID)
			endnoteID = fmt.Sprintf(config.EndnoteDownloadURLGDZ, workID+"_"+logID)
		}

		ds = types.Dataset{
			ID:      bibID,
			Format:  config.BibFormat,
			Profile: config.BibProfile,
		}
		seealso = append(seealso, ds)

		ds = types.Dataset{
			ID:      risID,
			Format:  config.RisFormat,
			Profile: config.RisProfile,
		}
		seealso = append(seealso, ds)

		ds = types.Dataset{
			ID:      endnoteID,
			Format:  config.EndnoteFormat,
			Profile: config.EndnoteProfile,
		}
		seealso = append(seealso, ds)

	}

	return seealso
}

func getImageInfo(infoJsonJob InfoJsonRetrievalJob, s3Client *s3.S3) (templating.ImageInfo, error) {

	b := bytes.Buffer{}
	var result *s3.GetObjectOutput
	var err error
	attempts := 0

	if !infoJsonJob.ForceOverwrite {
		query := &s3.HeadObjectInput{
			Bucket: aws.String(infoJsonJob.Product),
			Key:    aws.String(infoJsonJob.InfoJsonKey),
		}

		//_, err := c.outClient.HeadObject(query)
		_, err := s3Client.HeadObject(query)
		if err != nil {
			if strings.Contains(err.Error(), "NoSuchKey") {
				// go on: NoSuchKey
			} else {
				// go on: Unable to get object
			}
		} else {
			return templating.ImageInfo{}, fmt.Errorf("Key (%s)/%s exist", infoJsonJob.Product, infoJsonJob.InfoJsonKey)
		}
	} else {
		//log.Printf("Force overwrite Key (%s)/%s", c.img_out_bucket, content.img_out_key)
	}

	for {
		attempts++

		result, err = s3Client.GetObject(&s3.GetObjectInput{
			Bucket: aws.String(infoJsonJob.Product),
			Key:    aws.String(infoJsonJob.PageKey),
			//Key:    aws.String(infoJsonJob.Key),
		})
		if err != nil {
			if strings.Contains(err.Error(), "NoSuchKey") {
				// go on: NoSuchKey
				return templating.ImageInfo{}, fmt.Errorf("NoSuchKey %s", infoJsonJob.Key)
			}

			if attempts > 5 {
				time.Sleep(time.Duration(6*attempts) * time.Second)
				return templating.ImageInfo{}, fmt.Errorf("unable to load %s, %s", infoJsonJob.Key, err)
			}
			continue
		}
		break
	}

	defer result.Body.Close()
	if _, err := io.Copy(&b, result.Body); err != nil {
		if _, err := io.Copy(&b, result.Body); err != nil {
			return templating.ImageInfo{}, fmt.Errorf("failed to copy response body for %s, %s", infoJsonJob.Key, err.Error())
		}
	}

	mw := imagick.NewMagickWand()
	defer mw.Destroy()
	err = mw.ReadImageBlob(b.Bytes())
	if err != nil {
		return templating.ImageInfo{}, fmt.Errorf("Failed to create image object %s, due to %s\n", infoJsonJob.Key, err.Error())
	}

	var x, y float64
	depth := mw.GetImageDepth()
	height := mw.GetImageHeight()
	width := mw.GetImageWidth()
	size, _ := mw.GetImageLength()
	x, y, _ = mw.GetImageResolution()

	return templating.ImageInfo{
		Depth:       depth,
		Height:      int32(height),
		Width:       int32(width),
		Size:        int32(size),
		X:           x,
		Y:           y,
		Document:    infoJsonJob.Document,
		Context:     infoJsonJob.Context,
		Product:     infoJsonJob.Product,
		Page:        infoJsonJob.Page,
		InfoJsonKey: infoJsonJob.InfoJsonKey,
	}, nil
}

func createImageInfo(imageInfo templating.ImageInfo) string {
	infojsonString := templating.Create(imageInfo)
	// fmt.Printf("infojsonString: %v\n", infojsonString)
	return infojsonString
}

func uploadInfoJson(infojsonString string, key string, bucket string, s3Client *s3.S3) error {

	err := retry(5, 2*time.Second, func() (err error) {
		_, err = s3Client.PutObject((&s3.PutObjectInput{}).
			SetBucket(bucket).
			SetKey(key).
			SetBody(bytes.NewReader([]byte(infojsonString))),
		)
		return nil
	})
	if err != nil {
		return fmt.Errorf("Failed to write Info.json %s, due to %s\n", key, err.Error())
	} else {
		log.Printf("Finish upload %s\n", key)
	}
	return nil
}

func enqueueImageInfo(imageInfo templating.ImageInfo) {
	var err error

	imageInfoDataJson, err := json.Marshal(imageInfo)
	if err != nil {
		log.Errorf("could not marshall message %v, due to %s, exiting", imageInfoDataJson, err)
		return
	}

	key := fmt.Sprintf("%s:%s", imageInfo.Document, imageInfo.Page)
	_, err = redisClient.HSet(config.RedisImageInfoHSetQueue, key, string(imageInfoDataJson)).Result()
	if err != nil {
		log.Errorf("could not write to redis HSET, due to %s", err)
	}
}

func retry(attempts int, sleep time.Duration, f func() error) (err error) {
	for i := 0; ; i++ {
		err = f()
		if err == nil {
			return
		}

		if i >= (attempts - 1) {
			break
		}

		time.Sleep(sleep)

		log.Println("retrying after error:", err)
	}
	return fmt.Errorf("after %d attempts, last error: %s", attempts, err)
}
