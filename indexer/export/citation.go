package export

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/helper"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/index"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/templating"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/types"
)

var ()

func init() {

	if config.TestRun && config.TestRunLimit == 0 {
		config.TestRunLimit = 5
	}
}

func getBibtexStringArray(exportData types.ExportData) types.Data {

	var data = types.Data{}

	var bibtexStringArray []string

	bibtexStringArray = append(bibtexStringArray, exportData.ID+",\n")
	if len(exportData.Author) > 0 {
		bibtexStringArray = append(bibtexStringArray, "\tauthor = { "+strings.Join(exportData.Author[:], ", ")+" },\n")
	}
	if exportData.Title != "" {
		bibtexStringArray = append(bibtexStringArray, "\ttitle = { "+exportData.Title+" },\n")
	}
	if len(exportData.Publisher) > 0 {
		bibtexStringArray = append(bibtexStringArray, "\tpublisher = { "+strings.Join(exportData.Publisher, ",")+" },\n")
	}
	if exportData.Editor != "" {
		bibtexStringArray = append(bibtexStringArray, "\teditor = { "+exportData.Editor+" },\n")
	}
	if len(exportData.PublishingPlace) > 0 {
		bibtexStringArray = append(bibtexStringArray, "\taddress = { "+strings.Join(exportData.PublishingPlace, ",")+" },\n")
	}
	if exportData.PublishingYearString != "" {
		bibtexStringArray = append(bibtexStringArray, "\tyear = { "+exportData.PublishingYearString+" },\n")
	}
	if exportData.Issn != "" {
		bibtexStringArray = append(bibtexStringArray, "\tissn = { "+exportData.Issn+" },\n")
	}
	if exportData.Series != "" {
		bibtexStringArray = append(bibtexStringArray, "\tseries = { "+exportData.Series+" },\n")
	}
	if exportData.URL != "" {
		bibtexStringArray = append(bibtexStringArray, "\turl = { "+exportData.URL+" },\n")
	}
	if len(exportData.Language) > 0 {
		bibtexStringArray = append(bibtexStringArray, "\tlanguage = { "+strings.Join(exportData.Language[:], ", ")+" },\n")
	}

	data.DataFields = bibtexStringArray
	return data

}

func getRisStringArray(exportData types.ExportData) types.Data {

	var data = types.Data{}

	var risStringArray []string

	risStringArray = append(risStringArray, "TY - JOUR\n")
	if len(exportData.Author) > 0 {
		for _, author := range exportData.Author {
			risStringArray = append(risStringArray, "AU - "+author+"\n")
		}
	}
	if exportData.Title != "" {
		risStringArray = append(risStringArray, "TI - "+exportData.Title+"\n")
	}
	if len(exportData.Publisher) > 0 {
		risStringArray = append(risStringArray, "PB - "+strings.Join(exportData.Publisher, ",")+"\n")
	}
	if exportData.Editor != "" {
		risStringArray = append(risStringArray, "ED - "+exportData.Editor+"\n")
	}
	if len(exportData.PublishingPlace) > 0 {
		risStringArray = append(risStringArray, "PP - "+strings.Join(exportData.PublishingPlace, ",")+"\n")
	}
	if exportData.PublishingYearString != "" {
		risStringArray = append(risStringArray, "PY - "+exportData.PublishingYearString+"\n")
	}
	if exportData.Issn != "" {
		risStringArray = append(risStringArray, "SN - "+exportData.Issn+"\n")
	}
	if exportData.URL != "" {
		risStringArray = append(risStringArray, "UR - "+exportData.URL+"\n")
	}
	if len(exportData.Language) > 0 {
		risStringArray = append(risStringArray, "LA - "+strings.Join(exportData.Language[:], ", ")+"\n")
	}

	risStringArray = append(risStringArray, "ER -\n")

	data.DataFields = risStringArray
	return data

}

func getEndnoteDoc(exportData types.ExportData) []byte {

	endnoteData := &types.EndnoteData{}
	record := types.Record{}

	record.EndnoteRefType = types.RefType{
		Name:  "Journal Article",
		Value: "17",
	}

	if exportData.IsPeriodical {
		var title *types.Title = new(types.Title)
		title.Title = exportData.Title
		title.SecondaryTitle = exportData.Subtitle
		record.Titles = title
		record.ISSN = exportData.Issn
	} else if exportData.IsVolume {
		var authors []types.Author
		for _, aut := range exportData.Author {
			authors = append(authors, types.Author{
				Value: aut,
			})
		}

		// TODO check this
		record.Contributors = append(record.Contributors, types.Contributor{
			Authors: authors,
		})

		record.Volume = exportData.Volume
		record.ISSN = exportData.ParentIssn
		record.Periodical = &types.Periodical{
			FullTitle: exportData.Periodical,
		}

		record.Pages = strconv.FormatInt(exportData.Pages, 10)

		var pubDates []types.PubDate

		pubDates = append(pubDates, types.PubDate{
			Date: exportData.PublishingYearString,
		})

		var date *types.Date = new(types.Date)
		date.PubDates = pubDates
		date.Year = exportData.PublishingYear
		record.Dates = date

	}

	if len(exportData.Publisher) > 0 || len(exportData.PublishingPlace) > 0 {
		record.PubLocation = exportData.PublishingPlace
		record.Publisher = exportData.Publisher

	}

	record.Language = strings.Join(exportData.Language[:], ", ")

	record.URLs = types.URL{
		URL: exportData.URL,
	}

	endnoteData.Records = append(endnoteData.Records, record)

	output, err := xml.MarshalIndent(endnoteData, "  ", "    ")
	if err != nil {
		fmt.Printf("error: %v\n", err)
	}

	return output

}

func CreateCitationExport(mythread int) {

	// todo if periodiocal...  else if volume ...
	var err error
	var result string
	var resultArr []string

	for {
		// waiting for indexer to finish work
		val, err := redisClient.HGet(config.RedisStatusHsetQueue, config.RedisIndexingFinishedKey).Result()
		if err != nil {
			log.Errorf("could not get value from redis, due to %s", err.Error())
		} else {
			if val == "1" {
				break
			}
		}
		time.Sleep(60 * time.Second)
	}

	for {
		resultArr, err = redisClient.BRPop(3600*time.Second, config.RedisCitationQueue).Result()
		if err == redis.Nil {
			continue
		} else if err != nil {
			log.Errorf("indexer could not request redis, due to %s", err.Error())
			time.Sleep(30 * time.Second)
			continue
		}
		result = resultArr[1]

		citationData := types.ExportWork{}
		json.Unmarshal([]byte(result), &citationData)

		var esLogMap map[string]types.ESLog
		esLogMap, err = index.GetESLogFromES(citationData.WorkID, citationData.Context)
		if err != nil {
			log.Errorf("could not get logical info from index for %s, due to %s", citationData.WorkID, err.Error())
			continue
		}

		for _, esLog := range esLogMap {

			if esLog.Title == nil || esLog.Title.Title == "" {
				continue
			}

			exportData, err := index.GetExportDataFromEsLog(esLog)
			if err != nil {
				log.Errorf("Could not create citation data for %s, due to %s", citationData.WorkID, err.Error())
			}

			// --- bibtex (text)
			bibtexDoc := templating.ProcessFile("templates/bibtex.tmpl", getBibtexStringArray(exportData))
			key := fmt.Sprintf(config.ExportKeyPatternBibtex, citationData.WorkID, citationData.WorkID, esLog.Log)

			err = helper.UploadTo(citationData.Product, key, []byte(bibtexDoc))
			//err = helper.UploadTo(config.Product, key, []byte(bibtexDoc))
			if err != nil {
				log.Error("Could not upload bibtex citation doc for %s to S3, due to %s", key, err.Error())
			}

			// --- ris (text)
			risDoc := templating.ProcessFile("templates/ris.tmpl", getRisStringArray(exportData))
			key = fmt.Sprintf(config.ExportKeyPatternRis, citationData.WorkID, citationData.WorkID, esLog.Log)
			err = helper.UploadTo(citationData.Product, key, []byte(risDoc))
			//err = helper.UploadTo(config.Product, key, []byte(risDoc))
			if err != nil {
				log.Error("Could not upload ris citation doc for %s to S3, due to %s", key, err.Error())
			}

			// --- endnote (xml)
			endnoteDoc := getEndnoteDoc(exportData)
			key = fmt.Sprintf(config.ExportKeyPatternEndnote, citationData.WorkID, citationData.WorkID, esLog.Log)
			err = helper.UploadTo(citationData.Product, key, endnoteDoc)
			//err = helper.UploadTo(config.Product, key, endnoteDoc)
			if err != nil {
				log.Error("Could not upload endnote citation doc for %s to S3, due to %s", key, err.Error())
			}

		}

		log.Infof("Finish citation export creation for %s", citationData.WorkID)

	}

}
