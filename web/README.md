# WEB Module

* This module creates services endpoints for the importer


# POST requests

## Create Converter job

    POST http://localhost:1323/api/converter/jobs HTTP/1.1
    Accept: */*
    Cache-Control: no-cache
    Content-Type: application/json

    { 
        "document": "1047098326_0002",   
        "context": "digizeit", 
        "product": "indexer",  
        "log": ""
    }

* Field "log" could name a logical structure element to create a PDF for this or the document ID for full PDF creation
    * Examples
        * "log": "LOG_0001"
        * "log": "1047098326_0002"

## Create Indexer job

    POST http://localhost:1323/api/indexer/jobs HTTP/1.1
    Accept: */*
    Cache-Control: no-cache
    Content-Type: application/json

    { 
        "document": "1047098326_0002",   
        "context": "digizeit", 
        "product": "indexer",  
        "reindex": false
    }

# GET requests
* Within the docker environment one can call the services via "<web:port>", from outside via "<adr:port>".

## Get the number of Elements in converter queue

    GET http://localhost:1323/api/converter/jobs HTTP/1.1

## Get the number of Elements in indexer queue

    GET http://localhost:1323/api/indexer/jobs HTTP/1.1
