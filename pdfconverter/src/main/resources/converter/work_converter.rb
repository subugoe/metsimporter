# frozen_string_literal: true

require 'vertx/vertx'
# require 'vertx-redis/redis_client'

require 'logger'
require 'gelf'
require 'aws-sdk'
require 'redis'
require 'rsolr'
require 'json'
require 'elasticsearch'

require 'uri'
require 'net/http'

class WorkConverter
  MAX_ATTEMPTS = ENV['MAX_ATTEMPTS'].to_i
  
  def initialize
    # @logger       = GELF::Logger.new(ENV['GRAYLOG_URI'], ENV['GRAYLOG_PORT'].to_i, "WAN", {:facility => ENV['GRAYLOG_FACILITY']})
    # @logger.level = ENV['DEBUG_MODE'].to_i


    @logger       = Logger.new(STDOUT)
    @logger.level = ENV['DEBUG_MODE'].to_i

    @context = ENV['CONTEXT']
    @unique_queue = ENV['REDIS_UNIQUE_QUEUE']

    @img_convert_queue = ENV['REDIS_IMG_CONVERT_QUEUE']

    @rredis = Redis.new(
      host: ENV['REDIS_HOST'],
      port: ENV['REDIS_PORT'].to_i,
      db: ENV['REDIS_DB'].to_i,
      reconnect_attempts: 3
    )

    if @context.downcase != 'digizeit'
      @solr = RSolr.connect url: ENV['SOLR_HOST']
    end

  end

  def client
    @client ||= Elasticsearch::Client.new(
      url: ENV['ELASTICSEARCH_HOST'],
      # user: "elastic",
      # password: "password",
      log: true
    )
  end

  def get_s3_key(_id)
    nil
  end

  def process_response(res)
    @logger.info('[work_converter] process_response() 1 Start processing of conversion jobs')

    begin
      if res != '' && !res.nil?

        msg  = res[1]
        json = JSON.parse msg

        context = json['context']
        product = json['product']
        id      = json['document']
        log     = json['log']
        log_id  = "#{id}___#{log}"

        @logger.debug "[work_converter] Start processing for '#{log_id}'"

        @s3_bucket = ''
        @s3_bucket = product

        if context.nil?
          raise 'No context specified in request, use {gdz | nlh}'
        else

          unless context.downcase.start_with?('nlh') || (context.downcase == 'gdz') || (context.downcase == 'digizeit')
            raise "Unknown context '#{context}', use {gdz | nlh | digizeit}"
          end

          if context.downcase == 'gdz' || context.downcase.start_with?('nlh')
            build_jobs_from_solr(context, product, id, log, log_id)
          elsif context.downcase == 'digizeit'
            build_jobs_from_es(context, product, id, log, log_id)
          end

        end

      else
        raise 'Could not process empty string or nil'
      end
    rescue Exception => e
      @logger.error "[work_converter] Processing problem with '#{res}' \t#{e.message}\n#{e.backtrace}"
    end
  end

  def removeQueue(queue)
    keys = @rredis.hkeys(queue)
    @rredis.hdel(queue, keys) unless keys.empty?
  end

  def pushToQueue(queue, arr)
    @rredis.lpush(queue, arr)
  end

  def build_jobs_from_solr(context, product, id, log, log_id)
    request_logical_part = id != log

    if context.downcase == 'gdz'
      solr_resp = @solr.get 'select',
                            params: { q: "id:#{id}", fl: 'id doctype log_id record_identifier' }
    elsif context.downcase.start_with?('nlh')
      solr_resp = @solr.get 'select',
                            params: { q: "work:#{id}", fl: 'id doctype log_id record_identifier work' }
    end

    if (solr_resp['response']['numFound'] == 0) || (request_logical_part && solr_resp['response']['docs'].first['log_id'].nil?)
      @logger.error("[work_converter] Couldn't find #{id} in index, conversion for #{log_id} not possible")
      return
    end

    removeQueue(log_id)

    doctype           = solr_resp['response']['docs'].first['doctype']
    record_identifier = solr_resp['response']['docs'].first['record_identifier']

    if doctype == 'work'

      if context.downcase == 'gdz'
        resp = (@solr.get 'select',
                          params: { q: "id:#{id}",
                                    fl: 'page   log_id   log_start_page_index   log_end_page_index image_format ' })['response']['docs'].first
      elsif context.downcase.start_with?('nlh')
        resp = (@solr.get 'select',
                          params: { q: "work:#{id}",
                                    fl: 'page   log_id   log_start_page_index   log_end_page_index image_format ' })['response']['docs'].first
      end

      log_start_page_index = 0
      log_end_page_index   = -1

      image_format = resp['image_format']

      if request_logical_part

        log_id_index = resp['log_id'].index log

        if log_id_index.nil?
          @logger.error "[work_converter] Log-Id #{log} for work #{id} not found in index"
          return
        end

        log_start_page_index = (resp['log_start_page_index'][log_id_index]) - 1
        log_end_page_index   = (resp['log_end_page_index'][log_id_index]) - 1

        if log_end_page_index < log_start_page_index
          log_end_page_index = log_start_page_index
        end

      end

      pages       = resp['page'][log_start_page_index..log_end_page_index]
      pages_count = pages.size

      pages.each do |page|
        msg = {
          'context' => context,
          'id' => id,
          'record_identifier' => record_identifier,
          'log' => log,
          'log_id' => log_id,
          'request_logical_part' => request_logical_part,
          'page' => page,
          'pages_count' => pages_count,
          # 'pdf_exist' => pdf_exist,
          'log_start_page_index' => log_start_page_index,
          'log_end_page_index' => log_end_page_index,
          'image_format' => image_format,
          'product' => product
        }

        # if request_logical_part
        #   pushToQueue(@img_convert_log_queue, [msg.to_json])
        # else
        #   pushToQueue(@img_convert_full_queue, [msg.to_json])
        # end

        pushToQueue(@img_convert_queue, [msg.to_json])
      end

      # if request_logical_part
      #   @logger.debug "[work_converter] Start Part PDF creation #{id}"
      # else
      #   @logger.debug "[work_converter] Start Full PDF creation #{id}"
      # end

    else
      @logger.error("[work_converter] Could not create PDF for multivolume work '#{id}', PDF not created")
      @rredis.hdel(@unique_queue, log_id)
    end
  end

  def min (a,b)
    a<=b ? a : b
  end

  def max (a,b)
    a>=b ? a : b
  end

  # "log_id" : "1047098326_0002|LOG_0004"
  def build_jobs_from_es(context, product, id, log, log_id)
    request_logical_part = id != log

    # move to config
    log_index = 'meta.dz_log'
    phys_index = 'meta.dz_phys'

    #clientt = client

    # get start and end page index
    #
    #
    # body = if request_logical_part
    #          {
    #            query: {
    #              "bool": {
    #                "must": [
    #                  { "match": { "id.keyword": id + '|' + log } }
    #                ]
    #              }
    #            },
    #            "_source": %w[id start_page_index end_page_index]
    #          }
    #        else
    #          {
    #            query: {
    #              "bool": {
    #                "must": [
    #                  { "match": { "id": id } },
    #                  { "match": { "IsFirst": true } }
    #                ]
    #              }
    #            },
    #            "_source": %w[id start_page_index end_page_index]
    #          }
    #        end

    # resp = client.search(
    #   index: log_index,
    #   body: body,
    #   scroll: '1m',
    #   size: 3000
    # )

    # # "id", "order", "page", "format", "start_page_index", "end_page_index"]
    # total = resp['hits']['total']
    # if total == 0
    #   removeQueue(log_id)
    #   @logger.error("[work_converter] Couldn't find #{id}|#{log} in index, conversion not possible")
    #   return
    # end

    # start_page_index = resp['hits']['hits'].first['_source']['start_page_index']
    # end_page_index = resp['hits']['hits'].first['_source']['end_page_index']

    # get page array
    #
    body = {
      "query": {
        # "bool": {
        #   "must": [
        #     {
        #       "match": { "work": id }
        #     },
        #     {
        #       "range": {
        #         "Index": {
        #           "gte": start_page_index,
        #           "lte": end_page_index,
        #           "boost": 2.0
        #         }
        #       }
        #     }
        #   ]
        # }
        "bool": {
          "should": [
             {"match": {"log_id.keyword": id + '|' + log}},
             {"match": {"structrun.parent_id.keyword": id + '|' + log}}
          ]
    }
      },
      "sort": [
        {
          "order": { "order": 'asc' }
        }
      ],
      "_source": %w[work page format Index]
    }

    resp = client.search(
      index: phys_index,
      body: body,
      scroll: '1m',
      size: 3000
    )

    # "id", "order", "page", "format", "start_page_index", "end_page_index"]
    total = resp['hits']['total']
    if total == 0
      removeQueue(log_id)
      @logger.error("[work_converter] Couldn't find #{id} in physical index, conversion not possible")
      return
    end

    image_format = resp['hits']['hits'].first['_source']['format']

    pages = []
    start_page_index = 0
    end_page_index = 0

    loop do
      hits = resp.dig('hits', 'hits')
      break if hits.empty?

      hits.each do |hit|
        start_page_index = min(start_page_index, hit['_source']['Index'])
        end_page_index = max(end_page_index, hit['_source']['Index'])
        pages << hit['_source']['page']
      end

      resp = client.scroll(
        body: { scroll_id: resp['_scroll_id'] },
        scroll: '1m'
      )
    end

    pages_count = pages.size

    pages.each do |page|
      msg = {
        'context' => context,
        'id' => id,
        # "record_identifier"    => record_identifier,
        'log' => log,
        'log_id' => log_id,
        'request_logical_part' => request_logical_part,
        'page' => page,
        'pages_count' => pages_count,
        # "pdf_exist"            => false,
        'log_start_page_index' => start_page_index,
        'log_end_page_index' => end_page_index,
        'image_format' => image_format,
        'product' => product,
        'last_page' => pages[-1]

      }

      pushToQueue(@img_convert_queue, [msg.to_json])
    end

    #@logger.debug("[work_converter] total: #{total}, image_format: #{image_format}, start_page_index: #{start_page_index}, end_page_index: #{end_page_index}, pages: #{pages}, request_logical_part: #{request_logical_part}")
  end
end
