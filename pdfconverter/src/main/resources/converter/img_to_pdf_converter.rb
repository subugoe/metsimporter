# frozen  _string_literal: true

require 'rubygems'

require 'logger'
require 'gelf'
require 'aws-sdk'
require 'redis'
require 'rsolr'
require 'elasticsearch'
require 'json'
require 'open-uri'
require 'net/http'
require 'fileutils'
require 'mini_magick'
require 'vips'
require 'prawn-svg'

require 'model/disclaimer_info'
require 'helper/mappings'

class ImgToPdfConverter
  MAX_ATTEMPTS = ENV['MAX_ATTEMPTS'].to_i
  # CONVERSION_ERROR = 1
  # DOWNLOAD_ERROR   = 1

  def initialize
    @pdfoutpath = ENV['OUT'] + ENV['PDF_OUT_SUB_PATH']

    # @logger       = GELF::Logger.new(ENV['GRAYLOG_URI'], ENV['GRAYLOG_PORT'].to_i, "WAN", {:facility => ENV['GRAYLOG_FACILITY']})
    # @logger.level = ENV['DEBUG_MODE'].to_i

    @logger       = Logger.new(STDOUT)
    @logger.level = ENV['DEBUG_MODE'].to_i

    @rredis = Redis.new(
      host: ENV['REDIS_HOST'],
      port: ENV['REDIS_PORT'].to_i,
      db: ENV['REDIS_DB'].to_i,
      reconnect_attempts: 3
    )

    @context = ENV['CONTEXT']

    @unique_queue = ENV['REDIS_UNIQUE_QUEUE']

    if @context.downcase != 'digizeit'
      @solr = RSolr.connect url: ENV['SOLR_HOST']
    end

    @s3_pdf_key_pattern   = ENV['S3_PDF_KEY_PATTERN']
    @s3_image_key_pattern = ENV['S3_IMAGE_KEY_PATTERN']
  
    MiniMagick.configure do |config|
      config.validate_on_create = false
      config.validate_on_write  = false
      # config.whiny              = false
    end

    # MiniMagick.logger.level = Logger::DEBUG

    @s3 = Aws::S3::Client.new(
      access_key_id: ENV['STORAGE_KEY'],
      secret_access_key: ENV['STORAGE_SECRET'],
      endpoint: ENV['STORAGE_ENDPOINT'],
      region: ENV['STORAGE_REGION'],
      http_open_timeout: 30,
      retry_limit: 3,
      force_path_style: false
    )

  end

  # ---

  def download_from_s3(s3_bucket, s3_key, path)
    attempts = 0
    begin
      @s3.get_object(
        { bucket: s3_bucket, key: s3_key },
        target: path
      )
    rescue Exception => e
      @logger.error "[img_converter] [GDZ-527] Could not download file (#{s3_bucket}/#{s3_key}) from S3 \t#{e.message}"
      attempts += 1
      retry if attempts < MAX_ATTEMPTS
      return false
    end

    true
  end

  def upload_object_to_s3(to_pdf_path, s3_bucket, s3_key)
    @s3.put_object(bucket: s3_bucket,
                   key: s3_key,
                   body: File.read(to_pdf_path))
  rescue Aws::S3::MultipartUploadError => e
    @logger.error "[img_converter] MultipartUploadError - Could not push file (key: #{s3_key}) to S3 \n'#{e.message}'\n#{e.backtrace}"
  rescue Exception => e
    @logger.error "[img_converter] Exception - Could not push file (bucket: #{s3_bucket}, key: #{s3_key}) to S3 \n'#{e.message}'\n#{e.backtrace}"
  end

  def s3_pdf_exist?(bucket, id, object_id, _context)
    s3_key = format(@s3_pdf_key_pattern, id, object_id)

    res   = Aws::S3::Resource.new(client: @s3)
    exist = res.bucket(bucket).object(s3_key).exists?

    if exist
      true
    else
      false
    end
  end

  def process_response(json)
    @context          = json['context']
    product           = json['product']
    id                = json['id']
    log               = json['log']
    log_id            = json['log_id']
    record_identifier = json['record_identifier']

    request_logical_part = json['request_logical_part']
    pages_count          = json['pages_count']
    page                 = json['page']
    last_page            = json['last_page']

    pdf_exist            = json['pdf_exist']
    log_start_page_index = json['log_start_page_index']
    log_end_page_index   = json['log_end_page_index']

    image_format = json['image_format']

    product = 'gdz' if (product == '') || product.nil?

    # ---

    to_pdf_dir       = "#{@pdfoutpath}/#{product}/#{id}/#{log}"
    to_tmp_img       = "#{to_pdf_dir}/#{page}.#{image_format}"
    to_tmp_jpg       = "#{to_pdf_dir}/#{page}.jpg"
    to_page_pdf_path = "#{to_pdf_dir}/#{page}.pdf"
    to_log_pdf_path  = "#{to_pdf_dir}/#{log}.pdf"

    to_full_pdf_path = if request_logical_part
                         "#{to_pdf_dir}/#{log}.pdf"
                       else
                         "#{to_pdf_dir}/#{id}.pdf"
                       end

    FileUtils.mkdir_p(to_pdf_dir)

    s3_bucket = product

    s3_pdf_key     = format(@s3_pdf_key_pattern, id, id)
    s3_log_pdf_key = format(@s3_pdf_key_pattern, id, log)
    s3_page_pdf_key = format(@s3_pdf_key_pattern, id, page)
    
    if s3_pdf_exist?(s3_bucket, id, page, @context)
      s3_key = format(@s3_pdf_key_pattern, id, page)
      download_from_s3(s3_bucket, s3_key, to_page_pdf_path)
    else
      s3_image_key = format(@s3_image_key_pattern, id, page, image_format)
      load_succeed = download_from_s3(s3_bucket, s3_image_key, to_tmp_img)

      if load_succeed
        convert(to_tmp_img, to_tmp_jpg, to_page_pdf_path)
        upload_object_to_s3(to_page_pdf_path, s3_bucket, s3_page_pdf_key)
      elsif @context == 'digizeit'
        FileUtils.cp('templates/digizeit_page_not_found_error_1.pdf', to_page_pdf_path)
      elsif @context == 'gdz'
        FileUtils.cp('templates/gdz_page_not_found_error_1.pdf', to_page_pdf_path)
      elsif @context.downcase.start_with? 'nlh'
        FileUtils.cp('templates/nlh_page_not_found_error_1.pdf', to_page_pdf_path)
      end


    end

    pushToQueue(log_id, page, true)

    if all_images_converted?(log_id, pages_count)

      removeQueue(log_id)

      if @context == 'digizeit'
        merge_to_full_pdf_pdftk_system_via_elasticsearch(to_pdf_dir, id, log, log_id, product, last_page)
      else
        merge_to_full_pdf_pdftk_system_via_solr(to_pdf_dir, id, log, request_logical_part, product, last_page)
      end

      disclaimer_info = if @context == 'digizeit'
                          load_metadata_from_elasticsearch(id, log)
                        else
                          load_metadata_from_solr(id)
                        end

      add_bookmarks_pdftk_system(to_pdf_dir, id, log, disclaimer_info)
      
      add_disclaimer_pdftk_system(to_log_pdf_path, to_pdf_dir, id, log, record_identifier, request_logical_part,
                                  disclaimer_info, product)

      begin
        if request_logical_part
          upload_object_to_s3(to_full_pdf_path, s3_bucket, s3_log_pdf_key)
        else
          upload_object_to_s3(to_full_pdf_path, s3_bucket, s3_pdf_key)
        end
      rescue Exception => e
        GC.start

        remove_dir(to_pdf_dir)

        @logger.error("[img_converter] Upload to S3 failed #{log_id}\t#{e.message}")

        @rredis.del(@unique_queue, log_id)
      end

      GC.start

      remove_dir(to_pdf_dir)

      @logger.info("[img_converter] Finish PDF creation for #{log_id}")

      @rredis.del(@unique_queue, log_id)

    end

  rescue Exception => e
    #@logger.error "[img_converter] Processing problem with request data: \t#{e.message}"
    #@logger.error "[img_converter] Processing problem with request data '#{json}' \t#{e.message}"
    @logger.debug "[img_converter] Processing problem with request data \nmessage: #{e.message}\nbacktrace: #{e.backtrace}"
    @rredis.del(@unique_queue, log_id)
  end

  def remove_dir(path)
    FileUtils.remove_dir(path, force = true)
  end

  def pushToQueue(queue, field, value)
    @rredis.hset(queue, field, value)
  end

  def removeQueue(queue)
    keys = @rredis.hkeys(queue)
    @rredis.hdel(queue, keys) unless keys.empty?
  end

  def add_info_str(bookmark_str, info_key, info_value)
    bookmark_str << "InfoBegin\n"
    bookmark_str << "InfoKey: #{info_key}\n"
    bookmark_str << "InfoValue: #{info_value}\n"
  end

  def add_to_bookmark_str(bookmark_str, bm_title, bm_level, bm_page_number)
    bookmark_str << "BookmarkBegin\n"
    bookmark_str << "BookmarkTitle: #{bm_title}\n"
    bookmark_str << "BookmarkLevel: #{bm_level}\n"
    bookmark_str << "BookmarkPageNumber: #{bm_page_number}\n"
  end

  # from solr
  def load_metadata_from_solr(id)
    disclaimer_info = DisclaimerInfo.new

    if @context == 'gdz'
      solr_work = (@solr.get 'select',
                             params: { q: "id:#{id}",
                                       fl: 'id product purl catalogue log_id log_label  log_start_page_index  log_end_page_index log_level   log_type    title subtitle shelfmark bycreator year_publish_string publisher place_publish genre dc subject rights_owner parentdoc_work parentdoc_label parentdoc_type' })['response']['docs'].first
    elsif @context.downcase.start_with?('nlh')
      solr_work = (@solr.get 'select',
                             params: { q: "work:#{id}",
                                       fl: 'id product purl catalogue log_id log_label  log_start_page_index  log_end_page_index log_level   log_type    title subtitle shelfmark bycreator year_publish_string publisher place_publish genre dc subject rights_owner parentdoc_work parentdoc_label parentdoc_type navi_year navi_month navi_day' })['response']['docs'].first

      year  = solr_work['navi_year']
      month = solr_work['navi_month']
      day   = solr_work['navi_day']
      m     = Mappings.strctype_number_to_month(month)

      disclaimer_info.date = "#{day}. #{m} #{year}" unless m.nil?

      product = Mappings.strctype_product_short_to_long_name(solr_work['product'])

      disclaimer_info.product = product unless product.nil?

    elsif @context == 'digizeit'
      # todo
    end

    disclaimer_info.log_labels            = solr_work['log_label']
    disclaimer_info.log_levels            = solr_work['log_level']
    disclaimer_info.log_start_page_indexes = solr_work['log_start_page_index']

    disclaimer_info.id                       = solr_work['id']
    disclaimer_info.log_id                   = solr_work['log_id']
    disclaimer_info.log_label_arr            = solr_work['log_label']
    disclaimer_info.log_start_page_index_arr = solr_work['log_start_page_index']
    disclaimer_info.log_end_page_index_arr   = solr_work['log_end_page_index']
    disclaimer_info.log_level_arr            = solr_work['log_level']
    disclaimer_info.log_type_arr             = solr_work['log_type']

    disclaimer_info.purl          = solr_work['purl']
    disclaimer_info.catalogue_arr = solr_work['catalogue']

    disclaimer_info.title_arr           = solr_work['title']
    disclaimer_info.subtitle_arr        = solr_work['subtitle']
    disclaimer_info.bycreator           = solr_work['bycreator']
    disclaimer_info.publisher           = solr_work['publisher']
    disclaimer_info.place_publish       = solr_work['place_publish']
    disclaimer_info.year_publish_string = solr_work['year_publish_string']
    disclaimer_info.genre_arr           = solr_work['genre']
    disclaimer_info.dc_arr              = solr_work['dc']
    disclaimer_info.subject_arr         = solr_work['subject']
    disclaimer_info.year_publisher      = solr_work['year_publisher']
    disclaimer_info.shelfmark_arr       = solr_work['shelfmark']
    disclaimer_info.rights_owner_arr    = solr_work['rights_owner']

    disclaimer_info.parentdoc_work  = solr_work['parentdoc_work']
    disclaimer_info.parentdoc_label = solr_work['parentdoc_label']
    disclaimer_info.parentdoc_type  = solr_work['parentdoc_type']

    solr_work = nil

    disclaimer_info
  end

  # from elasticsearch (ES)
  def load_metadata_from_elasticsearch(id, log)
    disclaimer_info = DisclaimerInfo.new

    # config.ElasticsearchHost
    host = 'search'

    # config.DigizeitLogIndex
    log_index = 'meta.dz_log'

    client = Elasticsearch::Client.new url: "http://#{host}:9200", log: true
    client.transport.reload_connections!
    client.cluster.health

    isLog = true
    isLog = false if id == log

    # exact one hit
    if !isLog
      # if id == log
      body = {
        query: {
          "bool": {
            "must": [
              {
                "match": {
                  "IsFirst": true
                }
              },
              {
                "match": { "work": id }
              }
            ]
          }
        },
        "sort": [
          {
            "order": {
              "order": 'asc'
            }
          }
        ],
        "_source": [
          'id', 'product', 'purl', 'catalogue', 'log_id', 'label', 'start_page_index', 'end_page_index', 'level', 'type', 'title.title', 'title.subtitle', 'shelfmark_infos.shelfmark', 'bycreator', 'publish_infos.year_publish', 'publish_infos.place_publish', 'publish_infos.publisher', 'dc', 'subject_infos.subject', 'rights_info.rights_owner', 'parent.record_id', 'parent', 'navi_year', 'navi_month', 'navi_day'
        ]
      }
    else
      body = {
        query: {
          "bool": {
            "must": [
              {
                "match": { "work": id }
              },
              {
                "match": { "log": log }
              }
            ]
          }
        },
        "sort": [
          {
            "order": {
              "order": 'asc'
            }
          }
        ],
        "_source": [
          'id', 'product', 'purl', 'catalogue', 'log_id', 'label', 'start_page_index', 'end_page_index', 'level', 'type', 'title.title', 'title.subtitle', 'shelfmark_infos.shelfmark', 'bycreator', 'publish_infos.year_publish', 'publish_infos.place_publish', 'publish_infos.publisher', 'dc', 'subject_infos.subject', 'rights_info.rights_owner', 'parent.record_id', 'parent', 'navi_year', 'navi_month', 'navi_day'
        ]
      }
    end

    resp = client.search(
      index: log_index,
      body: body,
      scroll: '1m',
      size: 3000
    )

    # "id", "order", "page", "format", "start_page_index", "end_page_index"]
    total = resp['hits']['total']
    if total == 0
      removeQueue(log_id)
      @logger.error("[img_converter] Couldn't find #{id}|#{log} in log index, conversion not possible\n#{body}")
      return
    end

    year  = resp['hits']['hits'].first['_source']['navi_year']
    month = resp['hits']['hits'].first['_source']['navi_month']
    day   = resp['hits']['hits'].first['_source']['navi_day']
    m     = Mappings.strctype_number_to_month(month)

    disclaimer_info.date = "#{day}. #{m} #{year}" unless m.nil?

    product = Mappings.strctype_product_short_to_long_name(resp['hits']['hits'].first['_source']['product'])

    disclaimer_info.product = product unless product.nil?

    # TODO: check if String/Array
    disclaimer_info.id                       = resp['hits']['hits'].first['_source']['id']
    disclaimer_info.log_id                   = resp['hits']['hits'].first['_source']['log_id']
    disclaimer_info.log_label_arr            = resp['hits']['hits'].first['_source']['label']
    disclaimer_info.log_level_arr            = resp['hits']['hits'].first['_source']['level']
    disclaimer_info.log_start_page_index_arr = resp['hits']['hits'].first['_source']['start_page_index']
    disclaimer_info.log_end_page_index_arr   = resp['hits']['hits'].first['_source']['end_page_index']
    disclaimer_info.log_type_arr             = resp['hits']['hits'].first['_source']['type']

    disclaimer_info.purl          = resp['hits']['hits'].first['_source']['purl']
    disclaimer_info.catalogue_arr = resp['hits']['hits'].first['_source']['catalogue']

    unless resp['hits']['hits'].first['_source']['title'].nil?
      disclaimer_info.title_arr = resp['hits']['hits'].first['_source']['title']['title']
      disclaimer_info.subtitle_arr = resp['hits']['hits'].first['_source']['title']['subtitle']
    end

    disclaimer_info.bycreator           = resp['hits']['hits'].first['_source']['bycreator']
    disclaimer_info.publisher           = resp['hits']['hits'].first['_source']['publisher']
   
    unless resp['hits']['hits'].first['_source']['publish_infos'].nil?
      disclaimer_info.place_publish       = resp['hits']['hits'].first['_source']['publish_infos']['place_publish']
      disclaimer_info.year_publish_string = resp['hits']['hits'].first['_source']['publish_infos']['year_publish']
    end
   
    disclaimer_info.genre_arr           = resp['hits']['hits'].first['_source']['genre']
    disclaimer_info.dc_arr              = resp['hits']['hits'].first['_source']['dc']
    
    unless resp['hits']['hits'].first['_source']['subject_infos'].nil?
      disclaimer_info.subject_arr         = resp['hits']['hits'].first['_source']['subject_infos']['subject']
    end
    
    disclaimer_info.year_publisher = resp['hits']['hits'].first['_source']['year_publisher']
    
    unless resp['hits']['hits'].first['_source']['shelfmark_infos'].nil?
      disclaimer_info.shelfmark_arr       = resp['hits']['hits'].first['_source']['shelfmark_infos'][0]['shelfmark']
    end
    
    unless resp['hits']['hits'].first['_source']['rights_info'].nil?
      disclaimer_info.rights_owner_arr    = resp['hits']['hits'].first['_source']['rights_info']['rights_owner']
    end

    unless resp['hits']['hits'].first['_source']['parent_div_info'].nil?
      disclaimer_info.parentdoc_work = resp['hits']['hits'].first['_source']['parent_div_info']['ParentID']
      disclaimer_info.parentdoc_label = resp['hits']['hits'].first['_source']['parent_div_info']['ParentLabel']
      disclaimer_info.parentdoc_type  = resp['hits']['hits'].first['_source']['parent_div_info']['ParentType']
    end

    
    if !isLog

      # get all log
      body2 = {
        query: {
          "bool": {
            "must": [
              {
                "match": { "id": id }
              }
            ]
          }
        },
        "sort": [
          {
            "order": {
              "order": 'asc'
            }
          }
        ],
        "_source": [
          'label', 'start_page_index', 'level'
          # "id", "product", "purl", "catalogue", "log_id", "label", "start_page_index", "end_page_index", "level", "type", "title.title", "title.subtitle", "shelfmark_infos.shelfmark", "bycreator","publish_infos.year_publish", "publish_infos.place_publish" , "publish_infos.publisher","dc", "subject_infos.subject", "rights_info.rights_owner", "parent.record_id", "parent",  "navi_year", "navi_month", "navi_day"
        ]
      }
    else

      # get log + all which have log as parent
      body2 = {
        "query": {
          "bool": {
            "should": [
              { "match": { "id.keyword": id + '|' + log } },
              { "match": { "structrun.parent_id.keyword": id + '|' + log } }
            ]
          }
        },
        "sort": [
          {
            "order": {
              "order": 'asc'
            }
          }
        ],
        "_source": %w[label start_page_index level]
      }
    end

    resp2 = client.search(
      index: log_index,
      body: body2,
      scroll: '1m',
      size: 3000
    )

    log_labels = []
    log_levels = []
    log_start_page_indexes = []

    loop do
      hits = resp2.dig('hits', 'hits')
      break if hits.empty?

      hits.each do |hit|
        log_labels << hit['_source']['label']
        log_levels << hit['_source']['level']
        log_start_page_indexes << hit['_source']['start_page_index']
      end

      resp2 = client.scroll(
        body: { scroll_id: resp['_scroll_id'] },
        scroll: '1m'
      )
    end


    disclaimer_info.log_labels            = log_labels
    disclaimer_info.log_levels            = log_levels
    disclaimer_info.log_start_page_indexes = log_start_page_indexes

    resp = nil
    resp2 = nil

    disclaimer_info
  end

  def add_bookmarks_pdftk_system(to_pdf_dir, id, log, disclaimer_info)
    
    data_file = "#{to_pdf_dir}/data.txt"

    bookmark_str = ''

    if @context == 'digizeit'
      if check_nil_or_empty_string disclaimer_info.purl
        add_info_str(bookmark_str, 'PURL', disclaimer_info.purl)
      end

      if check_nil_or_empty_string disclaimer_info.catalogue_arr
        add_info_str(bookmark_str, 'Catalogue',
                     disclaimer_info.catalogue_arr)
      end

      add_info_str(bookmark_str, 'Work', id)
      add_info_str(bookmark_str, 'LOGID', log) if id != log
      if check_nil_or_empty_string disclaimer_info.title_arr
        add_info_str(bookmark_str, 'Title', disclaimer_info.title_arr)
      elsif disclaimer_info.log_label_arr != ''
        add_info_str(bookmark_str, 'Title', disclaimer_info.log_label_arr)
      end

      if check_nil_or_empty_string disclaimer_info.subtitle_arr
        add_info_str(bookmark_str, 'Subtitle',
                     disclaimer_info.subtitle_arr)
      end

      if check_nil_or_empty_string disclaimer_info.bycreator
        add_info_str(bookmark_str, 'Creator',
                     disclaimer_info.bycreator)
      end
      if check_nil_or_empty_string disclaimer_info.publisher
        add_info_str(bookmark_str, 'Publisher',
                     disclaimer_info.publisher)
      end
      if check_nil_or_empty_string disclaimer_info.place_publish
        add_info_str(bookmark_str, 'Place',
                     disclaimer_info.place_publish)
      end
      if check_nil_or_empty_string disclaimer_info.year_publish_string
        add_info_str(bookmark_str, 'Year',
                     disclaimer_info.year_publish_string)
      end

      if check_nil_or_empty_string disclaimer_info.dc_arr
        add_info_str(bookmark_str, 'Collection',
                     disclaimer_info.dc_arr)
      end
      if check_nil_or_empty_string disclaimer_info.genre_arr
        add_info_str(bookmark_str, 'Genre',
                     disclaimer_info.genre_arr)
      end

      # add_info_str(bookmark_str, 'Subject', disclaimer_info.subject_arr.join(' ')) if check_nil_or_empty_string disclaimer_info.subject_arr

      if check_nil_or_empty_string disclaimer_info.shelfmark_arr
        add_info_str(bookmark_str, 'Shelfmark',
                     disclaimer_info.shelfmark_arr)
      end
      if check_nil_or_empty_string disclaimer_info.rights_owner_arr
        add_info_str(bookmark_str, 'Digitized at',
                     disclaimer_info.rights_owner_arr)
      end

    else

      if check_nil_or_empty_string disclaimer_info.purl
        add_info_str(bookmark_str, 'PURL', disclaimer_info.purl)
      end
      if check_nil_or_empty_string disclaimer_info.catalogue_arr
        add_info_str(bookmark_str, 'Catalogue',
                     disclaimer_info.catalogue_arr.join(' '))
      end

      add_info_str(bookmark_str, 'Work', id)
      add_info_str(bookmark_str, 'LOGID', log) if id != log

      if check_nil_or_empty_string disclaimer_info.title_arr
        add_info_str(bookmark_str, 'Title', disclaimer_info.title_arr.join(' '))
      elsif disclaimer_info.log_label_arr != ''
        add_info_str(bookmark_str, 'Title',
                     disclaimer_info.log_label_arr.join(' '))
      end

      if check_nil_or_empty_string disclaimer_info.subtitle_arr
        add_info_str(bookmark_str, 'Subtitle',
                     disclaimer_info.subtitle_arr.join(' '))
      end

      if check_nil_or_empty_string disclaimer_info.bycreator
        add_info_str(bookmark_str, 'Creator',
                     disclaimer_info.bycreator)
      end
      if check_nil_or_empty_string disclaimer_info.publisher
        add_info_str(bookmark_str, 'Publisher',
                     disclaimer_info.publisher)
      end
      if check_nil_or_empty_string disclaimer_info.place_publish
        add_info_str(bookmark_str, 'Place',
                     disclaimer_info.place_publish)
      end
      if check_nil_or_empty_string disclaimer_info.year_publish_string
        add_info_str(bookmark_str, 'Year',
                     disclaimer_info.year_publish_string)
      end

      if check_nil_or_empty_string disclaimer_info.dc_arr
        add_info_str(bookmark_str, 'Collection',
                     disclaimer_info.dc_arr.join(' '))
      end
      if check_nil_or_empty_string disclaimer_info.genre_arr
        add_info_str(bookmark_str, 'Genre',
                     disclaimer_info.genre_arr.join(' '))
      end

      # add_info_str(bookmark_str, 'Subject', disclaimer_info.subject_arr.join(' ')) if check_nil_or_empty_string disclaimer_info.subject_arr

      if check_nil_or_empty_string disclaimer_info.shelfmark_arr
        add_info_str(bookmark_str, 'Shelfmark',
                     disclaimer_info.shelfmark_arr.join(' '))
      end
      if check_nil_or_empty_string disclaimer_info.rights_owner_arr
        add_info_str(bookmark_str, 'Digitized at',
                     disclaimer_info.rights_owner_arr.join(' '))
      end

    end

    
    unless disclaimer_info.log_labels.nil?

      if log != id 
        first = true
        start = disclaimer_info.log_start_page_indexes[0]
        start_level = disclaimer_info.log_levels[0]
        
        # unless request_logical_part
        (0..(disclaimer_info.log_labels.size - 1)).each do |index|

          if first == true
            start_level = disclaimer_info.log_levels[index]
            first = false
          end

          add_to_bookmark_str(
            bookmark_str,
            disclaimer_info.log_labels[index],
            (disclaimer_info.log_levels[index] - (start_level-2)) ,
            (disclaimer_info.log_start_page_indexes[index] - start) + 1
          )

        end
      
      elsif
       
        start = disclaimer_info.log_start_page_indexes[0]
        # unless request_logical_part
        (0..(disclaimer_info.log_labels.size - 1)).each do |index|

          add_to_bookmark_str(
            bookmark_str,
            disclaimer_info.log_labels[index],
            disclaimer_info.log_levels[index],
            (disclaimer_info.log_start_page_indexes[index] - start) + 1
          )

        end

      end
      
    end

    open(data_file, 'w') do |f|
      f.puts bookmark_str
    end

    attempts = 0
    loop do
      resp = system "pdftk #{to_pdf_dir}/tmp.pdf update_info_utf8 #{data_file} output #{to_pdf_dir}/tmp_2.pdf"
      break if resp == true

      attempts += 1
      if attempts > MAX_ATTEMPTS
        @logger.error("[img_converter] Could not add Metadata to #{to_pdf_dir}/tmp_2.pdf")
        return
      end
    end

    @logger.debug('[img_converter] Added bookmarks  (tmp.pdf + data.txt -> tmp_2.pdf)')
  end

  def add_label_and_value(label, text, pdf_obj)
    if text.instance_of?(Array)
      pdf_obj.text("<b>#{label}:</b> #{text.join '; '}", inline_format: true)
    else
      pdf_obj.text("<b>#{label}:</b> #{text}", inline_format: true)
    end
  end

  def check_nil_or_empty_string(obj)
    if obj.instance_of?(Array)
      if obj.nil? || obj.empty? || (obj.first == ' ') || (obj.first == '')
        return false
      end
    elsif obj.nil? || (obj == ' ') || (obj == '')
      return false
    end

    true
  end

  def request_catalogue(ppn)
    response   = ''
    unapi_url  = ENV['UNAPI_URI']
    unapi_path = ENV['UNAPI_PATH'] % ppn
    url        = URI(unapi_url)

    Net::HTTP.start(url.host, url.port) do |http|
      response = http.head(unapi_path)
      response
    end

    response
  end

  # @param [Object]  pdf_path
  # @param [Object]  to_pdf_dir
  # @param [Object]  id
  # @param [Object]  log
  # @param [Object]  request_logical_part
  # @param [Object]  disclaimer_info
  # @return [Object]
  def add_disclaimer_pdftk_system(pdf_path, to_pdf_dir, id, log, _record_identifier, request_logical_part, disclaimer_info, product)

    begin
      output = "#{to_pdf_dir}/disclaimer.pdf"

      # TODO: Avoid hard coded coordinates
      if @context == 'gdz'
        x = 4
        y = 650
      elsif @context == 'digizeit'
        x = 0
        y = 670
      elsif @context.downcase.start_with?('nlh')
        x = 11
        y = 650
      end

      page_size     = [595.00, 842.00]
        logo_path_svg = "#{Dir.pwd}/image/digizeit_rgb_klein.svg"
        logo_path_jpg = "#{Dir.pwd}/image/digizeit_rgb_klein.jpg"

        contact_header    = "<b>Kontakt/Contact</b>"
        contact_address   = "     <u><link href='http://www.digizeitschriften.de'>Digizeitschriften e.V.</link></u>
     SUB Göttingen
     Platz der Göttinger Sieben 1
     37073 Göttingen<br>
     <link href='mailto:info@digizeitschriften.de'>info@digizeitschriften.de</link>"

        contact_align = :left
        contact_pos    = [0, 100]
        contact_width  = 250
        contact_height = 105

        ccby_logo = "#{Dir.pwd}/image/cc-by.svg"
        tou_header_de = '<u><b>Nutzungsbedingungen</b><u>'
        tou_text_de = "Dieses Werk ist lizensiert unter einer <u><link href='https://creativecommons.org/licenses/by/4.0/'>Creative Commons Namensnennung 4.0 International Lizenz</link></u>."  
        tou_header_en = '<u><b>Terms of use</b><u>'
        tou_text_en = "This work is licensed under a <u><link href='https://creativecommons.org/licenses/by/4.0/'>Creative Commons Attribution 4.0 International License</link></u>."

        map_icon_path      = "#{Dir.pwd}/image/map-marker-alt-solid.svg"
        phone_icon_path    = "#{Dir.pwd}/image/phone-solid.svg"
        envelope_icon_path = "#{Dir.pwd}/image/envelope-regular.svg"
        bookmark_icon_path = "#{Dir.pwd}/image/bookmark-regular.svg"


      Prawn::Document.generate(output, page_size: [595, 842], page_layout: :portrait) do |pdf|
        
        #pdf.stroke_axis
        #pdf.stroke_bounds
          
        pdf.font_families.update(
          'OpenSans' => { normal: "#{ENV['FONT_PATH']}/OpenSans/OpenSans-Regular.ttf",
                          bold: "#{ENV['FONT_PATH']}/OpenSans/OpenSans-Bold.ttf" }
        )

        pdf.image logo_path_jpg, at: [400, 780], :align => :center ,  :scale => 0.1  #:width => 180
        
        pdf.bounding_box([x, y], width: 456, height: 275) do

          #pdf.stroke_axis
          #pdf.stroke_bounds
          
          pdf.default_leading 8
          pdf.font_size 9
          pdf.font 'OpenSans', style: :normal

          if !@context.downcase.start_with?('nlh')
            pdf.text "<font size='12'><b>Werk</b></font><br><br>", inline_format: true
          elsif check_nil_or_empty_string disclaimer_info.product
            add_label_and_value('Produkt', disclaimer_info.product,
                                pdf)
          end

          if check_nil_or_empty_string disclaimer_info.title_arr
            if disclaimer_info.title_arr.instance_of?(Array)
              title_arr = disclaimer_info.title_arr.map do |title|
                if title.size > 80
                  title[0..80] + '...'
                else
                  title
                end
              end
            else
              if disclaimer_info.title_arr.size > 80
                title_arr = disclaimer_info.title_arr[0..80] + '...'
              else
                title_arr = disclaimer_info.title_arr
              end
            end
            
            add_label_and_value('Titel', title_arr, pdf)

          elsif check_nil_or_empty_string disclaimer_info.log_label_arr
            if disclaimer_info.log_label_arr.instance_of?(Array)
              title_arr = disclaimer_info.log_label_arr.map do |title|
                if title.size > 80
                  title[0..80] + '...'
                else
                  title
                end
              end
            else
              if disclaimer_info.log_label_arr.size > 80
                title_arr = disclaimer_info.log_label_arr[0..80] + '...'
              else
                title_arr = disclaimer_info.log_label_arr
              end
            end

            add_label_and_value('Label', title_arr, pdf)

          end

          

          if check_nil_or_empty_string disclaimer_info.subtitle_arr
            if disclaimer_info.subtitle_arr.instance_of?(Array)
              subtitle_arr = disclaimer_info.subtitle_arr.map do |subtitle|
               if subtitle.size > 80
                subtitle[0..80] + '...'
               else
                subtitle
               end
             end
            else
              if disclaimer_info.subtitle_arr.size > 80
                subtitle_arr = disclaimer_info.subtitle_arr[0..80] + '...'
              else
                subtitle_arr = disclaimer_info.subtitle_arr
              end
            end
            add_label_and_value('Untertitel', subtitle_arr, pdf)
          end
          
          if check_nil_or_empty_string disclaimer_info.bycreator
            add_label_and_value('Autor', disclaimer_info.bycreator,
                                pdf)
          end
          if check_nil_or_empty_string disclaimer_info.publisher
            add_label_and_value('Verlag', disclaimer_info.publisher,
                                pdf)
          end
          if check_nil_or_empty_string disclaimer_info.place_publish
            add_label_and_value('Ort', disclaimer_info.place_publish,
                                pdf)
          end

          if !@context.downcase.start_with?('nlh')
            if check_nil_or_empty_string disclaimer_info.year_publish_string
              add_label_and_value('Jahr', disclaimer_info.year_publish_string,
                                  pdf)
            end
          elsif !disclaimer_info.date.nil?
            if check_nil_or_empty_string disclaimer_info.date
              add_label_and_value('Ausgabe', disclaimer_info.date, pdf)
            end
          elsif check_nil_or_empty_string disclaimer_info.year_publish_string
            add_label_and_value('Jahr', disclaimer_info.year_publish_string,
                                pdf)
          end


          if check_nil_or_empty_string disclaimer_info.genre_arr
            add_label_and_value('Gattung', disclaimer_info.genre_arr,
                                pdf)
          end

          if check_nil_or_empty_string disclaimer_info.shelfmark_arr
            add_label_and_value('Signatur', disclaimer_info.shelfmark_arr,
                                pdf)
          end

          # TODO: Avoid hard coded URLs
          if  check_nil_or_empty_string disclaimer_info.purl 
            add_label_and_value('PURL', disclaimer_info.purl, pdf)
          else
            add_label_and_value('ID', "#{id}|#{log}", pdf)
          end
          # end

          if (id.start_with? 'PPN') && (@context == 'gdz')
            ppn = id.match(/PPN(\S*)/)[1]

            if request_catalogue(ppn).code.to_i < 400
              add_label_and_value('OPAC', "http://opac.sub.uni-goettingen.de/DB=1/PPN?PPN=#{ppn}", pdf)
            end
          end
        end

        

        pdf.font 'OpenSans', :style => :normal

        # pdf.bounding_box([0, 375], :width => 600, :height => 100) do
        #   #pdf.stroke_axis
        #   #pdf.stroke_bounds
          
        #   pdf.text tou_header_de, :inline_format => true, :size => 12
        #   pdf.move_down 11
        #   pdf.svg IO.read(ccby_logo) #, at: [0, 25]
        #   pdf.move_down 15
        #   pdf.text tou_text_de, :inline_format => true, :size => 10 #, at: [0, 0]
        # end
      
        # pdf.bounding_box([0, 255], :width => 600, :height => 100) do
        #   #pdf.stroke_axis
        #   #pdf.stroke_bounds
          
        #   pdf.text tou_header_en, :inline_format => true, :size => 12
        #   pdf.move_down 11
        #   pdf.svg IO.read(ccby_logo) # , at: [0, 0]
        #   pdf.move_down 15
        #   pdf.text tou_text_en, :inline_format => true, :size => 10
        # end

        pdf.bounding_box(contact_pos, :width => contact_width, :height => contact_height) do
          #pdf.stroke_axis
          #pdf.stroke_bounds
          
          pdf.text contact_header, :inline_format => true, :size => 11, :align => contact_align
      
          pdf.move_down 10
          pdf.text contact_address, :inline_format => true, :size => 9, :align => contact_align, leading: 1
      
          #pdf.svg IO.read(map_icon_path), at: [-15, 54], :width => 7
          #pdf.svg IO.read(phone_icon_path), at: [-15, 39], :width => 8
          pdf.svg IO.read(envelope_icon_path), at: [-15, 22], :width => 8
          #pdf.svg IO.read(bookmark_icon_path), at: [-15, 9], :width => 7
         
      
        end



      end
      
      
      system "pdftk #{output} #{to_pdf_dir}/tmp_2.pdf  cat output #{pdf_path}"
      
      FileUtils.rm("#{to_pdf_dir}/tmp_2.pdf")
      FileUtils.rm("#{to_pdf_dir}/tmp.pdf")

    rescue Exception => e
      @logger.error("[img_converter] Problem with disclaimer creation \t#{e.message}")
      @logger.error("[img_converter] Problem with disclaimer creation \t#{e.backtrace}")

      if request_logical_part
        if @context == 'gdz'
          system "pdftk templates/gdz_disclaimer.pdf #{to_pdf_dir}/tmp.pdf  cat output #{pdf_path}"
        elsif @context.downcase.start_with?('nlh')
          system "pdftk templates/#{product}_disclaimer.pdf #{to_pdf_dir}/tmp.pdf  cat output #{pdf_path}"
        elsif @context == 'digizeit'
          system "pdftk templates/digizeit_disclaimer.pdf #{to_pdf_dir}/tmp.pdf  cat output #{pdf_path}"
        end

        FileUtils.rm("#{to_pdf_dir}/tmp.pdf")
      else

        # TODO: check this
        if @context == 'gdz'
          system "pdftk templates/gdz_disclaimer.pdf #{to_pdf_dir}/tmp_2.pdf  cat output #{pdf_path}"
        elsif @context.downcase.start_with?('nlh')
          system "pdftk templates/#{product}_disclaimer.pdf #{to_pdf_dir}/tmp_2.pdf  cat output #{pdf_path}"
        elsif @context == 'digizeit'
          system "pdftk templates/digizeit_disclaimer.pdf #{to_pdf_dir}/tmp_2.pdf  cat output #{pdf_path}"
        end

        FileUtils.rm("#{to_pdf_dir}/tmp_2.pdf")
      end
    end

    @logger.debug("[img_converter] Added disclaimer to #{pdf_path}")
  end

  # TODO: Avoid hard coded paths
  def createPdfDisclaimerGDZ(x, y, xx, yy, s, ss, path)
    Prawn::Document.generate(path, page_size: [595, 842], page_layout: :portrait) do |pdf|
      pdf.font_families.update(
        'OpenSans' => { normal: '../font/OpenSans/OpenSans-Regular.ttf',
                        bold: '../font/OpenSans/OpenSans-Bold.ttf' }
      )
      pdf.image "#{Dir.pwd}/image/nlh_logo_2.png", at: [x, y], scale: s

      pdf.image "#{Dir.pwd}/image/nlh_products_footer", at: [xx, yy], scale: ss
    end
  end

  # TODO: Avoid hard coded paths
  def createPdfDisclaimerDigizeit(x, y, xx, yy, s, ss, path)
    Prawn::Document.generate(path, page_size: [595, 842], page_layout: :portrait) do |pdf|
      pdf.font_families.update(
        'OpenSans' => { normal: '../font/OpenSans/OpenSans-Regular.ttf',
                        bold: '../font/OpenSans/OpenSans-Bold.ttf' }
      )
      pdf.image "#{Dir.pwd}/image/nlh_logo_2.png", at: [x, y], scale: s

      pdf.image "#{Dir.pwd}/image/nlh_products_footer", at: [xx, yy], vposition: :center, scale: ss
    end
  end

  # from solr, for Digizeitschriften/ES no cut
  def cut_from_full_pdf_pdftk_system(pdf_path, to_pdf_dir, id, _log, log_start_page_index, log_end_page_index)
    if @context == 'gdz'
      solr_resp = (@solr.get 'select', params: { q: "id:#{id}", fl: 'phys_order' })['response']['docs'].first
    elsif @context.downcase.start_with?('nlh')
      solr_resp = (@solr.get 'select', params: { q: "work:#{id}", fl: 'phys_order' })['response']['docs'].first
    end

    first_page = solr_resp['phys_order'][log_start_page_index].to_i + 1
    last_page  = solr_resp['phys_order'][log_end_page_index].to_i + 1

    system "pdftk #{pdf_path} cat #{first_page}-#{last_page} output #{to_pdf_dir}/tmp.pdf"

    @logger.debug("[img_converter] Intermediate PDF #{to_pdf_dir}/tmp.pdf created")
  end

  # from solr
  def merge_to_full_pdf_pdftk_system_via_solr(to_pdf_dir, id, log, request_logical_part, product, last_page)
    if @context == 'gdz'
      solr_resp = (@solr.get 'select',
                             params: { q: "id:#{id}",
                                       fl: 'page log_id log_start_page_index log_end_page_index' })['response']['docs'].first
    elsif @context.downcase.start_with?('nlh')
      solr_resp = (@solr.get 'select',
                             params: { q: "work:#{id}",
                                       fl: 'page log_id log_start_page_index log_end_page_index' })['response']['docs'].first
    end

    log_start_page_index = 0
    log_end_page_index   = -1

    if request_logical_part

      log_id_index = solr_resp['log_id'].index log

      log_start_page_index = (solr_resp['log_start_page_index'][log_id_index]) - 1
      log_end_page_index   = (solr_resp['log_end_page_index'][log_id_index]) - 1

    end

    solr_page_path_arr = (solr_resp['page'][log_start_page_index..log_end_page_index]).collect do |el|
      "#{to_pdf_dir}/#{el}.pdf"
    end

    system "pdftk #{solr_page_path_arr.join ' '} cat output #{to_pdf_dir}/tmp.pdf"

    @logger.debug("[img_converter] Temporary Full PDF #{to_pdf_dir}/tmp.pdf created")
  end

  # from elasticsearch (ES)
  def merge_to_full_pdf_pdftk_system_via_elasticsearch(to_pdf_dir, id, log, log_id, product, last_page)
    host = 'search'

    index = 'meta.dz_phys'

    client = Elasticsearch::Client.new url: "http://#{host}:9200", log: true
    client.transport.reload_connections!
    client.cluster.health

    body = if id == log
             {
               query: {
                 "bool": {
                   "must": [
                     {
                       "match": { "work": id }
                     }
                   ]
                 }
               },
               "sort": [
                 {
                   "order": {
                     "order": 'asc'
                   }
                 }
               ],
               "_source": %w[
                 page order
               ]
             }
           else
             {
               query: {
                 "bool": {
                   "should": [
                     { "match": { "log_id.keyword": id + '|' + log } },
                     { "match": { "structrun.parent_id.keyword": id + '|' + log } },
                     { "match": { "page_key.keyword": "#{product}:#{id}:#{last_page}"} }
                   ]
                 }
               },
               "sort": [
                 {
                   "order": {
                     "order": 'asc'
                   }
                 }
               ],
               "_source": %w[
                 page order
               ]
             }
           end

    resp = client.search(
      index: index,
      body: body,
      scroll: '1m',
      size: 3000
    )

    # "id", "order", "page", "format", "start_page_index", "end_page_index"]
    total = resp['hits']['total']
    if total == 0
      removeQueue(log_id)
      @logger.error("[img_converter] Couldn't find #{id}|#{log} in dz_phys index, conversion not possible\n#{body}")
      return
    end

    pdf_path_arr = []
    loop do
      hits = resp.dig('hits', 'hits')
      break if hits.empty?

      hits.each do |hit|
        pdf_path_arr << "#{to_pdf_dir}/#{hit['_source']['page']}.pdf"
      end

      resp = client.scroll(
        body: { scroll_id: resp['_scroll_id'] },
        scroll: '1m'
      )
    end

    system "pdftk #{pdf_path_arr.join ' '} cat output #{to_pdf_dir}/tmp.pdf"

    @logger.debug("[img_converter] Temporary Full PDF #{to_pdf_dir}/tmp.pdf created")
  end

  def get_image_depth_and_resolution(path)
    json = MiniMagick::Tool::Convert.new do |convert|
      convert << path
      convert << 'json:'
    end

    json.gsub!('\\"', "'")

    json.gsub!("\r\n", ' ')
    json.gsub!("\r", ' ')
    json.gsub!("\n", ' ')
    j = json.encode!('UTF-8', invalid: :replace, undef: :replace, replace: '')

    begin
      image = JSON.parse(j)['image']

      if !image.nil?
        [image['depth'], image['resolution']]
      else
        [nil, {}]
      end
    rescue Exception => e
      @logger.error("[img_converter] Problem with image meta data for path #{path} \t#{e.message}")
      @logger.error("[img_converter] Problem with image meta data for path \t#{e.backtrace}")

      [nil, {}]
    end
  end

  def convert(to_tmp_img, to_tmp_jpg, to_page_pdf_path)
    depth, resolution_hsh = get_image_depth_and_resolution(to_tmp_img)

    succeed = false

    if !resolution_hsh.nil? && !resolution_hsh.empty? && (resolution_hsh['x'].to_i > 72) && !depth.to_i.nil? && (depth.to_i > 1)
      begin
        Vips::Image.tiffload(to_tmp_img).jpegsave(to_tmp_jpg)
        succeed = true
      rescue Exception => e
        # nothing
      end
    end

    MiniMagick::Tool::Convert.new do |convert|
      convert << '-define' << 'pdf:use-cropbox=true'

      convert << if succeed
                   to_tmp_jpg.to_s
                 else
                   to_tmp_img.to_s
                 end
      convert << to_page_pdf_path.to_s
    end

    if succeed
      FileUtils.rm(to_tmp_jpg)
      FileUtils.rm(to_tmp_img)
    else
      FileUtils.rm(to_tmp_img)
    end

    # upload pdf to s3
  rescue Exception => e
    if @context == 'gdz'
      FileUtils.cp('templates/gdz_conversion_error_2.pdf', to_page_pdf_path)
    elsif @context.downcase.start_with?('nlh')
      FileUtils.cp('templates/nlh_conversion_error_2.pdf', to_page_pdf_path)
    elsif @context == 'digizeit'
      FileUtils.cp('templates/digizeit_conversion_error_2.pdf', to_page_pdf_path)
    end

    @logger.error("[img_converter] [GDZ-677] Could not convert '#{to_tmp_img}' to: '#{to_page_pdf_path}'")
    @logger.debug("[img_converter] [GDZ-677] Could not convert '#{to_tmp_img}' to: '#{to_page_pdf_path}' \t#{e.backtrace}")
  end

  def all_images_converted?(queue, pages_count)
    keys = @rredis.hkeys(queue)

    keys.size >= pages_count
  end
end
