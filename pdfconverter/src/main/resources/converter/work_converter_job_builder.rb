# frozen_string_literal: true

require 'vertx/vertx'

require 'logger'
require 'gelf'
require 'nokogiri'
require 'redis'
require 'json'
require 'fileutils'
require 'mini_magick'
require 'open-uri'
require 'converter/work_converter'

# @logger       = GELF::Logger.new(ENV['GRAYLOG_URI'], ENV['GRAYLOG_PORT'].to_i, "WAN", {:facility => ENV['GRAYLOG_FACILITY']})
# @logger.level = ENV['DEBUG_MODE'].to_i

@logger       = Logger.new(STDOUT)
@logger.level = ENV['DEBUG_MODE'].to_i

@queue        = ENV['REDIS_WORK_CONVERT_QUEUE']
@unique_queue = ENV['REDIS_UNIQUE_QUEUE']

@rredis = Redis.new(
  host: ENV['REDIS_HOST'],
  port: ENV['REDIS_PORT'].to_i,
  db: ENV['REDIS_DB'].to_i
)

$vertx.execute_blocking(lambda { |_future|
  GC.start
  @logger.debug "[work_converter_job_builder] Start verticle"
  begin
    loop do
      res = @rredis.brpop(@queue) # , :timeout => nil)
      converter = WorkConverter.new
      converter.process_response(res)
    end
  rescue Exception => e
    @logger.error "[work_converter_job_builder] Could not start processing in WorkConverter \t#{e.message}\t#{e.backtrace}"
    sleep(5)
    retry
  end

  # future.complete(@doc.to_s)
}) do |res_err, res|
end
