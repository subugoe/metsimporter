# frozen_string_literal: true

require 'vertx/vertx'

require 'rubygems'
require 'logger'
require 'gelf'
#require 'converter/work_converter_job_builder'


@logger       = Logger.new(STDOUT)
@logger.level = ENV['DEBUG_MODE'].to_i

# @logger = GELF::Logger.new(ENV['GRAYLOG_URI'], ENV['GRAYLOG_PORT'].to_i, 'WAN',
#                           { facility: ENV['GRAYLOG_FACILITY'] })
# @logger.level = ENV['DEBUG_MODE'].to_i

@logger.debug "[start_converter] Running in #{Java::JavaLang::Thread.current_thread.get_name}"


# start image converter instances
#
#
img_job_builder_options = {
  'instances' => 8,
  'worker' => true,
  'blockedThreadCheckInterval' => 3_600_000,
  'warningExceptionTime' => 3_600_000,
  'maxWorkerExecuteTime' => 3_400_000_000_000,
  'maxEventLoopExecuteTime' => 600_000_000_000,
  'GEM_PATH' => '/opt/jruby/lib/ruby/gems/shared/gems'
}
$vertx.deploy_verticle('converter/img_to_pdf_converter_job_builder.rb', img_job_builder_options)


# start work converter instance
#
#
work_job_builder_options = {
  'instances' => 1,
  'worker' => true,
  'blockedThreadCheckInterval' => 3_600_000,
  'warningExceptionTime' => 3_600_000,
  'maxWorkerExecuteTime' => 3_400_000_000_000,
  'maxEventLoopExecuteTime' => 600_000_000_000,
  'GEM_PATH' => '/opt/jruby/lib/ruby/gems/shared/gems'
}

$vertx.deploy_verticle('converter/work_converter_job_builder.rb', work_job_builder_options)
