require 'helper/mappings'

class LogicalElement
  attr_accessor :doctype, :id, :dmdid, :admid, :order, :start_page_index, :end_page_index, :part_product, :part_work,
                :part_key, :level, :parentdoc_work, :parentdoc_label, :parentdoc_type, :dmdsec_meta, :isLog_part, :label, :type

  def initialize
    @isLog_part = false
  end
end
