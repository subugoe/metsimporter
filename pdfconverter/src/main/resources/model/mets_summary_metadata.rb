class MetsSummaryMetadata
  attr_accessor :summary

  def initialize
    @summary = []
  end

  def addSummary=(summary)
    @summary += summary
  end

  def to_solr_string
    h = {}
    h.merge!({ summary_content: @summary })

    h
  end
end
