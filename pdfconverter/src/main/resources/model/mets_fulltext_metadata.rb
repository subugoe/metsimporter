class MetsFulltextMetadata
  attr_accessor :fulltexts,
                :fulltext_uris,
                :work

  def initialize
    @fulltext_uris = []
    @fulltexts     = []
  end

  def addFulltextUri=(fulltextUri)
    @fulltext_uris = fulltextUri
  end

  def addFulltext=(fulltext)
    @fulltexts = fulltext
  end

  def fulltext_to_solr_string
    docs = []

    unless @fulltexts.empty?
      @fulltexts.each do |ft|
        h = {}

        h.merge!({ id: "#{ft.fulltext_of_work}_page_#{ft.fulltext_page_number}" })
        h.merge!({ ft: ft.fulltext })
        h.merge!({ ft_ref: ft.fulltext_ref })
        h.merge!({ ft_of_work: ft.fulltext_of_work })
        h.merge!({ ft_page_number: ft.fulltext_page_number })
        h.merge!({ doctype: 'fulltext' })

        h.merge!({ work_id: @work })

        # merge_title_info(h)

        docs << h
      end
    end

    docs
  end

  def to_solr_string
    h = {}

    unless @fulltexts.empty?

      fulltext_arr     = []
      fulltext_ref_arr = []

      @fulltexts.each do |ft|
        fulltext_arr << ft.fulltext
        fulltext_ref_arr << ft.fulltext_ref
      end

      h.merge!({ fulltext: fulltext_arr })
      h.merge!({ fulltext_ref: fulltext_ref_arr })

    end

    h
  end
end
