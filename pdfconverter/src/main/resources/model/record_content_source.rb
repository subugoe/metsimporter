class RecordContentSource
  attr_accessor :authority, :recordContentSource

  def initialize(recordContentSource, _recordCreationDate, _recordChangeDate, _recordIdentifier, _recordOrigin)
    @recordContentSource = recordContentSource
  end
end
