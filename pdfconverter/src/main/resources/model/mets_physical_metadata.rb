class MetsPhysicalMetadata
  attr_accessor :physicalElements

  def initialize
    @physicalElements = {}
  end

  def addToPhysicalElement(physicalElement)
    @physicalElements[physicalElement.id] = physicalElement
  end

  def to_solr_string
    h = {}

    unless @physicalElements.empty?

      order      = []
      orderlabel = []
      contentids = []
      contentids_changed_at = []

      @physicalElements.values.each do |el|
        order << el.order.to_i
        orderlabel << el.orderlabel
        contentids << el.contentid
        contentids_changed_at << el.contentid_changed_at
      end

      h.merge!({ phys_order: order })
      h.merge!({ phys_orderlabel: orderlabel })
      h.merge!({ phys_content_id: contentids })
      h.merge!({ phys_content_id_changed_at: contentids_changed_at })
    end

    h
  end
end
