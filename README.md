# metsimporter
METS import, parsing and indexing.

# Subprojects

## Collector
The Collector Service retrievs METS-Keys from S3, it creates job descriptions and pushes these in a redis queue 'indexer'

## Indexer
The Indexer Service consumes the jobs from the redis queue, parses the METS and pushes index documents to Solr or ElasticSearch.

# Required config
* Copy file cfg/orig_app.env to cfg/app.env and modify the new file
    * fill out
        * S3 section

* To start the Collector and Indexer inside an IDE, instead of as part of the docker environment
    * remove 'collector' and 'indexer' from the 'docker-compose up' line in cfg/build.sh
    * change \<adr\> in the created file cfg/app.env to the host-ip where ES and Redis are running on (e.g. localhost), see:  
        * ELASTICSEARCH_LOCAL_HOST=http://\<adr\>:9200/
        * REDIS_LOCAL_ADR=\<adr\>:8442
    * Run Collector: open collector/api.go in your IDE and execute the IDE-Go-Run function
    * Run Indexer: open indexer/api.go in your IDE and execute the IDE-Go-Run function

# Special config
* Test run
    * is defined by variable TEST_RUN (if set to true)
    * the check if images exists (are accessible) is disabled in test run 
    * the processing of records will be rejected outside the test run, if images are missed
    * TEST_RUN_LIMIT specifies the number of records to process"


# Quick start

The `digizeit-aws` branch has been upgraded to use service composition and optimized for image size.

## Required Configurations

* local or VM Environment
  * Update variable _CONTEXT_ in config file _project/docker-env_ or export an environment variable with that name.
  * Set context specific variables (CONTEXT={digizeit|nlh|gdz}) via _project/cfg/$CONTEXT/app.env_ config file or export corresponding environment variable.
* GitLab CI or Kubernetes
  * You have to export environment variables as described before. See in the files to identify required variables.
* Required environment variables in context _digizeit_:
      
      # commons (values {digizeit|nlh|gdz})
      CONTEXT

      # storage 
      STORAGE_PROVIDER
      STORAGE_KEY
      STORAGE_SECRET
      STORAGE_SESSION_TOKEN
      STORAGE_REGION
      STORAGE_ENDPOINT
      STORAGE_IS_LOCAL


      # index
      ELASTICSEARCH_HOST
      

### via Docker CLI

* Env-files must be updated or environment variables must be set.

  $> COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose -f docker-compose.base.yaml -f docker-compose.local.yaml build

## Starting using `docker-compose`

** This section is still WIP, not everything works yet!**

### Files

The following `docker-compose` files are provided:

| File name                      | Purpose                                                     |
|--------------------------------|-------------------------------------------------------------|
| docker-compose.base.yaml       | Base file including references to Environment               | 
| docker-compose.local.yaml      | `docker-compose` specialisation for running the app locally |
| docker-compose.kibana.yaml     | Example for an additional developer service - Kibana        |
| docker-compose.validator.yaml  | Validation services                                         |

The environment is included in the base file (`docker-compose.base.yaml`) as `docker-env`, you can either copy the existing `docker-env.dist` or create a symlink (`ln -s docker-env.dist docker-env`). Since `docker-env` is blacklisted in `.gitignore` you can't accidentally commit it.

### Usage

`docker-compose` can't only compose services from one file but also from multiple given files. The first file reference can be considered as base and the following as extensions, so the order matters here. Each service can be referenced in every file, note that extending is one way only, you can't 'delete' a setting later on, only overwriting is possible.

```
docker-compose -f docker-compose.base.yaml -f docker-compose.local.yaml build
docker-compose -f docker-compose.base.yaml -f docker-compose.local.yaml up -d
docker-compose -f docker-compose.base.yaml -f docker-compose.local.yaml down -v
```

If you want to `exec` a container, you should make sure to pass all required `docker-compose` files to make sure that the service is found.

#### Examples

##### Starting the app locally

This example starts the app locally with all required services.

```
docker-compose -f docker-compose.base.yaml -f docker-compose.local.yaml up -d
```

##### Using Kibana with a local setup

This example starts the app with PHPMyAdmin attached.

```
docker-compose -f docker-compose.base.yaml -f docker-compose.local.yaml -f docker-compose.kibana.yaml up -d
```

##### Executing a shell in a container

This example executes a shell in the `indexer` container.

```
docker-compose -f docker-compose.base.yaml -f docker-compose.local.yaml indexer app /bin/sh
```

##### Start indexing job manually 

To start the indexer manually (for test purposes) you can push a job directly into the indexing queue:

```
$> docker-compose -f docker-compose.base.yaml -f docker-compose.local.yaml exec redis redis-cli -n 9 rpush  indexer "{\"basedir\":\"\",\"fulltextdir\":\"\",\"document\":\"514822910_0017\",\"context\":\"digizeit\",\"inkey\":\"mets/514822910_0017.xml\",\"product\":\"dzeit\",\"ftype\":\"TEI_2a\",\"reindex\":false,\"starttime\":\"2021-04-28T08:47:28.52478+02:00\"}"
```

##### Number of jobs in the queue

Request zu number of jobs in the indexing queue:

```
$> docker-compose -f docker-compose.base.yaml -f docker-compose.local.yaml exec redis redis-cli -n 9 llen indexer 
```

# Development

This is currently WIP

## Building Debug images

```
docker build --build-arg DEBUG=true --target builder collector
docker build --build-arg DEBUG=true --target builder indexer
docker build --build-arg DEBUG=true --target builder image_resolver
```

## Building a prepopulated S3 container

This builds a pre packaged S3 container, all data in `./data` is included if packaged as bzip2ed `tar` files.

```
docker build -t docker.gitlab.gwdg.de/subugoe/metsimporter/s3-test:master -f docker/s3/Dockerfile .
```

## Building a prepopulated `search` container

Make sure you have a dump of the Elastic search index directory named `index.tar` in the root directory before running `docker build`.
You can create one by starting the local `docker-compose` variant:
```
docker-compose -p indexer -f docker-compose.base.yaml -f docker-compose.local.yaml up -d
```
Then wait for the indexer to complete and extract the index directoty like this:
```
'docker-compose -p indexer -f docker-compose.base.yaml -f docker-compose.local.yaml exec search tar Ccf /usr/share/elasticsearch/data - /usr/share/elasticsearch/data > index.tar'
```

Now you should be ready to run:
```
docker build -f ./docker/search/Dockerfile.prepopulated  .
```

## Using IntelliJ / GoLand

See the [documentation](https://www.jetbrains.com/help/go/attach-to-running-go-processes-with-debugger.html) and [a blog post about debugging Go in Docker](https://blog.jetbrains.com/go/2020/05/06/debugging-a-go-application-inside-a-docker-container/).

## Accessing Minio (S3)

If started with `-f docker-compose.local.yaml` Minio provides a [web interface](http://localhost:18080/minio/indexer/), use `storage-key` as key and `storage-secret` as secret or ehat ever you have set in `docker-compose.local.yaml` to log in.

## Accesing Kibana (Elasticsearch frontend)

If started with `-f docker-compose.kibana.yaml` Kibana  provides a [web interface](http://localhost:5601/).

### Kibana queries examples
#### Request Works:
```json
GET /dz_log/_search/
{
  "size": 3,
  "from": 0, 
  "query": {
        "exists": {
            "field": "record_identifier"
        }
    },
  "_source": ["id", "log_id", "work", "record_identifier", "collection"]
}
```

#### Request works with fulltext:
```json
GET /dz_phys/_search/
{
  "size": 3,
  "from": 0, 
  "query": {
    "bool": {
      "must": [
        {"match": {"FulltextExist":"true"}}
      ]
    }
  },
  "_source": ["id", "log_id", "work", "record_identifier", "collection", "fulltext"]
}
```

#### Search ALL LOG:
```json
GET /dz_log/_search/
{
  "size": 20,
  "from": 0, 
  "query": {
    "match_all": {}
  },
  "_source": ["id", "log_id", "work", "record_identifier", "collection"]
}
```

#### Search ALL PHYS:
```json
GET /dz_phys/_search/
{ 
  "size": 20,
  "from": 0, 
  "query": {
    "match_all": {}
  },
  "_source": ["id", "log_id", "work", "record_identifier", "collection"]
}
```

## Accesing the queue (Redis)

#### Push a job to redis indexer queue 'indexer' at DB=9 (e.g. PPN345617002_0012):
```
docker-compose -f docker-compose.base.yaml -f docker-compose.local.yaml exec redis redis-cli -n 9  lpush indexer "{\"document\":\"345617002_0012\", \"inkey\":\"mets/345617002_0012.xml\", \"product\":\"dzeit\", \"context\":\"digizeit\", \"reindex\":false, \"ftype\":\"TEI_2a\"}")
```
