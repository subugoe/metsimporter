package disclaimer

import (
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/query"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/types"
)

var ()

func init() {

}

func CreatePDFDisclaimerWorker(createDisclaimerJobChan <-chan *types.CreateDisclaimerJob) {

	for createDisclaimerJob := range createDisclaimerJobChan {

		query.CreateDisclaimer(createDisclaimerJob.WorkConvertJob)

		createDisclaimerJob.WG.Done()
	}
}
