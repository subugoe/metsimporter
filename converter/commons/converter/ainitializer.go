package converter

import (
	"github.com/sirupsen/logrus"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/util"
)

var (
	config util.Config
	log    logrus.Logger
)

func init() {
	config, log = util.LoadConfig()
}
