package converter

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/go-redis/redis"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/bookmark"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/query"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/types"

	sub "gitlab.gwdg.de/subugoe/shared-product-configs/sub"
)

var (
	concurrency int
	redisClient *redis.Client
)

func init() {
	redisClient = getRedisClient()
}

func RequestProcessingWorker(
	workConvertJobChan <-chan *types.WorkConvertJob,
	pageConvertJobChan chan<- *types.PageConvertJob,
	createDisclaimerJobChan chan<- *types.CreateDisclaimerJob,
	mergeJobChan chan<- *types.MergeJob,
	uploadJobChan chan<- *types.UploadJob) {

	for {

		// retrieve Job from redis

		var err error
		var result string
		var resultArr []string
		var wg sync.WaitGroup

		resultArr, err = redisClient.BRPop(3600*time.Second, config.RedisWorkConvertQueue).Result()
		if err == redis.Nil {
			continue
		} else if err != nil {
			log.Errorf("indexer could not request redis, due to %s", err.Error())
		}

		result = resultArr[1]

		workconvertjob := &types.WorkConvertJob{}
		json.Unmarshal([]byte(result), &workconvertjob)

		start := time.Now()
		log.Printf("start processing of %s: %s___%s", workconvertjob.Context, workconvertjob.Document, workconvertjob.Log)

		imageConfig, err := sub.GetProductCfg(workconvertjob.Product, "img")
		if err != nil {
			log.Fatalf("could not read image config for %s, due to %s, exiting", workconvertjob.Product, err)
		}

		var pdfOutPath string
		var pdfPreOutPath string
		var pdfS3Key string
		isLog := workconvertjob.Document != workconvertjob.Log
		workLog := fmt.Sprintf("%s___%s", workconvertjob.Document, workconvertjob.Log)
		workDir := fmt.Sprintf("tmp/%s", workLog)

		if isLog {
			pdfOutPath = fmt.Sprintf("%s/%s.pdf", workDir, workconvertjob.Log)
			pdfPreOutPath = fmt.Sprintf("%s/%s.pdf", workDir, workLog)
			pdfS3Key = fmt.Sprintf(imageConfig.PdfOutKey, workconvertjob.Document, workconvertjob.Log)
		} else {
			pdfOutPath = fmt.Sprintf("%s/%s.pdf", workDir, workconvertjob.Document)
			pdfPreOutPath = fmt.Sprintf("%s/%s.pdf", workDir, workLog)
			pdfS3Key = fmt.Sprintf(imageConfig.PdfOutKey, workconvertjob.Document, workconvertjob.Document)
		}

		pdfDisclaimerOutPath := fmt.Sprintf("%s/%s", workDir, "disclaimer.pdf")
		pdfMetadataOutPath := fmt.Sprintf("%s/%s", workDir, "data.txt")

		workconvertjob.IsLog = isLog
		workconvertjob.WorkLog = workLog
		workconvertjob.WorkDir = workDir
		workconvertjob.PdfOutPath = pdfOutPath
		workconvertjob.PdfPreOutPath = pdfPreOutPath
		workconvertjob.PdfDisclaimerOutPath = pdfDisclaimerOutPath
		workconvertjob.PdfMetadataOutPath = pdfMetadataOutPath

		err = os.MkdirAll(workDir, 0777)
		if err != nil {
			log.Errorf("failed to create directory %s, due to %s\n", workDir, err.Error())
			continue
		}

		// create Disclaimer (async)
		//
		createDisclaimerJob := &types.CreateDisclaimerJob{}
		createDisclaimerJob.WorkConvertJob = workconvertjob
		createDisclaimerJob.WG = &wg
		wg.Add(1)
		createDisclaimerJobChan <- createDisclaimerJob

		pageInfos := query.QueryPageIDs(workconvertjob)

		// create page PDFs (async)
		for _, pageInfo := range pageInfos {

			pageConvertJob := &types.PageConvertJob{}
			pageConvertJob.WorkConvertJob = workconvertjob

			// use pattern from configuration file
			pageConvertJob.ImageName = pageInfo.PageKey
			pageConvertJob.ImageS3Key = fmt.Sprintf(imageConfig.OutKey, workconvertjob.Document, pageInfo.PageKey, pageInfo.Format)
			pageConvertJob.ImagePath = fmt.Sprintf("%s/%s.%s", workDir, pageInfo.PageKey, pageInfo.Format)
			pageConvertJob.PdfPath = fmt.Sprintf("%s/%s.%s", workDir, pageInfo.PageKey, "pdf")
			pageConvertJob.PdfS3Key = fmt.Sprintf(imageConfig.PdfOutKey, workconvertjob.Document, pageInfo.PageKey)
			pageConvertJob.Format = pageInfo.Format

			pageConvertJob.WG = &wg
			wg.Add(1)
			pageConvertJobChan <- pageConvertJob
		}

		// create bookmarks and references (async)
		createBookmarkJob := &types.CreateBookmarkJob{}
		createBookmarkJob.WorkConvertJob = workconvertjob
		bookmarks := bookmark.CreatePDFBookmark(createBookmarkJob)

		wg.Wait()

		// merge Disclaimer + page PDFs
		mergeJob := &types.MergeJob{}
		mergeJob.WorkConvertJob = workconvertjob
		mergeJob.Bookmark = bookmarks
		mergeJob.PageInfos = pageInfos
		mergeJob.WG = &wg
		wg.Add(1)
		mergeJobChan <- mergeJob

		wg.Wait()

		// upload
		uploadJob := &types.UploadJob{
			WorkConvertJob: workconvertjob,
			BaseJob: types.BaseJob{
				WG: &wg,
			},
			PdfS3Key:   pdfS3Key,
			PdfOutPath: pdfOutPath,
		}

		wg.Add(1)
		uploadJobChan <- uploadJob

		wg.Wait()

		// delete directory
		err = os.RemoveAll(workDir)
		if err != nil {
			log.Errorf("failed to delete directory %s, due to %s\n", workDir, err.Error())
			continue
		}

		deletefromRedisHQueue(config.RedisUniqueQueue, workLog)

		elapsed := time.Since(start)
		log.Printf("completed processing of %s: %s___%s (took: %v)", workconvertjob.Context, workconvertjob.Document, workconvertjob.Log, elapsed)
	}

}

func deletefromRedisHQueue(queue string, key string) bool {

	_, err := redisClient.HDel(queue, key).Result()
	if err != nil {
		return false
	} else {
		return true
	}
}

func getRedisClient() *redis.Client {

	return redis.NewClient(&redis.Options{
		Addr:         config.RedisAdr,
		DB:           config.RedisDB,
		MaxRetries:   config.RedisMaxRetries,
		MinIdleConns: config.RedisMinIDConns,
		DialTimeout:  time.Minute,
		ReadTimeout:  time.Minute,
	})
}
