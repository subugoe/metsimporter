package converter

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/pdfcpu/pdfcpu/pkg/api"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu"
	"github.com/signintech/gopdf"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/types"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/helper"
)

var (
	s3Client *s3.S3
)

func init() {
	creds := credentials.NewStaticCredentials(config.StorageKey, config.StorageSecret, "")
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(config.StorageRegion),
		Credentials: creds,
		Endpoint:    aws.String(config.StorageEndpoint),
		// true results for nlh-usc in problems
		S3ForcePathStyle: aws.Bool(true),
		//S3ForcePathStyle: aws.Bool(false),
		MaxRetries: aws.Int(3),

		HTTPClient: &http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   time.Duration(config.HttpTransportTimeout) * time.Second,
					KeepAlive: time.Duration(config.HttpTransportKeepAlive) * time.Second,
				}).DialContext,
				TLSHandshakeTimeout:   time.Duration(config.HttpTransportTslHandshakteTimeout) * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
				MaxIdleConns:          config.HttpTransportMaxIdleConns,
				MaxIdleConnsPerHost:   config.HttpTransportMaxIdleConnsPerHost,
				IdleConnTimeout:       90 * time.Second,
			}},
	})
	if err != nil {
		log.Fatalf("failed to create a new session, due to %s ", err.Error())
	}

	//Create S3 service client
	s3Client = s3.New(sess)
}

// ConverteImageWorker ...
func ConverteImageWorker(pageConvertJobChan <-chan *types.PageConvertJob) {

	for pageConvertJob := range pageConvertJobChan {

		var err error
		var pdfStaged bool

		// check if PDF already exists
		pdfExistinS3 := CheckIfS3ObjectExists(pageConvertJob.WorkConvertJob.Product, pageConvertJob.PdfS3Key)

		if pdfExistinS3 {
			// Download PDF
			err = DownloadFile(
				pageConvertJob.WorkConvertJob.Product,
				pageConvertJob.PdfS3Key,
				pageConvertJob.PdfPath,
			)
			if err == nil {
				pdfStaged = true
			} else {
				log.Printf("failed to stage existing PDF s3://%s/%s, due to %+v", pageConvertJob.WorkConvertJob.Product, pageConvertJob.WorkConvertJob.PdfS3Key, err)
			}
		} else {
			// Download Image and create PDF
			err = DownloadFile(
				pageConvertJob.WorkConvertJob.Product,
				pageConvertJob.ImageS3Key,
				pageConvertJob.ImagePath,
			)
			if err == nil {

				imp, err := pdfcpu.ParseImportDetails("form:A4, pos:c, scale:1.0", pdfcpu.CENTIMETRES)
				if err != nil {
					fmt.Println(err)
					log.Printf("failed to set up PDF configuration, due to %+v", err)
				}

				err = api.ImportImagesFile([]string{pageConvertJob.ImagePath}, pageConvertJob.PdfPath, imp, nil)
				if err == nil {
					pdfStaged = true

					// Uplad new created PDF
					b := bytes.Buffer{}
					pdfFile, err := os.Open(pageConvertJob.PdfPath)
					if err != nil {
						fmt.Println(err)
					}

					if _, err := io.Copy(&b, pdfFile); err == nil {

						err := helper.UploadTo(pageConvertJob.WorkConvertJob.Product, pageConvertJob.PdfS3Key, b.Bytes())
						if err != nil {
							fmt.Printf("failed to upload s3://%s/%s, due to %s\n", pageConvertJob.WorkConvertJob.Document, pageConvertJob.PdfS3Key, err.Error())
						}

					} else {
						pdfFile.Close()

					}
				}

			}
		}

		if !pdfStaged {
			// use blank dummy page
			err := CreateDummyPDF(pageConvertJob.PdfPath)
			if err != nil {
				log.Printf("failed to save blank dummy PDF, due to %+v", err)
			}
		}

		pageConvertJob.WG.Done()
	}

}

// CheckIfS3ObjectExists ...
func CheckIfS3ObjectExists(product string, pdfS3Key string) bool {

	query := &s3.HeadObjectInput{
		Bucket: aws.String(product),
		Key:    aws.String(pdfS3Key),
	}

	_, err := s3Client.HeadObject(query)
	if err != nil {
		if strings.Contains(err.Error(), "NoSuchKey") {
			// go on: NoSuchKey
			return false
		} else {
			// go on: Unable to get object
			return false
		}
	} else {
		return true
	}
}

// DownloadFile gets an object from a bucket and stores it in a local file.
func DownloadFile(bucketName string, objectKey string, fileName string) error {

	result, err := s3Client.GetObject(
		&s3.GetObjectInput{
			Bucket: aws.String(bucketName),
			Key:    aws.String(objectKey),
		})
	if err != nil {
		log.Printf("failed to download %v:%v, due to %v\n", bucketName, objectKey, err)
		return err
	}
	defer result.Body.Close()
	file, err := os.Create(fileName)
	if err != nil {
		log.Printf("failed to create %v, due to %v\n", fileName, err)
		return err
	}
	defer file.Close()
	body, err := io.ReadAll(result.Body)
	if err != nil {
		log.Printf("failed to read downloaded data for %v, due to %v\n", objectKey, err)
	}
	_, err = file.Write(body)
	if err != nil {
		log.Printf("failed to write %v, due to %v\n", objectKey, err)
	}
	return err
}

func CreateDummyPDF(fileName string) error {

	ipdf := gopdf.GoPdf{}
	ipdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: 595.28, H: 841.89}}) //595.28, 841.89 = A4

	ipdf.AddPage()

	//save updated disclaimer template PDF
	return ipdf.WritePdf(fileName)
}

// DownloadFile gets an object from a bucket and stores it in a local file.
func DownloadFileBytes(bucketName string, objectKey string, fileName string) ([]byte, error) {

	creds := credentials.NewStaticCredentials(config.StorageKey, config.StorageSecret, "")
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(config.StorageRegion),
		Credentials: creds,
		Endpoint:    aws.String(config.StorageEndpoint),
		// true results for nlh-usc in problems
		S3ForcePathStyle: aws.Bool(true),
		//S3ForcePathStyle: aws.Bool(false),
		MaxRetries: aws.Int(3),

		HTTPClient: &http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   time.Duration(config.HttpTransportTimeout) * time.Second,
					KeepAlive: time.Duration(config.HttpTransportKeepAlive) * time.Second,
				}).DialContext,
				TLSHandshakeTimeout:   time.Duration(config.HttpTransportTslHandshakteTimeout) * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
				MaxIdleConns:          config.HttpTransportMaxIdleConns,
				MaxIdleConnsPerHost:   config.HttpTransportMaxIdleConnsPerHost,
				IdleConnTimeout:       90 * time.Second,
			}},
	})
	if err != nil {
		log.Fatalf("failed to create a new session, due to %s ", err.Error())
	}

	//Create S3 service client
	s3Client = s3.New(sess)

	result, err := s3Client.GetObject(
		&s3.GetObjectInput{
			Bucket: aws.String(bucketName),
			Key:    aws.String(objectKey),
		})
	if err != nil {
		log.Printf("Couldn't get object %v:%v. Here's why: %v\n", bucketName, objectKey, err)
		return nil, err
	}
	defer result.Body.Close()
	file, err := os.Create(fileName)
	if err != nil {
		log.Printf("Couldn't create file %v. Here's why: %v\n", fileName, err)
		return nil, err
	}
	defer file.Close()
	body, err := io.ReadAll(result.Body)
	if err != nil {
		log.Printf("Couldn't read object body from %v. Here's why: %v\n", objectKey, err)
	}

	return body, err
}
