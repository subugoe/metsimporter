package converter

import (
	"fmt"
	"sort"

	"github.com/pdfcpu/pdfcpu/pkg/api"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/types"
)

var ()

func init() {

}

func MergePDFWorker(mergeJobChan <-chan *types.MergeJob) {

	for mergeJob := range mergeJobChan {

		// merge Disclaimer + page PDFs
		// sort by order
		// sortSlice := make(types.PageInfoSlice, 0, len(mergeJob.PageInfos))
		pinfos := mergeJob.PageInfos
		sort.Sort(types.PageInfoSlice(pinfos))
		var inFiles []string
		inFiles = append(inFiles, mergeJob.WorkConvertJob.PdfDisclaimerOutPath)
		for _, pageInfo := range pinfos {
			inFiles = append(inFiles, fmt.Sprintf("%s/%s.pdf", mergeJob.WorkConvertJob.WorkDir, pageInfo.PageKey))
		}

		// Merge inFiles by concatenation in the order specified and write the result to out.pdf.
		// out.pdf will be overwritten.
		err := api.MergeCreateFile(inFiles, mergeJob.WorkConvertJob.PdfPreOutPath, nil)
		if err != nil {
			// TODO logging
			fmt.Printf("failed to merge of page PDFs, due to %s\n", err.Error())
		}

		err = api.AddBookmarksFile(mergeJob.WorkConvertJob.PdfPreOutPath, mergeJob.WorkConvertJob.PdfOutPath, mergeJob.Bookmark, nil)
		if err != nil {
			// TODO logging
			fmt.Printf("failed to merge of page PDFs, due to %s\n", err.Error())
		}

		mergeJob.WG.Done()
	}

}
