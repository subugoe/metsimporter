package query

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/signintech/gopdf"
	"github.com/sirupsen/logrus"

	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/types"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/productspecifics/digizeitschriften"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/productspecifics/nlh"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/util"
)

var (
	config util.Config
	log    logrus.Logger
)

const fontHeight = 11
const lineHeight = fontHeight * 1.2

func init() {
	config, log = util.LoadConfig()
}

func CreateDisclaimer(workConverterJob *types.WorkConvertJob) {

	var disclaimerInfo types.DisclaimerInfo
	var err error

	if workConverterJob.Context == "digizeit" {

		// template method
		digizeit := &digizeitschriften.DigizeitSpecifics{}
		o := types.Productspecifics{
			IProductspecifics: digizeit,
		}

		disclaimerInfo, err = o.QueryDisclaimerInfo(workConverterJob.Document, workConverterJob.Log, workConverterJob.IsLog)
		// TODO check err
		if err != nil {
			fmt.Printf("failed to query disclaimer info, due to %s", err.Error())
		}
		disclaimerInfo.ProductName = o.MapProductToName(workConverterJob.Product)
		disclaimerInfo.TemplateName = o.MapAccessConditionToDisclaimerTemplateName(disclaimerInfo.RightsAccessConditionInfos, disclaimerInfo.YearPublish, disclaimerInfo.ParentID)
	} else if workConverterJob.Context == "nlh" {
		// TODO not yet implemented

		// template method
		nlh := &nlh.NLHSpecifics{}
		o := types.Productspecifics{
			IProductspecifics: nlh,
		}
		disclaimerInfo, err = o.QueryDisclaimerInfo(workConverterJob.Document, workConverterJob.Log, workConverterJob.IsLog)
		// TODO check err
		if err != nil {
			fmt.Printf("failed to query disclaimer info, due to %s", err.Error())
		}
		disclaimerInfo.ProductName = o.MapProductToName(workConverterJob.Product)
		disclaimerInfo.TemplateName = ""
	}

	// load template
	var src string
	if disclaimerInfo.TemplateName == "" {
		src = fmt.Sprintf("%s%s_disclaimer.pdf", config.ImageTemplateDirectory, workConverterJob.Product)
	} else {
		src = fmt.Sprintf("%s%s", config.ImageTemplateDirectory, disclaimerInfo.TemplateName)
	}
	target := workConverterJob.PdfDisclaimerOutPath

	ipdf := gopdf.GoPdf{}
	ipdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: 595.28, H: 841.89}}) //595.28, 841.89 = A4

	ipdf.AddPage()

	_, err = copy(src, target)
	if err == nil {

		// Import existing PDF as template
		disclaimerTemplate := ipdf.ImportPage(target, 1, "/MediaBox")
		// use imported template
		ipdf.UseImportedTemplate(disclaimerTemplate, 0, 0, 0, 0)
	} else {
		// log
		fmt.Printf("failed to load PDF template %s, due to %s\n", src, err.Error())

	}

	//err = ipdf.AddTTFFont("dejavu", "commons/font/dejavu-fonts-ttf-2.37/ttf/DejaVuSans.ttf")
	err = ipdf.AddTTFFont("opensans", "commons/font/OpenSans/static/OpenSans/OpenSans-Regular.ttf")
	if err != nil {
		log.Printf("Couldn't add font open-sans, due to %s", err)
		return
	}

	//err = ipdf.AddTTFFont("dejavu-bold", "commons/font/dejavu-fonts-ttf-2.37/ttf/DejaVuSans-Bold.ttf")
	err = ipdf.AddTTFFont("opensansbold", "commons/font/OpenSans/static/OpenSans/OpenSans-Bold.ttf")
	if err != nil {
		log.Printf("Couldn't add font open-sans, due to %s", err)
		return
	}

	//err = ipdf.SetFont("dejavu", "", 9)
	err = ipdf.SetFont("opensans", "", 9)
	if err != nil {
		log.Printf("Couldn't set font, due to %s", err)
	}

	err = addDisclaimerText(&ipdf, disclaimerInfo, 70, 140, 15)
	if err != nil {
		log.Printf("Couldn't add disclaimer text, duel to %+v", err)
		return
	}

	//save updated disclaimer template PDF
	err = ipdf.WritePdf(target)
	if err != nil {
		log.Printf("Couldn't save pdf, due to %+v", err)
		return
	}
}

func QueryBibliographicMetadataForBookmark(workConverterJob *types.WorkConvertJob) []types.BookmarkInfo {

	if workConverterJob.Context == "digizeit" {

		// template method
		es := &digizeitschriften.DigizeitSpecifics{}
		o := types.Productspecifics{
			IProductspecifics: es,
		}

		// retrive data from index pageID-Array, Product-Name, Title, Author, Publisher, Place, Year, WorkID, PURL
		// Log: ranges, label, level
		bookmarkInfo, err := o.GetLinkAndBookmarkInfo(workConverterJob.Document, workConverterJob.Log, workConverterJob.IsLog)
		// TODO check err
		if err != nil {
			fmt.Printf("failed to get link and bookmark info, due to %s", err.Error())
		}

		return bookmarkInfo

	} else if workConverterJob.Context == "nlh" {
		// template method
		solr := &nlh.NLHSpecifics{}
		o := types.Productspecifics{
			IProductspecifics: solr,
		}
		bookmarkInfo, err := o.GetLinkAndBookmarkInfo(workConverterJob.Document, workConverterJob.Log, workConverterJob.IsLog)
		// TODO check err
		if err != nil {
			fmt.Printf("failed to get link and bookmark info, due to %s", err.Error())
		}

		return bookmarkInfo
	}

	return nil
}

func QueryPageIDs(workConverterJob *types.WorkConvertJob) []types.PageInfo {
	var pageinfo []types.PageInfo
	var err error

	if workConverterJob.Context == "digizeit" {

		// template method
		es := &digizeitschriften.DigizeitSpecifics{}
		o := types.Productspecifics{
			IProductspecifics: es,
		}

		pageinfo, err = o.GetPageSequenceInfo(workConverterJob.Document, workConverterJob.Log, workConverterJob.IsLog)
		if err != nil {
			fmt.Printf("could not get page sequence for %s|%s, due to %s\n", workConverterJob.Document, workConverterJob.Log, err)
		}

	} else if workConverterJob.Context == "nlh" {
		// template method
		solr := &nlh.NLHSpecifics{}
		o := types.Productspecifics{
			IProductspecifics: solr,
		}
		pageinfo, err = o.GetPageSequenceInfo(workConverterJob.Document, workConverterJob.Log, workConverterJob.IsLog)
		if err != nil {
			fmt.Printf("could not get page sequence for %s|%s, due to %s\n", workConverterJob.Document, workConverterJob.Log, err)
		}

	}

	return pageinfo
}

func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

func addDisclaimerText(pdf *gopdf.GoPdf, disclaimerInfo types.DisclaimerInfo, x float64, y float64, verticleSpace float64) error {

	i := 0

	pdf.SetXY(x, y)

	title := disclaimerInfo.Title
	if len(title) > 95 {
		sub := title[0:95]
		title = fmt.Sprintf("%s...", sub)
	}
	y = addFieldToDisclaimerWithoutLabel(pdf, title, x, y, "B", 9, verticleSpace+2)
	pdf.SetX(x)
	i += 1

	if disclaimerInfo.Subtitle != "" {
		subtitle := disclaimerInfo.Subtitle

		if len(subtitle) > 95 {
			sub := subtitle[0:95]
			subtitle = fmt.Sprintf("%s...", sub)
		}
		y = addFieldToDisclaimerWithoutLabel(pdf, subtitle, x, y, "", 8, verticleSpace+5)
		pdf.SetX(x)
		i += 1
	}

	var publishInfos [3]string
	builder := strings.Builder{}
	for _, structrunElement := range disclaimerInfo.DisclaimerHierarchie {
		if strings.ToLower(structrunElement.Type) == "periodical" {
			publishInfos[0] = structrunElement.Title
		} else if strings.ToLower(structrunElement.Type) == "volume" {
			publishInfos[1] = strconv.FormatInt(int64(structrunElement.Partnumber), 10)
		} else if strings.ToLower(structrunElement.Type) == "issue" {
			publishInfos[2] = strconv.FormatInt(int64(structrunElement.Partnumber), 10)
		}
	}

	length := 0
	if len(publishInfos) > 0 {
		if publishInfos[0] != "" {
			builder.WriteString(publishInfos[0])
			length += len(publishInfos[0])
		}
		if publishInfos[1] != "" {
			if publishInfos[1] != "0" {
				builder.WriteString(" - Band ")
				builder.WriteString(publishInfos[1])
			} else {
				builder.WriteString(" - Band ")
			}
		}
		if publishInfos[2] != "" {
			if publishInfos[2] != "0" {
				builder.WriteString(" | Heft ")
				builder.WriteString(publishInfos[2])
			} else {
				builder.WriteString(" | Heft")
			}
		}

		y = addFieldToDisclaimer(pdf, "Erschienen bei", builder.String(), x, y, verticleSpace)
		pdf.SetX(x)
		i += 1
	}

	var ort string
	if len(disclaimerInfo.PlacePublish) > 0 {
		ort = strings.Join(disclaimerInfo.PlacePublish, ";")
	}
	if len(disclaimerInfo.Publisher) > 0 {

		publisher := strings.Join(disclaimerInfo.Publisher, ";")

		if ort != "" {
			publisher += fmt.Sprintf(" (%s)", ort)
		}
		y = addFieldToDisclaimer(pdf, "Erschienen bei", publisher, x, y, verticleSpace)
		pdf.SetX(x)
		i += 1
	}

	if disclaimerInfo.YearPublishString != "" {
		y = addFieldToDisclaimer(pdf, "Erscheinungsjahr", disclaimerInfo.YearPublishString, x, y, verticleSpace)
		pdf.SetX(x)
		i += 1
	}

	endPageIndex := disclaimerInfo.EndPageIndex
	startPageIndex := disclaimerInfo.StartPageIndex
	pageNumbers := endPageIndex - startPageIndex + 1
	if disclaimerInfo.Purl != "" {
		y = addFieldToDisclaimer(pdf, "Seitenumfang", strconv.FormatInt(int64(pageNumbers), 10), x, y, verticleSpace)
		pdf.SetX(x)
		i += 1
	}

	if disclaimerInfo.Purl != "" {
		y = addFieldToDisclaimer(pdf, "PURL", disclaimerInfo.Purl, x, y, verticleSpace)
		pdf.SetX(x)
		i += 1
	}

	return nil
}

func addFieldToDisclaimer(pdf *gopdf.GoPdf, label string, value string, x float64, y float64, verticleSpace float64) float64 {
	pdf.SetFont("opensansbold", "", 7)
	err := pdf.Cell(nil, fmt.Sprintf("%v:  ", label))
	// TODO check err
	if err != nil {
		fmt.Printf("failed to put text on pdf cell, due to %s", err.Error())
		return y
	}
	pdf.SetFont("opensans", "", 7)
	err = pdf.Cell(nil, value)
	// TODO check err
	if err != nil {
		fmt.Printf("failed to put text on pdf cell, due to %s", err.Error())
		return y
	}
	pdf.Br(lineHeight)
	y += verticleSpace
	pdf.SetXY(x, y)

	return y
}

func addFieldToDisclaimerWithoutLabel(pdf *gopdf.GoPdf, value string, x float64, y float64, style string, textSize int, verticleSpace float64) float64 {
	if style == "B" {
		pdf.SetFont("opensansbold", "", textSize)
	} else {
		pdf.SetFont("opensans", "", textSize)
	}

	err := pdf.Cell(nil, value)
	if err != nil {
		// TODO check err
		if err != nil {
			fmt.Printf("failed to put text on pdf cell, due to %s", err.Error())
			return y
		}
	}
	y += verticleSpace
	pdf.SetXY(x, y)

	return y
}
