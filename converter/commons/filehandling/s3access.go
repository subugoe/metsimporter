package filehandling

import (
	"bytes"
	"fmt"
	"io"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/types"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/helper"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/util"
)

var (
	config util.Config
	log    logrus.Logger
)

func init() {
	config, log = util.LoadConfig()
}

func UploadPDFWorker(uploadJobChan <-chan *types.UploadJob) {

	// Upload PDF
	for uploadJob := range uploadJobChan {

		b := bytes.Buffer{}
		pdfFile, err := os.Open(uploadJob.WorkConvertJob.PdfOutPath)
		if err != nil {
			fmt.Println(err)
		}

		if _, err := io.Copy(&b, pdfFile); err != nil {
			if _, err := io.Copy(&b, pdfFile); err != nil {
				// TODO logging
				pdfFile.Close()
				continue
			}
		}

		helper.UploadTo(uploadJob.WorkConvertJob.Product, uploadJob.PdfS3Key, b.Bytes())
		pdfFile.Close()
		uploadJob.WG.Done()
	}

}
