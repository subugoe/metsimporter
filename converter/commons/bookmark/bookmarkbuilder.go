package bookmark

import (
	"sort"
	"strings"

	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/query"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/types"
)

var ()

func init() {

}

type Stack []types.BookmarkInfo

// IsEmpty: check if stack is empty
func (s *Stack) IsEmpty() bool {
	return len(*s) == 0
}

// Push a new value onto the stack
func (s *Stack) Push(bmi types.BookmarkInfo) {
	*s = append(*s, bmi) // Simply append the new value to the end of the stack
}

// Remove and return top element of stack. Return false if stack is empty.
func (s *Stack) Pop() (types.BookmarkInfo, bool) {
	if s.IsEmpty() {
		return types.BookmarkInfo{}, false
	} else {
		index := len(*s) - 1   // Get the index of the top most element.
		element := (*s)[index] // Index into the slice and obtain the element.
		*s = (*s)[:index]      // Remove it from the stack by slicing it off.
		return element, true
	}
}

func CreatePDFBookmark(createBookmarkJob *types.CreateBookmarkJob) []pdfcpu.Bookmark {

	bookmarkInfo := query.QueryBibliographicMetadataForBookmark(createBookmarkJob.WorkConvertJob)
	return createPDFBookmarkFile(createBookmarkJob.WorkConvertJob, bookmarkInfo)
}

func createPDFBookmarkFile(workConverterJob *types.WorkConvertJob, bookmarkInfos []types.BookmarkInfo) []pdfcpu.Bookmark {

	// sort by order
	sort.Sort(types.BookmarkInfoSortSlice(bookmarkInfos))

	start := int(bookmarkInfos[0].BookmarkPageNumber)
	startlevel := int(bookmarkInfos[0].BookmarkLevel)

	var bm []pdfcpu.Bookmark
	boldStyle := true

	following := createBookmarkTreeFromChildren(bookmarkInfos, start, startlevel)

	first := []pdfcpu.Bookmark{
		{
			Title:    "Vorsatzblatt",
			PageFrom: 1,
			Bold:     boldStyle,
			Color:    &pdfcpu.DarkGray,
		},
	}
	bm = append(bm, first...)
	return append(bm, following...)
}

func createBookmarkTreeFromChildren(children []types.BookmarkInfo, start int, startlevel int) []pdfcpu.Bookmark {

	var bookmarkTree []pdfcpu.Bookmark

	for _, child := range children {
		ntimes := int(child.BookmarkLevel) - startlevel
		bookmarkTree = append(bookmarkTree, pdfcpu.Bookmark{
			Title:    strings.Repeat(" ", 3*ntimes) + child.BookmarkLabel,
			PageFrom: int(child.BookmarkPageNumber) - start + 2,
			Color:    &pdfcpu.LightGray,
		})
	}

	return bookmarkTree

}
