package types

// IProductspecifics Interface (for Template Method)
type IProductspecifics interface {
	GetLinkAndBookmarkInfo(string, string, bool) ([]BookmarkInfo, error)
	QueryDisclaimerInfo(string, string, bool) (DisclaimerInfo, error)
	GetPageSequenceInfo(string, string, bool) ([]PageInfo, error)
	MapProductToName(string) string
	MapAccessConditionToDisclaimerTemplateName([]string, int64, string) string
}

// Productspecifics ...
type Productspecifics struct {
	IProductspecifics IProductspecifics
}

func (q *Productspecifics) GetLinkAndBookmarkInfo(workID string, logID string, isLog bool) ([]BookmarkInfo, error) {
	return q.IProductspecifics.GetLinkAndBookmarkInfo(workID, logID, isLog)
}

func (q *Productspecifics) GetPageSequenceInfo(workID string, logID string, isLog bool) ([]PageInfo, error) {
	return q.IProductspecifics.GetPageSequenceInfo(workID, logID, isLog)
}

func (q *Productspecifics) QueryDisclaimerInfo(workID string, logID string, isLog bool) (DisclaimerInfo, error) {
	return q.IProductspecifics.QueryDisclaimerInfo(workID, logID, isLog)
}

func (q *Productspecifics) MapProductToName(product string) string {
	return q.IProductspecifics.MapProductToName(product)
}

func (q *Productspecifics) MapAccessConditionToDisclaimerTemplateName(accessConditions []string, yearOfPublishing int64, parentID string) string {
	return q.IProductspecifics.MapAccessConditionToDisclaimerTemplateName(accessConditions, yearOfPublishing, parentID)
}
