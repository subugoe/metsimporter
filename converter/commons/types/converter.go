package types

import (
	"sync"

	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu"
)

var ()

// WorkConvertJob ...
type WorkConvertJob struct {
	Context              string `json:"context"`
	Product              string `json:"product"`  // product == bucket
	Document             string `json:"document"` //
	Log                  string `json:"log"`      // if !IsLog => document == log
	IsLog                bool   `json:"isLog"`
	WorkLog              string `json:"workLog"`              // document___log
	WorkDir              string `json:"workDir"`              // document___log/
	PdfDisclaimerOutPath string `json:"pdfDisclaimerOutPath"` // document___log
	PdfMetadataOutPath   string `json:"pdfMetadataOutPath"`   // document___log
	PdfPreOutPath        string `json:"pdfPreOutPath"`        // document___log
	PdfOutPath           string `json:"pdfOutPath"`           // document___log
	PdfS3Key             string `json:"pdfS3Key"`             // document___log
}

// BaseJob ...
type BaseJob struct {
	WorkConvertJob *WorkConvertJob   `json:"workconvertjob"`
	PageInfos      []PageInfo        `json:"pageInfos"`
	Bookmark       []pdfcpu.Bookmark `json:"bookmark"`
	WG             *sync.WaitGroup   `json:"wg"`
}

// PageConvertJob ...
type PageConvertJob struct {
	BaseJob
	ImageName  string `json:"imageName"`
	ImageS3Key string `json:"imageKey"`
	ImagePath  string `json:"imagePath"`
	PdfPath    string `json:"pdfPath"`
	PdfS3Key   string `json:"pdfS3Key"`
	Format     string `json:"format"`
}

// CreateDisclaimerJob ...
type CreateDisclaimerJob struct {
	BaseJob
}

// CreateBookmarkJob ...
type CreateBookmarkJob struct {
	BaseJob
}

// DownloadJob ...
type DownloadJob struct {
	BaseJob
	PageInfos PageInfo `json:"pageInfos"`
}

// MergeJob ...
type MergeJob struct {
	BaseJob
	PageInfos []PageInfo `json:"pageInfos"`
}

// UploadJob ...
type UploadJob struct {
	WorkConvertJob *WorkConvertJob `json:"workconvertjob"`
	BaseJob
	PdfS3Key   string `json:"pdfS3Key"`
	PdfOutPath string `json:"pdfOutPath"`
}

type PageInfoSlice []PageInfo

// Len is part of sort.Interface.
func (d PageInfoSlice) Len() int {
	return len(d)
}

// Swap is part of sort.Interface.
func (d PageInfoSlice) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less is part of sort.Interface. We use count as the value to sort by
func (d PageInfoSlice) Less(i, j int) bool {
	return d[i].Order < d[j].Order
}

// PageInfo ...
type PageInfo struct {
	PageKey string `json:"pageKey"`
	Order   int64  `json:"order"`
	Format  string `json:"format"`
}

type Node struct {
	prev *Node
	next *Node

	BookmarkInfo BookmarkInfo    `json:"bookmarkInfo"`
	BookmarkTree pdfcpu.Bookmark `json:"bookmarktree"`
}

type List struct {
	head *Node
	tail *Node
}

func (L *List) Head() *Node {
	return L.head
}

func (L *List) Insert(bookmarkInfo BookmarkInfo) *Node {
	list := &Node{
		next:         L.head,
		BookmarkInfo: bookmarkInfo,
	}
	if L.head != nil {
		L.head.prev = list
	}
	L.head = list

	l := L.head
	for l.next != nil {
		l = l.next
	}
	L.tail = l

	return list
}

func (l *List) Reverse() *Node {
	curr := l.head
	var prev *Node
	l.tail = l.head

	for curr != nil {
		next := curr.next
		curr.next = prev
		prev = curr
		curr = next
	}
	l.head = prev
	return l.head
}

func getPreviou(node *Node, level int64) *Node {

	if node.BookmarkInfo.BookmarkLevel < level {
		return node
	} else {
		return getPreviou(node.prev, level)
	}
}

// BookmarkInfo ...
type BookmarkInfo struct {
	BookmarkLabel      string `json:"bookmarkLabel"`
	BookmarkLevel      int64  `json:"bookmarkLevel"`
	BookmarkPageNumber int64  `json:"bookmarkPageNumber"`
	BookmarkOrder      int    `json:"bookmarkOrder"`
}

type BookmarkInfoSortSlice []BookmarkInfo

// Len is part of sort.Interface.
func (d BookmarkInfoSortSlice) Len() int {
	return len(d)
}

// Swap is part of sort.Interface.
func (d BookmarkInfoSortSlice) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less is part of sort.Interface. We use count as the value to sort by
func (d BookmarkInfoSortSlice) Less(i, j int) bool {
	//return d[i].Order < d[j].Order
	return d[i].BookmarkOrder < d[j].BookmarkOrder
}

// DisclaimerInfo ...
type DisclaimerInfo struct {
	ID                         string                 `json:"id"`                  // workID|logID
	Work                       string                 `json:"work"`                //
	Product                    string                 `json:"product"`             //
	ProductName                string                 `json:"product_name"`        //
	TemplateName               string                 `json:"template_name"`       //
	Purl                       string                 `json:"purl"`                //
	LogId                      string                 `json:"log_id"`              //
	LogLabel                   string                 `json:"log_label"`           //
	LogLevel                   int8                   `json:"log_level"`           //
	LogType                    string                 `json:"log_type"`            //
	StartPageIndex             int64                  `json:"start_page_index"`    //
	EndPageIndex               int64                  `json:"end_page_index"`      //
	Title                      string                 `json:"title"`               //
	Subtitle                   string                 `json:"subtitle"`            //
	DC                         string                 `json:"dc"`                  //
	Shelfmark                  string                 `json:"shelfmark"`           //
	ByCreator                  string                 `json:"bycreator"`           //
	Publisher                  []string               `json:"publisher"`           //
	YearPublishString          string                 `json:"year_publish_string"` //
	YearPublish                int64                  `json:"year_publish"`        //
	PlacePublish               []string               `json:"place_publish"`       //
	ParentID                   string                 `json:"parent_id"`           //
	Currentno                  string                 `json:"currentno"`           //
	ParentdocTitle             string                 `json:"parentdoc_title"`     // -
	RightsAccessConditionInfos []string               `json:"rights_access_condition_infos,omitempty"`
	DisclaimerHierarchie       []DisclaimerHierarchie `json:"disclaimerHierarchie"` // -
}

// TitleInfo ...
type TitleInfo struct {
	Title string `xml:"title" json:"title,omitempty"`
}

// DisclaimerHierarchie ...
type DisclaimerHierarchie struct {
	Order      int    `json:"order"`
	Type       string `json:"type"`
	Title      string `json:"title"`
	Partnumber int64  `json:"partnumber"`
	ID         string `json:"id"`
}

// RightAccessCondition ...
type RightAccessCondition struct {
	Type  string `json:"type,omitempty"`
	Value string `json:"access_condition,omitempty"`
}
