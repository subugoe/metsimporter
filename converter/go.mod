module gitlab.gwdg.de/subugoe/metsimporter/converter

go 1.23

require (
	github.com/aws/aws-sdk-go v1.44.323
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/pdfcpu/pdfcpu v0.4.2
	github.com/signintech/gopdf v0.18.0
	github.com/sirupsen/logrus v1.9.3
	gitlab.gwdg.de/subugoe/metsimporter/indexer v0.0.0-20230814133905-1db80c6c1f61
	gitlab.gwdg.de/subugoe/shared-product-configs v0.8.11
)

require (
	github.com/akamensky/argparse v1.4.0 // indirect
	github.com/fsnotify/fsnotify v1.5.4 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/hhrutter/lzw v1.0.0 // indirect
	github.com/hhrutter/tiff v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/magiconair/properties v1.8.6 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/olivere/elastic v6.2.37+incompatible // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/pelletier/go-toml/v2 v2.0.1 // indirect
	github.com/phpdave11/gofpdi v1.0.14-0.20211212211723-1f10f9844311 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	github.com/spf13/afero v1.8.2 // indirect
	github.com/spf13/cast v1.5.0 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.12.0 // indirect
	github.com/subosito/gotenv v1.3.0 // indirect
	golang.org/x/image v0.5.0 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	gopkg.in/gographics/imagick.v3 v3.4.2 // indirect
	gopkg.in/ini.v1 v1.66.4 // indirect
	gopkg.in/sohlich/elogrus.v3 v3.0.0-20180410122755-1fa29e2f2009 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0 // indirect
)
