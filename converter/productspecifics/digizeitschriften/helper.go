package digizeitschriften

var (
	productToNameMap                           map[string]string
	accessConditionToDisclaimerTemplateNameMap map[string]string
)

func init() {
	productToNameMap = map[string]string{
		"dino":    "DigiZeitschriften",
		"dzeit":   "DigiZeitschriften",
		"indexer": "Test Product",
	}

	accessConditionToDisclaimerTemplateNameMap = map[string]string{
		"gesamtabo":      "digizeit_ClosedAccess_Alle_Rechte_vorbehalten_Zugang_nach_Autorisierung_inkl_Grabka_Wagner.pdf",
		"gesperrt":       "digizeit_Gesperrt_Graf-Beitraege.pdf",                                          // Gesperrt
		"free-1901-1930": "digizeit_OA_Alle_Rechte_vorbehalten-Freier_Zugang_1901-1930_inkl_Schlicht.pdf", // OA_Alle_Rechte_vorbehalten
		"free-1900":      "digizeit_OA_Gemeinfrei_Jgg-bis-1900.pdf",                                       // OA_Gemeinfrei
		"free-1996-2018": "digizeit_OA_Shakespeare_Jahrbuch_Jgg1996-2018.pdf",                             // OA_Shakespeare, special case "Shakespeare-Jahrbuch"
	}
}

// MapProductToName ...
func (s *DigizeitSpecifics) MapProductToName(product string) string {
	str := productToNameMap[product]
	if str != "" {
		return str
	}
	return product

}

// MapAccessConditionToDisclaimerTemplateName ...
func (s *DigizeitSpecifics) MapAccessConditionToDisclaimerTemplateName(accessConditions []string, yearOfPublishing int64, parentID string) string {

	if Contains(accessConditions, "gesperrt") {
		return accessConditionToDisclaimerTemplateNameMap["gesperrt"]
	} else if Contains(accessConditions, "gesamtabo") {
		return accessConditionToDisclaimerTemplateNameMap["gesamtabo"]
	} else if Contains(accessConditions, "free") {
		// special case for "Shakespeare-Jahrbuch"
		if yearOfPublishing >= 1996 && yearOfPublishing <= 2018 && parentID == "338286934" {
			return accessConditionToDisclaimerTemplateNameMap["free-1996-2018"]
		} else if yearOfPublishing < 1900 {
			return accessConditionToDisclaimerTemplateNameMap["free-1900"]
		} else if yearOfPublishing >= 1901 && yearOfPublishing <= 1930 {
			return accessConditionToDisclaimerTemplateNameMap["free-1901-1930"]
		} else if yearOfPublishing >= 1931 {
			return accessConditionToDisclaimerTemplateNameMap["free-1901-1930"]
		}
	}

	return ""
}

// Contains ... check if the slice contains the value
func Contains(arr []string, el string) bool {
	for _, a := range arr {
		if a == el {
			return true
		}
	}
	return false
}
