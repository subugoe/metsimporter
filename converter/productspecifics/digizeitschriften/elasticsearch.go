package digizeitschriften

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/elastic/go-elasticsearch"
	"github.com/sirupsen/logrus"
	types "gitlab.gwdg.de/subugoe/metsimporter/converter/commons/types"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/util"
)

var (
	config util.Config
	log    logrus.Logger
)

func init() {
	config, log = util.LoadConfig()
}

type LogStructSortSlice []LogStruct

// Len is part of sort.Interface.
func (d LogStructSortSlice) Len() int {
	return len(d)
}

// Swap is part of sort.Interface.
func (d LogStructSortSlice) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less is part of sort.Interface. We use count as the value to sort by
func (d LogStructSortSlice) Less(i, j int) bool {
	//return d[i].Order < d[j].Order
	return d[i].Index < d[j].Index
}

type PhysStructSortSlice []PhysStruct

// Len is part of sort.Interface.
func (d PhysStructSortSlice) Len() int {
	return len(d)
}

// Swap is part of sort.Interface.
func (d PhysStructSortSlice) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less is part of sort.Interface. We use count as the value to sort by
func (d PhysStructSortSlice) Less(i, j int) bool {
	//return d[i].Order < d[j].Order
	return d[i].Order < d[j].Order
}

// AgentInfo ...
type AgentInfo struct {
	Type               string `json:"type,omitempty"`
	Role               string `json:"role,omitempty"`
	Otherrole          string `json:"otherrole,omitempty"`
	AgentProcessorName string `json:"agentProcessorName,omitempty"`
	AgentProcessorNote string `json:"agentProcessorNote,omitempty"`
}

// LogStruct ...
type LogStruct struct {
	CreateDate                 string `json:"CREATEDATE,omitempty"`
	IsFirst                    bool
	IsExternalContent          bool
	ReindexedAt                string                 `json:"reindexedat,omitempty"`
	Bycreator                  string                 `json:"bycreator,omitempty"`
	Byperson                   string                 `json:"byperson,omitempty"`
	Currentno                  []string               `json:"currentno,omitempty"`
	Currentnosort              int64                  `json:"currentnosort,omitempty"`
	DateIndexed                string                 `json:"date_indexed,omitempty"`
	DateModified               string                 `json:"date_modified,omitempty"`
	Dc                         []string               `json:"dc,omitempty"`
	DmdID                      string                 `json:"dmdid,omitempty"`
	Log                        string                 `json:"log,omitempty"`
	LogID                      string                 `json:"log_id,omitempty"`
	Doctype                    string                 `json:"doctype,omitempty"`
	PID                        string                 `json:"pid,omitempty"`
	ID                         string                 `json:"id,omitempty"`
	Identifier                 []string               `json:"identifier,omitempty"`
	IdentifierInfo             []Identifier           `json:"identifier_info,omitempty"`
	Iscontribution             bool                   `json:"iscontribution"`
	Issn                       string                 `json:"issn,omitempty"`
	Label                      string                 `json:"label,omitempty"`
	Level                      int8                   `json:"level,omitempty"`
	Order                      int64                  `json:"order,omitempty"`
	Parent                     *Parent                `json:"parent,omitempty"`
	ParentID                   string                 `json:"parent_id,omitempty"`
	Product                    string                 `json:"product,omitempty"`
	PublishInfos               *PublishInfo           `json:"publish_infos,omitempty"`
	Purl                       string                 `json:"purl,omitempty"`
	RecordIdentifier           string                 `json:"record_identifier,omitempty"`
	Shelfmark                  []Shelfmark            `json:"shelfmark_infos,omitempty"`
	Structrun                  []Structrun            `json:"structrun,omitempty"`
	Title                      *TitleInfo             `json:"title,omitempty"`
	Type                       string                 `json:"type,omitempty"`
	Work                       string                 `json:"work,omitempty"`
	IssueNumber                string                 `json:"issue_number,omitempty"`
	VolumeNumber               string                 `json:"volume_number,omitempty"`
	Index                      int64                  `json:"index,omitempty"`
	StartPageIndex             int64                  `json:"start_page_index,omitempty"`
	StartPagePhysID            string                 `json:"start_page_phys_id,omitempty"`
	EndPageIndex               int64                  `json:"end_page_index,omitempty"`
	EndPagePhysID              string                 `json:"end_page_phys_id,omitempty"`
	RightsAccessConditionInfos []RightAccessCondition `json:"rights_access_condition_infos,omitempty"`
}

// Parent ...
type Parent struct {
	Title *TitleInfo `json:"title,omitempty"`
}

// Shelfmark ...
type Shelfmark struct {
	Shelfmark string `json:"shelfmark,omitempty"`
}

// PhysStruct ...
type PhysStruct struct {
	DateIndexed     string     `json:"date_indexed,omitempty"`
	DateModified    string     `json:"date_modified,omitempty"`
	PID             string     `json:"pid,omitempty"`
	ID              string     `json:"id,omitempty"`
	Label           string     `json:"label,omitempty"`
	Level           int8       `json:"level,omitempty"`
	Log             string     `json:"log,omitempty"`
	LogID           string     `json:"log_id,omitempty"`
	Order           int64      `json:"order,omitempty"`
	Filename        string     `json:"filename,omitempty"`
	Format          string     `json:"format,omitempty"`
	Page            string     `json:"page,omitempty"`
	PageKey         string     `json:"page_key,omitempty"`
	PageHeight      int32      `json:"page_height,omitempty"`
	PageWidth       int32      `json:"page_width,omitempty"`
	Phys            string     `json:"phys,omitempty"`
	StartPageIndex  int64      `json:"start_page_index"`
	EndPageIndex    int64      `json:"end_page_index"`
	Title           *TitleInfo `json:"title,omitempty"`
	Type            string     `json:"type,omitempty"`
	Work            string     `json:"work,omitempty"`
	Mimetype        string     `json:"mimetype,omitempty"`
	Externalcontent bool
	Index           int64
}

// Structrun ..
type Structrun struct {
	ID    string     `json:"id"`
	Type  string     `json:"type,omitempty"`
	Title *TitleInfo `json:"title,omitempty"`
}

// LogStructResponse ..
type LogStructResponse struct {
	Hits LogStructHits `json:"hits"`
}

// PhysStructResponse ..
type PhysStructResponse struct {
	Hits PhysStructHits `json:"hits"`
}

// LogStructHits ...
type LogStructHits struct {
	Total int64                 `json:"total"`
	Hits  []LogStructSourceHits `json:"hits"`
}

// PhysStructHits ...
type PhysStructHits struct {
	Total int64                  `json:"total"`
	Hits  []PhysStructSourceHits `json:"hits"`
}

// LogStructSourceHits ...
type LogStructSourceHits struct {
	LogStruct LogStruct `json:"_source"`
}

// TitleInfo ...
type TitleInfo struct {
	Type             string `xml:"type,attr" json:"type,omitempty"`
	AuthorityURI     string `xml:"authorityURI,attr" json:"authority_URI,omitempty"` // URI der Normdatei
	ValueURI         string `xml:"valueURI,attr" json:"ValueURI,omitempty"`          // URI des Sachtitels
	Title            string `xml:"title" json:"title,omitempty"`                     // required, not repeatable
	TitleOriginal    string `json:"title_original,omitempty"`                        // required, not repeatable
	Subtitle         string `xml:"subTitle" json:"subtitle,omitempty"`               // optinal, not repeatable
	Nonsort          string `xml:"nonSort" json:"-"`                                 // optinal, repeatable
	Sorttitle        string `json:"sorttitle,omitempty"`                             // optinal, repeatable
	Partname         string `xml:"partName" json:"partname,omitempty"`               // optinal, repeatable
	PartnumberOrig   string `xml:"partNumber" json:"-"`                              // optinal, repeatable
	Partnumber       int64  `json:"partnumber,omitempty"`                            // optinal, repeatable
	TitleAbbreviated string `json:"title_abbreviated,omitempty"`                     // required, not repeatable
	TitleAlternative string `json:"title_alternative,omitempty"`                     // required, not repeatable
	TitleUniform     string `json:"title_uniform,omitempty"`                         // required, not repeatable
}

// PhysStructSourceHits ...
type PhysStructSourceHits struct {
	PhysStruct PhysStruct `json:"_source"`
}

// PublishInfo ...
type PublishInfo struct {
	EventType           string   `json:"event_type,omitempty"`           //
	Place               []string `json:"place_publish,omitempty"`        //
	PlaceInfo           []Place  `json:"place_info,omitempty"`           //
	Publisher           []string `json:"publisher,omitempty"`            //
	PublisherNormalized []string `json:"publisher_normalized,omitempty"` //
	YearPublishString   string   `json:"year_publish_string,omitempty"`  //
	YearPublish         int64    `json:"year_publish,omitempty"`         //
	YearPublishStart    int64    `json:"year_publish_start,omitempty"`   //
	YearPublishEnd      int64    `json:"year_publish_end,omitempty"`     //
	Edition             string   `json:"edition,omitempty"`              //
}

// RightAccessCondition ...
type RightAccessCondition struct {
	Type                string `json:"type,omitempty"`
	Value               string `json:"access_condition,omitempty"`
	URL                 string `json:"url,omitempty"`
	Copyright           string `json:"copyright,omitempty"`
	License             string `json:"license,omitempty"`
	LicenseDisplaylabel string `json:"license_displaylabel,omitempty"`
	NormalizedPublisher string `json:"normalized_publisher,omitempty"`
}

// CreatorInfo ...
type CreatorInfo struct {
	Type              string `json:"type,omitempty"`
	Name              string `json:"name,omitempty"`
	Roleterm          string `json:"roleterm,omitempty"`
	RoletermAuthority string `json:"roleterm_authority,omitempty"`
	RoletermType      string `json:"roleterm_type,omitempty"`
	GndURI            string `json:"gndURI,omitempty"`
	GndNumber         string `json:"gndNumber,omitempty"`
	Date              string `json:"date,omitempty"`
	DateString        string `json:"date_string,omitempty"`
}

// Place ...
type Place struct {
	PlaceTerm []PlaceTerm `xml:"placeTerm" json:"place_term,omitempty"` // required, repeatabley
}

// PlaceTerm ...
type PlaceTerm struct {
	Type         string `xml:"type,attr" json:"type,omitempty"`                 // values: {text|code}
	Authority    string `xml:"authority,attr" json:"authority,omitempty"`       //
	AuthorityURI string `xml:"authorityURI,attr" json:"authorityURI,omitempty"` //
	ValueURI     string `xml:"valueURI,attr" json:"valueURI,omitempty"`         //
	Value        string `xml:",chardata" json:"value,omitempty"`                //
}

// Identifier ...
type Identifier struct {
	Type    string `xml:"type,attr" json:"type,omitempty"`
	Value   string `xml:",chardata" json:"id,omitempty"`
	InvalID string `xml:"invalid,attr" json:"-"`
}

type DigizeitSpecifics struct {
	types.Productspecifics
}

// getLog ...
//
//	var sampleQueryString = fmt.Sprintf(`
//	"bool": {
//		"must": [
//		    {"match": {"work.keyword": "%s"}}
//	    ]
//	},
//	"_source": %+q`, id, fields)
func getLog(query string, source string) (map[string]LogStruct, error) {
	// "id" : "1047098326_0002"

	var err error
	var esLogMap map[string]LogStruct = make(map[string]LogStruct)

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {
		return nil, fmt.Errorf("failed to connect to ES, due to %s", err.Error())
	}

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, source, 2000)

	// Instantiate a map interface object for storing returned documents
	//var mapResp map[string]interface{}
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		return nil, fmt.Errorf("json.NewEncoder() ERROR: %s", err)

		// Query is a valid JSON object
	} else {

		// Pass the JSON query to the Golang client's Search() method
		resp, err := client.Search(
			client.Search.WithContext(ctx),
			client.Search.WithIndex(config.LogIndex),
			client.Search.WithBody(read),
			client.Search.WithTrackTotalHits(true),
		)

		// Check for any errors returned by API call to Elasticsearch
		if err != nil {
			return nil, fmt.Errorf("failed to query Elasticsearch, due to %s", err.Error())

			// If no errors are returned, parse esapi.Response object
		} else {

			// Close the result body when the function call is complete
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				return nil, fmt.Errorf("could not read Elasticsearch response, due to %s", err.Error())
			}

			var esLogResponse *LogStructResponse = new(LogStructResponse)

			if err := json.Unmarshal(body, &esLogResponse); err != nil {
				panic(err)
			}

			for _, hit := range esLogResponse.Hits.Hits {
				l := hit.LogStruct.Log
				esLogMap[l] = hit.LogStruct
			}
			return esLogMap, nil
		}
	}
}

// getPhys ...
//
//	var sampleQueryString = fmt.Sprintf(`
//	"bool": {
//		"must": [
//		    {"match": {"work.keyword": "%s"}}
//	    ]
//	},
//	"_source": %+q`, id, fields)
func getPhys(query string, source string) (map[string]PhysStruct, error) {
	// "id" : "1047098326_0002"

	var err error
	var esPhysMap map[string]PhysStruct = make(map[string]PhysStruct)

	// Create a context object for the API calls
	ctx := context.Background()

	// Instantiate an Elasticsearch configuration
	cfg := elasticsearch.Config{
		Addresses: []string{
			config.ElasticsearchHost,
		},
	}

	// Instantiate a new Elasticsearch client object instance
	client, err := elasticsearch.NewClient(cfg)

	// Check for connection errors to the Elasticsearch cluster
	if err != nil {
		return nil, fmt.Errorf("failed to connect to ES, due to %s", err.Error())
	}

	// Pass the query string to the function and have it return a Reader object
	read := constructQuery(query, source, 2000)

	// Instantiate a map interface object for storing returned documents
	// var mapResp map[string]interface{}
	var buf bytes.Buffer

	// Attempt to encode the JSON query and look for errors
	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		log.Fatalf("json.NewEncoder() ERROR:", err)

		// Query is a valid JSON object
	} else {
		// Pass the JSON query to the Golang client's Search() method
		resp, err := client.Search(
			client.Search.WithContext(ctx),
			client.Search.WithIndex(config.PhysIndex),
			client.Search.WithBody(read),
			client.Search.WithTrackTotalHits(true),
		)

		// Check for any errors returned by API call to Elasticsearch
		if err != nil {
			return nil, fmt.Errorf("failed to connect to physical ES index, due to %s", err.Error())

			// If no errors are returned, parse esapi.Response object
		} else {

			// Close the result body when the function call is complete
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				return nil, fmt.Errorf("could not read Elasticsearch response, due to %s", err.Error())
			}

			var esPhysResponse *PhysStructResponse = new(PhysStructResponse)

			if err := json.Unmarshal(body, &esPhysResponse); err != nil {
				panic(err)
			}

			for _, hit := range esPhysResponse.Hits.Hits {
				p := hit.PhysStruct.Phys
				esPhysMap[p] = hit.PhysStruct
			}
			return esPhysMap, nil

		}
	}

	return nil, nil
}

func (s *DigizeitSpecifics) QueryDisclaimerInfo(workID string, logID string, isLog bool) (types.DisclaimerInfo, error) {

	var query string

	// load metadata

	fields := []string{"id", "product", "purl", "log_type", "title.title", "title.subtitle",
		"publish_infos.publisher", "bycreator",
		"publish_infos.year_publish_string", "publish_infos.year_publish",
		"publish_infos.place_publish",
		"structrun.title.title", "structrun.title.partnumber", "structrun.type", "structrun.parent_id",
		"shelfmark_infos.shelfmark", "dc", "level", "label", "log_id", "work",
		"parent_id", "parent.title.title",
		"rights_access_condition_infos.access_condition", "start_page_index", "end_page_index"}

	source := joinStringArrayToKommaSeparatedString(fields)

	if isLog {
		// all pages

		query = fmt.Sprintf(`
		"bool": {
			"must": [
		    	{ "match": { "log_id.keyword": "%s|%s" } }
			]
		}`, workID, logID)

	} else {
		query = fmt.Sprintf(`
		"bool": {
			"must": [
		    	{ "match": { "work.keyword": "%s" } },
				{ "match": { "IsFirst": true } }
			]
		}`, workID)

	}

	logResults, err := getLog(query, source)
	if err != nil {
		return types.DisclaimerInfo{}, fmt.Errorf("failed to query disclaimer metadata, due to %s", err.Error())
	}

	var disclaimerInfo types.DisclaimerInfo

	for _, log := range logResults {

		var disclaimerHierarchies []types.DisclaimerHierarchie
		order := 0
		for _, structrunElement := range log.Structrun {
			disclaimerHierarchies = append(disclaimerHierarchies, types.DisclaimerHierarchie{
				Order:      order,
				Type:       structrunElement.Type,
				Title:      structrunElement.Title.Title,
				Partnumber: structrunElement.Title.Partnumber,
				ID:         structrunElement.ID,
			})
			order += 1
		}

		var shelfmarks []string
		for _, shelfmarkElement := range log.Shelfmark {
			shelfmarks = append(shelfmarks, shelfmarkElement.Shelfmark)
		}

		var acs []string
		for _, ac := range log.RightsAccessConditionInfos {
			acs = append(acs, strings.ToLower(ac.Value))
		}

		disclaimerInfo = types.DisclaimerInfo{
			ID:                         log.ID,
			Work:                       log.Work,
			Product:                    log.Product,
			Purl:                       log.Purl,
			LogId:                      log.LogID,
			LogLabel:                   log.Label,
			LogLevel:                   log.Level,
			LogType:                    log.Type,
			StartPageIndex:             log.StartPageIndex,
			EndPageIndex:               log.EndPageIndex,
			Title:                      log.Title.Title,
			Subtitle:                   log.Title.Subtitle,
			DC:                         joinStringArrayToKommaSeparatedString(log.Dc),
			Shelfmark:                  joinStringArrayToKommaSeparatedString(shelfmarks),
			ByCreator:                  log.Bycreator,
			Publisher:                  log.PublishInfos.Publisher,
			YearPublishString:          log.PublishInfos.YearPublishString,
			YearPublish:                log.PublishInfos.YearPublish,
			PlacePublish:               log.PublishInfos.Place,
			ParentID:                   log.ParentID,
			ParentdocTitle:             log.Parent.Title.Title,
			Currentno:                  joinStringArrayToKommaSeparatedString(log.Currentno),
			RightsAccessConditionInfos: acs,
			DisclaimerHierarchie:       disclaimerHierarchies,
		}
	}

	return disclaimerInfo, nil
}

func (s *DigizeitSpecifics) GetLinkAndBookmarkInfo(workID string, logID string, isLog bool) ([]types.BookmarkInfo, error) {

	var query string

	// get sequence of logical structure elements

	fields := []string{"level", "label", "start_page_index", "order", "log"}
	source := joinStringArrayToKommaSeparatedString(fields)
	work_log := fmt.Sprintf("%s|%s", workID, logID)

	if isLog {
		query = fmt.Sprintf(`
		"bool": {
			"should": [
		    	{ "match": { "log_id.keyword": "%s|%s" } },
				{ "match": { "structrun.parent_id.keyword": "%s|%s" } }
			 ]
		  }`, workID, logID, workID, logID)

	} else {
		query = fmt.Sprintf(`
		"bool": {
			"should": [
		    	{ "match": { "work.keyword": "%s" } }
			 ]
		  }`, workID)
	}

	logResults, err := getLog(query, source)
	if err != nil {
		return nil, fmt.Errorf("could not get logical index document for %s, due to %s", work_log, err)
	}

	var bookmarkInfo []types.BookmarkInfo
	for _, log := range logResults {
		bookmarkInfo = append(bookmarkInfo, types.BookmarkInfo{
			BookmarkLabel:      log.Label,
			BookmarkLevel:      int64(log.Level),
			BookmarkOrder:      int(log.Order),
			BookmarkPageNumber: int64(log.StartPageIndex),
		})
	}

	return bookmarkInfo, nil
}

// GetPageSequenceInfo ...
func (s *DigizeitSpecifics) GetPageSequenceInfo(workID string, logID string, isLog bool) ([]types.PageInfo, error) {

	var query string

	// get PhysID for last page of logical structure element (special case)

	fields := []string{"end_page_phys_id", "log"}
	work_log := fmt.Sprintf("%s|%s", workID, logID)
	if isLog {
		query = fmt.Sprintf(`
		"match": { "log_id.keyword": "%s"} 
		`, work_log)
	} else {
		query = fmt.Sprintf(`
		"match": { "work.keyword": "%s"} 
		`, workID)
	}
	source := joinStringArrayToKommaSeparatedString(fields)
	logResults, err := getLog(query, source)
	if err != nil {
		return nil, fmt.Errorf("could not get logical index document for %s, due to %s", work_log, err)
	}

	// get pageID sequence

	fields = []string{"page", "order", "phys", "format"}
	source = joinStringArrayToKommaSeparatedString(fields)
	physId := logResults[logID].EndPagePhysID
	if isLog {
		query = fmt.Sprintf(`
		"bool": {
			"should": [
			  {"bool": {
			    "must": [
			        { "match": { "work.keyword": "%s" } },
			        { "match": { "phys.keyword": "%s" } }
			     
			      ]
			    }
			  },
		    { "match": { "log_id.keyword": "%s|%s" } },
			  { "match": { "structrun.parent_id.keyword": "%s|%s" } }
			  
			 ]
		  }`, workID, physId, workID, logID, workID, logID)

	} else {
		query = fmt.Sprintf(`
		"bool": {
			"should": [
			  {"bool": {
			    "must": [
			        { "match": { "work.keyword": "%s" } }
			     ]
			  }}
			]
		}`,
			workID)
	}

	physResults, err := getPhys(query, source)
	if err != nil {
		return nil, fmt.Errorf("could not get physical index document for %s, due to %s", work_log, err)
	}

	var pageInfos []types.PageInfo
	for _, phys := range physResults {
		pageInfos = append(pageInfos, types.PageInfo{
			PageKey: phys.Page,
			Order:   phys.Order,
			Format:  phys.Format,
		})
	}

	return pageInfos, nil
}

func joinStringArrayToKommaSeparatedString(strArr []string) string {
	builder := strings.Builder{}
	for i, el := range strArr {
		if i < len(strArr)-1 {
			builder.WriteString("\"")
			builder.WriteString(el)
			builder.WriteString("\"")
			builder.WriteString(",")
		} else {
			builder.WriteString("\"")
			builder.WriteString(el)
			builder.WriteString("\"")

		}
	}
	return builder.String()
}

// func constructQuery(q string, source string, sortfield string, size int) *strings.Reader {
func constructQuery(q string, source string, size int) *strings.Reader {

	// Build a query string from string passed to function
	var query = `{"query": {`

	// Concatenate query string with string passed to method call
	query = query + q
	query = query + `}, "_source": [` + source + `], "size": ` + strconv.Itoa(size) + `}`

	// Check for JSON errors
	isValid := json.Valid([]byte(query)) // returns bool

	// Default query is "{}" if JSON is invalid
	if !isValid {
		log.Printf("using the default query match_all, due to incorrect query: %s", query)
		query = "{}"
	}

	// Build a new string from JSON query
	var b strings.Builder
	b.WriteString(query)

	// Instantiate a *strings.Reader object from string
	read := strings.NewReader(b.String())

	// Return a *strings.Reader object
	return read
}
