package nlh

var (
	productToNameMap map[string]string
)

func init() {
	productToNameMap = map[string]string{
		"nlh-eai1": "Early American Imprints : Evans 1639-1800 (Series I) / EAI I",
		"nlh-eai2": "Early American Imprints : Shaw/Shoemaker 1801-1819 (Series II) / EAI II",
		"nlh-emo":  "Empire Online / EMO",
		"nlh-moc":  "Macmillan Cabinet Papers (1957-1963) / MOC",
		"nlh-ecj":  "Eighteenth Century Journals : A Portal to Newspapers and Periodicals, 1685-1815 / ECJ I & II",
		"nlh-usc":  "U.S. Congressional Serial Set, 1817-1980 / USC",
		"nlh-ecc":  "Eighteenth Century Collections Online / ECCO",
		"nlh-eha":  "The Economist: Historical Archive 1843 - 2007 / ECON",
		"nlh-tda1": "The Times Digital Archive I / TDA I",
		"nlh-tda2": "The Times Digital Archive II / TDA II",
		"nlh-tls":  "Times Literary Supplement Historical Archive 1902 - 2005 / TLS",
		"nlh-mme":  "Making of the Modern World: economics, politics and industry / MOME",
		"nlh-mml":  "Making of Modern Law : Legal Treatises 1800-1926 / MOML 1",
		"nlh-mms":  "Making of Modern Law : U.S. Supreme Court Records and Briefs, 1832-1978 / MOML 2 - SCRB",
		"nlh-ddr":  "U.S. Declassified Documents Online / USDD (vorher: Declassified Documents Reference System / DDRS)",
		"nlh-fta":  "Financial Times Historical Archive 1888-2006 / FTHA",
		"nlh-mmh":  "Making of Modern Law : Trials 1600-1926 / MOML 3 - MMLT",
		"nlh-nid":  "Northern Ireland : A Divided Community 1921-1972 : Cabinet papers of the Stormont Administration / NOIE",
		"nlh-ncn":  "Nineteenth Century U.S. Newspapers / NCNP",
		"nlh-bcn":  "17th - 18th Century Burney Collection Newspapers / BBCN",
		"nlh-mmp":  "Making of Modern Law : Primary Sources (1620-1926) / MOML 4 - MMLP",
		"nlh-bln":  "19th Century British Library Newspapers / BNCN",
		"nlh-tcn":  "Twentieth Century North American Drama / TCN",
		"nlh-ahn":  "North American Immigrant Letters, Diaries, and Oral Histories",
		"nlh-?":    "Wegbereiter der Österreichischen Psychologie",
		"nlh-??":   "Die 1848/49er Revolutionäre und ihre Einflüsse in den USA",
		"indexer":  "Test Product",
	}
}

// MapProductToName ...
func (s *NLHSpecifics) MapProductToName(product string) string {
	str := productToNameMap[product]
	if str != "" {
		return str
	}
	return product
}
