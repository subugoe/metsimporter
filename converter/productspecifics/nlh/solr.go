package nlh

import (
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/types"
)

var ()

func init() {

}

type LogStructSortSlice []LogStruct

// Len is part of sort.Interface.
func (d LogStructSortSlice) Len() int {
	return len(d)
}

// Swap is part of sort.Interface.
func (d LogStructSortSlice) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less is part of sort.Interface. We use count as the value to sort by
func (d LogStructSortSlice) Less(i, j int) bool {
	//return d[i].Order < d[j].Order
	return d[i].Index < d[j].Index
}

// AgentInfo ...
type AgentInfo struct {
	Type      string `json:"type,omitempty"`
	Othertype string `json:"othertype,omitempty"`
	Role      string `json:"role,omitempty"`
	Otherrole string `json:"otherrole,omitempty"`

	AgentProcessorName string `json:"agentProcessorName,omitempty"`
	AgentProcessorNote string `json:"agentProcessorNote,omitempty"`
}

// LogStruct ...
type LogStruct struct {
	CreateDate string `json:"CREATEDATE,omitempty"`

	IsFirst           bool
	IsExternalContent bool
	ReindexedAt       string        `json:"reindexedat,omitempty"`
	CreatorInfos      []CreatorInfo `json:"creator_infos,omitempty"`
	Currentno         []string      `json:"currentno,omitempty"`
	Currentnosort     int64         `json:"currentnosort,omitempty"`
	DateIndexed       string        `json:"date_indexed,omitempty"`
	DateModified      string        `json:"date_modified,omitempty"`
	DmdID             string        `json:"dmdid,omitempty"`
	Log               string        `json:"log,omitempty"`
	LogID             string        `json:"log_id,omitempty"`
	Doctype           string        `json:"doctype,omitempty"`
	PID               string        `json:"pid,omitempty"`
	ID                string        `json:"id,omitempty"`
	Identifier        []string      `json:"identifier,omitempty"`
	IdentifierInfo    []Identifier  `json:"identifier_info,omitempty"`
	Iscontribution    bool          `json:"iscontribution"`
	Issn              string        `json:"issn,omitempty"`
	Label             string        `json:"label,omitempty"`
	Level             int8          `json:"level,omitempty"`
	Order             int64         `json:"order,omitempty"`
	Product           string        `json:"product,omitempty"`
	PublishInfos      *PublishInfo  `json:"publish_infos,omitempty"`
	Purl              string        `json:"purl,omitempty"`
	RecordIdentifier  string        `json:"record_identifier,omitempty"`
	Title             *TitleInfo    `json:"title,omitempty"`
	Type              string        `json:"type,omitempty"`
	Work              string        `json:"work,omitempty"`
	IssueNumber       string        `json:"issue_number,omitempty"`
	VolumeNumber      string        `json:"volume_number,omitempty"`
	Index             int64         `json:"index,omitempty"`
	StartPageIndex    int64         `json:"start_page_index,omitempty"`
	StartPagePhysID   string        `json:"start_page_phys_id,omitempty"`
	EndPageIndex      int64         `json:"end_page_index,omitempty"`
	EndPagePhysID     string        `json:"end_page_phys_id,omitempty"`
	//NaviYear                   int64                  `json:"navi_year,omitempty"`
	//NaviMonth                  int64                  `json:"navi_month,omitempty"`
	//NaviDay                    int64                  `json:"navi_day,omitempty"`
	//NaviString                 string                 `json:"navi_string,omitempty"`
	//NaviDate                   string                 `json:"navi_date,omitempty"`
	//Zdb                        string                 `json:"zdb,omitempty"`
}

type PhysStructSortSlice []PhysStruct

// Len is part of sort.Interface.
func (d PhysStructSortSlice) Len() int {
	return len(d)
}

// Swap is part of sort.Interface.
func (d PhysStructSortSlice) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less is part of sort.Interface. We use count as the value to sort by
func (d PhysStructSortSlice) Less(i, j int) bool {
	//return d[i].Order < d[j].Order
	return d[i].Index < d[j].Index
}

// PhysStruct ...
type PhysStruct struct {
	DateIndexed     string     `json:"date_indexed,omitempty"`
	DateModified    string     `json:"date_modified,omitempty"`
	PID             string     `json:"pid,omitempty"`
	ID              string     `json:"id,omitempty"`
	Label           string     `json:"label,omitempty"`
	Level           int8       `json:"level,omitempty"`
	Log             string     `json:"log,omitempty"`
	LogID           string     `json:"log_id,omitempty"`
	Order           int64      `json:"order,omitempty"`
	Filename        string     `json:"filename,omitempty"`
	Format          string     `json:"format,omitempty"`
	Page            string     `json:"page,omitempty"`
	PageKey         string     `json:"page_key,omitempty"`
	PageHeight      int32      `json:"page_height,omitempty"`
	PageWidth       int32      `json:"page_width,omitempty"`
	Phys            string     `json:"phys,omitempty"`
	StartPageIndex  int64      `json:"start_page_index"`
	EndPageIndex    int64      `json:"end_page_index"`
	Title           *TitleInfo `json:"title,omitempty"`
	Type            string     `json:"type,omitempty"`
	Work            string     `json:"work,omitempty"`
	Mimetype        string     `json:"mimetype,omitempty"`
	Externalcontent bool
	Index           int64
}

// LogStructResponse ..
type LogStructResponse struct {
	Hits LogStructHits `json:"hits"`
}

// PhysStructResponse ..
type PhysStructResponse struct {
	Hits PhysStructHits `json:"hits"`
}

// LogStructHits ...
type LogStructHits struct {
	Total int64                 `json:"total"`
	Hits  []LogStructSourceHits `json:"hits"`
}

// PhysStructHits ...
type PhysStructHits struct {
	Total int64                  `json:"total"`
	Hits  []PhysStructSourceHits `json:"hits"`
}

// LogStructSourceHits ...
type LogStructSourceHits struct {
	LogStruct LogStruct `json:"_source"`
}

// TitleInfo ...
type TitleInfo struct {
	Type             string `xml:"type,attr" json:"type,omitempty"`
	AuthorityURI     string `xml:"authorityURI,attr" json:"authority_URI,omitempty"` // URI der Normdatei
	ValueURI         string `xml:"valueURI,attr" json:"ValueURI,omitempty"`          // URI des Sachtitels
	Title            string `xml:"title" json:"title,omitempty"`                     // required, not repeatable
	TitleOriginal    string `json:"title_original,omitempty"`                        // required, not repeatable
	Subtitle         string `xml:"subTitle" json:"subtitle,omitempty"`               // optinal, not repeatable
	Nonsort          string `xml:"nonSort" json:"-"`                                 // optinal, repeatable
	Sorttitle        string `json:"sorttitle,omitempty"`                             // optinal, repeatable
	Partname         string `xml:"partName" json:"partname,omitempty"`               // optinal, repeatable
	PartnumberOrig   string `xml:"partNumber" json:"-"`                              // optinal, repeatable
	Partnumber       int64  `json:"partnumber,omitempty"`                            // optinal, repeatable
	TitleAbbreviated string `json:"title_abbreviated,omitempty"`                     // required, not repeatable
	TitleAlternative string `json:"title_alternative,omitempty"`                     // required, not repeatable
	TitleUniform     string `json:"title_uniform,omitempty"`                         // required, not repeatable
}

// PhysStructSourceHits ...
type PhysStructSourceHits struct {
	PhysStruct PhysStruct `json:"_source"`
}

// PublishInfo ...
type PublishInfo struct {
	// publish -> date_issued or date_created
	EventType           string   `json:"event_type,omitempty"`           //
	Place               []string `json:"place_publish,omitempty"`        //
	PlaceInfo           []Place  `json:"place_info,omitempty"`           //
	Publisher           []string `json:"publisher,omitempty"`            //
	PublisherNormalized []string `json:"publisher_normalized,omitempty"` //
	YearPublishString   string   `json:"year_publish_string,omitempty"`  //
	YearPublish         int64    `json:"year_publish,omitempty"`         //
	YearPublishStart    int64    `json:"year_publish_start,omitempty"`   //
	YearPublishEnd      int64    `json:"year_publish_end,omitempty"`     //
	Edition             string   `json:"edition,omitempty"`              //
}

// CreatorInfo ...
type CreatorInfo struct {
	Type              string `json:"type,omitempty"`
	Name              string `json:"name,omitempty"`
	Roleterm          string `json:"roleterm,omitempty"`
	RoletermAuthority string `json:"roleterm_authority,omitempty"`
	RoletermType      string `json:"roleterm_type,omitempty"`
	GndURI            string `json:"gndURI,omitempty"`
	GndNumber         string `json:"gndNumber,omitempty"`
	Date              string `json:"date,omitempty"`
	DateString        string `json:"date_string,omitempty"`
}

// Place ...
type Place struct {
	PlaceTerm []PlaceTerm `xml:"placeTerm" json:"place_term,omitempty"` // // required, repeatabley
}

// PlaceTerm ...
type PlaceTerm struct {
	Type         string `xml:"type,attr" json:"type,omitempty"`                 // values: {text|code}
	Authority    string `xml:"authority,attr" json:"authority,omitempty"`       //
	AuthorityURI string `xml:"authorityURI,attr" json:"authorityURI,omitempty"` //
	ValueURI     string `xml:"valueURI,attr" json:"valueURI,omitempty"`         //
	Value        string `xml:",chardata" json:"value,omitempty"`                //
}

// Identifier ...
type Identifier struct {
	// type and value: ,omitempty?
	Type    string `xml:"type,attr" json:"type,omitempty"`
	Value   string `xml:",chardata" json:"id,omitempty"`
	InvalID string `xml:"invalid,attr" json:"-"`
}

type NLHSpecifics struct {
	types.Productspecifics
}

func getLog(query string) (map[string]LogStruct, error) {
	// TODO not yet implemented

	return nil, nil
}

func getPhys(query string) (map[string]PhysStruct, error) {
	// TODO not yet implemented

	return nil, nil
}

func (s *NLHSpecifics) GetLinkAndBookmarkInfo(workID string, logID string, isLog bool) ([]types.BookmarkInfo, error) {
	// TODO not yet implemented

	return nil, nil
}

func (s *NLHSpecifics) GetPageSequenceInfo(workID string, logID string, isLog bool) ([]types.PageInfo, error) {
	// TODO not yet implemented

	return nil, nil
}

func (s *NLHSpecifics) QueryDisclaimerInfo(workID string, logID string, isLog bool) (types.DisclaimerInfo, error) {
	// TODO not yet implemented

	return types.DisclaimerInfo{}, nil
}
