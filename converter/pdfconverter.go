package main

import (
	"io"
	"net/http"
	"os"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/converter"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/disclaimer"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/filehandling"
	"gitlab.gwdg.de/subugoe/metsimporter/converter/commons/types"
	"gitlab.gwdg.de/subugoe/metsimporter/indexer/util"
)

var (
	concurrency int // = 30
	config      util.Config
	log         logrus.Logger
	logFile     string = ""

	workConvertJobChan      chan *types.WorkConvertJob
	pageConvertJobChan      chan *types.PageConvertJob
	createDisclaimerJobChan chan *types.CreateDisclaimerJob
	mergeJobChan            chan *types.MergeJob
	uploadJobChan           chan *types.UploadJob
)

// S3Key ...
type S3Key struct {
	Name           string `json:"name"`
	Type           string `json:"type"`
	InRegex        string `json:"in_regex"`
	BaseDir        string `json:"base_dir"`
	InSuffix       string `json:"in_suffix"`
	InBucket       string `json:"in_bucket"`
	InKey          string `json:"in_key"`
	InPrefix       string `json:"in_prefix"`
	OutBucket      string `json:"out_bucket"`
	OutKey         string `json:"out_key"`
	OutPrefix      string `json:"out_prefix"`
	PdfOutKey      string `json:"pdf_out_key"`
	Packaging      string `json:"packaging"`
	Storage        string `json:"storage"`
	ForceOverwrite bool   `json:"force_overwrite"`
	Ftype          string `json:"ftype"` // fulltext type HTML, TXT_1, TXT_2, TEI_2

}

func init() {
	config, log = util.LoadConfig()

	if config.TestRun && config.TestRunLimit == 0 {
		config.TestRun = false
	}

	if config.TestRun {
		concurrency = 1
	} else {
		concurrency = config.Concurrency
	}

	if config.CContext == "digizeit" || config.CContext == "ocrd" {

		for {
			resp, err := http.Get(config.ElasticsearchHost)
			if err != nil {
				log.Printf("Info Elasticsearch not running yet, due to %s", err)
				time.Sleep(180 * time.Second)
				continue
			}
			defer resp.Body.Close()

			if resp.StatusCode > 300 {
				log.Printf("INFO Elasticsearch response with status code %v", resp.StatusCode)
				time.Sleep(180 * time.Second)
				continue
			} else {
				break
			}

		}

	}

	workConvertJobChan = make(chan *types.WorkConvertJob, 2*config.Concurrency)
	pageConvertJobChan = make(chan *types.PageConvertJob, 6*config.Concurrency)
	createDisclaimerJobChan = make(chan *types.CreateDisclaimerJob, 2*config.Concurrency)
	mergeJobChan = make(chan *types.MergeJob, 2*config.Concurrency)
	uploadJobChan = make(chan *types.UploadJob, 2*config.Concurrency)
}

func main() {

	var file *os.File
	var err error

	if logFile != "" {
		file, err = os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0755)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		mw := io.MultiWriter(os.Stdout, file)
		log.SetOutput(mw)
	}

	log.Info("PDF-Converter service starts processing...")

	for w := 1; w <= int((concurrency+1)/2); w++ {
		go converter.RequestProcessingWorker(
			workConvertJobChan,
			pageConvertJobChan,
			createDisclaimerJobChan,
			mergeJobChan,
			uploadJobChan) // #1
		go disclaimer.CreatePDFDisclaimerWorker(createDisclaimerJobChan) // #2
		go converter.MergePDFWorker(mergeJobChan)                        // #3
	}

	for w := 1; w <= concurrency*2; w++ {
		go converter.ConverteImageWorker(pageConvertJobChan) // #2
		go filehandling.UploadPDFWorker(uploadJobChan)       // #3 single PDF & #4 full PDF
	}

	for {
		time.Sleep(60 * time.Second)
	}

}
